package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/5/2 13:43
 * @ClassName: StoreOrderRefundReqFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class StoreOrderRefundReqFormDTO implements Serializable {


    private Long id;
    private String orderNo;


}
