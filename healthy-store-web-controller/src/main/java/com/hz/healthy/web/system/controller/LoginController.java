package com.hz.healthy.web.system.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.store.IHStoreUserService;
import com.hz.healthy.domain.store.model.HStoreUserModel;
import com.hz.healthy.domain.store.model.HStoreUserQueryModel;
import com.hz.healthy.web.system.dto.SysLoginModel;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.MD5Util;
import com.hz.ocean.common.utils.PasswordUtil;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.service.auth.ISysDepartService;
import com.hz.ocean.system.service.auth.ISysUserService;
import com.hz.ocean.system.service.auth.model.SysDepartModel;
import com.hz.ocean.system.service.auth.model.SysUserModel;
import com.hz.ocean.uac.api.ShiroAPI;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import com.hz.ocean.web.core.utils.jwt.JwtUtil;
import com.hz.ocean.web.core.utils.redis.RedisStringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author hyhuang
 * @date 2022-03-27 8:03 PM
 */
@RestController
@RequestMapping("/sys")
@Slf4j
public class LoginController {
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private RedisStringUtil redisUtil;
    @Autowired
    private ISysDepartService sysDepartService;
    @Autowired
    private ShiroAPI shiroAPI;

    @Autowired
    private OceanBaseConfig oceanBaseConfig;


    @Autowired
    private IHStoreUserService ihStoreUserService;



    private String getSystemId() {
        return oceanBaseConfig.getSystemId();
    }

    /**
     * 登录接口
     *
     * @param sysLoginModel
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public RespResult<JSONObject> login(@RequestBody SysLoginModel sysLoginModel) {
        RespResult<JSONObject> result = new RespResult<JSONObject>();
        String username = sysLoginModel.getUsername();
        String password = sysLoginModel.getPassword();
        //update-begin--Author:scott  Date:20190805 for：暂时注释掉密码加密逻辑，有点问题
        //前端密码加密，后端进行密码解密
        //password = AesEncryptUtil.desEncrypt(sysLoginModel.getPassword().replaceAll("%2B", "\\+")).trim();//密码解密
        //update-begin--Author:scott  Date:20190805 for：暂时注释掉密码加密逻辑，有点问题

        //update-begin-author:taoyan date:20190828 for:校验验证码
        String captcha = sysLoginModel.getCaptcha();
        if (captcha == null) {
            return RespResult.error("验证码无效");
        }
        String lowerCaseCaptcha = captcha.toLowerCase();
        String realKey = MD5Util.encode(lowerCaseCaptcha + sysLoginModel.getCheckKey());
        Object checkCode = redisUtil.get(realKey);
        //当进入登录页时，有一定几率出现验证码错误 #1714
        if (checkCode == null || !checkCode.toString().equals(lowerCaseCaptcha)) {
            log.warn("验证码错误，key= {} , Ui checkCode= {}, Redis checkCode = {}", sysLoginModel.getCheckKey(), lowerCaseCaptcha, checkCode);
            return RespResult.error("验证码错误");
        }
        //1. 校验用户是否有效
        HStoreUserModel storeUser = ihStoreUserService.getOne(HStoreUserQueryModel.builder()
                .loginName(username)
                .build());

        //登录代码验证用户是否注销bug，if条件永远为false
        if (ObjectUtil.isNull(storeUser)) {
            return RespResult.error("用户名不存在");
        }
        //2. 校验用户名或密码是否正确
        String userpassword = PasswordUtil.encrypt(username, password, storeUser.getSalt());
        String syspassword = storeUser.getPassword();
        if (!syspassword.equals(userpassword)) {
            return RespResult.error("用户名或密码错误");
        }

        //用户登录信息
        userInfo(storeUser, result);
        //update-begin--Author:liusq  Date:20210126  for：登录成功，删除redis中的验证码
        redisUtil.del(realKey);


        //update-begin--Author:liusq  Date:20210126  for：登录成功，删除redis中的验证码
        //LoginUser loginUser = BeanUtil.toBean(storeUser,LoginUser.class);
        ///baseCommonService.addLog("用户名: " + username + ",登录成功！", CommonConstant.LOG_TYPE_1, null,loginUser);
        //update-end--Author:wangshuai  Date:20200714  for：登录日志没有记录人员
        return result;
    }




    /**
     * 退出登录
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/logout")
    public RespResult<Object> logout(HttpServletRequest request, HttpServletResponse response) {
        //用户退出逻辑
        String token = request.getHeader(CommonConstant.X_ACCESS_TOKEN);
        if (oConvertUtils.isEmpty(token)) {
            return RespResult.error("退出登录失败！");
        }
        String username = JwtUtil.getUsername(token);
        LoginUser sysUser = shiroAPI.getUserByName(username);
        if (sysUser != null) {
            //update-begin--Author:wangshuai  Date:20200714  for：登出日志没有记录人员
            //baseCommonService.addLog("用户名: "+sysUser.getRealname()+",退出成功！", CommonConstant.LOG_TYPE_1, null,sysUser);
            //update-end--Author:wangshuai  Date:20200714  for：登出日志没有记录人员
            log.info(" 用户名:  " + sysUser.getRealname() + ",退出成功！ ");
            //清空用户登录Token缓存
            redisUtil.del(CommonConstant.PREFIX_USER_TOKEN + token);
            //清空用户登录Shiro权限缓存
            redisUtil.del(CommonConstant.PREFIX_USER_SHIRO_CACHE + sysUser.getId());
            //清空用户的缓存信息（包括部门信息），例如sys:cache:user::<username>
            //redisUtil.del(String.format("%s::%s", CacheConstant.SYS_USERS_CACHE, sysUser.getUsername()));
            //调用shiro的logout
            SecurityUtils.getSubject().logout();
            return RespResult.OK("退出登录成功！");
        } else {
            return RespResult.error("Token无效!");
        }
    }

    /**
     * 获取访问量
     *
     * @return
     */
    @GetMapping("loginfo")
    public RespResult<JSONObject> loginfo() {
        RespResult<JSONObject> result = new RespResult<JSONObject>();
        JSONObject obj = new JSONObject();
        //update-begin--Author:zhangweijian  Date:20190428 for：传入开始时间，结束时间参数
        // 获取一天的开始和结束时间
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date dayStart = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        Date dayEnd = calendar.getTime();
        // 获取系统访问记录
//		Long totalVisitCount = logService.findTotalVisitCount();
//		obj.put("totalVisitCount", totalVisitCount);
//		Long todayVisitCount = logService.findTodayVisitCount(dayStart,dayEnd);
//		obj.put("todayVisitCount", todayVisitCount);
//		Long todayIp = logService.findTodayIp(dayStart,dayEnd);
        //update-end--Author:zhangweijian  Date:20190428 for：传入开始时间，结束时间参数
//		obj.put("todayIp", todayIp);
        result.setResult(obj);
        result.success("登录成功");
        return result;
    }

    /**
     * 获取访问量
     *
     * @return
     */
    @GetMapping("visitInfo")
    public RespResult<List<Map<String, Object>>> visitInfo() {
        RespResult<List<Map<String, Object>>> result = new RespResult<List<Map<String, Object>>>();
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date dayEnd = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        Date dayStart = calendar.getTime();
//        List<Map<String,Object>> list = logService.findVisitCount(dayStart, dayEnd);
//		result.setResult(oConvertUtils.toLowerCasePageList(list));
        return result;
    }


    /**
     * 登陆成功选择用户当前部门
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/selectDepart", method = RequestMethod.PUT)
    public RespResult<JSONObject> selectDepart(@RequestBody SysUserModel user) {
        RespResult<JSONObject> result = new RespResult<JSONObject>();
        String username = user.getUsername();
        if (oConvertUtils.isEmpty(username)) {
            LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            username = sysUser.getUsername();
        }
        String orgCode = user.getOrgCode();
        this.sysUserService.updateUserDepart(username, orgCode, getSystemId());
        SysUserModel sysUser = sysUserService.getUserByName(username, getSystemId());
        JSONObject obj = new JSONObject();
        obj.put("userInfo", sysUser);
        result.setResult(obj);
        return result;
    }





    /**
     * 用户信息
     *
     * @param sysUser
     * @param result
     * @return
     */
    private RespResult<JSONObject> userInfo(HStoreUserModel sysUser, RespResult<JSONObject> result) {
        String syspassword = sysUser.getPassword();
        String username = sysUser.getLoginName();
        // 获取用户部门信息
        JSONObject obj = new JSONObject();
        // update-end--Author:sunjianlei Date:20210802 for：获取用户租户信息
        // 生成token
        String token = JwtUtil.sign(username, syspassword);
        // 设置token缓存有效时间
        redisUtil.set(CommonConstant.CURRENT_TOKEN_SYSTEM+token,sysUser.getOrgCode());
        redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
        redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME * 2 / 1000);
        obj.put("token", token);
        obj.put("userInfo", sysUser);
        //obj.put("sysAllDictItems", sysDictService.queryAllDictItems());
        result.setResult(obj);
        result.success("登录成功");
        return result;
    }

    /**
     * 获取加密字符串
     *
     * @return
     */
    @GetMapping(value = "/getEncryptedString")
    public RespResult<Map<String, String>> getEncryptedString() {
        RespResult<Map<String, String>> result = new RespResult<Map<String, String>>();
        Map<String, String> map = new HashMap<String, String>();
        //map.put("key", EncryptedString.key);
        //map.put("iv",EncryptedString.iv);
        result.setResult(map);
        return result;
    }

    /**
     * 后台生成图形验证码 ：有效
     *
     * @param response
     * @param key
     */
    @GetMapping(value = "/randomImage/{key}")
    public RespResult<String> randomImage(HttpServletResponse response, @PathVariable String key) {
        RespResult<String> res = new RespResult<String>();
        try {
            //设置验证码长、宽、验证码字符数、干扰元素个数：
            CircleCaptcha captcha =  CaptchaUtil.createCircleCaptcha(90, 40, 4, 5);

            //存到redis中
            String lowerCaseCode = captcha.getCode();
            String realKey = MD5Util.encode(lowerCaseCode + key);
            log.info("获取验证码，Redis checkCode = {}，key = {}", lowerCaseCode, key);
            redisUtil.set(realKey, lowerCaseCode, 120);

            //返回前端
            String base64 =  captcha.getImageBase64Data();
            res.setSuccess(true);
            res.setResult(base64);
        } catch (Exception e) {
            e.printStackTrace();
            return RespResult.error("获取验证码出错" + e.getMessage());
        }
        return res;
    }

    /**
     * app登录
     *
     * @param sysLoginModel
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/mLogin", method = RequestMethod.POST)
    public RespResult<JSONObject> mLogin(@RequestBody SysLoginModel sysLoginModel) throws Exception {
        RespResult<JSONObject> result = new RespResult<JSONObject>();
        String username = sysLoginModel.getUsername();
        String password = sysLoginModel.getPassword();

        //1. 校验用户是否有效
        SysUserModel sysUser = sysUserService.getUserByName(username, getSystemId());
//		result = sysUserService.checkUserIsEffective(sysUser);
//		if(!result.isSuccess()) {
//			return result;
//		}

        //2. 校验用户名或密码是否正确
        String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
        String syspassword = sysUser.getPassword();
        if (!syspassword.equals(userpassword)) {
            return RespResult.error("用户名或密码错误");
        }

        String orgCode = sysUser.getOrgCode();
        if (oConvertUtils.isEmpty(orgCode)) {
            //如果当前用户无选择部门 查看部门关联信息
            List<SysDepartModel> departs = sysDepartService.queryUserDeparts(sysUser.getId());
            //update-begin-author:taoyan date:20220117 for: JTC-1068【app】新建用户，没有设置部门及角色，点击登录提示暂未归属部，一直在登录页面 使用手机号登录 可正常
            if (departs == null || departs.size() == 0) {
				/*return RespResult.error("用户暂未归属部门,不可登录!");
				return result;*/
            } else {
                orgCode = departs.get(0).getOrgCode();
                sysUser.setOrgCode(orgCode);
                this.sysUserService.updateUserDepart(username, orgCode, getSystemId());
            }
            //update-end-author:taoyan date:20220117 for: JTC-1068【app】新建用户，没有设置部门及角色，点击登录提示暂未归属部，一直在登录页面 使用手机号登录 可正常
        }
        JSONObject obj = new JSONObject();
        //用户登录信息
        obj.put("userInfo", sysUser);

        // 生成token
        String token = JwtUtil.sign(username, syspassword);
        // 设置超时时间
        redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
        redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME * 2 / 1000);

        //token 信息
        obj.put("token", token);
        result.setResult(obj);
        result.setSuccess(true);
        result.setCode(200);
        //baseCommonService.addLog("用户名: " + username + ",登录成功[移动端]！", CommonConstant.LOG_TYPE_1, null);
        return result;
    }

    /**
     * 图形验证码
     *
     * @param sysLoginModel
     * @return
     */
    @RequestMapping(value = "/checkCaptcha", method = RequestMethod.POST)
    public RespResult<?> checkCaptcha(@RequestBody SysLoginModel sysLoginModel) {
        String captcha = sysLoginModel.getCaptcha();
        String checkKey = sysLoginModel.getCheckKey();
        if (captcha == null) {
            return RespResult.error("验证码无效");
        }
        String lowerCaseCaptcha = captcha.toLowerCase();
        String realKey = MD5Util.encode(lowerCaseCaptcha + checkKey);
        Object checkCode = redisUtil.get(realKey);
        if (checkCode == null || !checkCode.equals(lowerCaseCaptcha)) {
            return RespResult.error("验证码错误");
        }
        return RespResult.OK();
    }

    /**
     * 登录二维码
     */
    @GetMapping("/getLoginQrcode")
    public RespResult<?> getLoginQrcode() {
        String qrcodeId = CommonConstant.LOGIN_QRCODE_PRE + IdWorker.getIdStr();
        //定义二维码参数
        Map params = new HashMap(5);
        params.put("qrcodeId", qrcodeId);
        //存放二维码唯一标识30秒有效
        redisUtil.set(CommonConstant.LOGIN_QRCODE + qrcodeId, qrcodeId, 30);
        return RespResult.OK(params);
    }

    /**
     * 扫码二维码
     */
    @PostMapping("/scanLoginQrcode")
    public RespResult<?> scanLoginQrcode(@RequestParam String qrcodeId, @RequestParam String token) {
        Object check = redisUtil.get(CommonConstant.LOGIN_QRCODE + qrcodeId);
        if (oConvertUtils.isNotEmpty(check)) {
            //存放token给前台读取
            redisUtil.set(CommonConstant.LOGIN_QRCODE_TOKEN + qrcodeId, token, 60);
        } else {
            return RespResult.error("二维码已过期,请刷新后重试");
        }
        return RespResult.OK("扫码成功");
    }


    /**
     * 获取用户扫码后保存的token
     */
    @GetMapping("/getQrcodeToken")
    public RespResult getQrcodeToken(@RequestParam String qrcodeId) {
        Object token = redisUtil.get(CommonConstant.LOGIN_QRCODE_TOKEN + qrcodeId);
        Map result = new HashMap();
        Object qrcodeIdExpire = redisUtil.get(CommonConstant.LOGIN_QRCODE + qrcodeId);
        if (oConvertUtils.isEmpty(qrcodeIdExpire)) {
            //二维码过期通知前台刷新
            result.put("token", "-2");
            return RespResult.OK(result);
        }
        if (oConvertUtils.isNotEmpty(token)) {
            result.put("success", true);
            result.put("token", token);
        } else {
            result.put("token", "-1");
        }
        return RespResult.OK(result);
    }


    //发下是elemUi框架

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public RespResult getInfo() {
        RespResult<Map<String, Object>> result = new RespResult<Map<String, Object>>();
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // 角色集合
        Set<String> roles = shiroAPI.queryUserRoles(loginUser.getUsername());
        // 权限集合
        Set<String> permissions = shiroAPI.getUserPermissionSet(loginUser.getUsername());
        Map<String, Object> ajax = new HashMap<>();
        ajax.put("user", loginUser);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        result.setResult(ajax);
        result.setSuccess(true);
        return result;
    }


    /**
     * 删除用户身份信息
     */
    private void delLoginUser(String token) {
        if (StringUtils.isNotEmpty(token)) {
            String userKey = getTokenKey(token);
            redisUtil.del(userKey);
        }
    }

    private String getTokenKey(String uuid) {
        return CommonConstant.PREFIX_USER_TOKEN + uuid;
    }


}