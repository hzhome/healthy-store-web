package com.hz.healthy.web.his.controller;

import com.alibaba.fastjson.JSON;
import com.easemob.im.ApiException;
import com.easemob.im.api.UserApi;
import com.easemob.im.api.model.EMCreateUser;
import com.easemob.im.api.model.EMCreateUsersResult;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.doctor.model.HEmployeeModel;
import com.hz.healthy.domain.doctor.model.HEmployeeQueryModel;
import com.hz.healthy.domain.his.IHHisHouseKeepService;
import com.hz.healthy.domain.his.model.HHisHouseKeepModel;
import com.hz.healthy.enums.DoctorTypeEnums;
import com.hz.healthy.store.serve.IHEmployeeAggregationService;
import com.hz.healthy.store.serve.dto.HDoctorFromReqDTO;
import com.hz.healthy.store.serve.dto.HEmployeeDTO;
import com.hz.healthy.web.his.dto.request.HButlerFormDTO;
import com.hz.healthy.web.system.dto.KeyValueDTO;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 医生信息Controller
 *
 * @author hyhuang
 * @date 2023-01-17
 */
@Slf4j
@RestController
@RequestMapping("/store/employee")
public class HEmployeeController extends WebBaseController {

    @Autowired
    private IHEmployeeAggregationService iHEmployeeAggregationService;

    @Autowired
    private IHEmployeeService iHEmployeeService;

    @Autowired
    private IHHisHouseKeepService hHisHouseKeepService;

    private UserApi userApi = new UserApi();

    /**
     * 查询医生信息列表
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<ListResponse<HEmployeeDTO>> pageQuery(HEmployeeQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        return iHEmployeeAggregationService.pageQuery(queryModel, getLoginUser());
    }


    /**
     * 新增医生信息
     */
    @PostMapping
    public RespResult add(@Validated @RequestBody HDoctorFromReqDTO fromReq) {
        iHEmployeeAggregationService.save(fromReq, getLoginUser());
        return RespResult.OK("添加成功");
    }


    /**
     * 修改医生信息
     */
    @PutMapping
    public RespResult edit(@RequestBody HDoctorFromReqDTO fromReq) {
        iHEmployeeAggregationService.updateById(fromReq, getLoginUser());
        return RespResult.OK("变更完成");
    }


    /**
     * 获取医生信息详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult<HEmployeeDTO> getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(iHEmployeeAggregationService.getById(id));
    }


    /**
     * 删除医生信息
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        iHEmployeeAggregationService.deleteByIds(ids);
        return RespResult.OK("删除完成");
    }

    /**
     * 查询医生信息列表
     */
    @Deprecated
    @GetMapping("/select/list")
    public RespResult<List<KeyValueDTO>> selectList(HEmployeeQueryModel queryModel) {
        List<KeyValueDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        List<HEmployeeModel> list = iHEmployeeService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(list)) {
            list.forEach(l -> {
                dataList.add(new KeyValueDTO(String.valueOf(l.getId()), String.valueOf(l.getName())));
            });
        }
        return RespResult.OK(dataList);
    }


    /**
     * 创建管家身份
     */
    @PostMapping("/butler")
    public RespResult<List<KeyValueDTO>> butler(@RequestBody HButlerFormDTO form) {
        List<Long> ids = form.getIds();
        if (CollectionUtils.isNotEmpty(ids)) {
            Date now = new Date();
            List<HHisHouseKeepModel> list = new ArrayList<>();
            List<HEmployeeModel> dataList = iHEmployeeService.listById(ids);
            for (HEmployeeModel e : dataList) {
                list.add(HHisHouseKeepModel.builder()
                        .createTime(now)
                        .createUser(getLoginUser().getRealname())
                        .eyId(e.getId())
                        .eyName(e.getName())
                        .orgCode(getOrgCode())
                        .tenantId(getTenantId())
                        .status(CommonConstant.STATUS_1)
                        .speciality(e.getIntroduction())
                        .build());
            }
            hHisHouseKeepService.insertBatch(list);
            return RespResult.OK("创建成功");
        } else {
            return RespResult.OK("请选择员工创建管家身份");
        }

    }


    /**
     * 更新密码
     */
    @AutoDict
    @PostMapping("/update/app/pwd")
    public RespResult<String> updateAppPwd(@RequestBody HEmployeeQueryModel queryModel) {
        iHEmployeeAggregationService.updatePwd(queryModel.getId());
        return RespResult.OK("重置成功");
    }


    /**
     * 设置挂号费
     */
    @AutoDict
    @PostMapping("/setup/registration/fee")
    public RespResult<String> setUpRegistrationFee(@RequestBody HDoctorFromReqDTO fromReq) {
        iHEmployeeAggregationService.setUpRegistrationFee(fromReq, getLoginUser());
        return RespResult.OK("设置成功");
    }

    /**
     * 当前角色
     * 中医 还是西医
     */
    @AutoDict
    @PostMapping("/current/role")
    public RespResult<String> currentRole() {
        return RespResult.OK(DoctorTypeEnums.C0.getCode());
    }

    /**
     * 开通线上问诊
     * 环信chat sdk
     */
    @PostMapping("/openChat")
    public RespResult<List<KeyValueDTO>> openChat(@RequestBody HButlerFormDTO form) {
        List<Long> ids = form.getIds();
        if (CollectionUtils.isNotEmpty(ids)) {
            String pwd = "2025jsyxin";
            List<EMCreateUser> emCreateUserList = new ArrayList<>();
            List<HEmployeeModel> dataList = iHEmployeeService.listById(ids);
            if (CollectionUtils.isEmpty(dataList)) {
                return RespResult.error("暂无符合员工开通!");
            }
            List<HEmployeeModel> updateList = new ArrayList<>();
            for (HEmployeeModel e : dataList) {
                if (StringUtils.isBlank(e.getChatId())) {
                    String chatId = StringUtils.get32UUID();
                    EMCreateUser createUser = new EMCreateUser();
                    createUser.setNickname(e.getName());
                    createUser.setUsername(chatId);
                    createUser.setPassword(pwd);
                    emCreateUserList.add(createUser);
                    updateList.add(HEmployeeModel.builder()
                            .id(e.getId())
                            .chatId(chatId)
                            .build());
                }
            }
            if (CollectionUtils.isNotEmpty(emCreateUserList)) {
                try {
                    EMCreateUsersResult result = userApi.createUsers(emCreateUserList);
                    log.info("环信sdk开通帐号返回信息:{}", JSON.toJSONString(result));
                    for (HEmployeeModel update : updateList) {
                        iHEmployeeService.updateById(update);
                    }
                } catch (ApiException e) {
                    log.error("环信sdk开通帐号失败 开通人员:{}", updateList, e);
                    return RespResult.error("开通失败,请稍候尝试!");
                }
            } else {
                return RespResult.OK("开通成功");
            }
        }
        return RespResult.error("请选择员工开通!");
    }

}
