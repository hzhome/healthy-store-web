package com.hz.healthy.web.system.controller;

import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.service.auth.ISysDictService;
import com.hz.ocean.system.service.auth.model.DuplicateCheckQueryModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Title: DuplicateCheckAction
 * @Description: 重复校验工具
 * @author hyhuang
 * @date 2022-03-27 8:03 PM
 */
@Slf4j
@RestController
@RequestMapping("/sys/duplicate")
public class DuplicateCheckController {

	@Autowired
	private ISysDictService sysDictService;

	/**
	 * 校验数据是否在系统中是否存在
	 * 
	 * @return
	 */
	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public RespResult<String> doDuplicateCheck(DuplicateCheckQueryModel duplicateCheckVo, HttpServletRequest request) {
		return RespResult.OK("该值可用！");
//		log.info("----duplicate check------："+ duplicateCheckVo.toString());
//		Long num = sysDictService.doDuplicateCheck(duplicateCheckVo);
//		if (num == null || num == 0) {
//			// 该值可用
//			return RespResult.OK("该值可用！");
//		} else {
//			// 该值不可用
//			log.info("该值不可用，系统中已存在！");
//			return RespResult.error("该值不可用，系统中已存在！");
//		}
	}
}
