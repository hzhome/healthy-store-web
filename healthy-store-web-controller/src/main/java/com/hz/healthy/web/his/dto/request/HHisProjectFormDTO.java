package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/11/26 12:29
 * @ClassName: HHisProjectFormDT
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisProjectFormDTO implements Serializable {

    private Long id;


    /** 项目分类 1 治疗理疗 2检验 3检查 */
    private String categoryCode;

    /** 项目类型， 治疗理疗(1治疗 2理疗)  */
    private String projectType;

    /** 项目名称 */
    private String projectName;

    /** 项目编码 */
    private String projectCode;

    /** 执行划扣(消费一次划扣一次，适用于多次为一个疗程) 0不需要/1需要 */
    private String izMinus;

    /** 单位 次 时等 */
    private String unit;

    /** 成本价 */
    private BigDecimal costAmount;

    /** 销售价 */
    private BigDecimal saleAmount;

    /** 对应的医保的项目 */
    private String medicalInsuranceCode;

}
