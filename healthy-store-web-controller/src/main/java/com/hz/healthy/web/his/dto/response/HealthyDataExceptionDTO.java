package com.hz.healthy.web.his.dto.response;

import com.hz.healthy.web.his.dto.response.item.WorkHealthyDataToDayItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/5 23:25
 * @ClassName: HealthyDataExceptionDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HealthyDataExceptionDTO implements Serializable {

    /** 主键ID */
    private Long id;
    /**
     * 护理级别名称 记录当前的
     */
    private String nurseRank;
    /**
     * 患者id
     */
    private Long patientId;
    /**
     * 姓名
     */
    private String patientName;
    /**
     * 数据编码
     */
    private String keyCode;
    /**
     * 数据名称
     */
    private String keyName;

    private List<WorkHealthyDataToDayItem> itemList;

    /**
     * 年月日 yyyy-mm-dd
     */
    private String today;
    /**
     * 录入来源
     */
    private String inputSource;

    /**
     * 是否正常 0正常 1异常
     */
    private String resultStatus;

    /**
     * 正常值
     */
    private String normalValue;





    /**
     * 是否己处理
     */
    private String izHandle;

    /**
     * 处理人员
     */
    private String handleOp;
    /**
     * 处理结果
     */
    private String handleResult;

}
