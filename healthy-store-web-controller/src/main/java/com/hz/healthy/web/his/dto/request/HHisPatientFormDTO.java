package com.hz.healthy.web.his.dto.request;

import cn.hutool.core.util.DesensitizedUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import com.hz.ocean.common.aspect.Sensitive;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/12/18 14:47
 * @ClassName: HHisPatientFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientFormDTO implements Serializable {

    private Long id;
    private Long patientId;
    /** 姓名 */
    private String realName;
    /** 手机 */
    private String mobilePhone;
    /** 身份证 */
    private String cardNo;

    /** 性别，2，男；1，女 */
    private String sex;

    private String desc;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date birthday;


    /** 体重 例如62kg */
    private BigDecimal weight;
    /** 身高 例如178CM */
    private Integer height;

    private String nation;
    private String address;

    private String work;

    private String izMarry;

}
