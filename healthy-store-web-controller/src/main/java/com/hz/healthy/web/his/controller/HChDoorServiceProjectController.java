package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.ch.IHChDoorServiceProjectService;
import com.hz.healthy.domain.ch.model.HChDoorServiceProjectModel;
import com.hz.healthy.domain.ch.model.HChDoorServiceProjectQueryModel;
import com.hz.healthy.domain.his.IHCategoryService;
import com.hz.healthy.web.his.dto.request.HChDoorServiceProjectFormDTO;
import com.hz.healthy.web.his.dto.response.HChDoorServiceProjectDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 上门服务项目Controller
 *
 * @author ruoyi
 * @date 2023-09-24
 */
@RestController
@RequestMapping("/ch/door/project")
public class HChDoorServiceProjectController extends WebBaseController {

    @Autowired
    private IHChDoorServiceProjectService hChDoorServiceProjectService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    @Autowired
    private IHCategoryService hHCategoryService;

    /**
     * 查询上门服务项目列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HChDoorServiceProjectDTO>> pageQuery(HChDoorServiceProjectQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantIdList(Lists.newArrayList(0L,getTenantId()));
        ListResponse<HChDoorServiceProjectDTO> pageList = ListResponse.build(hChDoorServiceProjectService.pageQuery(queryModel),
                HChDoorServiceProjectDTO.class);
        if(CollectionUtils.isNotEmpty(pageList.getRecords())){
            List<Long> categoryIds = pageList.getRecords().stream().map(HChDoorServiceProjectDTO::getCategoryId).collect(Collectors.toList());   pageList.getRecords().stream().map(HChDoorServiceProjectDTO::getCategoryId).collect(Collectors.toList());
            Map<Long, String>  categoryMap=hHCategoryService.mapNameByIds(categoryIds);
            pageList.getRecords().forEach(item -> {
                if(!getTenantId().equals(item.getTenantId())){
                    item.setTenantId(0L);//不可编辑
                }
                item.setCategoryName(categoryMap.getOrDefault(item.getCategoryId(),"---"));
            });
        }

        return RespResult.OK(pageList);
    }


    /**
     * 获取上门服务项目详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hChDoorServiceProjectService.getById(id));
    }

    /**
     * 新增上门服务项目
     */
    @PostMapping
    public RespResult add(@Validated @RequestBody HChDoorServiceProjectFormDTO form) {
        HChDoorServiceProjectModel hChDoorServiceProjectModel = BeanUtil.toBean(form, HChDoorServiceProjectModel.class);
        hChDoorServiceProjectModel.setCreateTime(new Date());
        hChDoorServiceProjectModel.setOrgCode(getOrgCode());
        hChDoorServiceProjectModel.setTenantId(getTenantId());
        hChDoorServiceProjectModel.setProjectCode(seqNoAutoService.buildBatchNo());
        if (ObjectUtil.isNotNull(form.getTime()) && form.getTime() > 0) {
            hChDoorServiceProjectModel.setIzLimitTime("1");
        }
        hChDoorServiceProjectService.insert(hChDoorServiceProjectModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改上门服务项目
     */
    @PutMapping
    public RespResult edit(@RequestBody HChDoorServiceProjectFormDTO form) {
        HChDoorServiceProjectModel hChDoorServiceProjectModel = BeanUtil.toBean(form, HChDoorServiceProjectModel.class);
        hChDoorServiceProjectModel.setUpdateTime(new Date());
        if (ObjectUtil.isNotNull(form.getTime()) && form.getTime() > 0) {
            hChDoorServiceProjectModel.setIzLimitTime("1");
        } else {
            hChDoorServiceProjectModel.setIzLimitTime("0");
        }
        hChDoorServiceProjectService.updateById(hChDoorServiceProjectModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除上门服务项目
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        HChDoorServiceProjectModel projectModel=hChDoorServiceProjectService.getById(id);
        if(ObjectUtil.isNull(projectModel)){
            return RespResult.error("项目不存在");
        }
        if(projectModel.getTenantId().equals(0L)){
            return RespResult.error("项目不允许删除");
        }
        hChDoorServiceProjectService.deleteById(id);
        return RespResult.OK("已删除项目");
    }


    /**
     * 读取配套服务
     *
     * @param queryModel
     * @return
     */
    @GetMapping("/selectList")
    public RespResult<List<KeyLongModel>> selectList(HChDoorServiceProjectQueryModel queryModel) {
        queryModel.setTenantId(getTenantId());
        List<HChDoorServiceProjectModel> pageList = hChDoorServiceProjectService.selectList(queryModel);
        List<KeyLongModel> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList)) {
            for (HChDoorServiceProjectModel d : pageList) {
                dataList.add(KeyLongModel.builder().key(d.getId()).label(d.getProject()).build());
            }
        }
        return RespResult.OK(dataList);
    }
}
