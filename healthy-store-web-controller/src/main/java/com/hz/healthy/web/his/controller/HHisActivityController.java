package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PhoneUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.doctor.model.HEmployeeModel;
import com.hz.healthy.domain.his.*;
import com.hz.healthy.domain.his.model.*;
import com.hz.healthy.domain.patient.IHHisPatientService;
import com.hz.healthy.web.his.dto.request.HHisActivityFormReqDTO;
import com.hz.healthy.web.his.dto.request.HHisActivityQueryDTO;
import com.hz.healthy.web.his.dto.response.HHisActivityDTO;
import com.hz.healthy.web.his.dto.response.HHisActivityViewDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.ObjectUtils;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 活动管理Controller
 * 社区门店版
 * @author hyhuang
 * @date 2023-07-10
 */
@RestController
@RequestMapping("/his/activity")
public class HHisActivityController extends WebBaseController {
    @Autowired
    private IHHisActivityService hHisActivityService;

    @Autowired
    private IHHisActivityProcessService hisActivityProcessService;

    @Autowired
    private IHHisActivityMaterialsService hisActivityMaterialsService;
    @Autowired
    private IHHisActivityDetailService hisActivityDetailService;

    @Autowired
    private IHHisPatientService hisPatientService;

    @Autowired
    private IHEmployeeService ihEmployeeService;

    @Autowired
    private IHHisCommunityButlerService hisCommunityButlerService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    /**
     * 查询活动管理列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisActivityDTO>> pageQuery(HHisActivityQueryDTO query) {
        HHisActivityQueryModel queryModel = BeanUtil.toBean(query, HHisActivityQueryModel.class);
        queryModel.setTenantId(getTenantId());
        if (StringUtils.isNotBlank(query.getActivityTime())) {
            queryModel.setActivityStartTime(DateUtil.parseDateTime(query.getActivityTime() + " 00:00:01"));
            queryModel.setActivityEndTime(DateUtil.parseDateTime(query.getActivityTime() + " 23:59:59"));
        }
        ListResponse<HHisActivityDTO> pageList = ListResponse.build(hHisActivityService.pageQuery(queryModel), HHisActivityDTO.class);
        return RespResult.OK(pageList);
    }


    /**
     * 查询活动管理列表
     */
    @GetMapping("/list/detail")
    public RespResult<ListResponse<HHisActivityDetailModel>> pageQueryDetail(HHisActivityDetailQueryModel queryModel) {
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisActivityDetailModel> pageList = ListResponse.build(hisActivityDetailService.pageQuery(queryModel), HHisActivityDetailModel.class);
        return RespResult.OK(pageList);
    }

    /**
     * 获取活动管理详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        HHisActivityModel activityModel = hHisActivityService.getById(id);
        HHisActivityViewDTO activityView = BeanUtil.toBean(activityModel, HHisActivityViewDTO.class);
        List<HHisActivityProcessModel> planList1 = hisActivityProcessService.selectList(HHisActivityProcessQueryModel.builder()
                .source("1")
                .activityCode(activityModel.getActivityCode()).build());
        activityView.setDataSource1(planList1);

        List<HHisActivityProcessModel> planList2 = hisActivityProcessService.selectList(HHisActivityProcessQueryModel.builder()
                .source("0")
                .activityCode(activityModel.getActivityCode()).build());
        activityView.setDataSource2(planList2);

        List<HHisActivityMaterialsModel> materialsList = hisActivityMaterialsService.selectList(HHisActivityMaterialsQueryModel.builder()
                .activityCode(activityModel.getActivityCode())
                .build());
        activityView.setDataSource3(materialsList);

        List<HHisActivityDetailModel> detailModelList = hisActivityDetailService.selectList(HHisActivityDetailQueryModel.builder()
                .personTypeList(Lists.newArrayList("0", "1", "3", "4"))
                .build());
        if(CollectionUtils.isNotEmpty(detailModelList)){
            detailModelList.forEach(d->{
                if("0".equals(d.getPersonType())){
                    activityView.setDoctorId(d.getPersonId());
                    activityView.setDoctorName(d.getPersonName());
                }else if("1".equals(d.getPersonType())){
                    activityView.setNurseId(d.getPersonId());
                    activityView.setNurseName(d.getPersonName());
                }else if("3".equals(d.getPersonType())){
                    activityView.setCarerId(d.getPersonId());
                    activityView.setCarerName(d.getPersonName());
                }else if("4".equals(d.getPersonType())){
                    activityView.setSocialWorkerId(d.getPersonId());
                    activityView.setSocialWorkerName(d.getPersonName());
                }
            });
        }
        return RespResult.OK(activityView);
    }

    /**
     * 新增活动管理
     */
    //@PreAuthorize("@ss.hasPermi('system:activity:add')")
    //@Log(title = "活动管理", businessType = BusinessType.INSERT)
    @Transactional
    @PostMapping
    public RespResult add( @Validated @RequestBody HHisActivityFormReqDTO formReq) {
        if (!PhoneUtil.isPhone(formReq.getPhone())) {
            return RespResult.error("手机格式不正确");
        }
        int count = hHisActivityService.count(HHisActivityQueryModel.builder()
                .activityNo(formReq.getActivityNo())
                .orgCode(getOrgCode())
                .build());
        if (count > 0) {
            return RespResult.error("档案号已存在");
        }
        String activityCode = seqNoAutoService.buildBatchNo();
        Date time = new Date();
        List<HHisActivityDetailModel> dateilList = new ArrayList<>();

        if (!ObjectUtils.isLongNull(formReq.getDoctorId())) {
            dateilList.add(getEmployee(activityCode, time, formReq.getDoctorId(), "0"));
        }
        if (!ObjectUtils.isLongNull(formReq.getNurseId())) {
            dateilList.add(getEmployee(activityCode, time, formReq.getNurseId(), "1"));
        }
        if (!ObjectUtils.isLongNull(formReq.getCarerId())) {
            dateilList.add(getCommunity(activityCode, time, formReq.getCarerId(), "3"));
        }
        if (!ObjectUtils.isLongNull(formReq.getSocialWorkerId())) {
            dateilList.add(getCommunity(activityCode, time, formReq.getSocialWorkerId(), "4"));
        }

        HHisActivityModel hHisActivityModel = BeanUtil.toBean(formReq, HHisActivityModel.class);
        hHisActivityModel.setTenantId(getTenantId());
        hHisActivityModel.setCreateUser(getLoginUser().getRealname());
        hHisActivityModel.setCreateTime(time);
        hHisActivityModel.setOrgCode(getOrgCode());
        hHisActivityModel.setActivityCode(activityCode);
        hHisActivityModel.setOrganizerRole("2");//1社工 2职工
        hHisActivityModel.setIzEnroll("1");//社区版需要主动报名
        hHisActivityService.insert(hHisActivityModel);

        if (StringUtils.isNotBlank(formReq.getDataSource1())) {
            List<HHisActivityProcessModel> planList = JSON.parseArray(formReq.getDataSource1(), HHisActivityProcessModel.class);
            planList.forEach(p -> {
                p.setActivityCode(activityCode);
                p.setTenantId(getTenantId());
                p.setSource("1");
            });
            hisActivityProcessService.insertBatch(planList);
        }

        if (StringUtils.isNotBlank(formReq.getDataSource2())) {
            List<HHisActivityProcessModel> planList = JSON.parseArray(formReq.getDataSource2(), HHisActivityProcessModel.class);
            planList.forEach(p -> {
                p.setActivityCode(activityCode);
                p.setTenantId(getTenantId());
                p.setSource("0");
            });
            hisActivityProcessService.insertBatch(planList);
        }

        if (StringUtils.isNotBlank(formReq.getDataSource3())) {
            List<HHisActivityMaterialsModel> materialsList = JSON.parseArray(formReq.getDataSource3(), HHisActivityMaterialsModel.class);
            materialsList.forEach(p -> {
                p.setActivityCode(activityCode);
                p.setTenantId(getTenantId());
            });
            hisActivityMaterialsService.insertBatch(materialsList);
        }

        hisActivityDetailService.insertBatch(dateilList);
        return RespResult.OK("添加成功");
    }

    private HHisActivityDetailModel getCommunity(String activityCode, Date time, Long id, String personType) {
        HHisCommunityButlerModel butlerModel = hisCommunityButlerService.getById(id);
        return HHisActivityDetailModel.builder()
                .activityCode(activityCode)
                .personId(butlerModel.getId())
                .personName(butlerModel.getRealName())
                .sex("0".equals(butlerModel.getSex()) ? "女" : "男")
                .age(DateUtil.ageOfNow(butlerModel.getBirthday()))
                .personType(personType)
                .tenantId(getTenantId())
                .patientBed("")
                .createTime(time)
                .createUser(getLoginUser().getRealname())
                .build();
    }

    private HHisActivityDetailModel getEmployee(String activityCode, Date time, Long id, String personType) {
        HEmployeeModel employeeModel = ihEmployeeService.getById(id);
        return HHisActivityDetailModel.builder()
                .activityCode(activityCode)
                .personId(employeeModel.getId())
                .personName(employeeModel.getName())
                .sex("0".equals(employeeModel.getSex()) ? "女" : "男")
                .age(DateUtil.ageOfNow(employeeModel.getBirthday()))
                .personType(personType)
                .tenantId(getTenantId())
                .patientBed("")
                .createTime(time)
                .createUser(getLoginUser().getRealname())
                .build();
    }


    /**
     * 修改活动管理
     */
    @Transactional
    @PutMapping
    public RespResult edit(@Validated @ModelAttribute HHisActivityFormReqDTO formReq) {
        HHisActivityModel activityModel = hHisActivityService.getById(formReq.getId());
        if (ObjectUtil.isNull(activityModel)) {
            return RespResult.error("记录不存在");
        }
        Date time = new Date();
        HHisActivityModel hHisActivityModel = BeanUtil.toBean(formReq, HHisActivityModel.class);
        hHisActivityModel.setUpdateUser(getLoginUser().getRealname());
        hHisActivityModel.setUpdateTime(time);
        hHisActivityService.updateById(hHisActivityModel);
        String activityCode = activityModel.getActivityCode();


        List<HHisActivityDetailModel> dateilList = new ArrayList<>();
        if (!ObjectUtils.isLongNull(formReq.getDoctorId())) {
            HHisActivityDetailModel detailModel = hisActivityDetailService.getOne(HHisActivityDetailQueryModel.builder()
                    .personType("0")
                    .activityCode(activityCode)
                    .tenantId(getTenantId())
                    .build());
            if (ObjectUtil.isNotNull(detailModel) && !detailModel.getPersonId().equals(formReq.getDoctorId())) {
                hisActivityDetailService.deleteById(detailModel.getId());
                dateilList.add(getEmployee(activityCode, time, formReq.getDoctorId(), "0"));
            } else if (ObjectUtil.isNull(detailModel)) {
                dateilList.add(getEmployee(activityCode, time, formReq.getDoctorId(), "0"));
            }


        }
        if (!ObjectUtils.isLongNull(formReq.getNurseId())) {
            HHisActivityDetailModel detailModel = hisActivityDetailService.getOne(HHisActivityDetailQueryModel.builder()
                    .personType("1")
                    .activityCode(activityCode)
                    .tenantId(getTenantId())
                    .build());
            if (ObjectUtil.isNotNull(detailModel) && !detailModel.getPersonId().equals(formReq.getNurseId())) {
                hisActivityDetailService.deleteById(detailModel.getId());
                dateilList.add(getEmployee(activityCode, time, formReq.getNurseId(), "1"));
            } else if (ObjectUtil.isNull(detailModel)) {
                dateilList.add(getEmployee(activityCode, time, formReq.getNurseId(), "1"));
            }
        }
        if (!ObjectUtils.isLongNull(formReq.getCarerId())) {
            HHisActivityDetailModel detailModel = hisActivityDetailService.getOne(HHisActivityDetailQueryModel.builder()
                    .personType("3")
                    .activityCode(activityCode)
                    .tenantId(getTenantId())
                    .build());
            if (ObjectUtil.isNotNull(detailModel) && !detailModel.getPersonId().equals(formReq.getCarerId())) {
                hisActivityDetailService.deleteById(detailModel.getId());
                dateilList.add(getCommunity(activityCode, time, formReq.getCarerId(), "3"));
            } else if (ObjectUtil.isNull(detailModel)) {
                dateilList.add(getCommunity(activityCode, time, formReq.getCarerId(), "3"));
            }
        }
        if (!ObjectUtils.isLongNull(formReq.getSocialWorkerId())) {
            HHisActivityDetailModel detailModel = hisActivityDetailService.getOne(HHisActivityDetailQueryModel.builder()
                    .personType("4")
                    .activityCode(activityCode)
                    .tenantId(getTenantId())
                    .build());
            if (ObjectUtil.isNotNull(detailModel) && !detailModel.getPersonId().equals(formReq.getSocialWorkerId())) {
                hisActivityDetailService.deleteById(detailModel.getId());
                dateilList.add(getCommunity(activityCode, time, formReq.getSocialWorkerId(), "4"));
            } else if (ObjectUtil.isNull(detailModel)) {
                dateilList.add(getCommunity(activityCode, time, formReq.getSocialWorkerId(), "4"));
            }
        }

        List<Integer> processListId = hisActivityProcessService.selectIds(HHisActivityProcessQueryModel.builder().activityCode(activityCode)
                .tenantId(getTenantId())
                .build());
        if (CollectionUtils.isNotEmpty(processListId)) {
            hisActivityProcessService.deleteByIds(processListId);
        }

        List<Integer> materialaIds = hisActivityMaterialsService.selectIds(HHisActivityMaterialsQueryModel.builder().activityCode(activityCode)
                .tenantId(getTenantId())
                .build());
        if (CollectionUtils.isNotEmpty(materialaIds)) {
            hisActivityMaterialsService.deleteByIds(materialaIds);
        }

        if (StringUtils.isNotBlank(formReq.getDataSource1())) {
            List<HHisActivityProcessModel> planList = JSON.parseArray(formReq.getDataSource1(), HHisActivityProcessModel.class);
            planList.forEach(p -> {
                p.setActivityCode(activityCode);
                p.setTenantId(getTenantId());
                p.setSource("1");
            });
            hisActivityProcessService.insertBatch(planList);
        }
        if (StringUtils.isNotBlank(formReq.getDataSource2())) {
            List<HHisActivityProcessModel> planList = JSON.parseArray(formReq.getDataSource2(), HHisActivityProcessModel.class);
            planList.forEach(p -> {
                p.setActivityCode(activityCode);
                p.setTenantId(getTenantId());
                p.setSource("0");
            });
            hisActivityProcessService.insertBatch(planList);
        }
        if (StringUtils.isNotBlank(formReq.getDataSource3())) {
            List<HHisActivityMaterialsModel> materialsList = JSON.parseArray(formReq.getDataSource3(), HHisActivityMaterialsModel.class);
            materialsList.forEach(p -> {
                p.setActivityCode(activityCode);
                p.setTenantId(getTenantId());
            });
            hisActivityMaterialsService.insertBatch(materialsList);
        }
        if (CollectionUtils.isNotEmpty(dateilList)) {
            hisActivityDetailService.insertBatch(dateilList);
        }

        return RespResult.OK("变更完成");
    }

    /**
     * 删除活动管理
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        HHisActivityModel activityModel = hHisActivityService.getById(id);
        if (ObjectUtil.isNull(activityModel)) {
            return RespResult.error("记录不存在");
        }
        if (DateUtil.compare(activityModel.getActivityDate(), new Date()) == 1) {
            hHisActivityService.deleteById(id);
            return RespResult.OK("关闭成功");
        } else {
            return RespResult.OK("己进入活动时间，不能关闭");
        }
    }
}
