package com.hz.healthy.web.his.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/7/20 00:16
 * @ClassName: HHisInterviewPlanExecFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisInterviewPlanExecFormDTO implements Serializable {

    /** 主键ID */
    private Integer id;
    /** 走访记录内容 */
    private String recordContent;

    /** 签到时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signIn;


    /** 签退时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signOut;




    /**
     * 精神状态
     */
    private List<String> mentality;


    /**
     * 上次执行解决方案后，反馈如何
     */
    private String lastRecord;




    /** 运动情况(分钟/次) */
    private String sportsSituation;
    /** 每周运动情况(次/周) */
    private String weekSportsSituation;
    /** 摄盐情况 1轻 2中 3重 */
    private String saltSituation;
    /** 心理调整 1良好 2一般 3差 */
    private String psychologicalAdjust;
    /** 遵医行为 1良好 2一般 3差 */
    private String complianceBehavior;
    /** 服药依从性 1规律 2间断 3不服从 */
    private String medicationCompliance;
    /** 此次随访分类 1控制满意  2控制不满意 3不良反应 3并发症 */
    private String followUpSituation;
    /** 用药情况 */
    private String medicationSituation;
    /** 医生指导 */
    private String doctorGuidance;


    /**
     * 下次执行时间
     */
    private String nextTime;

}
