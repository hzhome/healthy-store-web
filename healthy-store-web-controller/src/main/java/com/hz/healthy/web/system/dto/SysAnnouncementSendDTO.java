package com.hz.healthy.web.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2025/2/12 22:25
 * @ClassName: SysAnnouncementSendDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class SysAnnouncementSendDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**id*/
    private String id;
    /**通告id*/
    private String anntId;
    /**用户id*/
    private String userId;

    private String userName;

    /**阅读状态*/
    private String readFlag;
    /**发布时间*/
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;


    private Long tenantId;
    private String orgCode;
}

