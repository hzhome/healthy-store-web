package com.hz.healthy.web.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class SysRoleDTO implements Serializable {

    private String id;
    private String roleName;
    private String roleCode;
    private String createBy;
    @DateTimeFormat(
      pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    private LocalDateTime createTime;
    private String systemId;
    private String systemIdName;

    /** 菜单组 */
    private String[] menuIds;
    /**
     * 租户id配置，编辑用户的时候设置
     */
    private Long tenantId;
    /**
     * 是否系统角色 创建平台或租户时间有默认的角色
     */
    private String izSystem;


}
