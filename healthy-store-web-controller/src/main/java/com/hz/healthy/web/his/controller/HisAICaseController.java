package com.hz.healthy.web.his.controller;

import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.*;
import com.hz.healthy.domain.his.model.*;
import com.hz.healthy.enums.CareAiTypeEnums;
import com.hz.healthy.model.AICareItem;
import com.hz.healthy.web.his.dto.response.AICareCategoryDTO;
import com.hz.healthy.web.his.dto.response.AICareCheckUpDTO;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.model.KeyVModel;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * AI智能诊断
 *
 * @author hyhuang
 * @date 2023-03-27
 */
@RestController
@RequestMapping("/his/case/ai")
public class HisAICaseController extends WebBaseController {


    @Autowired
    private IHHisCareAiService hHisCareAiService;

    @Autowired
    private IHHisCareAiSymptomService hisCareAiSymptomService;

    @Autowired
    private IHHisCareAiCheckupService hisCareAiCheckupService;


    /**
     * AI诊断列表
     * 诊断结果
     */
    @AutoDict
    @GetMapping("/diagnosis")
    public RespResult<List<AICareItem>> diagnosis(HisMedicineQueryModel queryModel) {
        List<HHisCareAiModel> dataList = hHisCareAiService.selectList(HHisCareAiQueryModel.builder()
                .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                .build());
        List<AICareItem> data = new ArrayList<>();
        for (HHisCareAiModel symptom : dataList) {
            data.add(AICareItem.builder()
                    .id(symptom.getId())
                    .name(symptom.getDiagnosis())
                    .build());
        }
        return RespResult.OK(data);
    }

    /**
     * 辨证
     *
     * @param queryModel
     * @return
     */
    @AutoDict
    @GetMapping("/dialectic")
    public RespResult<List<AICareItem>> dialectic(HisMedicineQueryModel queryModel) {

        List<AICareItem> data = new ArrayList<>();
        data.add(AICareItem.builder()
                .id(0)
                .name("风寒头痛")
                .build());
        data.add(AICareItem.builder()
                .id(0)
                .name("风热头痛")
                .build());
        data.add(AICareItem.builder()
                .id(0)
                .name("风湿头痛")
                .build());
        data.add(AICareItem.builder()
                .id(0)
                .name("气虚头痛")
                .build());

        data.add(AICareItem.builder()
                .id(0)
                .name("湿热头病")
                .build());
        return RespResult.OK(data);
    }


    /**
     * AI诊断列表
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<List<AICareCategoryDTO>> pageQuery(HisMedicineQueryModel queryModel) {
        List<HHisCareAiSymptomModel> dataList = hisCareAiSymptomService.selectList(HHisCareAiSymptomQueryModel.builder()
                .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                .build());
        List<AICareCategoryDTO> data = new ArrayList<>();
        Map<String, List<AICareItem>> map = new HashMap<>();
        for (HHisCareAiSymptomModel symptom : dataList) {
            List<AICareItem> itemList = map.getOrDefault(symptom.getDiagnosisCode(), new ArrayList<>());
            itemList.add(AICareItem.builder()
                    .id(symptom.getId())
                    .name(symptom.getSymptom())
                    .build());
            map.put(symptom.getDiagnosisCode(), itemList);
        }
        for (Map.Entry<String, List<AICareItem>> entry : map.entrySet()) {
            HHisCareAiModel careAiModel = hHisCareAiService.getOne(HHisCareAiQueryModel.builder()
                    .diagnosisCode(entry.getKey())
                    .build());
            if (ObjectUtil.isNotNull(careAiModel)) {
                List<AICareItem> checkList = new ArrayList<>();
                List<HHisCareAiCheckupModel> checkupList = hisCareAiCheckupService.selectList(HHisCareAiCheckupQueryModel.builder().diagnosisCode(entry.getKey()).build());
                if (CollectionUtils.isNotEmpty(checkupList)) {
                    checkupList.forEach(p -> {
                        checkList.add(AICareItem.builder()
                                .id(p.getId())
                                .name(p.getHealthCheckup())
                                .build());
                    });
                }
                data.add(AICareCategoryDTO.builder()
                        .key(careAiModel.getId().toString())
                        .category(careAiModel.getDiagnosis())
                        .itemList(entry.getValue())
                        .checkList(checkList)
                        .build());
            }
        }
        List<AICareItem> checkList = new ArrayList<>();
        for (int i = 1; i < 15; i++) {
            checkList.add(AICareItem.builder()
                    .id(i)
                    .name(i + "天")
                    .build());
        }
        data.add(AICareCategoryDTO.builder()
                .key("")
                .category("")
                .itemList(checkList)
                .checkList(new ArrayList<>())
                .build());
        return RespResult.OK(data);
    }


    /**
     * AI诊断列表
     * 既往史
     */
    @AutoDict
    @GetMapping("/past/history")
    public RespResult<List<AICareCategoryDTO>> pastHistory(HisMedicineQueryModel queryModel) {

        List<HHisCareAiModel> dataList = hHisCareAiService.selectList(HHisCareAiQueryModel.builder()
                .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                .diagnosisType(CareAiTypeEnums.S6.getCode())
                .build());
        List<AICareCategoryDTO> data = new ArrayList<>();
        String s1 = "既往体健,未见明显异常#";
        s1 += "否认药物过敏史,否认慢性疾病,否认传染病史,否认遗传病史,否认备孕,否认怀孕#";
        s1 += "既往有:高血压心脏病糖尿病,精神疾病,肝炎,肺结核,哮喘,甲亢#";
        s1 += "过敏史:青霉素链雷素卡那雷素,林可需产头陶类,磺胺类,酒精,碘伏,去痛片扑热息痛鲁米那阿司匹林,普鲁卡因安痛定安是,花粉大豆,小麦,花生,鱼虾坚果霉南尘锈毛发皮屑,牛奶,鸡蛋#";
        s1 += "个人史:吸烟偶尔吸烟,长期吸烟,不饮酒偶尔饮酒长期饮酒,未婚已婚,未孕,闭经有早产史,有流产史,有痛经史#";

        for (HHisCareAiModel careAiModel : dataList) {
            List<HHisCareAiSymptomModel> symptomList = hisCareAiSymptomService.selectList(HHisCareAiSymptomQueryModel.builder()
                    .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                    .diagnosisCode(careAiModel.getDiagnosisCode())
                    .diagnosisType(CareAiTypeEnums.S6.getCode())
                    .build());

            List<AICareItem> checkList = new ArrayList<>();
            for (HHisCareAiSymptomModel m : symptomList) {
                checkList.add(AICareItem.builder()
                        .id(m.getId())
                        .name(m.getSymptom())
                        .build());
            }
            data.add(AICareCategoryDTO.builder()
                    .key(careAiModel.getId().toString())
                    .category(careAiModel.getDiagnosis())
                    .izShow(careAiModel.getIzShow())
                    .itemList(checkList)
                    .checkList(new ArrayList<>())
                    .build());
        }
        return RespResult.OK(data);
    }


    /**
     * AI诊断列表
     * 过敏史
     */
    @AutoDict
    @GetMapping("/allergyResult")
    public RespResult<List<AICareCategoryDTO>> allergyResult(HisMedicineQueryModel queryModel) {
        List<HHisCareAiSymptomModel> dataList = hisCareAiSymptomService.selectList(HHisCareAiSymptomQueryModel.builder()
                .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                .diagnosisType(CareAiTypeEnums.S1.getCode())
                .build());
        List<AICareCategoryDTO> data = new ArrayList<>();
        //String s1 = "否认药物过敏史,否认食物过敏史#过敏史:青霉素链雷素卡那雷素,林可需产头陶类,磺胺类,酒精,碘伏,去痛片扑热息痛鲁米那阿司匹林,普鲁卡因安痛定安是,花粉大豆,小麦,花生,鱼虾坚果霉南尘锈毛发皮屑,牛奶,鸡蛋#";
        if (CollectionUtils.isNotEmpty(dataList)) {
            List<AICareItem> checkList = new ArrayList<>();
            for (HHisCareAiSymptomModel m : dataList) {
                checkList.add(AICareItem.builder()
                        .id(m.getId())
                        .name(m.getSymptom())
                        .build());
            }
            data.add(AICareCategoryDTO.builder()
                    .key(CareAiTypeEnums.S1.getCode())
                    .category(CareAiTypeEnums.S1.getCode())
                    .itemList(checkList)
                    .checkList(new ArrayList<>())
                    .build());
        }
        return RespResult.OK(data);
    }


    /**
     * AI诊断列表
     * 过敏史
     */
    @AutoDict
    @GetMapping("/select/allergyResult")
    public RespResult<List<KeyVModel>> selectAllergyResult(HisMedicineQueryModel queryModel) {
        List<KeyVModel> data = new ArrayList<>();
        //String s1 = "青霉素链雷素卡那雷素,林可需产头陶类,磺胺类,酒精,碘伏,去痛片扑热息痛鲁米那阿司匹林,普鲁卡因安痛定安是,花粉大豆,小麦,花生,鱼虾坚果霉南尘锈毛发皮屑,牛奶,鸡蛋";
        // String[] value = StringUtils.split(s1, ",");
        List<HHisCareAiSymptomModel> dataList = hisCareAiSymptomService.selectList(HHisCareAiSymptomQueryModel.builder()
                .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                .diagnosisType(CareAiTypeEnums.S1.getCode())
                .build());

        for (HHisCareAiSymptomModel m : dataList) {
            data.add(KeyVModel.builder()
                    .key(m.getId().toString())
                    .value(m.getSymptom())
                    .build());
        }
        return RespResult.OK(data);
    }


    /**
     * AI诊断列表
     * 月经
     */
    @AutoDict
    @GetMapping("/period")
    public RespResult<List<AICareCategoryDTO>> period(HisMedicineQueryModel queryModel) {
//        String s1 = "婚姻情况:未婚已婚未育，已婚已育适龄婚育配偶体健 离异丧偶#";
//        s1 += "经量色质:经量正常,量多 量少 闭经 经色正常,色淡红色鲜红,经质正常质稻质相夹有血块 痛经无痛经绝经#";
//        s1 += "既往有:高血压心脏病糖尿病,精神疾病,肝炎,肺结核,哮喘,甲亢#";
//        s1 += "白带情况:白带正常,白带量多,白带质稀,白带质稠,白带异味,白带色黄,白带色灰,血色白带,胶性白带,豆渣样白带,泡沫状白带#";
        List<AICareCategoryDTO> data = new ArrayList<>();
        List<HHisCareAiModel> dataList = hHisCareAiService.selectList(HHisCareAiQueryModel.builder()
                .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                .diagnosisType(CareAiTypeEnums.S5.getCode())
                .build());
        for (HHisCareAiModel careAiModel : dataList) {
            List<HHisCareAiSymptomModel> symptomList = hisCareAiSymptomService.selectList(HHisCareAiSymptomQueryModel.builder()
                    .orgCode(CommonConstant.PLATFORM_ORG_CODE)
                    .diagnosisCode(careAiModel.getDiagnosisCode())
                    .diagnosisType(CareAiTypeEnums.S5.getCode())
                    .build());

            List<AICareItem> checkList = new ArrayList<>();
            for (HHisCareAiSymptomModel m : symptomList) {
                checkList.add(AICareItem.builder()
                        .id(m.getId())
                        .name(m.getSymptom())
                        .build());
            }
            data.add(AICareCategoryDTO.builder()
                    .key(careAiModel.getId().toString())
                    .category(careAiModel.getDiagnosis())
                    .izShow(careAiModel.getIzShow())
                    .itemList(checkList)
                    .checkList(new ArrayList<>())
                    .build());
        }
        return RespResult.OK(data);
    }

    /**
     * AI诊断列表
     * 体证提示
     */
    @AutoDict
    @GetMapping("/tips")
    public RespResult<AICareCheckUpDTO> tips(HisMedicineQueryModel queryModel) {
        AICareCheckUpDTO careCheckUp = new AICareCheckUpDTO();

        List<AICareCategoryDTO> data = new ArrayList<>();
        List<HHisCareAiCheckupModel> checkupList = hisCareAiCheckupService.selectList(HHisCareAiCheckupQueryModel.builder().build());
        if (CollectionUtils.isNotEmpty(checkupList)) {
            List<AICareItem> checkList = new ArrayList<>();
            checkupList.forEach(p -> {
                checkList.add(AICareItem.builder()
                        .id(p.getId())
                        .name(p.getHealthCheckup())
                        .build());
            });
            careCheckUp.setItemList(checkList);
            careCheckUp.setCategory("智能体证提示");
            careCheckUp.setKey("100");
        }
        int k=1;
        LinkedHashMap<String, List<AICareItem>> listMap=CareAiTypeEnums.commonCareItem();
        for (Map.Entry<String, List<AICareItem>> map:listMap.entrySet()){
            data.add(AICareCategoryDTO.builder()
                    .key((k++) + "")
                    .category(map.getKey())
                    .itemList(map.getValue())
                    .checkList(new ArrayList<>())
                    .build());
        }
        careCheckUp.setCategoryList(data);
        return RespResult.OK(careCheckUp);
    }


}
