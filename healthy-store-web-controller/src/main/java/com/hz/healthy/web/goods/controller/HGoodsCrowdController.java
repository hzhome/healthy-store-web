package com.hz.healthy.web.goods.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.goods.IHGoodsCrowdService;
import com.hz.healthy.domain.goods.model.HGoodsCrowdModel;
import com.hz.healthy.domain.goods.model.HGoodsCrowdQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品关联适合的人群Controller
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/shop/goods/crowd")
public class HGoodsCrowdController extends WebBaseController
{
    @Autowired
    private IHGoodsCrowdService hGoodsCrowdService;

    /**
     * 查询商品关联适合的人群列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HGoodsCrowdModel>> pageQuery(HGoodsCrowdQueryModel queryModel)  {
        RespResult<ListResponse<HGoodsCrowdModel>> result = new RespResult<ListResponse<HGoodsCrowdModel>>();
        ListResponse<HGoodsCrowdModel> pageList = hGoodsCrowdService.pageQuery(queryModel);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }



    /**
     * 获取商品关联适合的人群详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id)
    {
        return RespResult.OK(hGoodsCrowdService.getById(id));
    }

    /**
     * 新增商品关联适合的人群
     */
    //@PreAuthorize("@ss.hasPermi('system:crowd:add')")
    //@Log(title = "商品关联适合的人群", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HGoodsCrowdModel hGoodsCrowdModel)
    {
        hGoodsCrowdService.insert(hGoodsCrowdModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改商品关联适合的人群
     */
    @PutMapping
    public RespResult edit(@RequestBody  HGoodsCrowdModel hGoodsCrowdModel)
    {
        hGoodsCrowdService.updateById(hGoodsCrowdModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除商品关联适合的人群
     */
	@DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids)
    {
        hGoodsCrowdService.deleteByIds(ids);
        return RespResult.OK();
    }
}
