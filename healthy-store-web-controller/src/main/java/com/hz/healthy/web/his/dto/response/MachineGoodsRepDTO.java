package com.hz.healthy.web.his.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/2/2 21:01
 * @ClassName: MachineGoodsRepDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MachineGoodsRepDTO implements Serializable {


    /** 商品id */
    private Long packageId;
    private String goodsCode;
    private String goodsName;
    private String goodsImg;
    private BigDecimal price;
    private String situation;
    /** 业务项目值，例如风险等级中的低，体质类型的 阴虚 */
    private String busValue;
    /**
     * 是否使用
     * 0 否 1 是
     */
    private String izUse;

    /**
     * 分类码
     */
    private String categoryCode;

    private String code;
}
