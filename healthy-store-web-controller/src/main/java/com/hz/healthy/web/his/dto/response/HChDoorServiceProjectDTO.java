package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/4/9 12:23
 * @ClassName: HChDoorServiceProjectDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HChDoorServiceProjectDTO implements Serializable {

    /** 主键ID */
    private Long id;

    /** 项目分类 取h_category */
    private Long categoryId;
    /** 项目分类 取h_category */
    private String categoryName;

    /** 项目分类 取h_category_tag */
    private Long categoryTagId;
    /** 创建时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 项目类型，1.护士上门 2居家保洁 3陪诊服务 4照护服务 */
    private String projectType;

    /** 项目名称 */
    private String project;

    /** 项目主图片 */
    private String img;

    /** 项目费用 */
    private BigDecimal amount;

    /** 销售数量 */
    private Integer time;

    /** 服务评分 */
    private Long score;

    /** 项目服务时间(次/分) */
    private Integer saleQty;

    /** 服务描述大文本 */
    private String description;

    /** 是否套餐服务 0否 1是 */
    private String izPackage;

    /** 描述 */
    private String remark;

    private Long tenantId;





}
