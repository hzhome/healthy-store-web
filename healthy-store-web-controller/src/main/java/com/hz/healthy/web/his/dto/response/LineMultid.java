package com.hz.healthy.web.his.dto.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2023/10/6 21:44
 * @ClassName: LineMultid
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
public class LineMultid implements Serializable {

    private List<String> fields;

    private List<LinkedHashMap<String,Object>> dataSource;
}
