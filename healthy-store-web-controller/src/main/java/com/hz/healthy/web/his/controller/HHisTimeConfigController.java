package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHHisTimeConfigService;
import com.hz.healthy.domain.doctor.model.HHisTimeConfigModel;
import com.hz.healthy.domain.doctor.model.HHisTimeConfigQueryModel;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisTimeConfigFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisTimeConfigDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * HIS源号设置Controller
 *
 * @author hyhuang
 * @date 2023-01-20
 */
@RestController
@RequestMapping("/his/time/config")
public class HHisTimeConfigController extends WebBaseController {
    @Autowired
    private IHHisTimeConfigService hHisTimeConfigService;

    /**
     * 查询HIS源号设置列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisTimeConfigDTO>> pageQuery(HHisTimeConfigQueryModel queryModel) {
        ListResponse<HHisTimeConfigModel> pageList = hHisTimeConfigService.pageQuery(queryModel);

        return RespResult.OK(ListResponse.build(pageList,HHisTimeConfigDTO.class));
    }



    /**
     * 获取HIS源号设置详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hHisTimeConfigService.getById(id));
    }

    /**
     * 新增HIS源号设置
     */
    //@PreAuthorize("@ss.hasPermi('system:config:add')")
    //@Log(title = "HIS源号设置", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HHisTimeConfigFromReqDTO fromReq) throws Exception {
        HHisTimeConfigModel configModel=BeanUtil.toBean(fromReq,HHisTimeConfigModel.class);
        configModel.setCreateTime(new Date());
        configModel.setCreateUser(getLoginUser().getRealname());
        configModel.setOrgCode(getLoginUser().getOrgCode());
        hHisTimeConfigService.insert(configModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改HIS源号设置
     */
    @PutMapping
    public RespResult edit(@RequestBody HHisTimeConfigFromReqDTO fromReq) throws Exception {
        Long id=fromReq.getId();
        int count=hHisTimeConfigService.count(HHisTimeConfigQueryModel.builder().id(id).build());
        if(count==0){
            return RespResult.error("记录不存在-校验未通过");
        }
        HHisTimeConfigModel configModel=BeanUtil.toBean(fromReq,HHisTimeConfigModel.class);
        configModel.setUpdateTime(new Date());
        configModel.setUpdateUser(getLoginUser().getRealname());
        hHisTimeConfigService.updateById(configModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除HIS源号设置
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hHisTimeConfigService.deleteByIds(ids);
        return RespResult.OK();
    }

    @GetMapping(value = "/selectList")
    public RespResult<List<KeyLongModel>> selectList() {
        List<KeyLongModel> dataList=new ArrayList<>();
        List<HHisTimeConfigModel> planList=hHisTimeConfigService.selectList(HHisTimeConfigQueryModel.builder()
                .orgCode(getLoginUser().getOrgCode())
                .build());
        if(CollectionUtils.isNotEmpty(planList)){
            planList.forEach(p->{
                KeyLongModel kl=new KeyLongModel();
                kl.setKey(p.getId());
                kl.setLabel(p.getName());
                dataList.add(kl);
            });
        }
        return RespResult.OK(dataList);
    }
}
