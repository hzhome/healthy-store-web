package com.hz.healthy.web.goods.dto.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/1/22 13:41
 * @ClassName: HGoodsDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HGoodsDTO implements Serializable {

    private Long id;

    /** 分类编码 唯一标识 */
    private String categoryName;

    /** 分类编码 唯一标识 */
    private String categoryCode;

    /** 商品的唯一货号 */
    private String goodsSn;

    /** 商品的名称 */
    private String goodsName;

    /** 商品名称显示的样式；包括颜色和字体样式；格式如#ff00ff+strong */
    private String goodsNameStyle;

    /** 商品点击数 */
    private Integer clickCount;

    /** 品牌id，取值于brand 的brand_id */
    private String brandCode;

    /** 供货人的名称 */
    private String providerName;

    /** 商品库存数量 */
    private Integer goodsNumber;

    /** 商品的重量，以千克为单位 */
    private BigDecimal goodsWeight;

    /** 市场售价 */
    private BigDecimal marketPrice;

    /** 本店售价 */
    private BigDecimal shopPrice;

    /** 促销价格 */
    private BigDecimal promotePrice;

    /** 促销价格开始日期 */
    private Date promoteStartDate;

    /** 促销价格结束日期 */
    private Date promoteEndDate;

    /** 商品报警数量 */
    private Integer warnNumber;

    /** 商品关键字，放在商品页的关键字中，为搜索引擎收录用 */
    private String keywords;

    /** 商品的简短描述 */
    private String goodsBrief;

    /** 商品的详细描述 */
    private String goodsDesc;

    /** 商品在前台显示的微缩图片，如在分类筛选时显示的小图片 */
    private String goodsThumb;

    /** 商品的实际大小图片，如进入该商品页时介绍商品属性所显示的大图片 */
    private String goodsImg;

    /** 是否是实物，1，是；0，否；比如虚拟卡就为0，不是实物 */
    private Integer isReal;

    /** 商品的扩展属性，比如像虚拟卡 */
    private String extensionCode;

    /** 该商品是否开放销售，1，是；0，否 */
    private Integer isOnSale;

    /** 是否能单独销售，1，是；0，否；如果不能单独销售，则只能作为某商品的配件或者赠品销售 */
    private Integer isAloneSale;

    /** 购买该商品可以使用的积分数量，估计应该是用积分代替金额消费；但程序好像还没有实现该功能 */
    private Integer integral;

    /** 应该是商品的显示顺序，不过该版程序中没实现该功能 */
    private Integer sortOrder;

    /** 是否特价促销；0，否；1，是 */
    private Integer isPromote;

    /** 商品所属类型id，取值表goods_type的cat_i */
    private String goodsTypeCode;

    /** 商品的商家备注，仅商家可见 */
    private String sellerNote;

    /** 购买该商品时每笔成功交易赠送的积分数量 */
    private Integer giveIntegral;


    /**
     * 是否可编辑
     */
    private boolean izEdit;


    /** 租户id */
    private Long tenantId;

}
