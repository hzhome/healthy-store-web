package com.hz.healthy.web.system.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2023/8/5 13:01
 * @ClassName: SysCytfdayDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class SysCytfdayDTO implements Serializable {

    private int leaveDay;
    private int monthDay;
    private String leaveBedIsRefund;
    private String nurseIsRefund;
    private String foodIsRefund;
}
