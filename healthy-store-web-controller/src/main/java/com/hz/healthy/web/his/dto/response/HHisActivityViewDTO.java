package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.healthy.domain.his.model.HHisActivityMaterialsModel;
import com.hz.healthy.domain.his.model.HHisActivityProcessModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2023/11/17 12:05
 * @ClassName: HHisActivityViewDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisActivityViewDTO implements Serializable {
    /** 主键ID */
    private Long id;

    /** 内部唯一编码 */
    private String activityCode;
    private Long activityType;
    private String activityNo;
    /**
     * 活动属性 0活动 1课程
     */
    private String activityAttr;

    /** 活动主题 */
    private String topic;

    /** 活动名称 */
    private String name;

    /** 活动地点 */
    private String place;

    /** 活动组织者 */
    private String organizer;
    private String organizerRole;
    private Long organizerId;
    /** 联系电话 */
    private String phone;

    /** 活动日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    private Date activityDate;
    /** 参与对像 */
    private String activityObj;
    /**
     * 活动背景
     */
    private String background;
    /**
     * 活动背景
     */
    private String target;

    /**
     * 分目标
     */
    private String targetSub;

    /**
     * 分目标
     */
    private String idea;

    private List<HHisActivityProcessModel> dataSource1;
    private List<HHisActivityProcessModel> dataSource2;
    private List<HHisActivityMaterialsModel> dataSource3;


    private Long doctorId;
    private String doctorName;

    private Long nurseId;
    private String nurseName;

    private Long carerId;
    private String carerName;

    private Long socialWorkerId;
    private String socialWorkerName;

    //----------- 来自 transient 修饰列
    //----------- 来自 transient 修饰列 End
}
