package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.JSON;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.*;
import com.hz.healthy.domain.his.model.*;
import com.hz.healthy.web.his.dto.request.HHisPrescriptionTemplateFormDTO;
import com.hz.healthy.web.his.dto.request.item.HHisPrescriptionTemplateGroupItem;
import com.hz.healthy.web.his.dto.request.item.HHisPrescriptionTemplateItem;
import com.hz.healthy.web.his.dto.response.HHisPrescriptionTemplateDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 处方模板Controller
 *
 * @author ruoyi
 * @date 2024-11-20
 */
@RestController
@RequestMapping("/his/prescription/template")
public class HHisPrescriptionTemplateController extends WebBaseController {
    @Autowired
    private IHHisPrescriptionTemplateService hHisPrescriptionTemplateService;

    @Autowired
    private IHHisPrescriptionTemplateDetailService hisPrescriptionTemplateDetailService;

    @Autowired
    private IHHisPrescriptionTemplateDetailExtService hisPrescriptionTemplateDetailExtService;
    @Autowired
    private IHHisPrescriptionTemplateCategoryService hisPrescriptionTemplateCategoryService;
    @Autowired
    private IHisMedicineService hisMedicineService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;
    @Autowired
    private IHHisPrescriptionTemplateCategoryService hHisPrescriptionTemplateCategoryService;
    /**
     * 查询处方模板列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisPrescriptionTemplateDTO>> pageQuery(HHisPrescriptionTemplateQueryModel queryModel) {
        List<HHisPrescriptionTemplateDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisPrescriptionTemplateModel> pageList = hHisPrescriptionTemplateService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> codeList = pageList.getRecords().stream().map(HHisPrescriptionTemplateModel::getCategoryCode).collect(Collectors.toList());
            Map<String, String> codeMap = hisPrescriptionTemplateCategoryService.mapName(codeList);
            pageList.getRecords().forEach(p -> {
                HHisPrescriptionTemplateDTO dto = BeanUtil.toBean(p, HHisPrescriptionTemplateDTO.class);

                dto.setCategoryName(codeMap.getOrDefault(p.getCategoryCode(), ""));
                dataList.add(dto);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 获取处方模板详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hHisPrescriptionTemplateService.getById(id));
    }

    /**
     * 获取处方模板详细信息
     */
    @GetMapping(value = "/detail/{prescriptionCode}")
    public RespResult<List<HHisPrescriptionTemplateItem>> getDetail(@PathVariable("prescriptionCode") String prescriptionCode) {
        List<HHisPrescriptionTemplateDetailModel> list = hisPrescriptionTemplateDetailService.selectList(HHisPrescriptionTemplateDetailQueryModel.builder()
                .prescriptionCode(prescriptionCode)
                .orgCode(getOrgCode())
                .tenantId(getTenantId())
                .build());
        return RespResult.OK(BeanUtil.copyToList(list, HHisPrescriptionTemplateItem.class));
    }

    /**
     * 新增处方模板（西药）
     */
    @Transactional
    @PostMapping
    public RespResult add(@Validated @RequestBody HHisPrescriptionTemplateFormDTO templateForm) {
        if (CollectionUtils.isEmpty(templateForm.getItems())) {
            return RespResult.error("缺失处方药明细");
        }
        if(StringUtils.isBlank(templateForm.getCategoryCode())){
            return RespResult.error("请选择处方模板分类");
        }
        Date now = new Date();
        String code = seqNoAutoService.buildBatchNo();
        HHisPrescriptionTemplateModel templateModel = BeanUtil.toBean(templateForm, HHisPrescriptionTemplateModel.class);
        templateModel.setCreateTime(now);
        templateModel.setCreateUser(getLoginUser().getRealname());
        templateModel.setPrescriptionCode(code);
        templateModel.setOrgCode(getOrgCode());
        templateModel.setTenantId(getTenantId());
        templateModel.setEyId(getDoctorId());
        hHisPrescriptionTemplateService.insert(templateModel);

        List<HHisPrescriptionTemplateDetailModel> items = BeanUtil.copyToList(templateForm.getItems(), HHisPrescriptionTemplateDetailModel.class);
        List<Long> medicineIds = items.stream().map(HHisPrescriptionTemplateDetailModel::getMedicineId).collect(Collectors.toList());

        Map<Long, HisMedicineSimpleModel> medicineMap = hisMedicineService.mapByIds(medicineIds);
        items.forEach(p -> {
            HisMedicineSimpleModel medicineSimpleModel = medicineMap.get(p.getMedicineId());
            p.setPrescriptionCode(code);
            p.setPrescriptionType(templateForm.getTemplateType());
            p.setCreateUser(getLoginUser().getRealname());
            p.setCreateTime(now);
            p.setOrgCode(getOrgCode());
            p.setTenantId(getTenantId());
            p.setMedicineNo(medicineSimpleModel.getMedicineNo());
            p.setMedicineName(medicineSimpleModel.getMedicineName());
            p.setStandard(medicineSimpleModel.getStandard());
            p.setPrice(medicineSimpleModel.getSalePrice());
            p.setGroupCode(code);
            p.setTotalDose(NumberUtil.mul(p.getDose(), p.getDay()));
        });
        hisPrescriptionTemplateDetailService.insertBatch(items);
        return RespResult.OK("添加成功");
    }

    /**
     * 新增处方模板（中药）
     */
    @Transactional
    @PostMapping(value = "/chinese/medicine")
    public RespResult addChineseMedicine(@Validated @RequestBody HHisPrescriptionTemplateFormDTO templateForm) {
        if (CollectionUtils.isEmpty(templateForm.getItems())) {
            return RespResult.error("缺失处方药明细");
        }
        Date now = new Date();
        String code = seqNoAutoService.buildBatchNo();
        HHisPrescriptionTemplateModel templateModel = BeanUtil.toBean(templateForm, HHisPrescriptionTemplateModel.class);
        templateModel.setCreateTime(now);
        templateModel.setCreateUser(getLoginUser().getRealname());
        templateModel.setPrescriptionCode(code);
        templateModel.setOrgCode(getOrgCode());
        templateModel.setTenantId(getTenantId());
        templateModel.setEyId(getDoctorId());
        hHisPrescriptionTemplateService.insert(templateModel);

        hisPrescriptionTemplateDetailExtService.insert(
                HHisPrescriptionTemplateDetailExtModel.builder()
                        .createTime(now)
                        .createUser(getLoginUser().getRealname())
                        .dayQty(templateForm.getDayQty())
                        .dose(templateForm.getDose())
                        .usages(templateForm.getUsages())
                        .frequency(templateForm.getFrequency())
                        .doseQty(templateForm.getDoseQty())
                        .attrType(templateForm.getAttrType())
                        .groupCode(code)
                        .prescriptionCode(code)
                        .orgCode(getOrgCode())
                        .tenantId(getTenantId())
                        .build()
        );

        List<HHisPrescriptionTemplateDetailModel> items = BeanUtil.copyToList(templateForm.getItems(), HHisPrescriptionTemplateDetailModel.class);
        List<Long> medicineIds = items.stream().map(HHisPrescriptionTemplateDetailModel::getMedicineId).collect(Collectors.toList());

        Map<Long, HisMedicineSimpleModel> medicineMap = hisMedicineService.mapByIds(medicineIds);
        items.forEach(p -> {
            HisMedicineSimpleModel medicineSimpleModel = medicineMap.get(p.getMedicineId());
            p.setPrescriptionCode(code);
            p.setPrescriptionType(templateForm.getTemplateType());
            p.setCreateUser(getLoginUser().getRealname());
            p.setCreateTime(now);
            p.setOrgCode(getOrgCode());
            p.setTenantId(getTenantId());
            p.setMedicineNo(medicineSimpleModel.getMedicineNo());
            p.setMedicineName(medicineSimpleModel.getMedicineName());
            p.setStandard(medicineSimpleModel.getStandard());
            p.setPrice(medicineSimpleModel.getSalePrice());
            p.setGroupCode(code);
            p.setTotalDose(NumberUtil.mul(p.getDose(), p.getDay()));
        });
        hisPrescriptionTemplateDetailService.insertBatch(items);
        return RespResult.OK("添加成功");
    }


    /**
     * 新增处方模板（输注）
     */
    @Transactional
    @PostMapping(value = "/infusion/medicine")
    public RespResult addInfusionMedicine(@Validated @RequestBody HHisPrescriptionTemplateFormDTO templateForm) {
        if (CollectionUtils.isEmpty(templateForm.getGroupList())) {
            return RespResult.error("缺失处方药明细");
        }
        Date now = new Date();
        String code = seqNoAutoService.buildBatchNo();
        HHisPrescriptionTemplateModel templateModel = BeanUtil.toBean(templateForm, HHisPrescriptionTemplateModel.class);
        templateModel.setCreateTime(now);
        templateModel.setCreateUser(getLoginUser().getRealname());
        templateModel.setPrescriptionCode(code);
        templateModel.setOrgCode(getOrgCode());
        templateModel.setTenantId(getTenantId());
        templateModel.setEyId(getDoctorId());
        hHisPrescriptionTemplateService.insert(templateModel);

        for(HHisPrescriptionTemplateGroupItem  groupItem:templateForm.getGroupList()){
            String groupCode = seqNoAutoService.buildBatchNo();
            hisPrescriptionTemplateDetailExtService.insert(
                    HHisPrescriptionTemplateDetailExtModel.builder()
                            .createTime(now)
                            .createUser(getLoginUser().getRealname())
                            .usages(groupItem.getType())
                            .frequency(groupItem.getFrequency())
                            .groupCode(groupCode)
                            .prescriptionCode(code)
                            .orgCode(getOrgCode())
                            .tenantId(getTenantId())
                            .build()
            );

            List<HHisPrescriptionTemplateDetailModel> items = BeanUtil.copyToList(groupItem.getItems(), HHisPrescriptionTemplateDetailModel.class);
            List<Long> medicineIds = items.stream().map(HHisPrescriptionTemplateDetailModel::getMedicineId).collect(Collectors.toList());

            Map<Long, HisMedicineSimpleModel> medicineMap = hisMedicineService.mapByIds(medicineIds);
            items.forEach(p -> {
                HisMedicineSimpleModel medicineSimpleModel = medicineMap.get(p.getMedicineId());
                p.setPrescriptionCode(code);
                p.setPrescriptionType(templateForm.getTemplateType());
                p.setCreateUser(getLoginUser().getRealname());
                p.setCreateTime(now);
                p.setOrgCode(getOrgCode());
                p.setTenantId(getTenantId());
                p.setMedicineNo(medicineSimpleModel.getMedicineNo());
                p.setMedicineName(medicineSimpleModel.getMedicineName());
                p.setStandard(medicineSimpleModel.getStandard());
                p.setPrice(medicineSimpleModel.getSalePrice());
                p.setGroupCode(groupCode);
                p.setTotalDose(NumberUtil.mul(p.getDose(), p.getDay()));
            });
            hisPrescriptionTemplateDetailService.insertBatch(items);
        }





        return RespResult.OK("添加成功");
    }


    /**
     * 修改处方模板
     */
    @PutMapping
    public RespResult edit(@RequestBody HHisPrescriptionTemplateModel hHisPrescriptionTemplateModel) {
        hHisPrescriptionTemplateService.updateById(hHisPrescriptionTemplateModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除处方模板
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hHisPrescriptionTemplateService.deleteByIds(ids);
        return RespResult.OK();
    }
}
