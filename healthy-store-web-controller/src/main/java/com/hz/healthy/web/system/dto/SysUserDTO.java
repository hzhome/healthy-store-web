package com.hz.healthy.web.system.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class SysUserDTO implements Serializable {

    private String id;
    private String username;
    private String realname;
    private String password;
    private String salt;
    private String avatar;
    @DateTimeFormat(
            pattern = "yyyy-MM-dd"
    )
    private LocalDateTime birthday;
    private Integer sex;
    private String email;
    private String phone;
    private String orgCode;
    private transient String orgCodeTxt;
    private Integer status;
    private Integer delFlag;
    private String workNo;
    private String post;
    private String telephone;
    private String createBy;
    private String updateBy;
    private Integer activitiSync;
    private Integer userIdentity;
    private String departIds;
    private String relTenantIds;
    private String clientId;
    private String systemId;
    private String systemIdName;



}
