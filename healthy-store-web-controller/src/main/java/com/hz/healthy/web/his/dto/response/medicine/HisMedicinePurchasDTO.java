package com.hz.healthy.web.his.dto.response.medicine;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hyhuang
 * @desc
 * @date 2023/3/29 18:11
 */
@Data
public class HisMedicinePurchasDTO implements Serializable {

    /** 主键 */
    private Long id;

    /** 药品名称 */
    private String medicineName;

    /** 药品编码 */
    private String medicineNo;

    /** 生产厂家 */
    private Long factoryId;

    /** 药品类型 */
    private String medicineType;

    /** 处方类型 */
    private String prescribe;

    /** 单位 */
    private String unit;

    /** 价格 */
    private BigDecimal price;

    /** 库存量 */
    private int stock;

    /** 总金额 */
    private BigDecimal totalAmount;

    private String batchNo;
    private String remark;


}
