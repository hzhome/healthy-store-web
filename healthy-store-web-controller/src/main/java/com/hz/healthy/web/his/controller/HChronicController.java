package com.hz.healthy.web.his.controller;

import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.ch.*;
import com.hz.healthy.domain.ch.model.*;
import com.hz.healthy.domain.his.IHHisSportPlanService;
import com.hz.healthy.domain.his.model.*;
import com.hz.healthy.domain.ord.IOrdCpInfoExtService;
import com.hz.healthy.domain.ord.IOrdCpInfoService;
import com.hz.healthy.domain.ord.model.OrdCpInfoExtModel;
import com.hz.healthy.domain.ord.model.OrdCpInfoExtQueryModel;
import com.hz.healthy.domain.ord.model.OrdCpInfoModel;
import com.hz.healthy.enums.RiskLevelEnums;
import com.hz.healthy.web.his.dto.request.MachineBindFormReqDTO;
import com.hz.healthy.web.his.dto.request.MachineQueryDTO;
import com.hz.healthy.web.his.dto.response.HStoreMachineGoodsModel;
import com.hz.healthy.web.his.dto.response.MachineGoodsRepDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.ObjectUtils;
import com.hz.ocean.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author hyhuang
 * @Date 2024/2/2 20:58
 * @ClassName: HChronicController
 * @Description: TODO
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/store/machine")
public class HChronicController extends WebBaseController {

    @Autowired
    private IOrdCpInfoService ordCpInfoService;
    @Autowired
    private IOrdCpInfoExtService ordCpInfoExtService;

    @Autowired
    private IHHisSportPlanService hHisSportPlanService;

    @Autowired
    private IHEvaluationPackageService hEvaluationPackageService;

    @Autowired
    private IHEvaluationPackageCategoryService hEvaluationPackageCategoryService;
    @Autowired
    private IHEvaluationPackageConditionService ihEvaluationPackageConditionService;


    @Autowired
    private IHChDoorProjectEvaluationPackageService hChDoorProjectEvaluationPackageService;


    /**
     * @param queryModel
     * @return
     */
    @GetMapping(value = "/list")
    public RespResult<ListResponse<MachineGoodsRepDTO>> queryPageList(MachineQueryDTO queryModel) {
        HEvaluationPackageCategoryModel categoryModel = hEvaluationPackageCategoryService.getOne(HEvaluationPackageCategoryQueryModel.builder()
                .packageType(queryModel.getPackageType())
                .orgCode(getOrgCode())
                .tenantId(0L)
                .build());
        if (ObjectUtil.isNull(categoryModel)) {
            return RespResult.OK(ListResponse.empty());
        }
        HEvaluationPackageQueryModel packageQueryModel = HEvaluationPackageQueryModel.builder()
                .packageType(queryModel.getPackageType())
                .categoryCode(categoryModel.getCategoryCode())
                .orgCode(getOrgCode())
                .tenantId(0L)
                .build();
        if (StringUtils.isNotBlank(queryModel.getBusValue())) {
            List<String> conditionCodeList = ihEvaluationPackageConditionService.listConditionCode(queryModel.getBusValue(), categoryModel.getCategoryCode());
            if(CollectionUtils.isEmpty(conditionCodeList)){
                return RespResult.OK(ListResponse.empty());
            }
            packageQueryModel.setConditionCodeList(conditionCodeList);
        }
        if (StringUtils.isNotBlank(queryModel.getProjectName())) {
            List<Long> projectIds = getProjectIds(queryModel.getProjectName(), queryModel.getPackageType());
            if (CollectionUtils.isNotEmpty(projectIds)) {
                packageQueryModel.setPackageIdList(projectIds);
            }
        }

        packageQueryModel.setPageNo(queryModel.getPageNo());
        packageQueryModel.setPageSize(queryModel.getPageSize());
        ListResponse<HEvaluationPackageModel> pageList = hEvaluationPackageService.pageQuery(packageQueryModel);
        if (CollectionUtils.isEmpty(pageList.getRecords())) {
            return RespResult.OK(ListResponse.empty());
        }
        List<String> conditionList = pageList.getRecords().stream().map(HEvaluationPackageModel::getConditionCode).collect(Collectors.toList());
        Map<String, String> conditionMap = ihEvaluationPackageConditionService.mapByConditionCode(conditionList);
        //套餐类型 1套餐方案 2膳食方案 3运动方案
        if ("1".equals(queryModel.getPackageType()) || "4".equals(queryModel.getPackageType()) || "5".equals(queryModel.getPackageType())) {
            return packagePageList(pageList, queryModel.getPackageType(), conditionMap);
        } else if ("2".equals(queryModel.getPackageType())) {
            return cookbookPageList(pageList, queryModel.getPackageType(), conditionMap);
        } else {
            return sportPageList(pageList, queryModel.getPackageType(), conditionMap);
        }
    }

    public List<Long> getProjectIds(String packageName, String packageType) {
        if ("1".equals(packageType) || "4".equals(packageType) || "5".equals(packageType)) {
            List<Long> idList = hChDoorProjectEvaluationPackageService.likeName(packageName, packageType,
                    getOrgCode());
            return idList;
        } else {
            List<Long> idList = ordCpInfoService.likeName(packageName, "2",
                    getOrgCode());
            return idList;
        }

    }

    /**
     * 一体机食谱
     *
     * @return
     */
    @GetMapping(value = "/list/cookbook")
    public RespResult<ListResponse<MachineGoodsRepDTO>> cookbookPageList(ListResponse<HEvaluationPackageModel> pageList, String categoryCode, Map<String, String> conditionMap) {
        List<MachineGoodsRepDTO> dataList = new ArrayList<>();
        List<Long> packetIds = pageList.getRecords().stream().map(HEvaluationPackageModel::getPackageId).collect(Collectors.toList());
        List<String> codes = pageList.getRecords().stream().map(HEvaluationPackageModel::getCode).collect(Collectors.toList());
        List<OrdCpInfoModel> dbList = ordCpInfoService.listById(packetIds);
        if (CollectionUtils.isEmpty(dbList)) {
            return RespResult.OK(ListResponse.empty());
        }
        Map<Long, OrdCpInfoModel> dbMap = dbList.stream().collect(Collectors.toMap(OrdCpInfoModel::getId, Function.identity()));

        List<String> set = hEvaluationPackageService.listCode(getOrgCode(), getTenantId(), codes);
        pageList.getRecords().forEach(data -> {
            OrdCpInfoModel item = dbMap.get(data.getPackageId());
            String busValue = conditionMap.getOrDefault(data.getConditionCode(), "");
            dataList.add(MachineGoodsRepDTO.builder()
                    .packageId(data.getId())
                    .goodsCode(item.getCpId())
                    .goodsName(item.getCpName())
                    .goodsImg(getImageUrl(item.getCpPicture()))
                    .price(item.getCpPrice())
                    .situation(item.getRemark())
                    .code(data.getCode())
                    .busValue(StringUtils.defaultIfBlank(RiskLevelEnums.getEnumToDesc(busValue),busValue))
                    .izUse(set.contains(data.getCode()) ? "1" : "0")
                    .categoryCode(categoryCode)
                    .build());
        });
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }

    private RespResult<HStoreMachineGoodsModel> cookbookPage(Long id) {
        HStoreMachineGoodsModel goodsModel = new HStoreMachineGoodsModel();
        OrdCpInfoModel cpInfoModel = ordCpInfoService.getById(id);
        OrdCpInfoExtModel cpInfoExtModel = ordCpInfoExtService.getOne(OrdCpInfoExtQueryModel.builder().cpId(cpInfoModel.getCpId()).build());

        if (ObjectUtil.isNotNull(cpInfoModel) && ObjectUtil.isNotNull(cpInfoExtModel)) {
            goodsModel = HStoreMachineGoodsModel.builder()
                    .goodsName(cpInfoModel.getCpName())
                    .price(cpInfoModel.getCpPrice())
                    .brief(cpInfoExtModel.getRemark())
                    .imageUrl(cpInfoModel.getCpPicture())
                    .description(cpInfoExtModel.getNutritionalValue())
                    .build();
        }
        return RespResult.OK(goodsModel);
    }


    /**
     * 一体机运动方案
     *
     * @return
     */
    @GetMapping(value = "/list/sport")
    public RespResult<ListResponse<MachineGoodsRepDTO>> sportPageList(ListResponse<HEvaluationPackageModel> pageList, String categoryCode, Map<String, String> conditionMap) {
        List<MachineGoodsRepDTO> dataList = new ArrayList<>();
        List<Long> packetIds = pageList.getRecords().stream().map(HEvaluationPackageModel::getPackageId).collect(Collectors.toList());
        List<String> codes = pageList.getRecords().stream().map(HEvaluationPackageModel::getCode).collect(Collectors.toList());
        List<HHisSportPlanModel> dbList = hHisSportPlanService.listById(packetIds);
        if (CollectionUtils.isEmpty(dbList)) {
            return RespResult.OK(ListResponse.empty());
        }
        Map<Long, HHisSportPlanModel> dbMap = dbList.stream().collect(Collectors.toMap(HHisSportPlanModel::getId, Function.identity()));

        List<String> set = hEvaluationPackageService.listCode(getOrgCode(), getTenantId(), codes);
        pageList.getRecords().forEach(data -> {
            HHisSportPlanModel item = dbMap.get(data.getPackageId());
            String busValue = conditionMap.getOrDefault(data.getConditionCode(), "");

            dataList.add(MachineGoodsRepDTO.builder()
                    .packageId(data.getId())
                    .goodsCode(item.getPlanCode())
                    .goodsName(item.getName())
                    .goodsImg(getImageUrl(item.getSportImg()))
                    .price(item.getAmount())
                    .situation(item.getRemark())
                    .code(data.getCode())
                    .busValue(StringUtils.defaultIfBlank(RiskLevelEnums.getEnumToDesc(busValue),busValue))
                    .izUse(set.contains(data.getCode()) ? "1" : "0")
                    .categoryCode(categoryCode)
                    .build());
        });
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }

    private RespResult<HStoreMachineGoodsModel> sportPage(Long id) {
        HStoreMachineGoodsModel goodsModel = new HStoreMachineGoodsModel();
        HHisSportPlanModel planModel = hHisSportPlanService.getById(id);
        if (ObjectUtil.isNotNull(planModel)) {
            goodsModel = HStoreMachineGoodsModel.builder()
                    .goodsName(planModel.getName())
                    .price(planModel.getAmount())
                    .brief(planModel.getRemark())
                    .imageUrl(planModel.getSportImg())
                    .description(planModel.getContent())
                    .build();
        }
        return RespResult.OK(goodsModel);
    }

    /**
     * 一体机配套服务
     *
     * @param pageList
     * @return
     */
    @GetMapping(value = "/list/package")
    public RespResult<ListResponse<MachineGoodsRepDTO>> packagePageList(ListResponse<HEvaluationPackageModel> pageList, String categoryCode, Map<String, String> conditionMap) {
        List<Long> packetIds = pageList.getRecords().stream().map(HEvaluationPackageModel::getPackageId).collect(Collectors.toList());
        List<String> codes = pageList.getRecords().stream().map(HEvaluationPackageModel::getCode).collect(Collectors.toList());
        List<HChDoorProjectEvaluationPackageModel> dbList = hChDoorProjectEvaluationPackageService.listById(packetIds);
        if (CollectionUtils.isEmpty(dbList)) {
            return RespResult.OK(ListResponse.empty());
        }
        Map<Long, HChDoorProjectEvaluationPackageModel> dbMap = dbList.stream().collect(Collectors.toMap(HChDoorProjectEvaluationPackageModel::getId, Function.identity()));
        List<MachineGoodsRepDTO> dataList = new ArrayList<>();
        List<String> set = hEvaluationPackageService.listCode(getOrgCode(), getTenantId(), codes);
        pageList.getRecords().forEach(data -> {
            HChDoorProjectEvaluationPackageModel item = dbMap.get(data.getPackageId());
            String busValue = conditionMap.getOrDefault(data.getConditionCode(), "");
            dataList.add(MachineGoodsRepDTO.builder()
                    .packageId(data.getId())
                    .goodsCode(item.getProjectCode())
                    .goodsName(item.getPackageName())
                    .goodsImg(getImageUrl(item.getImg()))
                    .price(item.getAmount())
                    .situation(item.getRemark())
                    .code(data.getCode())
                    .busValue(StringUtils.defaultIfBlank(RiskLevelEnums.getEnumToDesc(busValue),busValue))
                    .izUse(set.contains(data.getCode()) ? "1" : "0")
                    .categoryCode(categoryCode)
                    .build());
        });
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }

    private RespResult<HStoreMachineGoodsModel> packagePage(Long id) {
        HStoreMachineGoodsModel goodsModel = new HStoreMachineGoodsModel();
        HChDoorProjectEvaluationPackageModel packagePage = hChDoorProjectEvaluationPackageService.getById(id);
        if (ObjectUtil.isNotNull(packagePage)) {
            goodsModel = HStoreMachineGoodsModel.builder()
                    .goodsName(packagePage.getPackageName())
                    .price(packagePage.getAmount())
                    .brief(packagePage.getDescription())
                    .imageUrl(packagePage.getImg())
                    .description(packagePage.getDescription())
                    .build();
        }
        return RespResult.OK(goodsModel);
    }

    /**
     * 一体机配套服务
     * <pre>
     *     认购服务
     * </pre>
     *
     * @param formReq
     * @return
     */
    @Transactional
    @PostMapping(value = "/bind")
    public RespResult bind(@Validated @RequestBody MachineBindFormReqDTO formReq) {
        int count = hEvaluationPackageService.count(HEvaluationPackageQueryModel.builder()
                .code(formReq.getCode())
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .build());
        if (count > 0) {
            return RespResult.error("此服务已认购过");
        }
        HEvaluationPackageModel packageModel = hEvaluationPackageService.getOne(HEvaluationPackageQueryModel.builder()
                .code(formReq.getCode())
                .tenantId(0L).orgCode(getOrgCode())
                .build());
        if (ObjectUtil.isNull(packageModel)) {
            return RespResult.error("认购服务不存在");
        }
        HEvaluationPackageConditionModel conditionModel = ihEvaluationPackageConditionService.getOne(HEvaluationPackageConditionQueryModel.builder()
                .categoryCode(packageModel.getCategoryCode())
                .conditionCode(packageModel.getConditionCode())
                .orgCode(getOrgCode())
                .tenantId(getTenantId())
                .build());
        if (ObjectUtil.isNull(conditionModel)) {
            conditionModel = ihEvaluationPackageConditionService.getOne(HEvaluationPackageConditionQueryModel.builder()
                    .categoryCode(packageModel.getCategoryCode())
                    .conditionCode(packageModel.getConditionCode())
                    .orgCode(getOrgCode())
                    .tenantId(0L)
                    .build());
            conditionModel.setId(null);
            conditionModel.setTenantId(getTenantId());
            ihEvaluationPackageConditionService.insert(conditionModel);
        }
        //复制一份
        packageModel.setId(null);
        packageModel.setTenantId(getTenantId());
        packageModel.setCreateTime(new Date());
        packageModel.setCreateUser(getLoginUser().getRealname());
        hEvaluationPackageService.insert(packageModel);
        createCategoryCode(packageModel);
        return RespResult.OK("操作成功");
    }

    private String createCategoryCode(HEvaluationPackageModel packageModel) {
        String categoryCode = StringUtils.get16UUID();
        //判断当前是否有分类 没有这个分类就创建分类
        HEvaluationPackageCategoryModel categoryModel = hEvaluationPackageCategoryService.getOne(HEvaluationPackageCategoryQueryModel.builder()
                .packageType(packageModel.getPackageType())
                .orgCode(getOrgCode())
                .tenantId(getTenantId())
                .build());
        if (ObjectUtils.isNull(categoryModel)) {
            hEvaluationPackageCategoryService.insert(HEvaluationPackageCategoryModel.builder()
                    .packageType(packageModel.getPackageType())
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .createTime(new Date())
                    .categoryCode(packageModel.getCategoryCode())
                    .build());
        }
        return categoryCode;
    }

    /**
     * 一体机配套服务
     * <pre>
     *     取消认购服务
     * </pre>
     *
     * @param formReq
     * @return
     */
    @Transactional
    @PostMapping(value = "/unbind")
    public RespResult unbind(@Validated @RequestBody MachineBindFormReqDTO formReq) {
        HEvaluationPackageModel packageModel = hEvaluationPackageService.getOne(HEvaluationPackageQueryModel.builder()
                .code(formReq.getCode())
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .build());
        if (ObjectUtil.isNull(packageModel)) {
            return RespResult.error("此服务未认购");
        }
        String categoryCode = packageModel.getCategoryCode();
        int count = hEvaluationPackageService.count(HEvaluationPackageQueryModel.builder()
                .categoryCode(categoryCode)
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .build());
        count -= 1;
        if (count == 0) {
            //直接物理删除
            //删除分类与筛选条件
            HEvaluationPackageCategoryModel categoryModel = hEvaluationPackageCategoryService.getOne(HEvaluationPackageCategoryQueryModel.builder()
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .categoryCode(categoryCode)
                    .build());
            if (ObjectUtil.isNotNull(categoryModel)) {
                hEvaluationPackageCategoryService.deleteById(categoryModel.getId());
            }
            List<HEvaluationPackageConditionModel> cList = ihEvaluationPackageConditionService.selectList(HEvaluationPackageConditionQueryModel.builder()
                    .tenantId(getTenantId())
                    .orgCode(getOrgCode())
                    .categoryCode(categoryCode)
                    .conditionCode(packageModel.getConditionCode())
                    .build());
            if (CollectionUtils.isNotEmpty(cList)) {
                List<Long> cIds = cList.stream().map(HEvaluationPackageConditionModel::getId).collect(Collectors.toList());
                ihEvaluationPackageConditionService.deleteByIds(cIds);
            }
        }
        Long id = packageModel.getId();
        hEvaluationPackageService.deleteById(id);
        return RespResult.OK("操作成功");
    }


    /**
     * @param id
     * @return
     */
    @GetMapping(value = "/queryById")
    public RespResult<HStoreMachineGoodsModel> queryById(@RequestParam(name = "id", required = true) Long id) {
        HEvaluationPackageModel machineModel = hEvaluationPackageService.getById(id);
        if (ObjectUtil.isNull(machineModel)) {
            return RespResult.error("未找到对应数据");
        }
        //套餐类型 1套餐方案 2膳食方案 3运动方案
        if ("1".equals(machineModel.getPackageType())) {
            return packagePage(machineModel.getPackageId());
        } else if ("2".equals(machineModel.getPackageType())) {
            return cookbookPage(machineModel.getPackageId());
        } else {
            return sportPage(machineModel.getPackageId());
        }
    }

}
