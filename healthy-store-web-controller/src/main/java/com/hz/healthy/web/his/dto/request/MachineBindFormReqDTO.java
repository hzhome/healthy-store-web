package com.hz.healthy.web.his.dto.request;

import com.hz.ocean.common.dto.PageRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author hyhuang
 * @Date 2024/2/2 21:29
 * @ClassName: MachineQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class MachineBindFormReqDTO extends PageRequest {

    @NotNull(message = "项目信息不能为空")
    private String code ;
}
