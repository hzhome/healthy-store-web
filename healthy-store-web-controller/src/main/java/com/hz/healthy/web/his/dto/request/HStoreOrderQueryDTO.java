package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/12/17 22:51
 * @ClassName: HStoreOrderQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HStoreOrderQueryDTO implements Serializable {

    private Long id;

    /** 内部订单号 */
    private String orderNo;

    /** 第三方支付订单交易号 */
    private String payTrxNo;

    /** 买家留言 */
    private String buyerRemark;

    /** 支付方式 例如 (1现金 ,5余额支付，读取数据字典pay_type) */
    private String payType;

    /** 支付状态  支付中1 支付中2  支付成功3 支付失败4  等待退款5  退款中6  退款成功7  退款失败8   */
    private String payStatus;




    /** 订单来源(10微信商城小程序，20一体机订单) */
    private Integer orderSource;

    /** 会员id */
    private Long memberId;
    private String memberName;

}
