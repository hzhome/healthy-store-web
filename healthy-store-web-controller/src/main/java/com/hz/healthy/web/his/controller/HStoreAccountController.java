package com.hz.healthy.web.his.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.finance.IHHisFinanceIncomeDayService;
import com.hz.healthy.domain.finance.model.HHisFinanceIncomeDayModel;
import com.hz.healthy.domain.finance.model.HHisFinanceIncomeDayQueryModel;
import com.hz.healthy.domain.store.IHStoreAccountService;
import com.hz.healthy.domain.store.model.HStoreAccountModel;
import com.hz.healthy.domain.store.model.HStoreAccountQueryModel;
import com.hz.healthy.web.his.dto.response.HStoreAccountDTO;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 门店账户信息Controller
 *
 * @author hyhuang
 * @date 2024-03-12
 */
@RestController
@RequestMapping("/store/account")
public class HStoreAccountController extends WebBaseController {
    @Autowired
    private IHStoreAccountService hStoreAccountService;

    @Autowired
    private IHHisFinanceIncomeDayService hHisFinanceIncomeDayService;

    private HStoreAccountDTO buildAccount(){
      return HStoreAccountDTO.builder()
                .totalAmount(BigDecimal.ZERO)
                .realityAmount(BigDecimal.ZERO)
                .refundAmount(BigDecimal.ZERO)
                .waitPayAmount(BigDecimal.ZERO)
                .finishPayAmount(BigDecimal.ZERO)
                .build();
    }
    /**
     * 资产概况-昨天
     * 查询门店账户
     */
    @GetMapping("/yesterday")
    public RespResult<HStoreAccountDTO> yesterday() {
        HStoreAccountDTO hStoreAccount = buildAccount();
        String dateStr = DateUtil.formatDate(new Date());
        List<HHisFinanceIncomeDayModel> dataList = hHisFinanceIncomeDayService.selectList(HHisFinanceIncomeDayQueryModel.builder()
                .incomeDay(dateStr)
                .orgCode(getOrgCode())
                .tenantId(getTenantId())
                .build());
        if (CollectionUtils.isNotEmpty(dataList)) {
            HHisFinanceIncomeDayModel incomeDay = dataList.get(0);
            hStoreAccount.setTotalAmount(incomeDay.getTotalAmount());
            hStoreAccount.setWaitPayAmount(incomeDay.getTenantAmount());
            hStoreAccount.setFinishPayAmount(incomeDay.getTotalAmount());
            hStoreAccount.setRefundAmount(incomeDay.getRefundAmount());
            hStoreAccount.setRealityAmount(incomeDay.getTenantAmount());
        }
        return RespResult.OK(hStoreAccount);
    }

    /**
     * 资产概况-今天
     *
     * @return
     */
    @GetMapping("/today")
    public RespResult<HStoreAccountDTO> today() {
        HStoreAccountDTO hStoreAccount = buildAccount();
        String dateStr = DateUtil.formatDate(new Date());
        List<HHisFinanceIncomeDayModel> dataList = hHisFinanceIncomeDayService.selectList(HHisFinanceIncomeDayQueryModel.builder()
                .incomeDay(dateStr)
                .orgCode(getOrgCode())
                .tenantId(getTenantId())
                .build());
        if (CollectionUtils.isNotEmpty(dataList)) {
            HHisFinanceIncomeDayModel incomeDay = dataList.get(0);
            hStoreAccount.setTotalAmount(incomeDay.getTotalAmount());
            hStoreAccount.setWaitPayAmount(incomeDay.getTenantAmount());
            hStoreAccount.setFinishPayAmount(incomeDay.getTenantAmount());
            hStoreAccount.setRefundAmount(incomeDay.getRefundAmount());
            hStoreAccount.setRealityAmount(incomeDay.getTenantAmount());
        }
        return RespResult.OK(hStoreAccount);


    }

    /**
     * 资产概况-全部
     *
     * @return
     */
    @GetMapping("/all")
    public RespResult<HStoreAccountDTO> all() {
        HStoreAccountDTO hStoreAccount = buildAccount();
        List<HStoreAccountModel> dataList = hStoreAccountService.selectList(HStoreAccountQueryModel.builder()
                .orgCode(getOrgCode())
                .tenantId(getTenantId())
                .build());
        if (CollectionUtils.isNotEmpty(dataList)) {
            HStoreAccountModel incomeDay = dataList.get(0);
            hStoreAccount.setTotalAmount(incomeDay.getTotalAmount());
            hStoreAccount.setWaitPayAmount(incomeDay.getWaitTakeAmount());
            hStoreAccount.setFinishPayAmount(incomeDay.getCashTakeAmount());
            hStoreAccount.setRefundAmount(incomeDay.getFreezeAmount());
            hStoreAccount.setRealityAmount(incomeDay.getWaitTakeAmount());
        }
        return RespResult.OK(hStoreAccount);
    }


}
