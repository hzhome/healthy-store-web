package com.hz.healthy.web.goods.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/1/21 16:14
 * @ClassName: HGoodsCategoryFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HGoodsCategoryFormDTO implements Serializable {


    private Long id;

    /** 状态 */
    private String status;

    /** 备注 */
    private String remark;

    /** 分类名称 */
    @NotBlank(message = "分类名称不能为空")
    private String categoryName;

    /** 分类描述 */
    private String description;

    /** 排序 */
    @NotNull(message = "分类排序不能为空")
    private Integer sort;


}
