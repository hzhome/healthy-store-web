package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/11/6 12:08
 * @ClassName: HHisInterviewPlanLogDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisInterviewPlanLogDTO implements Serializable {

    private Integer id;

    /** 内部唯一编码 */
    private String planCode;
    /**
     * 标题
     */
    private String planTitle;

    /** 执行人id */
    private Long eyId;

    /** 执行人 */
    private String eyName;

    /** 签到时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signIn;

    /** 签到图片 */
    private String signInImg;

    /** 签退时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signOut;

    /** 签退图片 */
    private String signOutImg;

    /** 长者ID */
    private Long patientId;

    /** 长者 */
    private String patientName;

    /** 长者电话 */
    private String patientPhone;

    /** 长者性别 */
    private String patientSex;

    /** 长者年龄 */
    private Integer patientAge;

    /** 服务时长(分数) */
    private Integer duration;

    /** 走访记录事件 */
    private String recordEvent;

    /** 走访记录内容 */
    private String recordContent;

    /** 所属医院单位组织 */
    private String orgCode;

    /** 租户id */
    private Long tenantId;

    /** 更新人员 */
    private String updateUser;

    /** 最近一次更新时间  */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 创建人员  */
    private String createUser;

    /**  注册时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 是否已经删除，0，否；1，已删除 */
    private Integer isDeleted;

    /** 1走访 2去电 3事件提示 */
    private String planType;

    /** 经度 定位 */
    private BigDecimal positionLng;

    /** 纬度 定位 */
    private BigDecimal positionLat;

    //----------- 来自 transient 修饰列
    //----------- 来自 transient 修饰列 End

    /**
     * 本次事件 解决方案
     */
    private String solution;

    /**
     * 精神状态
     */
    private String mentality;


    /**
     * 上次执行解决方案后，反馈如何
     */
    private String lastRecord;




    /** 运动情况(分钟/次) */
    private String sportsSituation;
    /** 每周运动情况(次/周) */
    private String weekSportsSituation;
    /** 摄盐情况 1轻 2中 3重 */
    private String saltSituation;
    /** 心理调整 1良好 2一般 3差 */
    private String psychologicalAdjust;
    /** 遵医行为 1良好 2一般 3差 */
    private String complianceBehavior;
    /** 服药依从性 1规律 2间断 3不服从 */
    private String medicationCompliance;
    /** 此次随访分类 1控制满意  2控制不满意 3不良反应 3并发症 */
    private String followUpSituation;
    /** 用药情况 */
    private String medicationSituation;
    /** 医生指导 */
    private String doctorGuidance;
    /** 图片 */
    private String img;
}
