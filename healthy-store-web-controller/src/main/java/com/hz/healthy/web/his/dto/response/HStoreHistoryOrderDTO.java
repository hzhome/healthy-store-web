package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/12/17 18:06
 * @ClassName: HStoreHistoryOrderDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HStoreHistoryOrderDTO implements Serializable {

    private Long id;

    /** 内部订单号 */
    private String orderNo;

    /** 第三方支付订单交易号 */
    private String payTrxNo;

    /** 买家留言 */
    private String buyerRemark;

    /** 支付方式 例如 (1现金 ,5余额支付，读取数据字典pay_type) */
    private String payType;

    /** 支付状态  支付中1 支付中2  支付成功3 支付失败4  等待退款5  退款中6  退款成功7  退款失败8   */
    private String payStatus;

    /** 订单状态 0待支付 1等商家接单 2商家已接单  3进行中  4已完成 5已退单 */
    private String orderStatus;

    /** 付款完成时间 */
    private Date payFinishTime;

    /** 支付截止时间 */
    private Date payEndTime;

    /** 订单类型 1一体机订单   3上门护理 4上门照护 5陪诊服务 6慢性康复 7营养指导   */
    private Integer orderType;

    private String orderTypeText;

    /** 配送来源 0不需要配送 1商家配送 2第三方配送 */
    private Integer deliveryType;

    /** 配送状态(1未发货 2已发货) */
    private Integer deliveryStatus;

    /** 配送开始时间 */
    private Date deliveryTime;

    /** 客户收货状态(1未收货 2已收货) */
    private Integer receiptStatus;


    /** 取单号 */
    private String callNo;

    /** 运费金额 */
    private BigDecimal expressPrice;

    /** 包装费 */
    private BigDecimal bagPrice;

    /** 余额抵扣金额 */
    private BigDecimal balance;

    /** 商品总金额 */
    private BigDecimal goodsPrice;

    /** 订单总金额 */
    private BigDecimal originalPrice;

    /** 实际付款金额(包含运费)  */
    private BigDecimal consumeAmount;

    /** 商品数量 */
    private Integer qty;

    /** 订单时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date orderTime;


    private String orderDay;

}
