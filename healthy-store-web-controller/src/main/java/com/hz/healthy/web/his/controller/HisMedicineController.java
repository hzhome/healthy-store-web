package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.read.listener.PageReadListener;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHisFactoryService;
import com.hz.healthy.domain.his.IHisMedicineService;
import com.hz.healthy.domain.his.model.HisMedicineModel;
import com.hz.healthy.domain.his.model.HisMedicineQueryModel;
import com.hz.healthy.domain.his.model.HisMedicineSearchModel;
import com.hz.healthy.domain.his.model.HisMedicineSimpleModel;
import com.hz.healthy.enums.MedicineTypeEnums;
import com.hz.healthy.web.his.dto.response.medicine.HisMedicineDTO;
import com.hz.healthy.web.his.dto.response.medicine.HisMedicineFormDTO;
import com.hz.healthy.web.his.dto.response.medicine.HisMedicinePurchasDTO;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.ObjectUtils;
import com.hz.ocean.system.service.auth.model.DuplicateCheckQueryModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 药品Controller
 *
 * @author hyhuang
 * @date 2023-03-27
 */
@RestController
@RequestMapping("/his/warehousing/medicine")
public class HisMedicineController extends WebBaseController {
    @Autowired
    private IHisMedicineService hisMedicineService;

    @Autowired
    private IHisFactoryService hisFactoryService;

    /**
     * 查询药品列表
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<ListResponse<HisMedicineDTO>> pageQuery(HisMedicineQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        queryModel.setPageSize(50);
        ListResponse<HisMedicineModel> pageList = hisMedicineService.pageQuery(queryModel);
        ListResponse<HisMedicineDTO> listResponse = ListResponse.build(pageList, HisMedicineDTO.class);
        if (CollectionUtil.isNotEmpty(pageList.getRecords())) {
            List<Long> fIdList = pageList.getRecords().stream().map(HisMedicineModel::getFactoryId).collect(Collectors.toList());
            Map<Long, String> mapFactory = hisFactoryService.mapNameByIds(fIdList);
            listResponse.getRecords().forEach(r -> {
                r.setFactoryName(mapFactory.getOrDefault(r.getFactoryId(), "---"));
            });
        }
        return RespResult.OK(listResponse);
    }


    /**
     * 获取药品详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hisMedicineService.getById(id));
    }

    /**
     * 新增药品
     */
    @PostMapping
    public RespResult add(@Validated @RequestBody HisMedicineFormDTO form) {
        int num = hisMedicineService.count(HisMedicineQueryModel.builder().medicineNo(form.getMedicineNo()).orgCode(getOrgCode()).build());
        if (num > 0) {
            return RespResult.error("药品编码已存在重复,请确认后再添加操作");
        }

        HisMedicineModel hisMedicineModel = BeanUtil.copyProperties(form, HisMedicineModel.class);
        hisMedicineModel.setCreateTime(new Date());
        hisMedicineModel.setOrgCode(getLoginUser().getOrgCode());
        hisMedicineModel.setCreateUser(getLoginUser().getRealname());
        hisMedicineModel.setCreateTime(new Date());
        hisMedicineService.insert(hisMedicineModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改药品
     */
    @PutMapping
    public RespResult edit(@RequestBody HisMedicineFormDTO form) {
        HisMedicineModel hisMedicineModel = BeanUtil.copyProperties(form, HisMedicineModel.class);
        hisMedicineModel.setUpdateTime(new Date());
        hisMedicineModel.setUpdateUser(getLoginUser().getRealname());
        hisMedicineService.updateById(hisMedicineModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除药品
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hisMedicineService.deleteByIds(ids);
        return RespResult.OK();
    }

    /**
     * 删除药品
     */
    @DeleteMapping("/delete/{id}")
    public RespResult remove(@PathVariable Long id) {
        hisMedicineService.deleteById(id);
        return RespResult.OK("删除成功");
    }

    /**
     * 新增药品信息，提供采购使用
     * 查询药品供应商列表
     */
    @GetMapping("/purchas/list")
    public RespResult<List<HisMedicinePurchasDTO>> list(@RequestParam(value = "ids", required = false) String ids) {
        List<HisMedicinePurchasDTO> result = new ArrayList<>();
        if (StringUtils.isBlank(ids)) {
            return RespResult.OK(result);
        }
        List<Long> idList = com.hz.ocean.common.utils.StringUtils.splitToLongList(ids, ',');
        List<HisMedicineModel> pageList = hisMedicineService.listById(idList);
        if (CollectionUtil.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                HisMedicinePurchasDTO purchas = BeanUtil.copyProperties(p, HisMedicinePurchasDTO.class);
                purchas.setPrice(BigDecimal.ZERO);
                purchas.setStock(0);
                purchas.setTotalAmount(BigDecimal.ZERO);
                purchas.setBatchNo("");
                purchas.setRemark("");
                result.add(purchas);
            });
        }
        return RespResult.OK(result);
    }


    /**
     * 查询药品列表
     */
    @PostMapping("/search")
    public RespResult<List<HisMedicineSimpleModel>> seach(@RequestBody HisMedicineQueryModel queryModel) {
        HisMedicineSearchModel searchModel=HisMedicineSearchModel.builder()
                .orgCode(getOrgCode())
                .build();
        if(MedicineTypeEnums.WESTERN_MEDICINE.getCode().equals(queryModel.getMedicineType())){
            searchModel.setMedicineTypeList(Lists.newArrayList(
                    MedicineTypeEnums.CHINESE_PATENT_DRUG_MEDICINE.getCode(),
                    MedicineTypeEnums.WESTERN_MEDICINE.getCode()));
        }else{
            searchModel.setMedicineTypeList(Lists.newArrayList(
                    MedicineTypeEnums.CHINESE_HERBAL_MEDICINE.getCode(),
                    MedicineTypeEnums.CHINESE_GRAIN_MEDICINE.getCode(),
                    MedicineTypeEnums.WESTERN_MEDICINE.getCode()
            ));
        }
        searchModel.setMedicineName(queryModel.getMedicineName());
        List<HisMedicineSimpleModel> pageList = hisMedicineService.search(searchModel);
        return RespResult.OK(pageList);
    }


    @GetMapping("/page/search")
    public RespResult<ListResponse<HisMedicineSimpleModel>> pageQuerySearch(HisMedicineQueryModel queryModel) {
        ListResponse<HisMedicineModel> pageList = hisMedicineService.pageQuery(queryModel);
        return RespResult.OK(ListResponse.build(pageList, HisMedicineSimpleModel.class));
    }


    @GetMapping(value = "/get")
    public RespResult<HisMedicineModel> get(@ModelAttribute HisMedicineQueryModel queryModel) {
        HisMedicineModel hisMedicineModel = hisMedicineService.getOne(queryModel);
        return RespResult.OK(hisMedicineModel);
    }


    /**
     * 校验数据是否在系统中是否存在
     *
     * @return
     */
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public RespResult<String> doDuplicateCheck(DuplicateCheckQueryModel duplicateCheckVo, HttpServletRequest request) {
        Integer num = hisMedicineService.count(HisMedicineQueryModel.builder().medicineNo(duplicateCheckVo.getFieldVal()).orgCode(getOrgCode()).build());
        if (num == null || num == 0) {
            // 该值可用
            return RespResult.OK("该值可用！");
        } else {
            // 该值不可用
            return RespResult.error("该值不可用，系统中已存在！");
        }
    }


}
