package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHHisDepartmentService;
import com.hz.healthy.domain.doctor.model.HHisDepartmentModel;
import com.hz.healthy.domain.doctor.model.HHisDepartmentQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.service.auth.model.SysDepartTreeModel;
import com.hz.ocean.uac.model.LoginUser;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 科室Controller
 *
 * @author hyhuang
 * @date 2023-01-17
 */
@RestController
@RequestMapping("/his/department")
public class HHisDepartmentController extends WebBaseController {
    @Autowired
    private IHHisDepartmentService hHisDepartmentService;

    /**
     * 查询科室列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisDepartmentModel>> pageQuery(HHisDepartmentQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        RespResult<ListResponse<HHisDepartmentModel>> result = new RespResult<ListResponse<HHisDepartmentModel>>();
        ListResponse<HHisDepartmentModel> pageList = hHisDepartmentService.pageQuery(queryModel);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }


    /**
     * 获取科室详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hHisDepartmentService.getByDid(id));
    }



    /**
     * 删除科室
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        hHisDepartmentService.deleteByDid(id);
        return RespResult.OK("删除成功");
    }


    /**
     * 查询科室列表
     */
    @GetMapping("/queryMyDeptTreeList")
    public RespResult<List<SysDepartTreeModel>> queryMyDeptTreeList(HHisDepartmentQueryModel queryModel) {
        LoginUser loginUser = getLoginUser();
        queryModel.setOrgCode(loginUser.getOrgCode());
        List<HHisDepartmentModel> pageList = hHisDepartmentService.selectList(queryModel);
        List<SysDepartTreeModel> dataList=new ArrayList<>();
        if(CollectionUtils.isNotEmpty(pageList)){

            for(HHisDepartmentModel d:pageList){
                SysDepartTreeModel t=new SysDepartTreeModel();
                t.setKey(String.valueOf(d.getDid()));
                t.setValue(String.valueOf(d.getDid()));
                t.setTitle(d.getDepartmentName());
                t.setDepartName(d.getDepartmentName());
                t.setId(String.valueOf(d.getDid()));
                dataList.add(t);
            }
        }
        return RespResult.OK(dataList);
    }


}
