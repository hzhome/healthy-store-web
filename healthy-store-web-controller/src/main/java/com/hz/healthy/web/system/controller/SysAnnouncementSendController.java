package com.hz.healthy.web.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.hz.healthy.base.WebBaseController;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.SqlInjectionUtil;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.service.anountcement.ISysAnnouncementSendService;
import com.hz.ocean.system.service.anountcement.model.SysAnnouncementSendModel;
import com.hz.ocean.system.service.anountcement.model.SysAnnouncementSendQueryModel;
import com.hz.ocean.uac.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @Title: Controller
 * @Description: 用户通告阅读标记表
 * @author hyhuang
 * @date 2022-03-27 8:03 PM
*/
@RestController
@RequestMapping("/sys/sysAnnouncementSend")
@Slf4j
public class SysAnnouncementSendController extends WebBaseController {
   @Autowired
   private ISysAnnouncementSendService sysAnnouncementSendService;


   /**
     * 分页列表查询
    * @param query
    * @param req
    * @return
    */
   @GetMapping(value = "/list")
   public RespResult<ListResponse<SysAnnouncementSendModel>> queryPageList(SysAnnouncementSendModel query,
                                                                           HttpServletRequest req) {
       RespResult<ListResponse<SysAnnouncementSendModel>> result = new RespResult<ListResponse<SysAnnouncementSendModel>>();
         //排序逻辑 处理
       String column = req.getParameter("column");
       String order = req.getParameter("order");

       //issues/3331 SQL injection vulnerability
       SqlInjectionUtil.filterContent(column);
       SqlInjectionUtil.filterContent(order);

       if(oConvertUtils.isNotEmpty(column) && oConvertUtils.isNotEmpty(order)) {
//           if("asc".equals(order)) {
//               queryWrapper.orderByAsc(oConvertUtils.camelToUnderline(column));
//           }else {
//               queryWrapper.orderByDesc(oConvertUtils.camelToUnderline(column));
//           }
       }
       ListResponse<SysAnnouncementSendModel> pageList = sysAnnouncementSendService.pageQuery(query);
       //log.info("查询当前页："+pageList.getCurrent());
       //log.info("查询当前页数量："+pageList.getSize());
       //log.info("查询结果数量："+pageList.getRecords().size());
       //log.info("数据总数："+pageList.getTotal());
       result.setSuccess(true);
       result.setResult(pageList);
       return result;
   }

   /**
     *   添加
    * @param sysAnnouncementSend
    * @return
    */
   @PostMapping(value = "/add")
   public RespResult<SysAnnouncementSendModel> add(@RequestBody SysAnnouncementSendModel sysAnnouncementSend) {
       RespResult<SysAnnouncementSendModel> result = new RespResult<SysAnnouncementSendModel>();
       try {
           sysAnnouncementSendService.save(sysAnnouncementSend);
           result.success("添加成功！");
       } catch (Exception e) {
           log.error(e.getMessage(),e);
           return RespResult.error("操作失败");
       }
       return result;
   }

   /**
     *  编辑
    * @param sysAnnouncementSend
    * @return
    */
   @PutMapping(value = "/edit")
   public RespResult<SysAnnouncementSendModel> eidt(@RequestBody SysAnnouncementSendModel sysAnnouncementSend) {
       RespResult<SysAnnouncementSendModel> result = new RespResult<SysAnnouncementSendModel>();
       SysAnnouncementSendModel sysAnnouncementSendEntity = sysAnnouncementSendService.getById(sysAnnouncementSend.getId());
       if(sysAnnouncementSendEntity==null) {
           return RespResult.error("未找到对应实体");
       }else {
           sysAnnouncementSendService.updateById(sysAnnouncementSend);
           //TODO 返回false说明什么？
           result.success("修改成功!");
       }

       return result;
   }

   /**
     *   通过id删除
    * @param id
    * @return
    */
   @DeleteMapping(value = "/delete")
   public RespResult<SysAnnouncementSendModel> delete(@RequestParam(name="id",required=true) Long id) {
       RespResult<SysAnnouncementSendModel> result = new RespResult<SysAnnouncementSendModel>();
       SysAnnouncementSendModel sysAnnouncementSend = sysAnnouncementSendService.getById(id);
       if(sysAnnouncementSend==null) {
           return RespResult.error("未找到对应实体");
       }else {
           sysAnnouncementSendService.removeById(id);
           result.success("删除成功!");
       }

       return result;
   }

   /**
     *  批量删除
    * @param ids
    * @return
    */
   @DeleteMapping(value = "/deleteBatch")
   public RespResult<SysAnnouncementSendModel> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       RespResult<SysAnnouncementSendModel> result = new RespResult<SysAnnouncementSendModel>();
       if(ids==null || "".equals(ids.trim())) {
           return RespResult.error("参数不识别！");
       }else {
           List<Long> delIds=new ArrayList<>();
           List<String> idList=Arrays.asList(ids.split(","));

           for (String id:idList){
               delIds.add(Long.parseLong(id));
           }
           this.sysAnnouncementSendService.removeByIds(delIds);
           result.success("删除成功!");
       }
       return result;
   }

   /**
     * 通过id查询
    * @param id
    * @return
    */
   @GetMapping(value = "/queryById")
   public RespResult<SysAnnouncementSendModel> queryById(@RequestParam(name="id",required=true) Long id) {
       RespResult<SysAnnouncementSendModel> result = new RespResult<SysAnnouncementSendModel>();
       SysAnnouncementSendModel sysAnnouncementSend = sysAnnouncementSendService.getById(id);
       if(sysAnnouncementSend==null) {
           return RespResult.error("未找到对应实体");
       }else {
           result.setResult(sysAnnouncementSend);
           result.setSuccess(true);
       }
       return result;
   }

   /**
    * @功能：更新用户系统消息阅读状态
    * @param json
    * @return
    */
   @PutMapping(value = "/editByAnntIdAndUserId")
   public RespResult<SysAnnouncementSendModel> editById(@RequestBody JSONObject json) {
       RespResult<SysAnnouncementSendModel> result = new RespResult<SysAnnouncementSendModel>();
       Long anntId = json.getLong("anntId");
       LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
       String userId = sysUser.getId();
       sysAnnouncementSendService.updateReadStatus(anntId, userId);
       result.setSuccess(true);
       return result;
   }

   /**
    * @功能：获取我的消息
    * @return
    */
   @GetMapping(value = "/getMyAnnouncementSend")
   public RespResult<ListResponse<SysAnnouncementSendQueryModel>> getMyAnnouncementSend(SysAnnouncementSendQueryModel announcementSendModel)
   {
       RespResult<ListResponse<SysAnnouncementSendQueryModel>> result = new RespResult<ListResponse<SysAnnouncementSendQueryModel>>();
       LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
       String userId = sysUser.getId();
       announcementSendModel.setUserId(userId);
       ListResponse<SysAnnouncementSendQueryModel> pageList= sysAnnouncementSendService.getMyAnnouncementSendPage(announcementSendModel);
       result.setResult(pageList);
       result.setSuccess(true);
       return result;
   }

   /**
    * @功能：一键已读
    * @return
    */
   @PutMapping(value = "/readAll")
   public RespResult<SysAnnouncementSendModel> readAll() {
       RespResult<SysAnnouncementSendModel> result = new RespResult<SysAnnouncementSendModel>();
       LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
       String userId = sysUser.getId();
       sysAnnouncementSendService.updateReadStatus(null,userId);
       result.setSuccess(true);
       result.setMessage("全部已读");
       return result;
   }
}
