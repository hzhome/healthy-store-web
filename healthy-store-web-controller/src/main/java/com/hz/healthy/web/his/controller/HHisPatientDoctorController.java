package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.patient.IHHisPatientDoctorService;
import com.hz.healthy.domain.patient.IHHisPatientService;
import com.hz.healthy.domain.patient.model.HHisPatientDoctorModel;
import com.hz.healthy.domain.patient.model.HHisPatientDoctorQueryModel;
import com.hz.healthy.domain.store.IHHospitalStoreService;
import com.hz.healthy.web.his.dto.request.HHisPatientDoctorBindFormDTO;
import com.hz.healthy.web.his.dto.request.HHisPatientDoctorQueryDTO;
import com.hz.healthy.web.his.dto.response.HHisPatientDoctorDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 签约医生
 *
 * @author hyhuang
 * @date 2023-10-06
 */
@RestController
@RequestMapping("/ch/patient/doctor")
public class HHisPatientDoctorController extends WebBaseController {
    @Autowired
    private IHHisPatientDoctorService hisPatientDoctorService;

    @Autowired
    private IHEmployeeService hEmployeeService;

    @Autowired
    private IHHospitalStoreService hospitalStoreService;

    @Autowired
    private IHHisPatientService hisPatientService;

    /**
     * 查询患者社区档案列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisPatientDoctorDTO>> pageQuery(HHisPatientDoctorQueryDTO queryDto) {
        HHisPatientDoctorQueryModel queryModel = new HHisPatientDoctorQueryModel();
        queryModel.setPageNo(queryDto.getPageNo());
        queryModel.setPageSize(queryDto.getPageSize());
        queryModel.setOrgCode(getOrgCode());
        queryModel.setPatientId(queryDto.getMemberId());

        ListResponse<HHisPatientDoctorDTO> pageList = ListResponse.build(hisPatientDoctorService.pageQuery(queryModel), HHisPatientDoctorDTO.class);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<Long> doctorIds = pageList.getRecords().stream().map(HHisPatientDoctorDTO::getDoctorId).collect(Collectors.toList());
            Map<Long, String> doctorMap = hEmployeeService.mapNameByIds(doctorIds);
            pageList.getRecords().forEach(p -> {
                p.setDoctorName(doctorMap.getOrDefault(p.getDoctorId(), "---"));
            });
        }
        return RespResult.OK(pageList);
    }


    /**
     * 获取患者社区档案详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hisPatientDoctorService.getById(id));
    }


    /**
     * 绑定关系
     */
    @PostMapping
    public RespResult bind(@Validated @RequestBody HHisPatientDoctorBindFormDTO bindForm) {
        Long patientId = 0L;// hisPatientService.getIdByMemberId(bindForm.getMemberId());
        int count = hisPatientDoctorService.count(HHisPatientDoctorQueryModel.builder()
                .patientId(patientId)
                .doctorId(bindForm.getDoctorId())
                .build());
        if (count > 0) {
            return RespResult.error("不能重复绑定");
        }
        HHisPatientDoctorModel patientDoctorModel = new HHisPatientDoctorModel();
        ;
        patientDoctorModel.setDoctorId(bindForm.getDoctorId());
        patientDoctorModel.setPatientId(patientId);
        patientDoctorModel.setCreateTime(new Date());
        patientDoctorModel.setCreateUser(getLoginUser().getRealname());
        patientDoctorModel.setOrgCode(getOrgCode());
        patientDoctorModel.setTenantId(getTenantId());
        patientDoctorModel.setIzNew(CommonConstant.yes_1);
        hisPatientDoctorService.insert(patientDoctorModel);
        return RespResult.OK("添加成功");
    }


    /**
     * 解除绑定
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        hisPatientDoctorService.deleteById(id);
        return RespResult.OK("解除绑定成功");
    }
}
