package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author hyhuang
 * @desc
 * @date 2023/7/18 16:42
 */
@Data
public class HHisActivityDTO implements Serializable {


    private Long id;

    /** 内部唯一编码 */
    private String activityCode;
    private String activityNo;

    /** 活动主题 */
    private String topic;

    /** 活动名称 */
    private String name;

    /** 活动地点 */
    private String place;

    /** 活动组织者 */
    private String organizer;
    private String organizerRole;
    private Long organizerId;
    private String activityAttr;

    /** 联系电话 */
    private String phone;

    /** 活动日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date activityDate;

    private String createUser;

}
