package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/4/9 20:56
 * @ClassName: HHisRegistrationDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisRegistrationDTO implements Serializable {

    /** 主键 */
    private Long id;

    /** 1挂号 2出诊 */
    private String appointmentType;

    /** 患者ID(成员) */
    private Long patientId;
    /** 患者名称(成员) */
    private String patientName;
    /**
     * 长者年纪
     */
    private Integer patientAge;

    /**
     * 长者性别，是中文来的
     */
    private String patientSex;
    /** 医生名称 */
    private String doctorName;
    /** 医生主键 */
    private Long doctorId;

    /** 操作员ID */
    private Long operatorId;


    /** 科室ID */
    private Long departmentId;

    /** 挂号费用ID */
    private Long registeredFeeId;

    /** 挂号总金额 */
    private BigDecimal registrationAmount;

    /** 挂号编号 */
    private Long registrationNumber;

    /** 挂号状态,1为待就诊，3为已退号，2为已就诊,4为作废，5,为未付款,6，为部分支付 */
    @Dict(dicCode = "registration_status")
    private String registrationStatus;

    /** 排班主表ID */
    private Long schedulingId;


    /** 排班星期表ID */
    private Integer schedulingWeek;


    private Long schedulingTimeId;


    /** 商品的添加时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /**
     * 预约日期
     */
    private String schedulingDate;

    /**
     * 预约时间段
     */
    private String schedulingEndTime;
    private String schedulingBeginTime;


    private String sureStatus;
    private String sureMsg;
    /**
     * 评分
     * 五星评
     */
    private Integer score;
    /**
     * 评分内容
     */
    private String scoreMsg;



    /**
     * h_his_diagnosis_record
     * 诊断 精神状态
     */
    private String healthyStatus;


    /**
     * h_his_diagnosis_record
     * 诊断  建议
     * */
    private String suggest;


    private String  birthday;


    /** 排班星期表ID */
    private String subsectionName;

    private int sort;

    private String patientPhone;

    /**
     * 1初诊 2复诊
     */
    private String registrationType;
}
