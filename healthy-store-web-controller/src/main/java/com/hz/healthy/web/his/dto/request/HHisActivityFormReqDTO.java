package com.hz.healthy.web.his.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author hyhuang
 * @desc
 * @date 2023/7/18 16:42
 */
@Data
public class HHisActivityFormReqDTO implements Serializable {


    private Long id;


    /** 活动主题 */
    @NotBlank(message = "请输入活动主题")
    private String topic;

    @NotBlank(message = "请输入活动档号")
    private String activityNo;

    /** 活动名称 */
    @NotBlank(message = "请输入活动名称")
    private String name;

    /** 活动地点 */
    @NotBlank(message = "请输入活动地点")
    private String place;

    /** 活动组织者 */
   // @NotBlank(message = "请选择活动组织者")
    private String organizer;

    @NotNull(message = "请选择活动类型")
    private Long activityType;


    private Long organizerId;

    /** 联系电话 */
    @NotBlank(message = "请选择活动组织者联系电话")
    private String phone;

    /** 活动日期 */
    @NotNull(message = "请选择活动日期")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    private Date activityDate;

    private String activityAttr;

    /**
     * 参与活动的长者
     */
    private String patientIds;


    private Long doctorId;
    private Long nurseId;
    private Long carerId;
    private Long socialWorkerId;

    private String activityObj;
    private Integer enrollCount;
    /**
     * 活动背景
     */
    private String background;
    /**
     * 活动背景
     */
    private String target;

    /**
     * 分目标
     */
    private String targetSub;

    /**
     * 分目标
     */
    private String idea;


    private String dataSource1;
    private String dataSource2;
    private String dataSource3;

}
