package com.hz.healthy.web.system.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.hz.healthy.base.WebBaseController;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.file.IHzSysFileEntityService;
import com.hz.ocean.system.service.file.model.HzSysFileEntityModel;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import com.hz.ocean.web.core.utils.file.FileUploadHelper;
import com.hz.ocean.web.core.utils.minio.MinioUtils;
import com.hz.ocean.web.core.utils.oss.OssBootUtil;
import com.hz.ocean.web.core.utils.oss.OssUploadResult;
import io.minio.ObjectWriteResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/sys/common")
@Slf4j
public class CommonController extends WebBaseController {


    @Autowired
    private IHzSysFileEntityService hzSysFileEntityService;
    @Autowired
    private OceanBaseConfig oceanBaseConfig;


    private String getUploadType() {
        if (StringUtils.isBlank(oceanBaseConfig.getUploadType())) {
            return "local";
        }
        return oceanBaseConfig.getUploadType();
    }

    /**
     * 预览图片&下载文件
     * 请求地址：http://localhost:8080/common/static/{user/20190119/e1fe9925bc315c60addea1b98eb1cb1349547719_1547866868179.jpg}
     *
     * @param response
     */
    private void oss(@RequestParam(value = "key") String key, HttpServletResponse response) {
        // 其余处理略
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = OssBootUtil.getOssFile(key);


            response.setContentType("application/force-download");// 设置强制下载不打开
            response.addHeader("Content-Disposition", "attachment;fileName=" + key);
            outputStream = response.getOutputStream();
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, len);
            }
            response.flushBuffer();
        } catch (IOException e) {
            log.error("预览文件失败" + e.getMessage());
            response.setStatus(404);
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
    }

    public boolean checkFileSize(Long len,int size,String unit){
        if (null == len || len.longValue() <=0L || size <=0 || StringUtils.isEmpty(unit)){
            return true;
        }

        double file_size = 0;
        if ("B".equalsIgnoreCase(unit)){
            file_size = len.doubleValue();
        } else if ("K".equalsIgnoreCase(unit)){
            file_size = len.doubleValue() / 1024;
        } else if ("M".equalsIgnoreCase(unit)){
            file_size = len.doubleValue() / 1048576;
        } else if ("G".equalsIgnoreCase(unit)){
            file_size = len.doubleValue() / 1073741824;
        } else {
            return true;
        }

        if (file_size > size) {
            return true;
        }
        return false;
    }

    /**
     * 预览图片&下载文件
     * 请求地址：http://localhost:8080/common/static/{user/20190119/e1fe9925bc315c60addea1b98eb1cb1349547719_1547866868179.jpg}
     *
     * @param request
     * @param response
     */
    @GetMapping(value = "/static/**")
    public void view(HttpServletRequest request, HttpServletResponse response) {
        // log.info("预览图片&下载文件 > url:{}",RequestUtils.getHttpUrl(request));

        // ISO-8859-1 ==> UTF-8 进行编码转换
        String imgPath = extractPathFromPattern(request);
        if (StringUtils.isEmpty(imgPath) || imgPath == "null") {
            return;
        }
        if (imgPath.contains("MINIO/")) {
            MinioUtils.download("test", imgPath.replace("MINIO/", ""), response);
        } else if (imgPath.contains("OSS/")) {
            oss(imgPath.replace("OSS/", ""), response);
        } else {
            // 其余处理略
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                imgPath = imgPath.replace("..", "");
                if (imgPath.endsWith(",")) {
                    imgPath = imgPath.substring(0, imgPath.length() - 1);
                }
                String filePath = oceanBaseConfig.getPath().getUpload() + File.separator + imgPath;
                log.info("下载文件位置：{}", filePath);
                File file = new File(filePath);
                if (!file.exists()) {
                    response.setStatus(404);
                    throw new RuntimeException("文件不存在..");
                }

                response.setContentType("application/force-download");// 设置强制下载不打开
                response.addHeader("Content-Disposition", "attachment;fileName=" + new String(file.getName().getBytes("UTF-8"), "iso-8859-1"));
                inputStream = new BufferedInputStream(new FileInputStream(filePath));
                outputStream = response.getOutputStream();
                byte[] buf = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0) {
                    outputStream.write(buf, 0, len);
                }
                response.flushBuffer();
            } catch (IOException e) {
                log.error("预览文件失败" + e.getMessage());
                response.setStatus(404);
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        log.error(e.getMessage(), e);
                    }
                }
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        log.error(e.getMessage(), e);
                    }
                }
            }
        }


    }


    @PostMapping(value = "/upload")
    public RespResult<String> upload(HttpServletRequest request, HttpServletResponse response) throws IOException {
        return commonUpload(request);
    }

    private HzSysFileEntityModel uploadMinio(MultipartFile file, String newFileName, final String extension) {
        try {
            ObjectWriteResponse response = MinioUtils.putObject("test", file, newFileName, "application/octet-stream");
            String md5 = UUID.randomUUID().toString();
            HzSysFileEntityModel fileEntityModel = new HzSysFileEntityModel();
            fileEntityModel.setFilePath(newFileName);
            fileEntityModel.setFileType("MINIO");
            fileEntityModel.setFileKey(response.etag());
            fileEntityModel.setFileMd5(md5);
            fileEntityModel.setFileExtension(extension);
            fileEntityModel.setFileSize(10L);
            fileEntityModel.setFileMeta("application/octet-stream");
            //hzSysFileEntityService.insert(fileEntityModel);
            return fileEntityModel;
        } catch (Exception e) {
            throw new RuntimeException("上传失败");
        }
    }

    private HzSysFileEntityModel uploadOss(MultipartFile file, String biz) {
        OssUploadResult uploadResult = null;
        if ("protect".equals(biz)) {
            uploadResult = OssBootUtil.upload(file, "01", "hprotect");
        } else {
            uploadResult = OssBootUtil.upload(file, "01");
        }
        String md5 = UUID.randomUUID().toString();
        HzSysFileEntityModel fileEntityModel = new HzSysFileEntityModel();
        fileEntityModel.setFilePath(uploadResult.getFileUrl());
        fileEntityModel.setFileType("OSS");
        fileEntityModel.setFileKey(uploadResult.getKey());
        fileEntityModel.setFileMd5(md5);
        fileEntityModel.setFileExtension(".jpg");
        fileEntityModel.setFileSize(10L);
        fileEntityModel.setFileMeta("image");
        hzSysFileEntityService.insert(fileEntityModel);
        return fileEntityModel;
    }

    private String uploadLocal(MultipartFile file, String biz, String newFileName, String extension) throws Exception {
        Date date = new Date();
        String dateStr = DateUtil.format(date, "yyMMdd");

        //生成年月
        String rootPath = "";//文件绝对路径根目录
        if ("apk".equals(biz)) {
            rootPath = getBaseConfig().getPath().getApkPath();
        } else {
            rootPath = getBaseConfig().getPath().getUpload();
        }
        String dir = rootPath +  File.separator +dateStr;
        if(!FileUtil.isDirectory(dir)){
            FileUtil.mkdir(dir);
        }
        String filePath=dir+File.separator + newFileName;//文件相对路径
        BufferedOutputStream outputStream = null;
        System.out.println("filePath="+filePath);
        //创建文件夹，如果存在忽略
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
            outputStream.write(file.getBytes());
        } catch (Exception e) {
            throw e;
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    log.error("upload IOException", e);
                }
            }
        }
        return File.separator +dateStr+File.separator+newFileName;
    }


    private RespResult<String> commonUpload(HttpServletRequest request) throws IOException {
        /**
         * biz：业务类型
         * 包含：img-图片、apk-安卓apk、pay-支付证书
         * 默认：图片
         */
        final String biz = StringUtils.stringValue(request.getParameter("biz"), "img");
        if (!FileUploadHelper.isBiz(biz)) {
            return RespResult.error("文件格式非法");
        }
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        MultipartFile file = multipartRequest.getFile("file");// 获取上传文件对象
        //源文件名称
        final String originalFilename = file.getOriginalFilename();
        //新文件名称
        final String newFileName = FileUploadHelper.getGeneratorFileName(originalFilename);
        //后缀
        final String extension = (FileUploadHelper.getExtension(originalFilename)).trim();

        Map<String, String> extensionMap = new HashMap<>();
        Map<String, String> imgMap = new HashMap<>();
        imgMap.put("jpg", "jpg");
        imgMap.put("png", "png");
        imgMap.put("gif", "gif");
        imgMap.put("bmp", "bmp");
        imgMap.put("jpeg", "jpeg");
        imgMap.put("svg", "svg");
        imgMap.put("pdf", "pdf");
        imgMap.put("zip", "zip");
        imgMap.put("doc", "doc");
        imgMap.put("docx", "doc");

        extensionMap.putAll(imgMap);
        extensionMap.put("p12", "p12");
        extensionMap.put("apk", "apk");

        if (!extensionMap.containsKey(extension.toLowerCase())) {
            return RespResult.error("文件格式非法");
        }

        String uploadType = getUploadType();

        try {
            String url = "";
            if ("local".equals(uploadType)) {
                url = uploadLocal(file, biz, newFileName, extension);
            } else if ("mino".equals(uploadType)) {
                HzSysFileEntityModel fileEntityModel = uploadMinio(file, newFileName, extension);
                url = oceanBaseConfig.getResource().getUpfsUrl() + "/" + fileEntityModel.getFileType() + "/" + fileEntityModel.getFilePath();
            }
            return RespResult.OK(url);
        } catch (Exception e) {
            log.error("upload Exception", e);
        }
        return RespResult.error("上传失败");
    }


    /**
     * 把指定URL后的字符串全部截断当成参数
     * 这么做是为了防止URL中包含中文或者特殊字符（/等）时，匹配不了的问题
     *
     * @param request
     * @return
     */
    private static String extractPathFromPattern(final HttpServletRequest request) {
        String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        String bestMatchPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        return new AntPathMatcher().extractPathWithinPattern(bestMatchPattern, path);
    }


    /**
     * 产生订单号
     */
    @GetMapping(value = "/number")
    public RespResult<String> createNumber() {
        return RespResult.OK(IdWorker.getTimeId());
    }


    @GetMapping(value = "/dowmTemplate")
    public void dowmTemplate(HttpServletResponse response, @RequestParam("templateName") String templateName) throws IOException {
        File file = ResourceUtils.getFile("classpath:downTemplate/床位信息.xlsx");
        response.setContentType("application/octet-stream");
        response.setCharacterEncoding("UTF-8");//改为UTF-8的标准格式
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "attachment;filename=" + templateName);
        byte[] bytes = FileCopyUtils.copyToByteArray(file);
        OutputStream os = response.getOutputStream();
        os.write(bytes);
    }

}
