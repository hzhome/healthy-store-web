package com.hz.healthy.web.his.dto.response;

import com.hz.healthy.model.AICareItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/23 17:11
 * @ClassName: AICareCategoryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AICareCheckUpDTO implements Serializable {

    private String key;
    private String category;
    private List<AICareItem> itemList;

    private List<AICareCategoryDTO> categoryList;
}
