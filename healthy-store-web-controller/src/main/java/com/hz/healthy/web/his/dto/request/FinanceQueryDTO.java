package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/4/9 22:23
 * @ClassName: FinanceQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class FinanceQueryDTO implements Serializable {

    /**
     * 0全部
     * 1今天
     * 2昨天
     */
    private String timeType;


    /**
     * 门店ID
     */
    private Long storeId;

}
