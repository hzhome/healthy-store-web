package com.hz.healthy.web.system.controller;


import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.dao.auth.entity.SysDictItem;
import com.hz.ocean.system.service.auth.ISysDictItemService;
import com.hz.ocean.system.service.auth.model.SysDictItemModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/sys/dictItem")
@Slf4j
public class SysDictItemController {

	@Autowired
	private ISysDictItemService sysDictItemService;
	
	/**
	 * @功能：查询字典数据
	 * @param sysDictItem
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public RespResult<ListResponse<SysDictItemModel>> queryPageList(SysDictItemModel sysDictItem, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
																	@RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req) {
		RespResult<ListResponse<SysDictItemModel>> result = new RespResult<ListResponse<SysDictItemModel>>();
//		QueryWrapper<SysDictItem> queryWrapper = QueryGenerator.initQueryWrapper(sysDictItem, req.getParameterMap());
////		queryWrapper.orderByAsc("sort_order");
//		Page<SysDictItem> page = new Page<SysDictItem>(pageNo, pageSize);
		ListResponse<SysDictItemModel> pageList = sysDictItemService.pageQuery(sysDictItem);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}
	
	/**
	 * @功能：新增
	 * @return
	 */
	//@RequiresRoles({"admin"})
	@RequestMapping(value = "/add", method = RequestMethod.POST)
//	@CacheEvict(value= {CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	public RespResult<SysDictItemModel> add(@RequestBody SysDictItemModel sysDictItem) {
		RespResult<SysDictItemModel> result = new RespResult<SysDictItemModel>();
		try {
			sysDictItem.setCreateTime(new Date());
			sysDictItemService.save(sysDictItem);
			result.success("保存成功！");
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			//result.error500("操作失败");
		}
		return result;
	}
	
	/**
	 * @功能：编辑
	 * @param sysDictItem
	 * @return
	 */
	//@RequiresRoles({"admin"})
	@RequestMapping(value = "/edit",  method = { RequestMethod.PUT,RequestMethod.POST })
	//@CacheEvict(value={CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	public RespResult<SysDictItemModel> edit(@RequestBody SysDictItemModel sysDictItem) {
		RespResult<SysDictItemModel> result = new RespResult<SysDictItemModel>();
		SysDictItemModel sysdict = sysDictItemService.getById(sysDictItem.getId());
		if(sysdict==null) {
			//result.error500("未找到对应实体");
		}else {
			sysDictItem.setUpdateTime(new Date());
			try{
				sysDictItemService.updateById(sysDictItem);
				result.success("编辑成功!");
			}catch (Exception e){
			}
		}
		return result;
	}
	
	/**
	 * @功能：删除字典数据
	 * @param id
	 * @return
	 */
	//@RequiresRoles({"admin"})
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	//@CacheEvict(value={CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	public RespResult<SysDictItemModel> delete(@RequestParam(name="id",required=true) String id) {
		RespResult<SysDictItemModel> result = new RespResult<SysDictItemModel>();
		SysDictItemModel joinSystem = sysDictItemService.getById(id);
		if(joinSystem==null) {
			//result.error500("未找到对应实体");
		}else {
			sysDictItemService.removeById(id);
			result.success("删除成功!");
		}
		return result;
	}
	
	/**
	 * @功能：批量删除字典数据
	 * @param ids
	 * @return
	 */
	//@RequiresRoles({"admin"})
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	//@CacheEvict(value={CacheConstant.SYS_DICT_CACHE, CacheConstant.SYS_ENABLE_DICT_CACHE}, allEntries=true)
	public RespResult<SysDictItemModel> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		RespResult<SysDictItemModel> result = new RespResult<SysDictItemModel>();
		if(ids==null || "".equals(ids.trim())) {
			//result.error500("参数不识别！");
		}else {
			this.sysDictItemService.removeByIds(Arrays.asList(ids.split(",")));
			result.success("删除成功!");
		}
		return result;
	}

	/**
	 * 字典值重复校验
	 * @param sysDictItem
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/dictItemCheck", method = RequestMethod.GET)
	//@ApiOperation("字典重复校验接口")
	public RespResult<Object> doDictItemCheck(SysDictItem sysDictItem, HttpServletRequest request) {
		int num = sysDictItemService.doDictItemCheck(sysDictItem.getItemValue(),sysDictItem.getDictId(),sysDictItem.getId());
		if (num == 0) {
			// 该值可用
			return RespResult.OK("该值可用！");
		} else {
			// 该值不可用
			log.info("该值不可用，系统中已存在！");
			return RespResult.error("该值不可用，系统中已存在！");
		}
	}
	
}
