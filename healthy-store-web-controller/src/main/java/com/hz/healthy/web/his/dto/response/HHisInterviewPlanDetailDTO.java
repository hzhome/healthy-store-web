package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/11/5 18:29
 * @ClassName: HHisInterviewPlanDetailDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisInterviewPlanDetailDTO implements Serializable {


    /** 主键ID */
    private Integer id;

    /** 内部唯一编码 */
    private String planCode;

    private String planTitle;

    private String orderSn;
    /** 长者ID */
    private Long patientId;

    /** 长者 */
    private String patientName;

    /** 长者电话 */
    private String patientPhone;

    /** 长者性别 */
    private String patientSex;

    /** 长者年龄 */
    private Integer patientAge;

    private String patientAddress;

    /** 开始月/执行日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date startTime;

    /** 结束月/执行日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date endTime;
    /** 执行人类型，0社区  1护理员 3其他 */
    private String execType;

    /** 执行人id */
    private Long eyId;

    /** 执行人 */
    private String eyName;


    /** 执行次数(次/月) */
    private Integer execCount;

    /** 完成执行次数(次/月) */
    private Integer finishCount;

    /**
     * 还有多少次未执行完成
     */
    private Integer qty;

    /** 0待执行 1开始执行 2执行中 3执行完成 */
    private String status;

    /** 签到时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signIn;

    /** 签到图片 */
    private String signInImg;

    /** 签退时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date signOut;

    /** 签退图片 */
    private String signOutImg;

    /** 服务时长(分数) */
    private Integer duration;

    /** 经度 */
    private BigDecimal positionLng;

    /** 纬度 */
    private BigDecimal positionLat;

    /** 上报签到数据的住址 */
    private String address;

    /** 走访记录内容 */
    private String recordContent;



    /**  注册时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 1走访 2去电 3事件提示 */
    private String planType;


}
