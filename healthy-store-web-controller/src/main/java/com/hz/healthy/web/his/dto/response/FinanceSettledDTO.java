package com.hz.healthy.web.his.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/4/9 22:22
 * @ClassName: FinanceSettledDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FinanceSettledDTO implements Serializable {

    /** 营业收入 */
    private BigDecimal totalAmount;
    /** 实际收入 */
    private BigDecimal incomeAmount;

    /** 营业笔数 */
    private Integer qty;

    private BigDecimal waitTakeAmount;

    private BigDecimal finishTakeAmount;

    /** 退款金额 */
    private BigDecimal refundAmount;

    /** 退款笔数 */
    private Integer refundQty;



}
