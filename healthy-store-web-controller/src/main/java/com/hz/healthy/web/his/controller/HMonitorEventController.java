package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.report.IHMonitorEventHandleResultService;
import com.hz.healthy.domain.report.IHMonitorEventService;
import com.hz.healthy.domain.report.model.HMonitorEventHandleResultModel;
import com.hz.healthy.domain.report.model.HMonitorEventHandleResultQueryModel;
import com.hz.healthy.domain.report.model.HMonitorEventModel;
import com.hz.healthy.domain.report.model.HMonitorEventQueryModel;
import com.hz.healthy.web.his.dto.response.HMonitorEventDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/3/7 11:00
 * @ClassName: HMonitorEventController
 * @Description: 监控事件
 * @Version 1.0
 */
@RestController
@RequestMapping("/patient/monitor/record")
public class HMonitorEventController extends WebBaseController {


    @Autowired
    private IHMonitorEventService hMonitorEventService;

    @Autowired
    private IHMonitorEventHandleResultService hMonitorEventHandleResultService;


    @GetMapping("/list")
    public RespResult<ListResponse<HMonitorEventDTO>> pageQuery(HMonitorEventQueryModel queryModel) {
        List<HMonitorEventDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HMonitorEventModel> pageList = hMonitorEventService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            pageList.getRecords().forEach(p -> {
                HMonitorEventDTO dto = BeanUtil.toBean(p, HMonitorEventDTO.class);
                dataList.add(dto);
            });
        }


        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        HMonitorEventModel monitorEventModel = hMonitorEventService.getById(id);
        return RespResult.OK(BeanUtil.toBean(monitorEventModel, HMonitorEventDTO.class));
    }

    @GetMapping(value = "/result/{monitorCode}")
    public RespResult<List<HMonitorEventHandleResultModel>> listGoods(@PathVariable("monitorCode") String monitorCode) {
        List<HMonitorEventHandleResultModel> itemList = hMonitorEventHandleResultService.selectList(HMonitorEventHandleResultQueryModel.builder()
                .monitorCode(monitorCode)
                .build());
        return RespResult.OK(itemList);
    }


    /**
     * 处理结果
     *
     * @param hMonitorEventHandleResultModel
     * @return
     */
    @Transactional
    @PostMapping(value = "/result")
    public RespResult<String> handleResult(@RequestBody HMonitorEventHandleResultModel hMonitorEventHandleResultModel) {
        String monitorCode = hMonitorEventHandleResultModel.getMonitorCode();
        if (StringUtils.isEmpty(monitorCode)) {
            return RespResult.error("监控编码出错");
        }
        HMonitorEventModel monitorEventModel = hMonitorEventService.getOne(HMonitorEventQueryModel.builder().monitorCode(monitorCode)
                .tenantId(getTenantId())
                .build());
        if (ObjectUtil.isNull(monitorEventModel)) {
            return RespResult.error("监控记录不存在");
        }
        hMonitorEventHandleResultModel.setOrgCode(getOrgCode());
        hMonitorEventHandleResultModel.setTenantId(getTenantId());
        hMonitorEventHandleResultModel.setCreateTime(new Date());
        hMonitorEventHandleResultModel.setCreateUser(getLoginUser().getRealname());
        hMonitorEventHandleResultModel.setEyId(getLoginUser().getEyId());
        hMonitorEventHandleResultModel.setEyName(getLoginUser().getRealname());
        hMonitorEventHandleResultService.insert(hMonitorEventHandleResultModel);
        hMonitorEventService.updateById(HMonitorEventModel.builder()
                .id(monitorEventModel.getId())
                .status(hMonitorEventHandleResultModel.getStatus())
                .build());
        return RespResult.OK("保存处理成功");
    }


}
