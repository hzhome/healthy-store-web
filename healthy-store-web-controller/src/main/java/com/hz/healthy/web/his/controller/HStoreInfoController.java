package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.PhoneUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.store.IHStoreAccountService;
import com.hz.healthy.domain.store.IHStoreInfoService;
import com.hz.healthy.domain.store.IHStoreUserService;
import com.hz.healthy.domain.store.model.*;
import com.hz.healthy.web.his.dto.request.StoreInfoFormReqDTO;
import com.hz.healthy.web.his.dto.request.StoreInfoSetupFormReqDTO;
import com.hz.healthy.web.his.dto.response.StoreInfoRepDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.constant.ToolConfig;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.PasswordUtil;
import com.hz.ocean.common.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 门店Controller
 *
 * @author hyhuang
 * @date 2024-02-01
 */
@Slf4j
@RestController
@RequestMapping("/store/info")
public class HStoreInfoController extends WebBaseController {
    @Autowired
    private IHStoreInfoService hStoreInfoService;

    @Autowired
    private IHStoreAccountService hStoreAccountService;

    @Autowired
    private IHStoreUserService hStoreUserService;

    /**
     * 查询门店列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<StoreInfoRepDTO>> pageQuery(HStoreInfoQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HStoreInfoModel> pageList = hStoreInfoService.pageQuery(queryModel);
        if (pageList != null && CollectionUtils.isNotEmpty(pageList.getRecords())) {
            StoreInfoRepDTO infoRepDto=BeanUtil.toBean(pageList.getRecords().get(0), StoreInfoRepDTO.class);

            HStoreAccountModel accountModel=hStoreAccountService.getOne(HStoreAccountQueryModel.builder().tenantId(getTenantId()).build());
            if(ObjectUtil.isNotNull(accountModel)){
                infoRepDto.setTotalAmount(accountModel.getTotalAmount());
                infoRepDto.setWaitTakeAmount(accountModel.getWaitTakeAmount());
                infoRepDto.setFinishTakeAmount(accountModel.getCashTakeAmount());
            }
            String loginName = hStoreUserService.getLoginName(pageList.getRecords().get(0).getStoreCode());
            if (StringUtils.isNotBlank(loginName)) {
                infoRepDto.setLoginName(loginName);
            }
            return RespResult.OK(ListResponse.build(Lists.newArrayList(infoRepDto), pageList));
        }

        return RespResult.OK(ListResponse.empty());
    }


    @GetMapping("/selectList")
    public RespResult<List<KeyLongModel>> selectList(HStoreInfoQueryModel queryModel) {
        List<KeyLongModel> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        List<HStoreInfoModel> pageList = hStoreInfoService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                dataList.add(KeyLongModel.builder()
                        .key(p.getId())
                        .label(p.getStoreName())
                        .build());
            });
        }
        return RespResult.OK(dataList);
    }


    /**
     * 获取门店详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hStoreInfoService.getById(id));
    }


    @GetMapping(value = "/setup/{id}")
    public RespResult getSetUpInfo(@PathVariable("id") Long id) {
        StoreInfoSetupFormReqDTO formReq = BeanUtil.toBean(hStoreInfoService.getById(id), StoreInfoSetupFormReqDTO.class);
        if (ObjectUtil.isNotNull(formReq)) {
            if (BigDecimal.ZERO.compareTo(formReq.getPositionLng()) == 0) {
                formReq.setPositionLat(null);
                formReq.setPositionLng(null);
            }
        }
        return RespResult.OK(formReq);
    }

    /**
     * 新增门店
     */
    @PostMapping
    public RespResult setUp(@RequestBody StoreInfoSetupFormReqDTO formReq) {
        if (StringUtils.isAllBlank(formReq.getStoreTimeBegin(), formReq.getStoreTimeEnd())) {
            return RespResult.error("请填写店内营业时间");
        }
        if (ObjectUtil.isNull(formReq.getPositionLat()) || ObjectUtil.isNull(formReq.getPositionLng())) {
            return RespResult.error("店铺的经纬度不能为空");
        }
        if (StringUtils.isBlank(formReq.getAddress())) {
            return RespResult.error("店铺的详细地址不能为空");
        }
        HStoreInfoModel ordStoreInfoModel = hStoreInfoService.getById(formReq.getId());
        if (ObjectUtil.isNull(ordStoreInfoModel)) {
            return RespResult.error("记录不存在");
        }
        HStoreInfoModel updateModel = BeanUtil.toBean(formReq, HStoreInfoModel.class);
        updateModel.setPhone(ordStoreInfoModel.getPhone());
        hStoreInfoService.updateById(updateModel);
        return RespResult.OK("设置保存成功");
    }

    /**
     * 修改门店
     */
    @PutMapping
    public RespResult edit(@Validated @RequestBody StoreInfoFormReqDTO reqDto) {
        if (!PhoneUtil.isPhone(reqDto.getPhone())) {
            return RespResult.error("请输入正确的联系手机");
        }
        if (!PhoneUtil.isPhone(reqDto.getCustomerTel())) {
            return RespResult.error("请输入正确的客服电话");
        }
        HStoreInfoModel ordStoreInfoModel = BeanUtil.toBean(reqDto, HStoreInfoModel.class);

        ordStoreInfoModel.setUpdateTime(new Date());
        hStoreInfoService.updateById(ordStoreInfoModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除门店
     */
    @DeleteMapping("/delete")
    public RespResult remove(@RequestParam("id") Long id) {
        hStoreInfoService.deleteById(id);
        return RespResult.OK("删除成功");
    }


    /**
     * 启用/禁用
     *
     * @param formDTO
     * @return
     */
    @PostMapping(value = "/changeStatus")
    public RespResult changeStatus(@RequestBody StoreInfoFormReqDTO formDTO) {
        HStoreInfoModel db = hStoreInfoService.getById(formDTO.getId());
        if (db == null) {
            return RespResult.error("记录不存在");
        }

        Date dateTime = new Date();
        HStoreInfoModel po = new HStoreInfoModel();
        po.setId(db.getId());
        po.setUpdateTime(dateTime);

        if ("1".equals(db.getStatus())) {
            po.setStatus(CommonConstant.STATUS_0);
        } else {
            po.setStatus(CommonConstant.STATUS_1);
        }
        hStoreInfoService.updateById(po);
        return RespResult.OK("变更成功");
    }


    @PostMapping(value = "/modifyPwd")
    public RespResult modifyPwd(@RequestBody StoreInfoFormReqDTO formDTO) {
        if (formDTO.getId() == null || formDTO.getId() <= 0L) {
            return RespResult.error(ToolConfig.VALID_REQ_PARAM_FAIL);
        }
        HStoreInfoModel storeInfoModel = hStoreInfoService.getById(formDTO.getId());
        if (ObjectUtil.isNull(storeInfoModel)) {
            return RespResult.error("记录不存在");
        }

        HStoreUserModel db = hStoreUserService.getOne(HStoreUserQueryModel.builder()
                .storeCode(storeInfoModel.getStoreCode())
                .isAdmin(1)
                .isDeleted(CommonConstant.DEL_FLAG_0)
                .build());
        if (db == null) {
            return RespResult.error("用户不存在");
        }
        //小写字母+数字 修改
        String pwd = formDTO.getPwd();
        String passwordEncode = PasswordUtil.encrypt(db.getPhone(), pwd, db.getSalt());
        HStoreUserModel updateModel = new HStoreUserModel();
        updateModel.setId(db.getId());
        updateModel.setPassword(passwordEncode);
        updateModel.setPhone(db.getPhone());
        hStoreUserService.updateById(updateModel);
        return RespResult.OK("修改完成");
    }


    private void modifyPhoneVerificationParam(String phone) {
        if (StringUtils.isBlank(phone)) {
            throw new BizException(ToolConfig.VALID_REQ_PARAM_FAIL);
        }
        if (!PhoneUtil.isMobile(phone)) {
            throw new BizException("手机号格不正确");
        }
    }


    /**
     * 生成门店小程序码
     *
     * @param response
     */
    /**
     * @param response
     */
    @GetMapping(value = "/queryWeChatCode")
    public void queryWeChatCode(@RequestParam(value = "id") Long id, HttpServletResponse response) {
        HStoreInfoModel storeInfoModel = hStoreInfoService.getById(id);
        OutputStream outputStream = null;
        String path = "";//cateringUrl + "?storeId=" + storeInfoModel.getId();
        try {
            byte[] bytes = null;//QrCodeUtil.generateQrCodeStream(path);
            response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            outputStream = response.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("生成小程序码失败，message:{}", ex.getMessage());
        } finally {
            try {
                outputStream.close();
            } catch (Exception ex) {
                log.error("关闭IO流失败,msg:{}", ex.getMessage());
            }
        }
    }


}

