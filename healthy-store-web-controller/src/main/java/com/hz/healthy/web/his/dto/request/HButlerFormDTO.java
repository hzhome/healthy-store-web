package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/6/18 23:11
 * @ClassName: HButlerFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HButlerFormDTO implements Serializable {

    private List<Long> ids;


}
