package com.hz.healthy.web.his.dto.request;

import com.hz.healthy.web.his.dto.request.item.HHisPrescriptionTemplateGroupItem;
import com.hz.healthy.web.his.dto.request.item.HHisPrescriptionTemplateItem;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/20 22:12
 * @ClassName: HHisPrescriptionTemplateFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPrescriptionTemplateFormDTO implements Serializable {

    private Long id;

    /** 模板分类code */
    @NotBlank(message = "请选择处方模板分类")
    private String categoryCode;

    /** 处方类型 0中药处方  1西药处方 2输注处方 3外治处方 8康复处方 9常规项目处方*/
    private String templateType;

    /** 处方名称 */
    @NotBlank(message = "请输入处方模板名称")
    private String prescriptionName;

    /** 功能或作用 */
    @NotBlank(message = "请输入处功能或作用")
    private String functions;

    /** 主治 */
    @NotBlank(message = "请输入主治")
    private String indications;

    /** 来源 出自于谁，个人，教材等 */
    private String source;

    /** 备注，用于处方加内容或减内容，根据体质等特点 */
    private String remark;


    /**
     * 西药与中药使用
     */
    private List<HHisPrescriptionTemplateItem> items;




    /** 剂数，代表多少剂(中药) */
    private String attrType;
    private Integer doseQty;

    /** 剂药用法(中药) */
    private String usages;

    /** 剂药服用频次(中药) */
    private String frequency;

    /** 每次用量(中药) */
    private String dose;

    /** 服用剂数 一天一剂 一天二剂等(中药) */
    private String dayQty;



    /**
     * 输注使用
     */
    private List<HHisPrescriptionTemplateGroupItem> groupList;
}
