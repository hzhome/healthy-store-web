package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/11/5 15:41
 * @ClassName: HHisInterviewPlanDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisInterviewPlanDTO implements Serializable {

    /** ID */
    private Integer id;

    /** 内部唯一编码 */
    private String planCode;

    private String planTitle;

    /** 1走访 2去电 3事件提示 */
    private String planType;

    /** 1未执行 2执行中 3结束 */
    private String taskStatus;

    /** 执行年 */
    private String year;

    /** 开始月/执行日期 */
    private String startTime;

    /** 结束月/执行日期 */
    private String endTime;

    /** 服务人数 */
    private Integer serviceQty;

    /** 执行次数(次/月) */
    private Integer execCount;

    /** 完成执行次数(次/月) */
    private Integer finishCount;

    /** 执行人类型，0社区  1护理员 3其他 */
    private String execType;

    /** 执行人id */
    private Long eyId;

    /** 执行人 */
    private String eyName;

    /** 计划走访事务 */
    private String followUpContent;



    /** 创建人员  */
    private String createUser;

    /**  建档时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
