package com.hz.healthy.web.his.dto.response.item;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/2/3 21:22
 * @ClassName: HStoreOrderItemDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HStoreOrderItemDTO implements Serializable {

    private Long id;


    /** 门店唯一码 */
    private String storeCode;

    /** 类型 1一体机食谱 2一体机运动方案 3一体机整套方案 */
    private String goodsType;

    /** 商品id */
    private Long goodsId;

    /** 商品名称 */
    private String goodsName;

    /** 商品封面图 */
    private String goodsImg;

    /** 商品价格(单价) */
    private BigDecimal goodsPrice;

    /** 商品划线价 */
    private BigDecimal linePrice;

    /** 商品重量(Kg) */
    private String goodsWeight;

    /** 是否存在会员等级折扣 0否  1是 */
    private Integer isUserGrade;

    /** 会员折扣比例(0-10) */
    private Integer gradeRatio;

    /** 会员折扣的商品单价 */
    private BigDecimal gradeGoodsPrice;

    /** 会员折扣的总额差 */
    private BigDecimal gradeTotalMoney;

    /** 平台优惠券抵扣 */
    private BigDecimal couponMoneySys;

    /** 优惠券折扣金额 */
    private BigDecimal couponMoney;

    /** 积分金额 */
    private BigDecimal pointsMoney;

    /** 积分抵扣数量 */
    private Integer pointsNum;

    /** 赠送的积分数量 */
    private BigDecimal pointsBonus;

    /** 购买数量 */
    private Integer totalNum;

    /** 商品总价(数量×单价) */
    private BigDecimal totalPrice;

    /** 实际付款价(折扣和优惠后) */
    private BigDecimal totalPayPrice;



    /** 满减金额 */
    private BigDecimal fullreduceMoney;




    /** 是否已评价(0否 1是) */
    private Integer isComment;

    /** 订单号 */
    private String orderNo;


    /** 包装费 */
    private BigDecimal bagPrice;

    /** 优惠金额 */
    private BigDecimal discountMoney;



}
