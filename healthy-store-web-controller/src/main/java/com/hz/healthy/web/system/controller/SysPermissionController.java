package com.hz.healthy.web.system.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.convert.Convert;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.web.system.dto.SysPermissionTreeDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.constant.ToolConfig;
import com.hz.ocean.common.dto.OrderItem;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.model.TreeModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.MD5Util;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.enums.RoleIndexConfigEnum;
import com.hz.ocean.system.service.auth.*;
import com.hz.ocean.system.service.auth.helper.PermissionDataUtil;
import com.hz.ocean.system.service.auth.model.*;
import com.hz.ocean.uac.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单权限表 前端控制器
 * </p>
 */
@Slf4j
@RestController
@RequestMapping("/sys/permission")
public class SysPermissionController extends WebBaseController {

    @Autowired
    private ISysPermissionService sysPermissionService;

    @Autowired
    private ISysRolePermissionService sysRolePermissionService;

    @Autowired
    private ISysPermissionDataRuleService sysPermissionDataRuleService;

    @Autowired
    private ISysDepartPermissionService sysDepartPermissionService;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISyzTldChildPermissionService syzTldChildPermissionService;

    /**
     * 加载数据节点
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RespResult<List<SysPermissionTreeDTO>> list(@RequestParam(value = "systemId", required = false) String systemId) {
        long start = System.currentTimeMillis();
        RespResult<List<SysPermissionTreeDTO>> result = new RespResult<>();
        List<OrderItem> orderBy = new ArrayList<>();
        OrderItem orderItem = new OrderItem();
        orderItem.withOrder(OrderItem.ASC).withField("sort_no");
        orderBy.add(orderItem);
        SysPermissionModel sysPermissionModel = new SysPermissionModel();
        sysPermissionModel.setDelFlag(CommonConstant.DEL_FLAG_0);
        sysPermissionModel.setOrderBy(orderBy);
        sysPermissionModel.setSystemId(StringUtils.isNotBlank(systemId) ? systemId : getSystemId());
        List<SysPermissionModel> list = sysPermissionService.list(sysPermissionModel);

        try {
            List<SysPermissionTreeDTO> treeList = new ArrayList<>();
            getTreeList(treeList, list, null);
            result.setResult(treeList);
            result.setSuccess(true);
            log.info("======获取全部菜单数据=====耗时:" + (System.currentTimeMillis() - start) + "毫秒");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }



    /*update_begin author:wuxianquan date:20190908 for:先查询一级菜单，当用户点击展开菜单时加载子菜单 */

    /**
     * 系统菜单列表(一级菜单)
     *
     * @return
     */
    @RequestMapping(value = "/getSystemMenuList", method = RequestMethod.GET)
    public RespResult<List<SysPermissionTreeModel>> getSystemMenuList() {
        long start = System.currentTimeMillis();
        RespResult<List<SysPermissionTreeModel>> result = new RespResult<>();
        try {
            List<OrderItem> orderBy = new ArrayList<>();
            OrderItem orderItem = new OrderItem();
            orderItem.withOrder(OrderItem.ASC).withField("sort_no");
            orderBy.add(orderItem);
            SysPermissionModel sysPermissionModel = new SysPermissionModel();
            sysPermissionModel.setDelFlag(CommonConstant.DEL_FLAG_0);
            sysPermissionModel.setMenuType("M");
            sysPermissionModel.setOrderBy(orderBy);
            sysPermissionModel.setSystemId(getSystemId());
            List<SysPermissionModel> list = sysPermissionService.list(sysPermissionModel);
            List<SysPermissionTreeModel> sysPermissionTreeList = new ArrayList<SysPermissionTreeModel>();
            for (SysPermissionModel sysPermission : list) {
                SysPermissionTreeModel sysPermissionTree = new SysPermissionTreeModel(sysPermission);
                sysPermissionTreeList.add(sysPermissionTree);
            }
            result.setResult(sysPermissionTreeList);
            result.setSuccess(true);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        log.info("======获取一级菜单数据=====耗时:" + (System.currentTimeMillis() - start) + "毫秒");
        return result;
    }

    /**
     * 查询子菜单
     *
     * @param parentId
     * @return
     */
    @RequestMapping(value = "/getSystemSubmenu", method = RequestMethod.GET)
    public RespResult<List<SysPermissionTreeModel>> getSystemSubmenu(@RequestParam("parentId") String parentId) {
        RespResult<List<SysPermissionTreeModel>> result = new RespResult<>();
        try {
            List<OrderItem> orderBy = new ArrayList<>();
            OrderItem orderItem = new OrderItem();
            orderItem.withOrder(OrderItem.ASC).withField("sort_no");
            orderBy.add(orderItem);
            SysPermissionModel sysPermissionModel = new SysPermissionModel();
            sysPermissionModel.setDelFlag(CommonConstant.DEL_FLAG_0);
            sysPermissionModel.setParentId(parentId);
            sysPermissionModel.setOrderBy(orderBy);
            sysPermissionModel.setSystemId(getSystemId());
            List<SysPermissionModel> list = sysPermissionService.list(sysPermissionModel);
            List<SysPermissionTreeModel> sysPermissionTreeList = new ArrayList<SysPermissionTreeModel>();
            for (SysPermissionModel sysPermission : list) {
                SysPermissionTreeModel sysPermissionTree = new SysPermissionTreeModel(sysPermission);
                sysPermissionTreeList.add(sysPermissionTree);
            }
            result.setResult(sysPermissionTreeList);
            result.setSuccess(true);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }
    /*update_end author:wuxianquan date:20190908 for:先查询一级菜单，当用户点击展开菜单时加载子菜单 */

    // update_begin author:sunjianlei date:20200108 for: 新增批量根据父ID查询子级菜单的接口 -------------

    /**
     * 查询子菜单
     *
     * @param parentIds 父ID（多个采用半角逗号分割）
     * @return 返回 key-value 的 Map
     */
    @GetMapping("/getSystemSubmenuBatch")
    public RespResult getSystemSubmenuBatch(@RequestParam("parentIds") String parentIds) {
        try {
            List<String> parentIdList = Arrays.asList(parentIds.split(","));
            List<OrderItem> orderBy = new ArrayList<>();
            OrderItem orderItem = new OrderItem();
            orderItem.withOrder(OrderItem.ASC).withField("sort_no");
            orderBy.add(orderItem);
            SysPermissionModel sysPermissionModel = new SysPermissionModel();
            sysPermissionModel.setDelFlag(CommonConstant.DEL_FLAG_0);
            sysPermissionModel.setParentIdList(parentIdList);
            sysPermissionModel.setOrderBy(orderBy);
            sysPermissionModel.setSystemId(getSystemId());
            List<SysPermissionModel> list = sysPermissionService.list(sysPermissionModel);
            Map<String, List<SysPermissionTreeModel>> listMap = new HashMap<>();
            for (SysPermissionModel item : list) {
                String pid = item.getParentId();
                if (parentIdList.contains(pid)) {
                    List<SysPermissionTreeModel> mapList = listMap.get(pid);
                    if (mapList == null) {
                        mapList = new ArrayList<>();
                    }
                    mapList.add(new SysPermissionTreeModel(item));
                    listMap.put(pid, mapList);
                }
            }
            return RespResult.OK(listMap);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("批量查询子菜单失败：" + e.getMessage());
        }
    }


    /**
     * 查询租户
     * 用户拥有的菜单权限和按钮权限
     *
     * @return
     */
    @RequestMapping(value = "/getUserPermissionByToken", method = RequestMethod.GET)
    public RespResult<?> getTenantUserPermissionByToken(HttpServletRequest request) {
        RespResult<JSONObject> result = new RespResult<JSONObject>();
        try {
            //直接获取当前用户不适用前端token
            LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            if (oConvertUtils.isEmpty(loginUser)) {
                return RespResult.error("请登录系统！");
            }
            List<SysPermissionModel> metaList=new ArrayList<>();
            List<SyzTldChildPermissionModel> childPermissionList = syzTldChildPermissionService.selectList(SyzTldChildPermissionQueryModel.builder()
                    .userId(getUserId())
                    .orgCode(getOrgCode())
                    .build());
            if(CollectionUtil.isNotEmpty(childPermissionList)){
                List<String> permissionIdList=childPermissionList.stream().map(SyzTldChildPermissionModel::getPermissionId).collect(Collectors.toList());
                metaList.addAll(sysPermissionService.list(permissionIdList));
            }
            // List<SysPermissionModel> metaList = sysPermissionService.queryByUser(loginUser.getUsername(), getSystemId());
            //添加首页路由
            //update-begin-author:taoyan date:20200211 for: TASK #3368 【路由缓存】首页的缓存设置有问题，需要根据后台的路由配置来实现是否缓存
            if (!PermissionDataUtil.hasIndexPage(metaList)) {
                SysPermissionModel queryModel = new SysPermissionModel();
                queryModel.setName("首页");
                queryModel.setSystemId(getSystemId());
                SysPermissionModel indexMenu = sysPermissionService.list(queryModel).get(0);
                metaList.add(0, indexMenu);
            }
            //update-end-author:taoyan date:20200211 for: TASK #3368 【路由缓存】首页的缓存设置有问题，需要根据后台的路由配置来实现是否缓存

            //update-begin--Author:liusq  Date:20210624  for:自定义首页地址LOWCOD-1578
            List<String> roles = sysUserService.getRole(loginUser.getUsername(), getSystemId());
            String compUrl = RoleIndexConfigEnum.getIndexByRoles(roles);
            if (StringUtils.isNotBlank(compUrl)) {
                List<SysPermissionModel> menus = metaList.stream().filter(sysPermission -> "首页".equals(sysPermission.getName())).collect(Collectors.toList());
                menus.get(0).setComponent(compUrl);
            }
            //update-end--Author:liusq  Date:20210624  for：自定义首页地址LOWCOD-1578
            JSONObject json = new JSONObject();
            JSONArray menujsonArray = new JSONArray();
            this.getPermissionJsonArray(menujsonArray, metaList, null);
            //一级菜单下的子菜单全部是隐藏路由，则一级菜单不显示
            this.handleFirstLevelMenuHidden(menujsonArray);

            JSONArray authjsonArray = new JSONArray();
            this.getAuthJsonArray(authjsonArray, metaList);
            //查询所有的权限
            SysPermissionModel queryModel = new SysPermissionModel();
            queryModel.setDelFlag(CommonConstant.DEL_FLAG_0);
            queryModel.setMenuType(CommonConstant.MENU_TYPE_2);
            queryModel.setSystemId(getSystemId());
            List<SysPermissionModel> allAuthList = sysPermissionService.list(queryModel);
            JSONArray allauthjsonArray = new JSONArray();
            this.getAllAuthJsonArray(allauthjsonArray, allAuthList);
            //路由菜单
            json.put("menu", menujsonArray);
            //按钮权限（用户拥有的权限集合）
            json.put("auth", authjsonArray);
            //全部权限配置集合（按钮权限，访问权限）
            json.put("allAuth", allauthjsonArray);
            json.put("sysSafeMode", false);
            result.setResult(json);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("查询失败:" + e.getMessage());
        }
        return result;
    }

    /**
     * 【vue3专用】获取
     * 1、查询用户拥有的按钮/表单访问权限
     * 2、所有权限 (菜单权限配置)
     * 3、系统安全模式 (开启则online报表的数据源必填)
     */
    @RequestMapping(value = "/getPermCode", method = RequestMethod.GET)
    public RespResult<?> getPermCode() {
        try {
            // 直接获取当前用户
            LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            if (oConvertUtils.isEmpty(loginUser)) {
                return RespResult.error("请登录系统！");
            }
            // 获取当前用户的权限集合
            List<SysPermissionModel> metaList = sysPermissionService.queryByUser(loginUser.getUsername(), getSystemId());
            // 按钮权限（用户拥有的权限集合）
            List<String> codeList = metaList.stream()
                    .filter((permission) -> CommonConstant.MENU_TYPE_2.equals(permission.getMenuType()) && CommonConstant.STATUS_1.equals(permission.getStatus()))
                    .collect(ArrayList::new, (list, permission) -> list.add(permission.getPerms()), ArrayList::addAll);
            //
            JSONArray authArray = new JSONArray();
            this.getAuthJsonArray(authArray, metaList);
            // 查询所有的权限
            SysPermissionModel queryModel = new SysPermissionModel();
            queryModel.setDelFlag(CommonConstant.DEL_FLAG_0);
            queryModel.setMenuType("C");
            List<SysPermissionModel> allAuthList = sysPermissionService.list(queryModel);
            JSONArray allAuthArray = new JSONArray();
            this.getAllAuthJsonArray(allAuthArray, allAuthList);
            JSONObject result = new JSONObject();
            // 所拥有的权限编码
            result.put("codeList", codeList);
            //按钮权限（用户拥有的权限集合）
            result.put("auth", authArray);
            //全部权限配置集合（按钮权限，访问权限）
            result.put("allAuth", allAuthArray);
            // 系统安全模式
            result.put("sysSafeMode", false);
            return RespResult.OK(result);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("查询失败:" + e.getMessage());
        }
    }

    /**
     * 添加菜单
     *
     * @param permission
     * @return
     */
    //@RequiresRoles({ "admin" })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public RespResult<SysPermissionModel> add(@RequestBody SysPermissionModel permission) {
        RespResult<SysPermissionModel> result = new RespResult<SysPermissionModel>();
        try {
            permission.setSystemId(getSystemId());
            permission = PermissionDataUtil.intelligentProcessData(permission);
            sysPermissionService.addPermission(permission);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    /**
     * 编辑菜单
     *
     * @param permission
     * @return
     */
    //@RequiresRoles({ "admin" })
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public RespResult<SysPermissionModel> edit(@RequestBody SysPermissionModel permission) {
        RespResult<SysPermissionModel> result = new RespResult<>();
        try {
            permission = PermissionDataUtil.intelligentProcessData(permission);
            sysPermissionService.editPermission(permission);
            result.success("修改成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    /**
     * 删除菜单
     *
     * @param id
     * @return
     */
    //@RequiresRoles({ "admin" })
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public RespResult<SysPermissionModel> delete(@RequestParam(name = "id", required = true) String id) {
        RespResult<SysPermissionModel> result = new RespResult<SysPermissionModel>();
        try {
            sysPermissionService.deletePermission(id);
            result.success("删除成功!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error(e.getMessage());
        }
        return result;
    }

    /**
     * 批量删除菜单
     *
     * @param ids
     * @return
     */
    //@RequiresRoles({ "admin" })
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public RespResult<SysPermissionModel> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        RespResult<SysPermissionModel> result = new RespResult<>();
        try {
            String[] arr = ids.split(",");
            for (String id : arr) {
                if (StringUtils.isNotEmpty(id)) {
                    sysPermissionService.deletePermission(id);
                }
            }
            result.success("删除成功!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("删除成功!");
        }
        return result;
    }

    /**
     * 获取全部的权限树
     *
     * @return
     */
    @RequestMapping(value = "/queryTreeList", method = RequestMethod.GET)
    public RespResult<Map<String, Object>> queryTreeList() {
        RespResult<Map<String, Object>> result = new RespResult<>();
        // 全部权限ids
        List<String> ids = new ArrayList<>();
        try {
            SysPermissionModel queryModel = new SysPermissionModel();
            queryModel.setDelFlag(CommonConstant.DEL_FLAG_0);
            queryModel.setSystemId(getSystemId());
            //query.orderByAsc(SysPermission::getSortNo);
            List<SysPermissionModel> list = sysPermissionService.list(queryModel);
            for (SysPermissionModel sysPer : list) {
                ids.add(sysPer.getId());
            }
            List<TreeModel> treeList = new ArrayList<>();
            getTreeModelList(treeList, list, null);
            Map<String, Object> resMap = new HashMap<String, Object>();
            resMap.put("treeList", treeList); // 全部树节点数据
            resMap.put("ids", ids);// 全部树ids
            result.setResult(resMap);
            result.setSuccess(true);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 异步加载数据节点 [接口是废的,没有用到]
     *
     * @return
     */
    @RequestMapping(value = "/queryListAsync", method = RequestMethod.GET)
    public RespResult<List<SysPermissionTreeModel>> queryAsync(@RequestParam(name = "pid", required = false) String parentId) {
        RespResult<List<SysPermissionTreeModel>> result = new RespResult<>();
        try {
            List<SysPermissionTreeModel> list = sysPermissionService.queryListByParentId(parentId);
            if (list == null || list.size() <= 0) {
                return RespResult.error("未找到角色信息");
            } else {
                result.setResult(list);
                result.setSuccess(true);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return result;
    }

    /**
     * 查询角色授权
     * 根据租户下的当前用户角色查询已绑定的菜单
     *
     * @return
     */
    @RequestMapping(value = "/queryRolePermission", method = RequestMethod.GET)
    public RespResult<List<String>> queryRolePermission(@RequestParam(name = "roleId", required = true) String roleId) {
        try {
            List<String> list = sysRolePermissionService.listByRoleIdNew(roleId);
            return RespResult.OK(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return RespResult.OK(new ArrayList<>());
    }

    /**
     * 保存租户下用户的角色授权菜单
     *
     * @return
     */
    @RequestMapping(value = "/saveRolePermission", method = RequestMethod.POST)
    //@RequiresRoles({ "admin" })
    public RespResult<String> saveRolePermission(@RequestBody JSONObject json) {
        long start = System.currentTimeMillis();
        try {
            List<String> permissionIdList = new ArrayList<>();
            String roleId = json.getString("roleId");
            String permissionIds = json.getString("permissionIds");
            //String lastPermissionIds = json.getString("lastpermissionIds");
            if (StringUtils.isNotEmpty(permissionIds)) {
                permissionIdList.addAll((List<String>) Convert.toList(permissionIds.split(",")));
            }
            this.sysRolePermissionService.saveRolePermissionNew(roleId, permissionIdList, getTenantId(), getSystemId());
            log.info("======角色授权成功=====耗时:" + (System.currentTimeMillis() - start) + "毫秒");
            return RespResult.OK("保存成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("授权失败！");
        }
    }

    private void getTreeList(List<SysPermissionTreeDTO> treeList, List<SysPermissionModel> metaList, SysPermissionTreeDTO temp) {
        for (SysPermissionModel permission : metaList) {
            String tempPid = permission.getParentId();
            SysPermissionTreeDTO tree = new SysPermissionTreeDTO(permission, getBaseConfig().getSystemIdMaps());
            if (temp == null && StringUtils.isEmpty(tempPid)) {
                treeList.add(tree);
                if (!tree.getIsLeaf()) {
                    getTreeList(treeList, metaList, tree);
                }
            } else if (temp != null && tempPid != null && tempPid.equals(temp.getId())) {
                temp.getChildren().add(tree);
                if (!tree.getIsLeaf()) {
                    getTreeList(treeList, metaList, tree);
                }
            }

        }
    }

    private void getTreeModelList(List<TreeModel> treeList, List<SysPermissionModel> metaList, TreeModel temp) {
        for (SysPermissionModel permission : metaList) {
            String tempPid = permission.getParentId();
            TreeModel tree = new TreeModel(permission.getId(), permission.getParentId(), permission.getIcon(), permission.getName(), permission.getLeaf());
            if (temp == null && StringUtils.isEmpty(tempPid)) {
                treeList.add(tree);
                if (!tree.getIsLeaf()) {
                    getTreeModelList(treeList, metaList, tree);
                }
            } else if (temp != null && tempPid != null && tempPid.equals(temp.getKey())) {
                temp.getChildren().add(tree);
                if (!tree.getIsLeaf()) {
                    getTreeModelList(treeList, metaList, tree);
                }
            }

        }
    }

    /**
     * 一级菜单的子菜单全部是隐藏路由，则一级菜单不显示
     *
     * @param jsonArray
     */
    private void handleFirstLevelMenuHidden(JSONArray jsonArray) {
        jsonArray = jsonArray.stream().map(obj -> {
            JSONObject returnObj = new JSONObject();
            JSONObject jsonObj = (JSONObject) obj;
            if (jsonObj.containsKey("children")) {
                JSONArray childrens = jsonObj.getJSONArray("children");
                childrens = childrens.stream().filter(arrObj -> !"true".equals(((JSONObject) arrObj).getString("hidden"))).collect(Collectors.toCollection(JSONArray::new));
                if (childrens == null || childrens.size() == 0) {
                    jsonObj.put("hidden", true);

                    //vue3版本兼容代码
                    JSONObject meta = new JSONObject();
                    meta.put("hideMenu", true);
                    jsonObj.put("meta", meta);
                }
            }
            return returnObj;
        }).collect(Collectors.toCollection(JSONArray::new));
    }


    /**
     * 获取权限JSON数组
     *
     * @param jsonArray
     * @param allList
     */
    private void getAllAuthJsonArray(JSONArray jsonArray, List<SysPermissionModel> allList) {
        JSONObject json = null;
        for (SysPermissionModel permission : allList) {
            json = new JSONObject();
            json.put("action", permission.getPerms());
            json.put("status", permission.getStatus());
            //1显示2禁用
            json.put("type", permission.getPermsType());
            json.put("describe", permission.getName());
            jsonArray.add(json);
        }
    }

    /**
     * 获取权限JSON数组
     *
     * @param jsonArray
     * @param metaList
     */
    private void getAuthJsonArray(JSONArray jsonArray, List<SysPermissionModel> metaList) {
        for (SysPermissionModel permission : metaList) {
            if (permission.getMenuType() == null) {
                continue;
            }
            JSONObject json = null;
            //去除有效判断 && CommonConstant.STATUS_1.equals(permission.getStatus())
            if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_2)) {
                json = new JSONObject();
                json.put("action", permission.getPerms());
                json.put("type", permission.getPermsType());
                json.put("describe", permission.getName());
                jsonArray.add(json);
            }
        }
    }

    /**
     * 获取菜单JSON数组
     *
     * @param jsonArray
     * @param metaList
     * @param parentJson
     */
    private void getPermissionJsonArray(JSONArray jsonArray, List<SysPermissionModel> metaList, JSONObject parentJson) {
        for (SysPermissionModel permission : metaList) {
            if (permission.getMenuType() == null) {
                continue;
            }
            String tempPid = permission.getParentId();
            JSONObject json = getPermissionJsonObject(permission);
            if (json == null) {
                continue;
            }
            if (parentJson == null && StringUtils.isEmpty(tempPid)) {
                jsonArray.add(json);
                if (!permission.getLeaf()) {
                    getPermissionJsonArray(jsonArray, metaList, json);
                }
            } else if (parentJson != null && StringUtils.isNotEmpty(tempPid) && tempPid.equals(parentJson.getString("id"))) {
                // 类型( 0：一级菜单 1：子菜单 2：按钮 )
                if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_2)) {
                    JSONObject metaJson = parentJson.getJSONObject("meta");
                    if (metaJson.containsKey("permissionList")) {
                        metaJson.getJSONArray("permissionList").add(json);
                    } else {
                        JSONArray permissionList = new JSONArray();
                        permissionList.add(json);
                        metaJson.put("permissionList", permissionList);
                    }
                    // 类型( 0：一级菜单 1：子菜单 2：按钮 )
                } else if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_1) || permission.getMenuType().equals(CommonConstant.MENU_TYPE_0)) {
                    if (parentJson.containsKey("children")) {
                        parentJson.getJSONArray("children").add(json);
                    } else {
                        JSONArray children = new JSONArray();
                        children.add(json);
                        parentJson.put("children", children);
                    }

                    if (!permission.getLeaf()) {
                        getPermissionJsonArray(jsonArray, metaList, json);
                    }
                }
            }

        }
    }

    /**
     * 根据菜单配置生成路由json
     *
     * @param permission
     * @return
     */
    private JSONObject getPermissionJsonObject(SysPermissionModel permission) {
        JSONObject json = new JSONObject();
        // 类型(0：一级菜单 1：子菜单 2：按钮)
        if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_2)) {
            //json.put("action", permission.getPerms());
            //json.put("type", permission.getPermsType());
            //json.put("describe", permission.getName());
            return null;
        } else if (permission.getMenuType().equals(CommonConstant.MENU_TYPE_0) || permission.getMenuType().equals(CommonConstant.MENU_TYPE_1)) {
            json.put("id", permission.getId());
            if (permission.getRoute()) {
                json.put("route", "1");// 表示生成路由
            } else {
                json.put("route", "0");// 表示不生成路由
            }

            if (isWWWHttpUrl(permission.getUrl())) {
                json.put("path", MD5Util.encode(permission.getUrl()));
            } else {
                json.put("path", permission.getUrl());
            }

            // 重要规则：路由name (通过URL生成路由name,路由name供前端开发，页面跳转使用)
            if (StringUtils.isNotEmpty(permission.getComponentName())) {
                json.put("name", permission.getComponentName());
            } else {
                json.put("name", urlToRouteName(permission.getUrl()));
            }


            JSONObject meta = new JSONObject();
            // 是否隐藏路由，默认都是显示的
            if (BooleanUtils.isTrue(permission.getHidden())) {
                json.put("hidden", true);
                //vue3版本兼容代码
                meta.put("hideMenu", true);
            }
            // 聚合路由
            if (BooleanUtils.isTrue(permission.getAlwaysShow())) {
                json.put("alwaysShow", true);
            }
            json.put("component", permission.getComponent());
            // 由用户设置是否缓存页面 用布尔值
            if (permission.getKeepAlive() != null && permission.getKeepAlive()) {
                meta.put("keepAlive", true);
            } else {
                meta.put("keepAlive", false);
            }

            /*update_begin author:wuxianquan date:20190908 for:往菜单信息里添加外链菜单打开方式 */
            //外链菜单打开方式
            if (BooleanUtils.isTrue(permission.getInternalOrExternal())) {
                meta.put("internalOrExternal", true);
            } else {
                meta.put("internalOrExternal", false);
            }
            /* update_end author:wuxianquan date:20190908 for: 往菜单信息里添加外链菜单打开方式*/

            meta.put("title", permission.getName());

            //update-begin--Author:scott  Date:20201015 for：路由缓存问题，关闭了tab页时再打开就不刷新 #842
            String component = permission.getComponent();
            if (StringUtils.isNotEmpty(permission.getComponentName()) || oConvertUtils.isNotEmpty(component)) {
                meta.put("componentName", oConvertUtils.getString(permission.getComponentName(), component.substring(component.lastIndexOf("/") + 1)));
            }
            //update-end--Author:scott  Date:20201015 for：路由缓存问题，关闭了tab页时再打开就不刷新 #842

            if (oConvertUtils.isEmpty(permission.getParentId())) {
                // 一级菜单跳转地址
                json.put("redirect", permission.getRedirect());
                if (oConvertUtils.isNotEmpty(permission.getIcon())) {
                    meta.put("icon", permission.getIcon());
                }
            } else {
                if (oConvertUtils.isNotEmpty(permission.getIcon())) {
                    meta.put("icon", permission.getIcon());
                }
            }
            if (isWWWHttpUrl(permission.getUrl())) {
                meta.put("url", permission.getUrl());
            }
            // update-begin--Author:sunjianlei  Date:20210918 for：新增适配vue3项目的隐藏tab功能
            if (BooleanUtils.isTrue(permission.getHideTab())) {
                meta.put("hideTab", true);
            }
            // update-end--Author:sunjianlei  Date:20210918 for：新增适配vue3项目的隐藏tab功能
            json.put("meta", meta);
        }

        return json;
    }

    /**
     * 判断是否外网URL 例如： http://localhost:8080/jeecg-boot/swagger-ui.html#/ 支持特殊格式： {{
     * window._CONFIG['domianURL'] }}/druid/ {{ JS代码片段 }}，前台解析会自动执行JS代码片段
     *
     * @return
     */
    private boolean isWWWHttpUrl(String url) {
        if (url != null && (url.startsWith("http://") || url.startsWith("https://") || url.startsWith("{{"))) {
            return true;
        }
        return false;
    }

    /**
     * 通过URL生成路由name（去掉URL前缀斜杠，替换内容中的斜杠‘/’为-） 举例： URL = /isystem/role RouteName =
     * isystem-role
     *
     * @return
     */
    private String urlToRouteName(String url) {
        if (oConvertUtils.isNotEmpty(url)) {
            if (url.startsWith("/")) {
                url = url.substring(1);
            }
            url = url.replace("/", "-");

            // 特殊标记
            url = url.replace(":", "@");
            return url;
        } else {
            return null;
        }
    }

    /**
     * 根据菜单id来获取其对应的权限数据
     *
     * @param sysPermissionDataRule
     * @return
     */
    @RequestMapping(value = "/getPermRuleListByPermId", method = RequestMethod.GET)
    public RespResult<List<SysPermissionDataRuleModel>> getPermRuleListByPermId(SysPermissionDataRuleModel sysPermissionDataRule) {
        List<SysPermissionDataRuleModel> permRuleList = sysPermissionDataRuleService.getPermRuleListByPermId(sysPermissionDataRule.getPermissionId());
        RespResult<List<SysPermissionDataRuleModel>> result = new RespResult<List<SysPermissionDataRuleModel>>();
        result.setSuccess(true);
        result.setResult(permRuleList);
        return result;
    }

    /**
     * 添加菜单权限数据
     *
     * @param sysPermissionDataRule
     * @return
     */
    //@RequiresRoles({ "admin" })
    @RequestMapping(value = "/addPermissionRule", method = RequestMethod.POST)
    public RespResult<SysPermissionDataRuleModel> addPermissionRule(@RequestBody SysPermissionDataRuleModel sysPermissionDataRule) {
        RespResult<SysPermissionDataRuleModel> result = new RespResult<SysPermissionDataRuleModel>();
        try {
            sysPermissionDataRule.setCreateTime(new Date());
            sysPermissionDataRuleService.savePermissionDataRule(sysPermissionDataRule);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    //@RequiresRoles({ "admin" })
    @RequestMapping(value = "/editPermissionRule", method = {RequestMethod.PUT, RequestMethod.POST})
    public RespResult<SysPermissionDataRuleModel> editPermissionRule(@RequestBody SysPermissionDataRuleModel sysPermissionDataRule) {
        RespResult<SysPermissionDataRuleModel> result = new RespResult<SysPermissionDataRuleModel>();
        try {
            sysPermissionDataRuleService.saveOrUpdate(sysPermissionDataRule);
            result.success("更新成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    /**
     * 删除菜单权限数据
     *
     * @param id
     * @return
     */
    //@RequiresRoles({ "admin" })
    @RequestMapping(value = "/deletePermissionRule", method = RequestMethod.DELETE)
    public RespResult<SysPermissionDataRuleModel> deletePermissionRule(@RequestParam(name = "id", required = true) String id) {
        RespResult<SysPermissionDataRuleModel> result = new RespResult<SysPermissionDataRuleModel>();
        try {
            sysPermissionDataRuleService.deletePermissionDataRule(id);
            result.success("删除成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    /**
     * 查询菜单权限数据
     *
     * @param sysPermissionDataRule
     * @return
     */
    @RequestMapping(value = "/queryPermissionRule", method = RequestMethod.GET)
    public RespResult<List<SysPermissionDataRuleModel>> queryPermissionRule(SysPermissionDataRuleModel sysPermissionDataRule) {
        RespResult<List<SysPermissionDataRuleModel>> result = new RespResult<>();
        try {
            List<SysPermissionDataRuleModel> permRuleList = sysPermissionDataRuleService.queryPermissionRule(sysPermissionDataRule);
            result.setResult(permRuleList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    /**
     * 部门权限表
     *
     * @param departId
     * @return
     */
    @RequestMapping(value = "/queryDepartPermission", method = RequestMethod.GET)
    public RespResult<List<String>> queryDepartPermission(@RequestParam(name = "departId", required = true) String departId) {
        RespResult<List<String>> result = new RespResult<>();
        try {
            SysDepartPermissionModel queryModel = new SysDepartPermissionModel();
            queryModel.setDataRuleIds(departId);
            List<SysDepartPermissionModel> list = sysDepartPermissionService.list(queryModel);
            result.setResult(list.stream().map(SysDepartPermission -> String.valueOf(SysDepartPermission.getPermissionId())).collect(Collectors.toList()));
            result.setSuccess(true);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 保存部门授权
     *
     * @return
     */
    @RequestMapping(value = "/saveDepartPermission", method = RequestMethod.POST)
    //@RequiresRoles({ "admin" })
    public RespResult<?> saveDepartPermission(@RequestBody JSONObject json) {
        long start = System.currentTimeMillis();
        RespResult<?> result = new RespResult<>();
        try {
            String departId = json.getString("departId");
            String permissionIds = json.getString("permissionIds");
            String lastPermissionIds = json.getString("lastpermissionIds");
            this.sysDepartPermissionService.saveDepartPermission(departId, permissionIds, lastPermissionIds);
            result.success("保存成功");
            log.info("======部门授权成功=====耗时:" + (System.currentTimeMillis() - start) + "毫秒");
        } catch (Exception e) {

            log.error(e.getMessage(), e);
            return RespResult.error("授权失败！");
        }
        return result;
    }


    /**
     * 检测菜单路径是否存在
     *
     * @param id
     * @param url
     * @return
     */
    @RequestMapping(value = "/checkPermDuplication", method = RequestMethod.GET)
    public RespResult<String> checkPermDuplication(@RequestParam(name = "id", required = false) String id, @RequestParam(name = "url") String url, @RequestParam(name = "alwaysShow") Boolean alwaysShow) {
        try {
            boolean check = sysPermissionService.checkPermDuplication(id, url, alwaysShow, getSystemId());
            if (check) {
                return RespResult.OK("该值可用！");
            }
            return RespResult.error("该值不可用，系统中已存在！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return RespResult.error("操作失败");
    }


    /**
     * 获取单位旗下已分配给子商户菜单权限
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/queryRolePermission2Child", method = RequestMethod.GET)
    public RespResult<List<String>> queryRolePermission2Child(@RequestParam(name = "userId", required = true) String userId) {
        //init
        RespResult<List<String>> result = new RespResult<>();
        result.setSuccess(true);
        result.setResult(new ArrayList<>());
        if (StringUtils.isEmpty(userId)) {
            log.error("queryRolePermission2Child param参数不完整");
            return result;
        }

        //已分配给商户的权限
        SyzTldChildPermissionQueryModel tldChildPermissionQueryModel = new SyzTldChildPermissionQueryModel();
        tldChildPermissionQueryModel.setSystemType(CommonConstant.MENU_TYPE_0);
        tldChildPermissionQueryModel.setUserId(userId);
        tldChildPermissionQueryModel.setOrgCode(getOrgCode());
        List<SyzTldChildPermissionModel> tldList = syzTldChildPermissionService.selectList(tldChildPermissionQueryModel);
        if (CollectionUtil.isEmpty(tldList)) {
            return result;
        }
        result.setResult(tldList.stream().map(SyzTldChildPermissionModel -> String.valueOf(SyzTldChildPermissionModel.getPermissionId())).collect(Collectors.toList()));
        return result;
    }


    /**
     * 保存角色授权 - 单位旗下的子商户分配菜单权限
     *
     * @return
     */
    @RequestMapping(value = "/saveRolePermission2Child", method = RequestMethod.POST)
    public RespResult<String> saveRolePermission2Child(@RequestBody JSONObject json) {
        long start = System.currentTimeMillis();
        RespResult<String> result = new RespResult<>();
        try {
            String sysOrgCode = getOrgCode();
            String userId = json.getString("userId");
            String permissionIds = json.getString("permissionIds");
            String lastPermissionIds = json.getString("lastpermissionIds");
            if (StringUtils.isEmpty(sysOrgCode) || StringUtils.isEmpty(userId)) {
                throw new BizException(ToolConfig.VALID_REQ_PARAM_FAIL);
            }
            this.sysRolePermissionService.saveOtherTldRoleChildPermission(userId, CommonConstant.MENU_TYPE_0, permissionIds, lastPermissionIds, "", sysOrgCode);
            log.info("======角色授权成功=====耗时:" + (System.currentTimeMillis() - start) + "毫秒");
            return RespResult.OK("保存成功！");
        } catch (BizException e) {

            return RespResult.error(e.getMessage());
        } catch (Exception e) {
            return RespResult.error("授权失败");
        }
    }


}
