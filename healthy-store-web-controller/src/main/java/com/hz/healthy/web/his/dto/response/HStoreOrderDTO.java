package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.healthy.domain.store.model.HStoreOrderAddrModel;
import com.hz.healthy.web.his.dto.response.item.HStoreOrderItemDTO;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/2/3 21:21
 * @ClassName: HStoreOrderDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HStoreOrderDTO implements Serializable {

    private Long id;

    /** 状态 */
    private String status;
    /** 订单号 */
    private String orderNo;
    /** 商品数量 */
    private Integer qty;
    /** 商品原价总金额 */
    private BigDecimal originalPrice;

    /** 商品实付总金额 */
    private BigDecimal consumeAmount;

    /** 买家留言 */
    private String buyerRemark;
    /** 支付方式 例如 (10余额支付 20微信支付) */
    @Dict(dicCode = "pay_type")
    private String payType;
    /** 支付状态 */
    @Dict(dicCode = "pay_status")
    private String payStatus;
    /** 订单状态 10=>进行中，20=>已经取消，30=>已完成 */
    private String orderStatus;

    /** 支付截止时间 */
    private Date payEndTime;
    /** 配送方式(10外卖配送 20上门取30打包带走40店内就餐 */
    private Integer deliveryType;
    /** 运费金额 */
    private BigDecimal expressPrice;
    /** 发货状态(10未发货 20已发货) */
    private Integer deliveryStatus;
     /** 付款完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date payFinishTime;
    /** 发货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date deliveryTime;
    /** 收货状态(10未收货 20已收货) */
    private Integer receiptStatus;
    /** 收货时间 */
    private Integer receiptTime;
    /** 订单是否已结算(0未结算 1已结算) */
    private Integer isSettled;
    /** 订单来源(10普通 20收银台) */
    private Integer orderSource;
    /** 会员id */
    private Long memberId;
    private String memberName;
    /** 拼团等活动失败退款 */
    private Integer isRefund;
    /** 活动id，对应拼团，秒杀，砍价活动id */
    private Integer activityId;
    /** 供应商结算金额,支付金额-平台结算金额 */
    private BigDecimal supplierMoney;
    /** 商家取消订单备注 */
    private String cancelRemark;
    /** 包装费 */
    private BigDecimal bagPrice;
    /** 退款金额 */
    private BigDecimal refundMoney;
    /** 订单类型 1一体机订单 2.商城订单 3上门护理 4上门照护 5陪诊服务 6慢性康复 7营养指导 */
    private Integer orderType;
    private String orderTypeText;
    /** 就餐桌号 */
    private String tableNo;
    /** 10商家配送 */
    private Integer deliverSource;
    /** 配送状态，待接单＝1,待取货＝2,配送中＝3,已完成＝4 */
    private Integer deliverStatus;
    /** 取餐号 */
    private String callno;
    /** 桌位id */
    private Integer tableId;
    /** 余额抵扣金额 */
    private BigDecimal balance;
    /** 支付订单号 */
    private String payTrxNo;

    /** 订单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date orderTime;
    /** 业务编码 */
    private String busCode;


}
