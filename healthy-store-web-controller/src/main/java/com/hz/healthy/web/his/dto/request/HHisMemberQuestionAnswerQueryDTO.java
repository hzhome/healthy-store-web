package com.hz.healthy.web.his.dto.request;

import com.hz.ocean.common.dto.PageRequest;
import lombok.Data;

/**
 * @Author hyhuang
 * @Date 2024/3/8 20:58
 * @ClassName: HHisMemberQuestionAnswerQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisMemberQuestionAnswerQueryDTO extends PageRequest {

    private Long patientId;

    private String keyCode;
}
