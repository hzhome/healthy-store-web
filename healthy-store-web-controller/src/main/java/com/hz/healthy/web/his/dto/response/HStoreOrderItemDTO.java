package com.hz.healthy.web.his.dto.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/2/7 18:09
 * @ClassName: HStoreOrderItemDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HStoreOrderItemDTO implements Serializable {

    /** $comment */
    private Long id;
    /** 商品id */
    private Long goodsId;

    /** 商品名称 */
    private String goodsName;

    /** 商品封面图 */
    private String goodsImg;

    /** 库存计算方式(10下单减库存 20付款减库存) */
    private Integer deductStockType;

    /** 商品价格(单价) */
    private BigDecimal productPrice;

    /** 商品划线价 */
    private BigDecimal linePrice;

    /** 商品重量(Kg) */
    private String productWeight;

    /** 购买数量 */
    private Integer totalNum;

    /** 商品总价(数量×单价) */
    private BigDecimal totalPrice;

    /** 实际付款价(折扣和优惠后) */
    private BigDecimal totalPayPrice;

    /** 供应商金额 */
    private BigDecimal supplierMoney;

    /** 平台结算金额 */
    private BigDecimal sysMoney;


}
