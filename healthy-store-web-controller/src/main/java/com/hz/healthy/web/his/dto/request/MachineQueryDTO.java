package com.hz.healthy.web.his.dto.request;

import com.hz.ocean.common.dto.PageRequest;
import lombok.Data;

/**
 * @Author hyhuang
 * @Date 2024/2/2 21:29
 * @ClassName: MachineQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class MachineQueryDTO extends PageRequest {

    /**
     * 套餐类型 1套餐方案 2膳食方案 3运动方案
     */
    private String packageType;
    /** 业务项目值，例如风险等级中的低，体质类型的 阴虚 */
    private String busValue;

    private String projectName;
}
