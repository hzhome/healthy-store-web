package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHHisRegistrationService;
import com.hz.healthy.domain.doctor.IHHisSchedulingService;
import com.hz.healthy.domain.doctor.model.*;
import com.hz.healthy.domain.his.IHHisDiagnosisRecordService;
import com.hz.healthy.domain.his.model.HHisDiagnosisRecordModel;
import com.hz.healthy.enums.DiagnosisTypeEnums;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingDoctorDTO;
import com.hz.healthy.store.serve.dto.HEmployeeDTO;
import com.hz.healthy.web.his.dto.request.HHisRegistrationQueryDTO;
import com.hz.healthy.web.his.dto.response.HHisRegistrationDTO;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 通用分类下的标记Controller
 *
 * @author ruoyi
 * @date 2023-10-27
 */
@RestController
@RequestMapping("/his/registration")
public class HHisRegistrationController extends WebBaseController {

    @Autowired
    private IHHisSchedulingService hisSchedulingService;
    @Autowired
    private IHHisRegistrationService hisRegistrationService;
    @Autowired
    private IHHisDiagnosisRecordService hisDiagnosisRecordService;

    /**
     * 查询预约医生上门诊断服务 列表
     */
    @AutoDict
    @GetMapping("/door/list")
    public RespResult<ListResponse<HHisRegistrationDTO>> pageQuery(HHisRegistrationQueryModel queryModel) {
        List<HHisRegistrationDTO> dataList=new ArrayList<>();
        queryModel.setTenantId(getTenantId());
        queryModel.setOrgCode(getOrgCode());
        ListResponse<HHisRegistrationModel> pageList = hisRegistrationService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> registrationNumberList = pageList.getRecords().stream().map(HHisRegistrationModel::getRegistrationNumber).collect(Collectors.toList());
            Map<String, HHisDiagnosisRecordModel> map = hisDiagnosisRecordService.mapByDiagnosisCode(registrationNumberList, DiagnosisTypeEnums.D2_TYPE.getCode());
            pageList.getRecords().forEach(p->{
                HHisRegistrationDTO dto= BeanUtil.toBean(p,HHisRegistrationDTO.class);
                if(map.containsKey(p.getRegistrationNumber())){
                    HHisDiagnosisRecordModel recordModel=map.get(p.getRegistrationNumber());
                    dto.setHealthyStatus(recordModel.getHealthyStatus());
                    dto.setSuggest(recordModel.getSuggest());
                }
                dataList.add(dto);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }



    /**
     * 测试门诊
     * 查询预约医生上门诊断服务 列表
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<ListResponse<HHisRegistrationDTO>> pageQueryList(HHisRegistrationQueryDTO query) {
        HHisRegistrationQueryModel queryModel=BeanUtil.toBean(query,HHisRegistrationQueryModel.class);
        List<HHisRegistrationDTO> dataList=new ArrayList<>();
        queryModel.setTenantId(getTenantId());
        queryModel.setOrgCode(getOrgCode());
        if(StringUtils.isNotBlank(query.getBeginToday())&&StringUtils.isNotBlank(query.getEndToday())){
            queryModel.setTodayBegin(DateUtil.parseDateTime(query.getBeginToday()+" 00:00:00"));
            queryModel.setTodayEnd(DateUtil.parseDateTime(query.getEndToday()+" 23:59:59"));
        }
       // queryModel.setSchedulingDate(DateUtil.formatDate(new Date()));
        ListResponse<HHisRegistrationModel> pageList = hisRegistrationService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            pageList.getRecords().forEach(p->{
                HHisRegistrationDTO dto= BeanUtil.toBean(p,HHisRegistrationDTO.class);
                dto.setBirthday("1980-11-23");
                dataList.add(dto);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 查询医生信息列表
     */
    @AutoDict
    @GetMapping("/doctor/list")
    public RespResult<ListResponse<HHisSchedulingDoctorDTO>> pageQueryDoctor(HHisSchedulingQueryModel queryModel) {
        List<HHisSchedulingDoctorDTO> dataList=new ArrayList<>();
        HHisSchedulingDoctorDTO dto1=new HHisSchedulingDoctorDTO();
        dto1.setId(0L);
        dto1.setName("全部医生");
        dto1.setTotalQty(0);
        dto1.setWaitQty(0);
        dataList.add(dto1);

        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisSchedulingModel> pageList=hisSchedulingService.pageQuery(queryModel);
        if(CollectionUtils.isNotEmpty(pageList.getRecords())){
            pageList.getRecords().forEach(p->{
                HHisSchedulingDoctorDTO dto=new HHisSchedulingDoctorDTO();
                dto.setId(p.getId());
                dto.setName(p.getEmployeeName());
                dto.setTotalQty(p.getTotalQty());
                dto.setWaitQty(0);
                dataList.add(dto);
            });
        }
        return RespResult.OK(ListResponse.build(dataList,pageList));
    }


}
