package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/2/2 14:08
 * @ClassName: StoreInfoRepDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class StoreInfoRepDTO implements Serializable {

    /**
     * 主键
     */
    private Long id;


    /**
     * 创建日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 所属组织
     */
    private String sysOrgCode;

    /**
     * 状态
     */
    @Dict(dicCode = "start_stop_status")
    private String status;

    /**
     * 食堂名称
     */
    private String storeName;

    /**
     * 管理员名称
     */
    private String adminName;

    /**
     * 管理员联系电话
     */
    private String phone;

    /**
     * 工号
     */
    private String workNo;

    /**
     * 头像
     */
    private String avatar;

    /** 门店唯一码 */
    private String storeCode;
    /**
     * 备注
     */
    private String remark;

    private BigDecimal totalAmount;
    /**
     * 当前可提现金额
     */
    private BigDecimal waitTakeAmount;
    /**
     * 已冻结金额
     */
    private BigDecimal freezeMoney;
    /**
     * 累积提现佣金
     */
    private BigDecimal finishTakeAmount;
    /**
     * 关注人数
     */
    private Integer favCount;


    /**
     * 省ID
     */
    private Integer province;
    /**
     * 城市ID
     */
    private Integer city;
    /**
     * 区域ID
     */
    private Integer district;
    /**
     * 店铺地址
     */
    private String address;
    /**
     * 门牌号
     */
    private String houseNo;
    /**
     * 营业执照 图片
     */
    private String businessLicense;

    /**
     * 食品经营许可 图片
     */
    private String foodBusinessLicense;
    /**
     * 法人身份证 正面
     */
    private String cardFront;
    /**
     * 法人身份证 反面
     */
    private String cardOpposite;


    private String loginName;

    private String customerTel;

}
