package com.hz.healthy.web.system.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.patient.IHHospitalArchivesService;
import com.hz.healthy.domain.patient.model.HHospitalArchivesModel;
import com.hz.healthy.domain.patient.model.HHospitalArchivesQueryModel;
import com.hz.healthy.domain.store.IHHospitalStoreService;
import com.hz.healthy.domain.store.IHStoreUserService;
import com.hz.healthy.domain.store.model.HHospitalStoreModel;
import com.hz.healthy.domain.store.model.HHospitalStoreQueryModel;
import com.hz.healthy.web.system.dto.SysAnnouncementSendDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.constant.CommonSendStatus;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.anountcement.ISysAnnouncementSendService;
import com.hz.ocean.system.service.anountcement.ISysAnnouncementService;
import com.hz.ocean.system.service.anountcement.model.SysAnnouncementModel;
import com.hz.ocean.system.service.anountcement.model.SysAnnouncementSendModel;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.web.core.utils.XSSUtils;
import com.hz.ocean.web.core.utils.jwt.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author hyhuang
 * @Title: Controller
 * @Description: 系统通告表
 * @date 2022-03-27 8:03 PM
 */
@RestController
@RequestMapping("/sys/annountCement")
@Slf4j
public class SysAnnouncementController extends WebBaseController {
    @Autowired
    private ISysAnnouncementService sysAnnouncementService;
    @Autowired
    private ISysAnnouncementSendService sysAnnouncementSendService;

    @Autowired
    private IHHospitalStoreService hospitalStoreService;
    @Autowired
    private IHHospitalArchivesService hospitalArchivesService;

    /**
     * 分页列表查询
     *
     * @param query
     * @param req
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RespResult<ListResponse<SysAnnouncementModel>> queryPageList(SysAnnouncementModel query,
                                                                        HttpServletRequest req) {
        RespResult<ListResponse<SysAnnouncementModel>> result = new RespResult<ListResponse<SysAnnouncementModel>>();
        query.setDelFlag(CommonConstant.DEL_FLAG_0.toString());
        query.setTenantId(getTenantId());
        //update-end-author:lvdandan date:20211229 for: sqlserver mssql-jdbc 8.2.2.jre8版本下系统公告列表查询报错 查询SQL中生成了两个create_time DESC；故注释此段代码
        ListResponse<SysAnnouncementModel> pageList = sysAnnouncementService.pageQuery(query);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }


    /**
     * 分页列表查询
     *
     * @param query
     * @return
     */
    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public RespResult<ListResponse<SysAnnouncementSendDTO>> queryPageUserList(SysAnnouncementSendModel query) {
        if (ObjectUtil.isNull(query.getAnntId())||query.getAnntId()==0L) {
            return RespResult.OK(ListResponse.empty());
        }
        query.setTenantId(getTenantId());
        query.setDelFlag(CommonConstant.DEL_FLAG_0.toString());
        ListResponse<SysAnnouncementSendModel> pageList = sysAnnouncementSendService.pageQuery(query);

        List<SysAnnouncementSendDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<Long> patientId = new ArrayList<>();
            for (SysAnnouncementSendModel s : pageList.getRecords()) {
                patientId.add(Long.valueOf(s.getUserId()));
            }
            Map<Long, String> patientMap = hospitalArchivesService.mapNameByIds(patientId, getOrgCode());
            pageList.getRecords().forEach(p -> {
                SysAnnouncementSendDTO dto = BeanUtil.toBean(p, SysAnnouncementSendDTO.class);
                dto.setUserName(patientMap.getOrDefault(Long.valueOf(p.getUserId()), ""));
                dataList.add(dto);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 添加
     *
     * @param sysAnnouncement
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public RespResult<SysAnnouncementModel> add(@RequestBody SysAnnouncementModel sysAnnouncement) {
        try {
            // update-begin-author:liusq date:20210804 for:标题处理xss攻击的问题
            String title = XSSUtils.striptXSS(sysAnnouncement.getTitile());
            sysAnnouncement.setTitile(title);
            sysAnnouncement.setCancelTime(new Date());
            sysAnnouncement.setCreateBy(getLoginUser().getRealname());
            sysAnnouncement.setDelFlag(CommonConstant.DEL_FLAG_0.toString());
            sysAnnouncement.setSendStatus(CommonSendStatus.UNPUBLISHED_STATUS_0);//未发布
            sysAnnouncement.setOrgCode(getOrgCode());
            sysAnnouncement.setTenantId(getTenantId());

            if (sysAnnouncement.getMsgType().equals(CommonConstant.MSG_TYPE_ALL)) {
                List<HHospitalStoreModel> datalist = hospitalStoreService.selectList(HHospitalStoreQueryModel.builder()
                        .orgCode(getOrgCode())
                        .tenantId(getTenantId())
                        .build());
                if (CollectionUtils.isNotEmpty(datalist)) {
                    StringBuffer sb = new StringBuffer();
                    for (HHospitalStoreModel storeModel : datalist) {
                        sb.append(storeModel.getPatientId()).append(",");
                    }
                    sysAnnouncement.setUserIds(sb.toString());
                }
            } else if (StringUtils.isBlank(sysAnnouncement.getUserIds())) {
                return RespResult.error("请选择接收用户");
            }
            sysAnnouncementService.saveAnnouncement(sysAnnouncement);
            return RespResult.OK("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
    }

    /**
     * 编辑
     *
     * @param sysAnnouncement
     * @return
     */
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public RespResult<SysAnnouncementModel> eidt(@RequestBody SysAnnouncementModel sysAnnouncement) {
        RespResult<SysAnnouncementModel> result = new RespResult<SysAnnouncementModel>();
        SysAnnouncementModel sysAnnouncementEntity = sysAnnouncementService.getById(sysAnnouncement.getId());
        if (sysAnnouncementEntity == null) {
            return RespResult.error("未找到对应实体");
        } else {
            // update-begin-author:liusq date:20210804 for:标题处理xss攻击的问题
            String title = XSSUtils.striptXSS(sysAnnouncement.getTitile());
            sysAnnouncement.setTitile(title);
            // update-end-author:liusq date:20210804 for:标题处理xss攻击的问题
            boolean ok = sysAnnouncementService.upDateAnnouncement(sysAnnouncement);
            //TODO 返回false说明什么？
            if (ok) {
                result.success("修改成功!");
            }
        }

        return result;
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @Transactional
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public RespResult<SysAnnouncementModel> delete(@PathVariable(name = "id", required = true) Long id) {
        RespResult<SysAnnouncementModel> result = new RespResult<SysAnnouncementModel>();
        SysAnnouncementModel sysAnnouncement = sysAnnouncementService.getById(id);
        if (sysAnnouncement == null) {
            return RespResult.error("未找到对应实体");
        } else {
            List<SysAnnouncementSendModel> dataList = sysAnnouncementSendService.list(SysAnnouncementSendModel.builder()
                    .anntId(sysAnnouncement.getId())
                    .build());
            if (CollectionUtils.isNotEmpty(dataList)) {
                for (SysAnnouncementSendModel sendModel : dataList) {
                    sysAnnouncementSendService.updateById(SysAnnouncementSendModel.builder()
                            .id(sendModel.getId())
                            .delFlag(CommonConstant.DEL_FLAG_1.toString())
                            .build());
                }
            }
            sysAnnouncement.setDelFlag(CommonConstant.DEL_FLAG_1.toString());
            sysAnnouncementService.updateById(sysAnnouncement);
            result.success("删除成功!");
        }
        return result;
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @Transactional
    @RequestMapping(value = "/user/delete/{id}", method = RequestMethod.DELETE)
    public RespResult<SysAnnouncementModel> userDelete(@PathVariable(name = "id", required = true) Long id) {
        SysAnnouncementSendModel sysAnnouncement = sysAnnouncementSendService.getById(id);
        if (sysAnnouncement == null) {
            return RespResult.error("未找到对应实体");
        } else {
            if (CommonConstant.HAS_READ_FLAG.equals(sysAnnouncement.getReadFlag())) {
                return RespResult.error("用户己读，不能删除");
            }
            sysAnnouncementSendService.updateById(SysAnnouncementSendModel.builder()
                    .id(sysAnnouncement.getId())
                    .delFlag(CommonConstant.DEL_FLAG_1.toString())
                    .build());
            return RespResult.OK("删除成功!");
        }
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public RespResult<SysAnnouncementModel> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        RespResult<SysAnnouncementModel> result = new RespResult<SysAnnouncementModel>();
        if (ids == null || "".equals(ids.trim())) {
            return RespResult.error("参数不识别！");
        } else {
            String[] id = ids.split(",");
            for (int i = 0; i < id.length; i++) {
                SysAnnouncementModel announcement = sysAnnouncementService.getById(Long.valueOf(id[i]));
                announcement.setDelFlag(CommonConstant.DEL_FLAG_1.toString());
                sysAnnouncementService.updateById(announcement);
            }
            result.success("删除成功!");
        }
        return result;
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public RespResult<SysAnnouncementModel> queryById(@RequestParam(name = "id", required = true) Long id) {
        RespResult<SysAnnouncementModel> result = new RespResult<SysAnnouncementModel>();
        SysAnnouncementModel sysAnnouncement = sysAnnouncementService.getById(id);
        if (sysAnnouncement == null) {
            return RespResult.error("未找到对应实体");
        } else {
            result.setResult(sysAnnouncement);
            result.setSuccess(true);
        }
        return result;
    }

    /**
     * 更新发布操作
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/doReleaseData", method = RequestMethod.GET)
    public RespResult<SysAnnouncementModel> doReleaseData(@RequestParam(name = "id", required = true) Long id, HttpServletRequest request) {
        RespResult<SysAnnouncementModel> result = new RespResult<SysAnnouncementModel>();
        SysAnnouncementModel sysAnnouncement = sysAnnouncementService.getById(id);
        if (sysAnnouncement == null) {
            return RespResult.error("未找到对应实体");
        } else {
            sysAnnouncement.setSendStatus(CommonSendStatus.PUBLISHED_STATUS_1);//发布中
            sysAnnouncement.setSendTime(new Date());
            String currentUserName = JwtUtil.getUserNameByToken(request);
            sysAnnouncement.setSender(currentUserName);
            sysAnnouncementService.updateById(sysAnnouncement);
            result.success("该系统通知发布成功");
        }

        return result;
    }

    /**
     * 更新撤销操作
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/doReovkeData", method = RequestMethod.GET)
    public RespResult<SysAnnouncementModel> doReovkeData(@RequestParam(name = "id", required = true) Long id, HttpServletRequest request) {
        RespResult<SysAnnouncementModel> result = new RespResult<SysAnnouncementModel>();
        SysAnnouncementModel sysAnnouncement = sysAnnouncementService.getById(id);
        if (sysAnnouncement == null) {
            return RespResult.error("未找到对应实体");
        } else {
            sysAnnouncement.setSendStatus(CommonSendStatus.REVOKE_STATUS_2);//撤销发布
            sysAnnouncement.setCancelTime(new Date());
            sysAnnouncementService.updateById(sysAnnouncement);
            result.success("该系统通知撤销成功");
        }

        return result;
    }

    /**
     * @return
     * @功能：补充用户数据，并返回系统消息
     */
    @RequestMapping(value = "/listByUser", method = RequestMethod.GET)
    public RespResult<Map<String, Object>> listByUser(@RequestParam(required = false, defaultValue = "5") Integer pageSize) {
        RespResult<Map<String, Object>> result = new RespResult<Map<String, Object>>();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String userId = sysUser.getId();
        // 1.将系统消息补充到用户通告阅读标记表中
        SysAnnouncementModel querySaWrapper = new SysAnnouncementModel();
        querySaWrapper.setMsgType(CommonConstant.MSG_TYPE_ALL); // 全部人员
        querySaWrapper.setDelFlag(CommonConstant.DEL_FLAG_0.toString());  // 未删除
        querySaWrapper.setSendStatus(CommonConstant.HAS_SEND); //已发布
        //update-begin--Author:liusq  Date:20210108 for：[JT-424] 【开源issue】bug处理--------------------
        //querySaWrapper.notInSql(SysAnnouncement::getId,"select annt_id from sys_announcement_send where user_id='"+userId+"'");
        //update-begin--Author:liusq  Date:20210108  for： [JT-424] 【开源issue】bug处理--------------------
        List<SysAnnouncementModel> announcements = sysAnnouncementService.list(querySaWrapper);
        if (announcements.size() > 0) {
            for (int i = 0; i < announcements.size(); i++) {
                //update-begin--Author:wangshuai  Date:20200803  for： 通知公告消息重复LOWCOD-759--------------------
                //因为websocket没有判断是否存在这个用户，要是判断会出现问题，故在此判断逻辑
                SysAnnouncementSendModel query = new SysAnnouncementSendModel();
                query.setAnntId(announcements.get(i).getId());
                query.setUserId(userId);
                SysAnnouncementSendModel one = sysAnnouncementSendService.getOne(query);
                if (null == one) {
                    log.info("listByUser接口新增了SysAnnouncementSend：pageSize{}：" + pageSize);
                    SysAnnouncementSendModel announcementSend = new SysAnnouncementSendModel();
                    announcementSend.setAnntId(announcements.get(i).getId());
                    announcementSend.setUserId(userId);
                    announcementSend.setReadFlag(CommonConstant.NO_READ_FLAG);
                    sysAnnouncementSendService.save(announcementSend);
                    log.info("announcementSend.toString()", announcementSend.toString());
                }
                //update-end--Author:wangshuai  Date:20200803  for： 通知公告消息重复LOWCOD-759------------
            }
        }
        // 2.查询用户未读的系统消息
        SysAnnouncementModel page = new SysAnnouncementModel();
        page.setUserIds(userId);
        page.setMsgCategory("1");
        page.setPageNo(0);
        page.setPageSize(pageSize);
        ListResponse<SysAnnouncementModel> anntMsgList = sysAnnouncementService.pageQuery(page);//通知公告消息

        page.setMsgCategory("2");
        ListResponse<SysAnnouncementModel> sysMsgList = sysAnnouncementService.pageQuery(page);//系统消息
        Map<String, Object> sysMsgMap = new HashMap<String, Object>();
        sysMsgMap.put("sysMsgList", sysMsgList.getRecords());
        sysMsgMap.put("sysMsgTotal", sysMsgList.getTotal());
        sysMsgMap.put("anntMsgList", anntMsgList.getRecords());
        sysMsgMap.put("anntMsgTotal", anntMsgList.getTotal());
        result.setSuccess(true);
        result.setResult(sysMsgMap);
        return result;
    }


    /**
     * 通告查看详情页面（用于第三方APP）
     *
     * @param modelAndView
     * @param id
     * @return
     */
    @GetMapping("/show/{id}")
    public ModelAndView showContent(ModelAndView modelAndView, @PathVariable("id") String id, HttpServletRequest request) {
//        SysAnnouncementModel announcement = sysAnnouncementService.getById(id);
//        if (announcement != null) {
//            boolean tokenOK = false;
//            try {
//                // 验证Token有效性
//                tokenOK = TokenUtils.verifyToken(request, sysBaseAPI, redisUtil);
//            } catch (Exception ignored) {
//            }
//            // 判断是否传递了Token，并且Token有效，如果传了就不做查看限制，直接返回
//            // 如果Token无效，就做查看限制：只能查看已发布的
//            if (tokenOK || ANNOUNCEMENT_SEND_STATUS_1.equals(announcement.getSendStatus())) {
//                modelAndView.addObject("data", announcement);
//                modelAndView.setViewName("announcement/showContent");
//                return modelAndView;
//            }
//        }
        modelAndView.setStatus(HttpStatus.NOT_FOUND);
        return modelAndView;
    }

}
