package com.hz.healthy.web.system.controller;

import com.hz.healthy.domain.device.IHApiLogService;
import com.hz.healthy.domain.device.model.HApiLogModel;
import com.hz.healthy.domain.device.model.HApiLogQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 接口日志Controller
 * 
 * @author hyhuang
 * @date 2023-09-24
 */
@RestController
@RequestMapping("/system/log")
public class HApiLogController
{
    @Autowired
    private IHApiLogService hApiLogService;

    /**
     * 查询接口日志列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HApiLogModel>> pageQuery(HApiLogQueryModel queryModel)  {
        RespResult<ListResponse<HApiLogModel>> result = new RespResult<ListResponse<HApiLogModel>>();
        ListResponse<HApiLogModel> pageList = hApiLogService.pageQuery(queryModel);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }



    /**
     * 获取接口日志详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id)
    {
        return RespResult.OK(hApiLogService.getById(id));
    }

    /**
     * 新增接口日志
     */
    //@PreAuthorize("@ss.hasPermi('system:log:add')")
    //@Log(title = "接口日志", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HApiLogModel hApiLogModel)
    {
        hApiLogService.insert(hApiLogModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改接口日志
     */
    @PutMapping
    public RespResult edit(@RequestBody  HApiLogModel hApiLogModel)
    {
        hApiLogService.updateById(hApiLogModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除接口日志
     */
	@DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids)
    {
        hApiLogService.deleteByIds(ids);
        return RespResult.OK();
    }
}
