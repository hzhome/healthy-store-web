package com.hz.healthy.web.his.controller;

import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.doctor.model.HEmployeeModel;
import com.hz.healthy.domain.store.IHStoreUserService;
import com.hz.healthy.domain.store.model.HStoreUserModel;
import com.hz.healthy.domain.store.model.HStoreUserQueryModel;
import com.hz.healthy.web.his.dto.request.AccountRoleReqDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.PasswordUtil;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.auth.ISysUserRoleService;
import com.hz.ocean.system.service.auth.ISysUserService;
import com.hz.ocean.system.service.auth.model.SysUserModel;
import com.hz.ocean.system.service.auth.model.SysUserRoleModel;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import com.hz.ocean.uac.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 医生信息Controller
 *
 * @author hyhuang
 * @date 2023-01-17
 */
@RestController
@RequestMapping("/his/account/role")
public class HHisAccountRoleController extends WebBaseController {


    @Autowired
    private IHEmployeeService iHEmployeeService;

    @Autowired
    private ISysUserRoleService iSysUserRoleService;
    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;
    @Autowired
    private IHStoreUserService ihStoreUserService;



    /**
     * 新增帐号信息
     */
    @PostMapping
    public RespResult add(@Validated @RequestBody AccountRoleReqDTO fromReq) {
        if (StringUtils.isBlank(fromReq.getPassword())) {
            fromReq.setPassword("123456");
        }
        createAccount(fromReq, getLoginUser());
        return RespResult.OK("创建登录帐号成功");
    }

    /**
     * 新增帐号信息
     */
    @PostMapping(value = "/get")
    public RespResult get(@RequestBody AccountRoleReqDTO fromReq) {
        AccountRoleReqDTO roleReq = new AccountRoleReqDTO();
        //roleReq.setRoleId("");
        HEmployeeModel HEmployeeModel = iHEmployeeService.getById(fromReq.getUserId());
        String userId = HEmployeeModel.getUserId();
        if (StringUtils.isNoneBlank(userId)) {
            SysUserRoleModel userRoleModel = new SysUserRoleModel();
            userRoleModel.setUserId(userId);
            //SysUserRoleModel roleModel = iSysUserRoleService.getOne(userRoleModel);
            // roleReq.setRoleId(roleModel.getRoleId());
            roleReq.setSource("1");
            roleReq.setSurePassword("");
            roleReq.setPassword("");
            roleReq.setUserId(fromReq.getUserId());
            roleReq.setLoginName(HEmployeeModel.getMobilePhone());
        }
        return RespResult.OK(roleReq);
    }


    public void createAccount(AccountRoleReqDTO fromReq, LoginUser loginUser) {
        Long tableId = 0L;
        HEmployeeModel doctorModel = iHEmployeeService.getById(fromReq.getUserId());
        if (ObjectUtil.isNull(doctorModel)) {
            throw new BizException("人员信息不存在");
        }
        String userId = doctorModel.getUserId();
        String phone = doctorModel.getMobilePhone();
        tableId = doctorModel.getId();
        String password = fromReq.getPassword();
        String salt = PasswordUtil.randomGen(8);
        String passwordEncode = PasswordUtil.encrypt(phone, password, salt);
        if (StringUtils.isBlank(userId)) {
            userId = seqNoAutoService.buildUserId();
            //一个手机只能一条记录，员工可以在多个机构流动就职
            HStoreUserModel userModel = ihStoreUserService.getOne(HStoreUserQueryModel.builder()
                    .phone(phone)
                    .build());
            if (ObjectUtil.isNull(userModel)) {
                userModel = new HStoreUserModel();
                userModel.setPhone(phone);
                userModel.setSalt(salt);
                userModel.setLoginName(phone);
                userModel.setRealName(doctorModel.getName());
                userModel.setPassword(passwordEncode);
                userModel.setStatus(CommonConstant.STATUS_1);
                userModel.setOrgCode(loginUser.getOrgCode());
                userModel.setTenantId(loginUser.getTenantId());
                userModel.setIsAdmin(0);
                userModel.setBirthday(doctorModel.getBirthday());
                userModel.setCreateTime(new Date());//设置创建时间
                userModel.setUserId(userId);
                userModel.setStoreCode(getStoreCode());
                ihStoreUserService.insert(userModel);
            }
            iHEmployeeService.updateById(HEmployeeModel.builder()
                    .id(tableId)
                    .userId(userId)
                    .build());
        }
    }
}
