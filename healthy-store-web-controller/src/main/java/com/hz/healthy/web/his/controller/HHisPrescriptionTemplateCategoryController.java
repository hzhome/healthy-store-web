package com.hz.healthy.web.his.controller;

import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.device.model.HDeviceInfoModel;
import com.hz.healthy.domain.device.model.HDeviceInfoQueryModel;
import com.hz.healthy.domain.his.IHHisPrescriptionTemplateCategoryService;
import com.hz.healthy.domain.his.model.HHisPrescriptionTemplateCategoryModel;
import com.hz.healthy.domain.his.model.HHisPrescriptionTemplateCategoryQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.model.KeyStrModel;
import com.hz.ocean.common.model.Tree2Model;
import com.hz.ocean.common.model.TreeModel;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 处方模板分类Controller
 *
 * @author ruoyi
 * @date 2024-11-20
 */
@RestController
@RequestMapping("/his/prescription/template/category")
public class HHisPrescriptionTemplateCategoryController extends WebBaseController {
    @Autowired
    private IHHisPrescriptionTemplateCategoryService hHisPrescriptionTemplateCategoryService;

    /**
     * 查询处方模板分类列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisPrescriptionTemplateCategoryModel>> pageQuery(HHisPrescriptionTemplateCategoryQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantIdList(Lists.newArrayList(getTenantId(), 0L));
        queryModel.setEyId(getDoctorId());
        ListResponse<HHisPrescriptionTemplateCategoryModel> pageList = hHisPrescriptionTemplateCategoryService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }


//    /**
//     * 获取处方模板分类详细信息
//     */
//    @GetMapping(value = "/{id}")
//    public RespResult getInfo(@PathVariable("id") Long id) {
//        return RespResult.OK(hHisPrescriptionTemplateCategoryService.getById(id));
//    }

    /**
     * 新增处方模板分类
     */
    //@PreAuthorize("@ss.hasPermi('system:category:add')")
    //@Log(title = "处方模板分类", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HHisPrescriptionTemplateCategoryModel hHisPrescriptionTemplateCategoryModel) {
        if (ObjectUtil.isNull(hHisPrescriptionTemplateCategoryModel.getParentId())) {
            return RespResult.error("父分类缺少，添加无效");
        }
        if (StringUtils.isBlank(hHisPrescriptionTemplateCategoryModel.getName())) {
            return RespResult.error("分类名称不能为空，添加无效");
        }
        hHisPrescriptionTemplateCategoryModel.setCategoryType("2");
        hHisPrescriptionTemplateCategoryModel.setEyId(getDoctorId());
        hHisPrescriptionTemplateCategoryModel.setTenantId(getTenantId());
        hHisPrescriptionTemplateCategoryModel.setOrgCode(getOrgCode());
        hHisPrescriptionTemplateCategoryModel.setCreateTime(new Date());
        hHisPrescriptionTemplateCategoryModel.setCode(System.currentTimeMillis() + "");
        hHisPrescriptionTemplateCategoryService.insert(hHisPrescriptionTemplateCategoryModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改处方模板分类
     */
    @PutMapping
    public RespResult edit(@RequestBody HHisPrescriptionTemplateCategoryModel hHisPrescriptionTemplateCategoryModel) {
        hHisPrescriptionTemplateCategoryService.updateById(hHisPrescriptionTemplateCategoryModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除处方模板分类
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hHisPrescriptionTemplateCategoryService.deleteByIds(ids);
        return RespResult.OK();
    }


    /**
     * 查询设备列表
     */
    @GetMapping("/selectList")
    public RespResult<List<KeyStrModel>> selectList(HHisPrescriptionTemplateCategoryQueryModel queryModel) {
        List<KeyStrModel> dataList = new ArrayList<>();
        queryModel.setTenantIdList(Lists.newArrayList(getTenantId(), 0L));
        queryModel.setEyId(getDoctorId());
        List<HHisPrescriptionTemplateCategoryModel> pageList = hHisPrescriptionTemplateCategoryService.selectList(queryModel);
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                dataList.add(KeyStrModel.builder()
                        .key(p.getCode())
                        .label(p.getName())
                        .build());
            });
        }
        return RespResult.OK(dataList);
    }

    @GetMapping("/queryTreeList")
    public RespResult<List<Tree2Model>> treeDataSource(HHisPrescriptionTemplateCategoryQueryModel queryModel) {
        List<Tree2Model> dataList = new ArrayList<>();
        queryModel.setTenantIdList(Lists.newArrayList(getTenantId(), 0L));
        List<HHisPrescriptionTemplateCategoryModel> pageList = hHisPrescriptionTemplateCategoryService.selectList(queryModel);
        for (HHisPrescriptionTemplateCategoryModel p : pageList) {
            String id = String.valueOf(p.getId());
            dataList.add(Tree2Model.builder()
                    .id(id)
                    .isLeaf(false)
                    .title(p.getName())
                    .parentId(p.getParentId().toString())
                    .key(id)
                    .build());
        }
        return RespResult.OK(Tree2Model.getChildPerms(dataList,"0"));
    }


}
