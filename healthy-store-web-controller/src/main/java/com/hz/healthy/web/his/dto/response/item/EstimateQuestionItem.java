package com.hz.healthy.web.his.dto.response.item;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/4/13 12:38
 * @ClassName: EstimateOptionItem
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EstimateQuestionItem implements Serializable {

    private Long id;
    /** 1.单选题 2.多选题 3.判断题 4.填空题 5.简答题 */
    private Integer questionType;
    private String title;

    private List<EstimateOptionItem> optionsList;


}
