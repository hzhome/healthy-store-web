package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.healthy.web.his.dto.response.item.WorkHealthyDataToDayItem;
import com.hz.ocean.common.model.KeyVModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/7/24 12:02
 * @ClassName: HHisPatientHealthyDataDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HHisPatientHealthyDataDTO implements Serializable {

    /** 主键ID */
    private Long id;
    /**
     * 护理级别名称 记录当前的
     */
    private String nurseRank;
    /**
     * 患者id
     */
    private Long patientId;
    /**
     * 姓名
     */
    private String patientName;
    /**
     * 患者床位 记录当前的
     */
    private String patientBed;
    /**
     * 长者年纪
     */
    private Integer patientAge;

    /**
     * 长者性别，是中文来的
     */
    private String patientSex;
    /**
     * 数据编码
     */
    private String keyCode;
    /**
     * 数据名称
     */
    private String keyName;

    /**
     * 年月日 yyyy-mm-dd
     */
    private String today;
    /**
     * 创建人员
     */
    private String createUser;
    /**
     * 注册时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date createTime;

    /**
     * 录入来源
     */
    private String inputSource;

    /**
     * 状态是否达标
     */
    private String resultStatus;

    /**
     * 设备号
     */
    private String deviceId;

    /**
     * 注册时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date inputTime;

    /**
     * 业务方标记
     */
    private String busFlag;


    private List<WorkHealthyDataToDayItem> itemList;
}
