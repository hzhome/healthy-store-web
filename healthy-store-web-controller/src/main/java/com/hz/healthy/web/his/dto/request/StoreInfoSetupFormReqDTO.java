package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/2/2 14:21
 * @ClassName: StoreInfoSetupFormReqDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class StoreInfoSetupFormReqDTO implements Serializable {

    private Long id;

    /** 店铺类型10加盟20自营 */
    private String storeType;
    /** 是否总店，0否1是 */
    private Integer isMain;
    /** 配送费 */
    private BigDecimal shippingFee;
    /** 包装费类型0按商品收费1按单收费 */
    private String bagType;
    /** 包装费 */
    private BigDecimal bagPrice;
    /** 店内包装费类型0按商品收费1按单收费 */
    private String storebagType;
    /** 店内包装费 */
    private BigDecimal storebagPrice;
    /** 外卖营业时间 */
    private String deliveryTimeBegin;
    private String deliveryTimeEnd;
    /** 自提营业时间 */
    private String pickTimeBegin;
    private String pickTimeEnd;
    /** 店内营业时间 */
    private String storeTimeBegin;
    private String storeTimeEnd;
    /** 配送范围km */
    private BigDecimal deliveryDistance;
    /** 外卖配送方式 */
    private String deliverySet;
    /** 店内用餐方式 */
    private String storeSet;
    /** 最低消费 */
    private BigDecimal minMoney;
    /** 计算模式10先结账后用餐20先用餐后结账 */
    private String settleType;
    /** 服务费类型0按就餐人数1按桌台收费 */
    private String serviceType;
    /** 服务费 */
    private BigDecimal serviceMoney;
    /** 0定时清台1立即清台 */
    private Integer autoClose;
    /** 0分钟清台 */
    private Integer closeTime;
    /** 商品分类设置10同步主店20分店创建 */
    private Integer categorySet;

    /** 经度 */
    private BigDecimal positionLng;
    /** 纬度 */
    private BigDecimal positionLat;

    private String address;

}
