package com.hz.healthy.web.his.dto.response.medicine;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hyhuang
 * @desc
 * @date 2023/3/28 22:16
 */
@Data
public class HisMedicineFormDTO implements Serializable {

    /**
     * 主键
     */
    private Long id;

    /**
     * 药品名称
     */
    @NotBlank(message = "请输入药品名称")
    private String medicineName;

    /**
     * 药品编码
     */
    @NotBlank(message = "请输入药品编码")
    private String medicineNo;

    /**
     * 生产厂家
     */
    private Long factoryId;

    /**
     * 药品类型
     */
    @NotBlank(message = "请选择药品类型")
    private String medicineType;

    /**
     * 处方类型
     */
    @NotBlank(message = "请选择处方类型")
    private String prescribe;

    /**
     * 单位
     */
    @NotBlank(message = "请输入规格")
    private String unit;

    /**
     * 处方价格
     */
    @NotNull(message = "请输入药品进货价格")
    private BigDecimal price;

    /** 销售价格 */
    @NotNull(message = "请输入药品销售价格")
    private BigDecimal salePrice;
    /**
     * 库存量
     */
    @NotNull(message = "请输入库存量")
    private Integer stock;

    /**
     * 预警值
     */
    private Integer warn;

    /**
     * 换算量
     */
    private Integer conversion;

    /**
     * 状态 正常 停用
     */
    private String status;


    /**
     * $comment
     */
    private String keyword;

    private String standard;

    private String saleStandard;

}
