package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import java.io.Serializable;
/**
 * @Author hyhuang
 * @Date 2024/11/8 13:13
 * @ClassName: HHisMarketingConsultReplyDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisMarketingConsultReplyDTO implements Serializable {

    private Long id;

    private String reply;
}
