package com.hz.healthy.web.his.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.finance.IHHisFinanceIncomeDayService;
import com.hz.healthy.domain.finance.model.HHisFinanceIncomeDayModel;
import com.hz.healthy.domain.finance.model.HHisFinanceIncomeDayQueryModel;
import com.hz.healthy.web.his.dto.request.FinanceQueryDTO;
import com.hz.healthy.web.his.dto.response.FinanceSettledDTO;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/4/9 22:16
 * @ClassName: FinanceController
 * @Description: 资产管理
 * @Version 1.0
 */
@RestController
@RequestMapping("/ch/finance")
public class FinanceController extends WebBaseController {


    @Autowired
    private IHHisFinanceIncomeDayService hHisFinanceIncomeDayService;

    /**
     * 财务管理-资产
     */
    @GetMapping("/settled")
    public RespResult<FinanceSettledDTO> reportSettled(FinanceQueryDTO query) {
        /**0全部 1今天 2昨天*/
        Date now = new Date();
        if ("0".equals(query.getTimeType())) {
            HHisFinanceIncomeDayModel incomeDayModel = hHisFinanceIncomeDayService.sum(getOrgCode(),getTenantId());
            return RespResult.OK(covert(incomeDayModel));
        } else if ("1".equals(query.getTimeType())) {
            String incomeDay = DateUtil.formatDate(now);
            HHisFinanceIncomeDayModel incomeDayModel = hHisFinanceIncomeDayService.getOne(HHisFinanceIncomeDayQueryModel.builder()
                    .incomeDay(incomeDay)
                    .tenantId(getTenantId())
                    .build());
            return RespResult.OK(covert(incomeDayModel));
        } else if ("2".equals(query.getTimeType())) {
            String incomeDay = DateUtil.formatDate(DateUtil.offsetDay(now, -1));
            HHisFinanceIncomeDayModel incomeDayModel = hHisFinanceIncomeDayService.getOne(HHisFinanceIncomeDayQueryModel.builder()
                    .incomeDay(incomeDay)
                    .tenantId(getTenantId())
                    .build());
            return RespResult.OK(covert(incomeDayModel));
        }

        return RespResult.OK(FinanceSettledDTO.builder().build());
    }

    private FinanceSettledDTO covert(HHisFinanceIncomeDayModel incomeDayModel) {
        if (ObjectUtil.isNull(incomeDayModel)) {
            return FinanceSettledDTO.builder()
                    .totalAmount(BigDecimal.ZERO)
                    .incomeAmount(BigDecimal.ZERO)
                    .refundAmount(BigDecimal.ZERO)
                    .waitTakeAmount(BigDecimal.ZERO)
                    .finishTakeAmount(BigDecimal.ZERO)
                    .qty(0)
                    .refundQty(0)
                    .build();
        }
        return FinanceSettledDTO.builder()
                .incomeAmount(NumberUtil.sub(incomeDayModel.getTenantAmount(), incomeDayModel.getRefundAmount()))
                .totalAmount(incomeDayModel.getTotalAmount())
                .refundAmount(incomeDayModel.getRefundAmount())
                .waitTakeAmount(incomeDayModel.getTenantAmount())
                .finishTakeAmount(BigDecimal.ZERO)
                .qty(incomeDayModel.getSaleQty())
                .refundQty(0)
                .build();

    }


}
