package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/11/24 17:38
 * @ClassName: HDeviceInfoBindFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HDeviceInfoBindFormDTO implements Serializable {

    @NotBlank(message = "请输入设备SN")
    private String deviceId;

    /** 设备名称 */
    @NotBlank(message = "请输入设备名称")
    private String deviceName;

    /** 设备通讯状态 */
    private String deviceStatus;


    private String brandName;

    @NotBlank(message = "请选择设备通讯模式")
    private String netType;
    /** 设备类型 */
    @NotBlank(message = "请选择设备类型")
    private String deviceType;

    @NotNull(message = "请输客户信息")
    private Long patientId;
}
