package com.hz.healthy.web.goods.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.goods.IHReportOrderDayService;
import com.hz.healthy.domain.goods.model.HReportOrderDayModel;
import com.hz.healthy.domain.goods.model.HReportOrderDayQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 门店单位日统计Controller
 *
 * @author ruoyi
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/shop/order/day")
public class HReportOrderDayController extends WebBaseController {
    @Autowired
    private IHReportOrderDayService hReportOrderDayService;

    /**
     * 查询门店单位日统计列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HReportOrderDayModel>> pageQuery(HReportOrderDayQueryModel queryModel) {
        RespResult<ListResponse<HReportOrderDayModel>> result = new RespResult<ListResponse<HReportOrderDayModel>>();
        ListResponse<HReportOrderDayModel> pageList = hReportOrderDayService.pageQuery(queryModel);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }

    /**
     * 获取门店单位日统计详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hReportOrderDayService.getById(id));
    }

    /**
     * 新增门店单位日统计
     */
    //@PreAuthorize("@ss.hasPermi('system:day:add')")
    //@Log(title = "门店单位日统计", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HReportOrderDayModel hReportOrderDayModel) {
        hReportOrderDayService.insert(hReportOrderDayModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改门店单位日统计
     */
    @PutMapping
    public RespResult edit(@RequestBody HReportOrderDayModel hReportOrderDayModel) {
        hReportOrderDayService.updateById(hReportOrderDayModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除门店单位日统计
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hReportOrderDayService.deleteByIds(ids);
        return RespResult.OK();
    }
}
