package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.report.IHReportWorkDashboardDayService;
import com.hz.healthy.domain.report.model.HReportWorkDashboardDayModel;
import com.hz.healthy.domain.report.model.HReportWorkDashboardDayQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 医生工作看板Controller
 *
 * @author ruoyi
 * @date 2024-11-26
 */
@RestController
@RequestMapping("/report/work/dashboard/day")
public class HReportWorkDashboardDayController extends WebBaseController {
    @Autowired
    private IHReportWorkDashboardDayService hReportWorkDashboardDayService;

    /**
     * 查询医生工作看板列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HReportWorkDashboardDayModel>> pageQuery(HReportWorkDashboardDayQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HReportWorkDashboardDayModel> pageList = hReportWorkDashboardDayService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }

    /**
     * 获取医生工作看板详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hReportWorkDashboardDayService.getById(id));
    }

    /**
     * 新增医生工作看板
     */
    //@PreAuthorize("@ss.hasPermi('system:day:add')")
    //@Log(title = "医生工作看板", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HReportWorkDashboardDayModel hReportWorkDashboardDayModel) {
        hReportWorkDashboardDayService.insert(hReportWorkDashboardDayModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改医生工作看板
     */
    @PutMapping
    public RespResult edit(@RequestBody HReportWorkDashboardDayModel hReportWorkDashboardDayModel) {
        hReportWorkDashboardDayService.updateById(hReportWorkDashboardDayModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除医生工作看板
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hReportWorkDashboardDayService.deleteByIds(ids);
        return RespResult.OK();
    }
}
