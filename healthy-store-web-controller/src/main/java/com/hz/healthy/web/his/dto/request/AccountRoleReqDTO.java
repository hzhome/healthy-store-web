package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/3/22 18:51
 * @ClassName: AccountRoleReqDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class AccountRoleReqDTO implements Serializable {

    /**
     * 登录账号
     */
    @NotBlank(message = "请输入登录账号")
    private String loginName;

    /**
     * 密码
     */
    //@NotBlank(message = "请输入登录密码")
    private String password;

    /**
     * 密码
     */
   // @NotBlank(message = "请输入确认密码")
    private String surePassword;


//    @NotBlank(message = "请选择角色")
//    private String roleId;

    /**
     * 来自于那个模块
     */
    private String source;


    private Long userId;

}
