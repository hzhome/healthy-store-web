package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.*;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeGroupService;
import com.hz.healthy.domain.doctor.model.HEmployeeGroupModel;
import com.hz.healthy.domain.his.IHHisInhospitalBedService;
import com.hz.healthy.domain.his.IHHisInhospitalInfoService;
import com.hz.healthy.domain.his.IHHisNurseVitalSignsService;
import com.hz.healthy.domain.member.IHMemberService;
import com.hz.healthy.domain.patient.*;
import com.hz.healthy.domain.patient.model.*;
import com.hz.healthy.domain.store.IHHospitalStoreService;
import com.hz.healthy.domain.store.model.HHospitalStoreModel;
import com.hz.healthy.enums.DiseaseTypeEnums;
import com.hz.healthy.enums.NationalityEnum;
import com.hz.healthy.web.his.dto.request.HHisPatientFormDTO;
import com.hz.healthy.web.his.dto.response.HHisPatientDTO;
import com.hz.healthy.web.his.dto.response.HHisPatientSimpleDTO;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.enums.SexEnum;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.ObjectUtils;
import com.hz.ocean.common.utils.SensitiveUtil;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.auth.ISysDictService;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 会员信息Controller
 * 与patient关系
 *
 * @author hyhuang
 * @date 2023-10-06
 */
@RestController
@RequestMapping("/his/patient")
public class HHisPatientController extends WebBaseController {
    @Autowired
    private IHMemberService hzMemberService;
    @Autowired
    private IHHisPatientCaregiveeService hisPatientCaregiveeService;
    @Autowired
    private IHHospitalArchivesService hospitalArchivesService;
    @Autowired
    private IHHospitalStoreService hospitalStoreService;


    @Autowired
    private IHHisInhospitalInfoService hHisInhospitalInfoService;

    @Autowired
    private IHHisInhospitalBedService hisInhospitalBedService;

    @Autowired
    private IHHisNurseVitalSignsService hisNurseVitalSignsService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    @Autowired
    private IHHospitalArchivesDetailService hospitalArchivesDetailService;

    @Autowired
    private ISysDictService sysDictService;

    @Autowired
    private IHHisPatientDoctorService hisPatientDoctorService;
    @Autowired
    private IHEmployeeGroupService hEmployeeGroupService;
    @Autowired
    private IHHisPatientService hisPatientService;

    public static void main(String[] args) {
        //55AA0101023033 55AA0101023538  55AA0101023336
        String hexStr = hex(HexUtil.decodeHex("55AA0101023336"), 4, 2);

        int distanceStr = HexUtil.hexToInt(hexStr);//得出头部到身高探测器之间距离
        //总高是2.15米
        BigDecimal distance = new BigDecimal(distanceStr).divide(new BigDecimal("1000"));
        BigDecimal newDistance = new BigDecimal("2.23").subtract(distance).setScale(2, RoundingMode.HALF_UP);
        System.out.println("身高探测距离 $distanceStr  真实身高距离" + newDistance + " " + distance);


        receiveWeight(HexUtil.decodeHex("01060242060015F65C"));
        receiveWeight(HexUtil.decodeHex("01060242060016167D"));
        receiveWeight(HexUtil.decodeHex("01060242060016278E"));


    }
    static Map<String, BigDecimal> w1 = new HashMap<>();
    static {

        w1.put("0", new BigDecimal("0.0001"));
        w1.put("4", new BigDecimal("0.002"));
        w1.put("8", new BigDecimal("0.05"));
        w1.put("C", new BigDecimal("1"));
        w1.put("1", new BigDecimal("0.0002"));
        w1.put("5", new BigDecimal("0.005"));
        w1.put("9", new BigDecimal("0.1"));
        w1.put("D", new BigDecimal("2"));
        w1.put("2", new BigDecimal("0.0005"));
        w1.put("6", new BigDecimal("0.01"));
        w1.put("A", new BigDecimal("0.2"));
        w1.put("E", new BigDecimal("5"));
        w1.put("3", new BigDecimal("0.001"));
        w1.put("7", new BigDecimal("0.02"));
        w1.put("B", new BigDecimal("0.5"));
    }


    public static BigDecimal getWeightBase(String key){
        return  w1.get(key);
    }

    private static void receiveWeight(byte[]  result) {
        StringBuffer log = new StringBuffer();
        String statusHex= hex(result, 3, 1);
        String status10 = to8BitBinaryString(statusHex);
        String divisionValue = hex(result, 4, 1);
        String divisionNum= hex(result, 5, 3);
        log.append("状态值 ").append(statusHex);
        log.append(" 状态值二进制 ").append(status10);
        log.append(" 分度值 ").append(divisionValue);
        log.append(" 分度数 ").append(divisionNum);
        // 00 00 a7是分度数(直接转成 10 进制)
        int decimal = HexUtil.hexToInt(divisionNum) ;// 转换为十进制
        log.append(" 分度数转换为十进制为: $decimal");
        //分度数*分度值=重量(单位是 kg)
        divisionValue = divisionValue.substring(1, 2);
        log.append(" 分度值代号 ").append(divisionValue);
        BigDecimal weight= getWeightBase(divisionValue).multiply(new BigDecimal(decimal));
        log.append(" 体重 $weight kg");
        System.out.println("体重 "+weight+" kg");

    }

    public static String to8BitBinaryString(String hexValue) {

        int number = Integer.parseInt(hexValue, 16); // 假设输入的整数是10
        String binaryString1 = Integer.toBinaryString(number);
        //System.out.println("转换为二进制字符串：" + binaryString1);

        int index = 1; // 假设要查看的位是第1位
        int bitValue = (number >> index) & 1;
        //System.out.println("第" + index + "位的值是：" + bitValue);


        // 将十六进制转换为二进制字符串
        String binaryString = Integer.toBinaryString(Integer.parseInt(hexValue, 16));
        // 如果二进制不足8位，使用String.format补0
        return binaryString.length() < 8 ? String.format("%8s", binaryString).replace(' ', '0') : binaryString;
    }

    public static String hex(byte[] source, int start, int size) {
        byte[] destination = new byte[size];
        System.arraycopy(source, start, destination, 0, size);
        return HexUtil.encodeHexStr(destination);
    }

    /**
     * 查询会员信息列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisPatientSimpleDTO>> pageQuery(HHospitalArchivesStoreQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HHospitalArchivesModel> pageList = hospitalArchivesService.pageQueryByStore(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<HHisPatientSimpleDTO> dataList = new ArrayList<>();
            pageList.getRecords().forEach(p -> {
                HHisPatientSimpleDTO dto = BeanUtil.toBean(p, HHisPatientSimpleDTO.class);
                dto.setSexText(SexEnum.getEnumToDesc(p.getSex()));
                dto.setAge(DateUtil.ageOfNow(p.getBirthday()));
                dataList.add(dto);
            });
            return RespResult.OK(ListResponse.build(dataList, pageList));
        }
        return RespResult.OK(ListResponse.empty());
    }


    /**
     * 获取用户信息
     * 是读取本机构下的用户信息，
     */
    @GetMapping(value = "/{id}")
    public RespResult<HHisPatientDTO> getInfo(@PathVariable("id") Integer id) {
        HHospitalArchivesModel archivesModel = hospitalArchivesService.getOne(HHospitalArchivesQueryModel.builder()
                .id(id)
                .orgCode(getOrgCode())
                .build());
        HHisPatientDTO dto = BeanUtil.toBean(archivesModel, HHisPatientDTO.class);
        if (ObjectUtil.isNotNull(dto)) {
            dto.setNationText(NationalityEnum.getEnumToDesc(StringUtils.defaultIfBlank(dto.getNation(), "未知")));
            dto.setSexText(SexEnum.getEnumToDesc(dto.getSex()));
            dto.setDiseaseLevel(StringUtils.defaultIfBlank(dto.getDiseaseLevel(), ""));
        }
        return RespResult.OK(dto);
    }

    /**
     * 获取用户信息
     * 是读取本机构下的用户信息，
     */
    @GetMapping(value = "/tag/{id}")
    public RespResult<List<String>> getTagInfo(@PathVariable("id") Long patientId) {
        List<String> dataList = new ArrayList<>();
        List<HHisPatientDoctorModel> doctorModelList = hisPatientDoctorService.selectList(HHisPatientDoctorQueryModel.builder()
                .patientId(patientId)
                .build());
        if (CollectionUtils.isNotEmpty(doctorModelList)) {
            List<String> groupIds = new ArrayList<>();
            doctorModelList.forEach(p -> {
                if (StringUtils.isNotBlank(p.getGroupId())) {
                    groupIds.addAll(JSON.parseArray(p.getGroupId(), String.class));
                }
            });
            List<HEmployeeGroupModel> groupModelList = hEmployeeGroupService.listByGroupId(groupIds);
            if (org.apache.commons.collections.CollectionUtils.isNotEmpty(groupModelList)) {
                groupModelList.forEach(p -> {
                    if (!dataList.contains(p.getGroupName())) {
                        dataList.add(p.getGroupName());
                    }
                });
            }
        }
        return RespResult.OK(dataList);
    }

    /**
     * 查询会员被护理人员信息列表
     */
    @GetMapping("/list/caregivee")
    public RespResult<ListResponse<HHisPatientCaregiveeModel>> caregivee(HHisPatientCaregiveeQueryModel queryModel) {
        ListResponse<HHisPatientCaregiveeModel> pageList = hisPatientCaregiveeService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }


    /**
     * 社区版
     * 客户档案-tab1-基本信息
     *
     * @return
     */
    @AutoDict
    @GetMapping(value = "/base/info/community/{patientId}")
    public RespResult<HHisPatientDTO> baseInfoCommunity(@PathVariable("patientId") Long patientId) {
        HHospitalArchivesModel hHospitalArchivesModel = hospitalArchivesService.getByPatientId(patientId);
        //获得当前客户或长者
        HHisPatientDTO patient = BeanUtil.toBean(hHospitalArchivesModel, HHisPatientDTO.class);
        patient.setDrugAllergyList(new ArrayList<>());
        List<HHospitalArchivesDetailModel> detailList = hospitalArchivesDetailService.selectList(HHospitalArchivesDetailQueryModel.builder()
                .patientId(patientId)
                .build());
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(detailList)) {
            Map<String, List<HHospitalArchivesDetailModel>> diseaseMap = detailList.stream().collect(Collectors.groupingBy(HHospitalArchivesDetailModel::getDiseaseFlag));
            if (diseaseMap.containsKey(DiseaseTypeEnums.D1.getCode())) {
                List<HHospitalArchivesDetailModel> list = diseaseMap.get(DiseaseTypeEnums.D1.getCode());
                List<String> strList = list.stream().map(HHospitalArchivesDetailModel::getContent).collect(Collectors.toList());
                patient.setBaseDisease(String.join(",", strList));
            }
            if (diseaseMap.containsKey(DiseaseTypeEnums.D6.getCode())) {
                List<HHospitalArchivesDetailModel> list = diseaseMap.get(DiseaseTypeEnums.D6.getCode());
                List<String> strList = list.stream().map(HHospitalArchivesDetailModel::getContent).collect(Collectors.toList());
                for (String str : strList) {
                    patient.getDrugAllergyList().addAll(JSON.parseArray(str, String.class));
                }
            }
            if (diseaseMap.containsKey(DiseaseTypeEnums.D2.getCode())) {
                List<HHospitalArchivesDetailModel> list = diseaseMap.get(DiseaseTypeEnums.D2.getCode());
                patient.setSurgicalName(list.get(0).getName());
                patient.setSurgicalTime(DateUtil.formatDate(list.get(0).getRecordTime()));
            }
            if (diseaseMap.containsKey(DiseaseTypeEnums.D3.getCode())) {
                List<HHospitalArchivesDetailModel> list = diseaseMap.get(DiseaseTypeEnums.D3.getCode());
                patient.setTraumaName(list.get(0).getName());
                patient.setTraumaTime(DateUtil.formatDate(list.get(0).getRecordTime()));
            }
            if (diseaseMap.containsKey(DiseaseTypeEnums.D5.getCode())) {
                List<HHospitalArchivesDetailModel> list = diseaseMap.get(DiseaseTypeEnums.D5.getCode());
                patient.setInheritance(list.get(0).getContent());
            }
            if (diseaseMap.containsKey(DiseaseTypeEnums.D4.getCode())) {
                List<String> f = new ArrayList<>();
                List<HHospitalArchivesDetailModel> list = diseaseMap.get(DiseaseTypeEnums.D4.getCode());
                for (HHospitalArchivesDetailModel pd : list) {
                    f.add(pd.getContent());
                }
                patient.setFatherFamily(String.join(",", f));
            }

            if (diseaseMap.containsKey(DiseaseTypeEnums.D7.getCode())) {
                List<String> w = new ArrayList<>();
                List<HHospitalArchivesDetailModel> list = diseaseMap.get(DiseaseTypeEnums.D7.getCode());
                for (HHospitalArchivesDetailModel pd : list) {
                    w.add(pd.getContent());
                }
                patient.setMotherFamily(String.join(",", w));
            }
        }
        return RespResult.OK(SensitiveUtil.desensitize(patient));
    }


    /**
     * 客户档案-tab1页面-基本信息 保存
     *
     * @return
     */
    @Transactional
    @PostMapping(value = "/base/info/community/save")
    public RespResult saveBaseInfoCommunity(@RequestBody HHisPatientDTO formDto) {
        Long patientId = formDto.getPatientId();
        HHospitalArchivesModel patientModel = hospitalArchivesService.getByPatientId(patientId);
        if (ObjectUtils.isNull(patientModel)) {
            return RespResult.error("客户档案不存在");
        }
        Date now = new Date();
        HHospitalArchivesModel updateArchivesModel = HHospitalArchivesModel.builder()
                .id(patientModel.getId())
                .politicsType(formDto.getPoliticsType())
                .cardNo(formDto.getCardNo())
                .build();
        if (ObjectUtil.isNotNull(formDto.getBirthday())) {
            updateArchivesModel.setBirthday(formDto.getBirthday());
            updateArchivesModel.setAge(DateUtil.ageOfNow(formDto.getBirthday()));
        }
        if (StringUtils.isNotBlank(formDto.getAddress()) && !formDto.getAddress().contains("**")) {
            updateArchivesModel.setAddress(formDto.getAddress());
        }
        updateArchivesModel.setUpdateTime(now);
        updateArchivesModel.setUpdateUser(getLoginUser().getRealname());
        hospitalArchivesService.updateById(updateArchivesModel);


        List<HHospitalArchivesDetailModel> insertList = new ArrayList<>();
        if (StringUtils.isNotBlank(formDto.getBaseDisease())) {
            hospitalArchivesDetailService.deleteById(patientModel.getPatientId(), DiseaseTypeEnums.D1.getCode());
            List<String> baseDiseaseList = StrUtil.split(formDto.getBaseDisease(), ",");
            for (String baseDisease : baseDiseaseList) {
                insertList.add(HHospitalArchivesDetailModel.builder()
                        .content(baseDisease)
                        .archivesCode(patientModel.getArchivesCode())
                        .createTime(now)
                        .orgCode(getOrgCode())
                        .diseaseFlag(DiseaseTypeEnums.D1.getCode())
                        .patientName(patientModel.getRealName())
                        .patientId(patientModel.getPatientId())
                        .tenantId(patientModel.getTenantId())
                        .build());
            }
        }
        if (CollectionUtils.isNotEmpty(formDto.getDrugAllergyList())) {
            hospitalArchivesDetailService.deleteById(patientModel.getPatientId(), DiseaseTypeEnums.D6.getCode());
            insertList.add(HHospitalArchivesDetailModel.builder()
                    .content(JSON.toJSONString(formDto.getDrugAllergyList()))
                    .archivesCode(patientModel.getArchivesCode())
                    .createTime(now)
                    .diseaseFlag(DiseaseTypeEnums.D6.getCode())
                    .orgCode(getOrgCode())
                    .patientName(patientModel.getRealName())
                    .patientId(patientModel.getPatientId())
                    .tenantId(patientModel.getTenantId())
                    .build());
        }
        if (StringUtils.isNotBlank(formDto.getInheritance())) {
            hospitalArchivesDetailService.deleteById(patientModel.getPatientId(), DiseaseTypeEnums.D5.getCode());
            insertList.add(HHospitalArchivesDetailModel.builder()
                    .content(formDto.getInheritance())
                    .archivesCode(patientModel.getArchivesCode())
                    .createTime(now)
                    .diseaseFlag(DiseaseTypeEnums.D5.getCode())
                    .orgCode(getOrgCode())
                    .patientName(patientModel.getRealName())
                    .patientId(patientModel.getPatientId())
                    .tenantId(patientModel.getTenantId())
                    .build());
        }

        if (StringUtils.isNotBlank(formDto.getSurgicalName())) {
            hospitalArchivesDetailService.deleteById(patientModel.getPatientId(), DiseaseTypeEnums.D2.getCode());
            insertList.add(HHospitalArchivesDetailModel.builder()
                    .content(formDto.getSurgicalName())
                    .name(formDto.getSurgicalName())
                    .recordTime(DateUtil.parseDate(formDto.getSurgicalTime()))
                    .archivesCode(patientModel.getArchivesCode())
                    .createTime(now)
                    .diseaseFlag(DiseaseTypeEnums.D2.getCode())
                    .orgCode(getOrgCode())
                    .patientName(patientModel.getRealName())
                    .patientId(patientModel.getPatientId())
                    .tenantId(patientModel.getTenantId())
                    .build());
        }
        if (StringUtils.isNotBlank(formDto.getTraumaName())) {
            hospitalArchivesDetailService.deleteById(patientModel.getPatientId(), DiseaseTypeEnums.D3.getCode());
            insertList.add(HHospitalArchivesDetailModel.builder()
                    .content(formDto.getTraumaName())
                    .name(formDto.getTraumaName())
                    .recordTime(DateUtil.parseDate(formDto.getTraumaTime()))
                    .archivesCode(patientModel.getArchivesCode())
                    .createTime(now)
                    .diseaseFlag(DiseaseTypeEnums.D3.getCode())
                    .orgCode(getOrgCode())
                    .patientName(patientModel.getRealName())
                    .patientId(patientModel.getPatientId())
                    .tenantId(patientModel.getTenantId())
                    .build());
        }

        if (StringUtils.isNotBlank(formDto.getFatherFamily())) {
            hospitalArchivesDetailService.deleteById(patientModel.getPatientId(), DiseaseTypeEnums.D4.getCode());
            List<String> fatherFamilyList = StrUtil.split(formDto.getFatherFamily(), ",");
            for (String fatherFamily : fatherFamilyList) {
                insertList.add(HHospitalArchivesDetailModel.builder()
                        .content(fatherFamily)
                        .archivesCode(patientModel.getArchivesCode())
                        .createTime(now)
                        .orgCode(getOrgCode())
                        .diseaseFlag(DiseaseTypeEnums.D4.getCode())
                        .patientName(patientModel.getRealName())
                        .patientId(patientModel.getPatientId())
                        .tenantId(patientModel.getTenantId())
                        .build());
            }
        }
        if (StringUtils.isNotBlank(formDto.getMotherFamily())) {
            hospitalArchivesDetailService.deleteById(patientModel.getPatientId(), DiseaseTypeEnums.D7.getCode());
            List<String> motherFamilyList = StrUtil.split(formDto.getMotherFamily(), ",");
            for (String motherFamily : motherFamilyList) {
                insertList.add(HHospitalArchivesDetailModel.builder()
                        .content(motherFamily)
                        .archivesCode(patientModel.getArchivesCode())
                        .createTime(now)
                        .orgCode(getOrgCode())
                        .diseaseFlag(DiseaseTypeEnums.D7.getCode())
                        .patientName(patientModel.getRealName())
                        .patientId(patientModel.getPatientId())
                        .tenantId(patientModel.getTenantId())
                        .build());
            }
        }
        hospitalArchivesDetailService.insertBatch(insertList);
        return RespResult.OK("更新成功");
    }


    /**
     * 查询会员信息列表
     */
    @GetMapping("/search")
    public RespResult<List<HHisPatientSimpleDTO>> searchQuery(HHospitalArchivesStoreQueryModel queryModel) {
        List<HHisPatientSimpleDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        queryModel.setPageSize(50);
        ListResponse<HHospitalArchivesModel> pageList = hospitalArchivesService.pageQueryByStore(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            pageList.getRecords().forEach(p -> {
                HHisPatientSimpleDTO dto = BeanUtil.toBean(p, HHisPatientSimpleDTO.class);
                dto.setSexText(SexEnum.getEnumToDesc(p.getSex()));
                dto.setAge(DateUtil.ageOfNow(p.getBirthday()));
                dataList.add(dto);
            });
        }
        return RespResult.OK(dataList);
    }


    @PostMapping(value = "/add")
    public RespResult<String> add(@RequestBody HHisPatientFormDTO form) {
        try {
            createPatient(form);
            return RespResult.OK("添加成功");
        } catch (BizException e) {
            return RespResult.error(e.getMessage());
        }
    }


    private HHospitalArchivesModel createPatient(HHisPatientFormDTO form) {
        Date now = new Date();
        HHisPatientModel patientModel = hisPatientService.getOne(HHisPatientQueryModel.builder()
                .mobilePhone(form.getMobilePhone())
                .orgCode(getOrgCode())
                .build());
        if (ObjectUtil.isNull(patientModel)) {
            patientModel = HHisPatientModel.builder()
                    .realName(form.getRealName())
                    .mobilePhone(form.getMobilePhone())
                    .orgCode(getOrgCode())
                    .createTime(now)
                    .build();
            hisPatientService.insert(patientModel);
        }
        int count = hospitalArchivesService.count(HHospitalArchivesQueryModel.builder()
                .mobilePhone(form.getMobilePhone())
                .orgCode(getOrgCode())
                .tenantId(getTenantId())
                .build());
        if (count > 0) {
            throw new BizException("手机号已存在,不允许重复添加");
        }



        //创建档案
        String archivesCode = System.currentTimeMillis() + "";
        HHospitalArchivesModel archivesModel = HHospitalArchivesModel.builder()
                .archivesCode(archivesCode)
                .patientId(patientModel.getId())
                .realName(patientModel.getRealName())
                .mobilePhone(patientModel.getMobilePhone())
                .status(CommonConstant.STATUS_1)
                .orgCode(getOrgCode())
                .activateBeginTime(new Date())
                .age(DateUtil.ageOfNow(form.getBirthday()))
                .birthday(form.getBirthday())
                .height(form.getHeight())
                .weight(form.getWeight())
                .izMarry(form.getIzMarry())
                .nation(form.getNation())
                .sex(form.getSex())
                .work(StringUtils.defaultIfBlank(form.getWork(),""))
                .address(StringUtils.defaultIfBlank(form.getAddress(), ""))
                .cardNo(StringUtils.defaultIfBlank(form.getCardNo(), ""))
                .hospitalNumber(archivesCode)
                .tenantId(getTenantId())
                .cardNo("")
                .build();
        hospitalArchivesService.insert(archivesModel);
        //关联与门店关系
        hospitalStoreService.insert(HHospitalStoreModel.builder()
                .patientId(patientModel.getId())
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .createTime(now)
                .createUser(CommonConstant.SYSTEM)
                .build());
        return archivesModel;
    }
}
