package com.hz.healthy.web.his.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.device.IHDeviceInfoService;
import com.hz.healthy.domain.device.IHDevicePatientService;
import com.hz.healthy.domain.device.model.HDeviceInfoModel;
import com.hz.healthy.domain.device.model.HDeviceInfoQueryModel;
import com.hz.healthy.domain.device.model.HDevicePatientModel;
import com.hz.healthy.domain.device.model.HDevicePatientQueryModel;
import com.hz.healthy.domain.patient.IHHospitalArchivesService;
import com.hz.healthy.domain.patient.model.HHospitalArchivesModel;
import com.hz.healthy.domain.store.IHStoreInfoService;
import com.hz.healthy.web.his.dto.request.HDeviceInfoBindFormDTO;
import com.hz.healthy.web.his.dto.response.HDeviceInfoDTO;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 门店设备信息Controller
 *
 * @author hyhuang
 * @date 2023-09-24
 */
@RestController
@RequestMapping("/store/ter/device/info")
public class HStoreDeviceInfoController extends WebBaseController {
    @Autowired
    private IHDeviceInfoService hDeviceInfoService;

    @Autowired
    private IHHospitalArchivesService hospitalArchivesService;
    @Autowired
    private IHDevicePatientService hDevicePatientService;
    @Autowired
    private IHStoreInfoService hStoreInfoService;

    /**
     * 查询设备信息列表
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<ListResponse<HDeviceInfoDTO>> pageQuery(HDeviceInfoQueryModel queryModel) {
        queryModel.setTenantId(getTenantId());
        queryModel.setOrgCode(getOrgCode());
        ListResponse<HDeviceInfoDTO> pageList = ListResponse.build(hDeviceInfoService.pageQuery(queryModel), HDeviceInfoDTO.class);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> deviceIdList = pageList.getRecords().stream().map(HDeviceInfoDTO::getDeviceId).collect(Collectors.toList());
            Map<String, String> mapName = hDevicePatientService.mapByDevice(deviceIdList, getTenantId());
            pageList.getRecords().forEach(p -> {
                p.setIsBind(mapName.containsKey(p.getDeviceId()));
                p.setPatientName(mapName.getOrDefault(p.getDeviceId(), "~"));
            });
        }
        return RespResult.OK(pageList);
    }


    /**
     * 获取设备信息详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hDeviceInfoService.getById(id));
    }


    /**
     * 删除设备信息
     */
    @DeleteMapping("/delete/{id}")
    public RespResult remove(@PathVariable(value = "id") Long id) {
        HDeviceInfoModel db = hDeviceInfoService.getById(id);
        if (ObjectUtil.isNull(db)) {
            return RespResult.error("记录不存在");
        }
        HDevicePatientModel patientModel = hDevicePatientService.getOne(HDevicePatientQueryModel.builder().deviceId(db.getDeviceId())
                .tenantId(getTenantId())
                .build());
        if (ObjectUtil.isNull(patientModel)) {
            return RespResult.error("设备绑定信息不存在");
        }
        hDevicePatientService.deleteById(patientModel.getId());
        return RespResult.OK("解除绑定成功");
    }


    /**
     * 查询设备列表
     */
    @GetMapping("/selectList")
    public RespResult<List<KeyLongModel>> selectList(HDeviceInfoQueryModel queryModel) {
        List<KeyLongModel> dataList = new ArrayList<>();
        queryModel.setTenantId(getTenantId());
        List<HDeviceInfoModel> pageList = hDeviceInfoService.selectList(queryModel);
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                dataList.add(KeyLongModel.builder()
                        .key(p.getId())
                        .label(p.getDeviceName())
                        .build());
            });
        }
        return RespResult.OK(dataList);
    }

    /**
     * 启用/禁用
     *
     * @param formDTO
     * @return
     */
    @PostMapping(value = "/changeStatus")
    public RespResult changeStatus(@RequestBody HDeviceInfoQueryModel formDTO) {
        HDeviceInfoModel db = hDeviceInfoService.getById(formDTO.getId());
        if (db == null) {
            return RespResult.error("记录不存在");
        }

        Date dateTime = new Date();
        HDeviceInfoModel po = new HDeviceInfoModel();
        po.setId(db.getId());
        po.setUpdateTime(dateTime);
        po.setUpdateUser(getLoginUser().getRealname());
        if ("1".equals(db.getAuditStatus())) {
            po.setAuditStatus(CommonConstant.STATUS_0);
        } else {
            po.setAuditStatus(CommonConstant.STATUS_1);
        }
        hDeviceInfoService.updateById(po);
        return RespResult.OK("变更成功");
    }


    /**
     * 启用/禁用
     *
     * @param bindForm
     * @return
     */
    @Transactional
    @PostMapping(value = "/bind")
    public RespResult bind(@Validated @RequestBody HDeviceInfoBindFormDTO bindForm) {
        String deviceId = bindForm.getDeviceId().trim();
        HDeviceInfoModel db = hDeviceInfoService.getOne(HDeviceInfoQueryModel.builder()
                .deviceId(deviceId)
                .build());
        HHospitalArchivesModel archivesModel = hospitalArchivesService.getByPatientId(bindForm.getPatientId(), getOrgCode());
        if (ObjectUtil.isNull(archivesModel)) {
            return RespResult.error("客户信息不存在,请确认!");
        }

        Date now = new Date();
        if (ObjectUtil.isNotNull(db)) {
            int count = hDevicePatientService.count(HDevicePatientQueryModel.builder()
                    .deviceId(deviceId)
                    .build());
            if (count > 0) {
                return RespResult.error("设备已绑定");
            }
            hDevicePatientService.insert(HDevicePatientModel.builder()
                    .createTime(now)
                    .createUser(getLoginUser().getRealname())
                    .deviceId(deviceId)
                    .patientId(bindForm.getPatientId())
                    .patientName(archivesModel.getRealName())
                    .deviceSeq(deviceId)
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .build());

            hDeviceInfoService.updateById(HDeviceInfoModel.builder()
                    .id(db.getId())
                    .brandName(bindForm.getBrandName())
                    .deviceType(bindForm.getDeviceType())
                    .netType(bindForm.getNetType())
                    .build());
        } else {
            HDeviceInfoModel po = new HDeviceInfoModel();
            po.setCreateTime(now);
            po.setCreateUser(getLoginUser().getRealname());
            po.setAuditStatus(CommonConstant.STATUS_1);
            po.setDeviceId(bindForm.getDeviceId().trim());
            po.setDeviceType(bindForm.getDeviceType());
            po.setBrandName(bindForm.getBrandName());
            po.setDeviceName(StringUtils.isBlank(bindForm.getDeviceName()) ? bindForm.getDeviceId() : bindForm.getDeviceName());
            po.setOrgCode(getOrgCode());
            po.setTenantId(getTenantId());
            hDeviceInfoService.insert(po);

            hDevicePatientService.insert(HDevicePatientModel.builder()
                    .createTime(now)
                    .createUser(getLoginUser().getRealname())
                    .deviceId(deviceId)
                    .patientId(bindForm.getPatientId())
                    .patientName(archivesModel.getRealName())
                    .deviceSeq(deviceId)
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .build());
        }
        return RespResult.OK("绑定成功");
    }
}
