package com.hz.healthy.web.his.dto.response;

import cn.hutool.core.util.DesensitizedUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import com.hz.ocean.common.aspect.Sensitive;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/3/7 18:29
 * @ClassName: HHisPatientDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientDTO implements Serializable {

    /** 主键 */
    private Long id;

    private Long patientId;
    /** 姓名 */
    private String realName;
    /** 手机 */
    @Sensitive(type = DesensitizedUtil.DesensitizedType.MOBILE_PHONE)
    private String mobilePhone;
    /** 身份证 */
    @Sensitive(type = DesensitizedUtil.DesensitizedType.ID_CARD)
    private String cardNo;
    /** 患者年龄 */
    private Integer age;
    /** 性别，2，男；1，女 */
    @Dict(dicCode = "sex")
    private String sex;

    private String desc;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
     * 床位
     */
    private String bedInfo;

    /** 体重 例如62kg */
    private Integer weight;
    /** 身高 例如178CM */
    private Integer height;

    /** 婚姻状况 0:否 1:是 */
    private String izMarry;


    /**
     * 预生成UUID
     */
    private String uuid;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Sensitive(type = DesensitizedUtil.DesensitizedType.ADDRESS)
    private String address;

    private String flag;




    /** 备住内容 */
    private String remark;


    /**
     * 基础疾病标记
     */
    private String baseDisease;


    /** 爱好 */
    private String likes;


    /**
     * 手术名
     */
    private String surgicalName;

    /**
     * 手术时间
     */
    private String surgicalTime;


    /**
     * 外伤名
     */
    private String traumaName;

    /**
     * 外伤时间
     */
    private String traumaTime;

    /**
     * 家族史-父亲
     */
    private String fatherFamily;

    /**
     * 家族史-母亲
     */
    private String motherFamily;


    /**
     * 遗传病史
     */
    private String inheritance;


    /**
     * 药物过敏史
     */
    private List<String> drugAllergyList;


    /**
     * 民族
     */
    private String nation;
    /**
     * 政治面貌 1群众 2党员
     */
    private String politicsType;

    /** 疾病等级 */
    private String diseaseLevel;

    private String bloodType;

    private String nationText;
    private String sexText;

    private String archivesCode;
}
