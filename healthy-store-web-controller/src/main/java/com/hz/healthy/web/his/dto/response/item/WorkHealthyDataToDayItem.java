package com.hz.healthy.web.his.dto.response.item;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/11/13 22:08
 * @ClassName: WorkHealthyDataToDayItem
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkHealthyDataToDayItem implements Serializable {


    /**
     * 测量项
     */
    private String indexName;

    /**
     * 测量项
     */
    private String indexCode;

    /**
     * 测量值
     */
    private String dataValue;


    /**
     * 之前的测量值
     */
    private String beforeDataValue;


    /**
     * 比较得出结果
     */
    private String compareResult;


    private Integer develop;


    /**
     * 级别
     * 1～5 数字越大，级别越高
     */
    private int level;


    /**
     * 正常值
     */
    private String normal;
}
