package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.ch.IHChDoorServiceProjectService;
import com.hz.healthy.domain.his.IHCategoryService;
import com.hz.healthy.domain.his.IHCategoryTagService;
import com.hz.healthy.domain.his.model.HCategoryModel;
import com.hz.healthy.domain.his.model.HCategoryQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.model.SysTreeModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * 通用分类Controller
 *
 * @author hyhuang
 * @date 2023-07-10
 */
@RestController
@RequestMapping("/his/common/category")
public class HCategoryController extends WebBaseController {
    @Autowired
    private IHCategoryService hCategoryService;

    @Autowired
    private IHCategoryTagService hCategoryTagService;
    @Autowired
    private IHChDoorServiceProjectService hChDoorServiceProjectService;

    /**
     * 查询通用分类列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HCategoryModel>> pageQuery(HCategoryQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        ListResponse<HCategoryModel> pageList = hCategoryService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }

    /**
     * 查询通用分类列表
     */
    @GetMapping("/selectList")
    public RespResult<List<SysTreeModel>> selectTreeList(HCategoryQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        List<HCategoryModel> pageList = hCategoryService.selectList(queryModel);
        List<SysTreeModel> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList)) {
            for (HCategoryModel d : pageList) {
                SysTreeModel t = new SysTreeModel();
                t.setKey(String.valueOf(d.getId()));
                t.setValue(String.valueOf(d.getId()));
                t.setTitle(d.getName());
                t.setId(String.valueOf(d.getId()));
                dataList.add(t);
            }
        }
        return RespResult.OK(dataList);
    }

    /**
     * 查询通用分类列表
     */
    @GetMapping("/selectList/long")
    public RespResult<List<KeyLongModel>> selectList(HCategoryQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        List<HCategoryModel> pageList = hCategoryService.selectList(queryModel);
        List<KeyLongModel> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList)) {
            for (HCategoryModel d : pageList) {
                dataList.add(KeyLongModel.builder()
                        .key(d.getId())
                        .label(d.getName())
                        .build());
            }
        }
        return RespResult.OK(dataList);
    }



}
