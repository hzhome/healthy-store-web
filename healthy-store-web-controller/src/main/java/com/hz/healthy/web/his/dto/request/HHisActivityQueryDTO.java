package com.hz.healthy.web.his.dto.request;

import com.hz.ocean.common.dto.PageRequest;
import lombok.Data;

/**
 * @Author hyhuang
 * @Date 2023/10/19 20:59
 * @ClassName: HHisActivityQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisActivityQueryDTO extends PageRequest {

    /** 主键ID */
    private Long id;

    /** 内部唯一编码 */
    private String activityCode;
    private Long activityType;

    /**
     * 活动属性 0活动 1课程
     */
    private String activityAttr;

    /** 活动主题 */
    private String topic;

    /** 活动名称 */
    private String name;


    /** 活动组织者 */
    private String organizer;
    private String organizerRole;
    private Long organizerId;


    private String activityTime;
}
