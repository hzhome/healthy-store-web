package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.ch.IHChCareDoorOrderTrackService;
import com.hz.healthy.domain.ch.model.HChCareDoorOrderTrackModel;
import com.hz.healthy.domain.ch.model.HChCareDoorOrderTrackQueryModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 上门服务订单跟踪Controller
 *
 * @author hyhuang
 * @date 2023-09-24
 */
@RestController
@RequestMapping("/ch/door/order/track")
public class HStoreOrderTrackController extends WebBaseController {
    @Autowired
    private IHChCareDoorOrderTrackService hChCareDoorOrderTrackService;

    /**
     * 查询上门服务订单跟踪列表
     */
    @GetMapping("/list")
    public RespResult<List<String>> pageQuery(@ModelAttribute HChCareDoorOrderTrackQueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getOrderSn())) {
            return RespResult.error("");
        }
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        List<String> dataList = new ArrayList<>();
        List<HChCareDoorOrderTrackModel> pageList = hChCareDoorOrderTrackService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                dataList.add(p.getTaskDesc());
            });
        }
        return RespResult.OK(dataList);
    }


}
