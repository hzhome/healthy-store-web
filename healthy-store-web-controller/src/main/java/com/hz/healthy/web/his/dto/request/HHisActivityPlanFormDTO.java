package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2023/11/17 23:30
 * @ClassName: HHisActivityPlanFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisActivityPlanFormDTO implements Serializable {


    @NotBlank(message = "请输入月份")
    private String summarizeTime;
    @NotBlank(message = "请输入月份计划")
    private String rows;
}
