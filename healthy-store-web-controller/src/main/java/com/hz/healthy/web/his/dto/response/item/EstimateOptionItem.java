package com.hz.healthy.web.his.dto.response.item;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/4/13 12:38
 * @ClassName: EstimateOptionItem
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EstimateOptionItem implements Serializable {

    /** 主键 */
    private Long id;

    /** 题目主键 */
    private Long questionId;

    /** 选项内容 */
    private String textContent;

    /** 排序 */
    private Integer sort;

    /** 选项得分 */
    private BigDecimal optionScore;

    private boolean izCheck;


}
