package com.hz.healthy.web.his.dto.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2023/11/5 18:20
 * @ClassName: PatientPlanDetailDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
public class PatientPlanDetailDTO implements Serializable {

    private Long id;
    private String realName;
}
