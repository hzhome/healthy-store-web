package com.hz.healthy.web.system.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2023/11/1 11:25
 * @ClassName: HWxModuleDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HWxModuleDTO implements Serializable {


    private Long id;

    /** app模块名称 */
    private String appName;

    /** app模块编码 */
    private String appCode;

    private String selectedroles;


    private String rolesName;

}
