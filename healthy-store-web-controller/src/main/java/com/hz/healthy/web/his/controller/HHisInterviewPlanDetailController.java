package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisInterviewPlanDetailService;
import com.hz.healthy.domain.his.IHHisInterviewPlanLogService;
import com.hz.healthy.domain.his.IHHisTaskPlanService;
import com.hz.healthy.domain.his.model.*;
import com.hz.healthy.domain.patient.IHHisPatientService;
import com.hz.healthy.domain.patient.IHHospitalArchivesService;
import com.hz.healthy.domain.patient.model.HHisPatientModel;
import com.hz.healthy.domain.patient.model.HHospitalArchivesModel;
import com.hz.healthy.enums.TaskPlanRemindEnums;
import com.hz.healthy.web.his.dto.request.HHisInterviewPlanExecFormDTO;
import com.hz.healthy.web.his.dto.response.HHisInterviewPlanDetailDTO;
import com.hz.healthy.web.his.dto.response.PatientPlanDetailDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 走访明细Controller
 *
 * @author ruoyi
 * @date 2023-11-05
 */
@RestController
@RequestMapping("/ch/interview/detail")
public class HHisInterviewPlanDetailController extends WebBaseController {
    @Autowired
    private IHHisInterviewPlanDetailService hHisInterviewPlanDetailService;

    @Autowired
    private IHHisInterviewPlanLogService hHisInterviewPlanLogService;


    @Autowired
    private IHHospitalArchivesService hospitalArchivesService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    @Autowired
    private IHHisTaskPlanService hisTaskPlanService;

    /**
     * 查询走访明细列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisInterviewPlanDetailDTO>> pageQuery(HHisInterviewPlanDetailQueryModel queryModel) {
        List<HHisInterviewPlanDetailDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisInterviewPlanDetailModel> pageList = hHisInterviewPlanDetailService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<Long> patientList = pageList.getRecords().stream().map(HHisInterviewPlanDetailModel::getPatientId).collect(Collectors.toList());
            Map<Long, HHospitalArchivesModel> patientMap = hospitalArchivesService.mapByIds(patientList,getOrgCode());
            pageList.getRecords().forEach(p -> {
                HHospitalArchivesModel patientModel = patientMap.getOrDefault(p.getPatientId(), new HHospitalArchivesModel());
                HHisInterviewPlanDetailDTO planDetail = BeanUtil.toBean(p, HHisInterviewPlanDetailDTO.class);
                planDetail.setPatientAddress(patientModel.getAddress());
                planDetail.setQty(planDetail.getExecCount() - planDetail.getFinishCount());
                dataList.add(planDetail);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }

    /**
     * 查询走访明细列表
     */
    @GetMapping("/list/wait")
    public RespResult<ListResponse<HHisInterviewPlanDetailDTO>> pageQueryWait(HHisInterviewPlanDetailQueryModel queryModel) {
        queryModel.setStatusList(Lists.newArrayList("1", "2"));
        return pageQuery(queryModel);
    }

    /**
     * 查询走访明细列表
     */
    @GetMapping("/list/finish")
    public RespResult<ListResponse<HHisInterviewPlanDetailDTO>> pageQueryFinish(HHisInterviewPlanDetailQueryModel queryModel) {
        queryModel.setStatusList(Lists.newArrayList("3"));
        return pageQuery(queryModel);
    }


    /**
     * 查询走访明细列表
     */
    @GetMapping("/selectList")
    public RespResult<List<PatientPlanDetailDTO>> selectList(@ModelAttribute HHisInterviewPlanDetailQueryModel queryModel) {
        List<PatientPlanDetailDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        List<HHisInterviewPlanDetailModel> pageList = hHisInterviewPlanDetailService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                dataList.add(PatientPlanDetailDTO.builder()
                        .id(p.getPatientId())
                        .realName(p.getPatientName())
                        .build());
            });
        }
        return RespResult.OK(dataList);
    }

    /**
     * 获取走访明细详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Integer id) {
        return RespResult.OK(hHisInterviewPlanDetailService.getById(id));
    }

    /**
     * 新增走访明细
     */
    @Transactional
    @PostMapping(value = "/exec")
    public RespResult exec(@RequestBody HHisInterviewPlanExecFormDTO execForm) {
        HHisInterviewPlanDetailModel planDetailModel = hHisInterviewPlanDetailService.getById(execForm.getId());
        if (ObjectUtil.isNull(planDetailModel)) {
            return RespResult.error("记录不存在");
        }
        if ("1".equals(planDetailModel.getPlanType())) {
            if (ObjectUtil.isNull(execForm.getSignIn()) || ObjectUtil.isNull(execForm.getSignOut())) {
                return RespResult.error("签到时间不能为空");
            }
        } else if (ObjectUtil.isNull(execForm.getSignIn())) {
            return RespResult.error("签到时间不能为空");
        }
        if (StringUtils.isBlank(execForm.getRecordContent())) {
            return RespResult.error("请填写本次随访记录！");
        }
        if (CommonConstant.STATUS_0.equals(planDetailModel.getStatus())) {
            //0待执行 1开始执行 2执行中 3执行完成 4终止执行
            hHisInterviewPlanDetailService.updateById(HHisInterviewPlanDetailModel.builder()
                    .id(execForm.getId())
                    .status("1")
                    .startTime(new Date())
                    .finishCount(planDetailModel.getFinishCount() + 1)
                    .updateUser(getLoginUser().getRealname())
                    .build());
        } else {
            hHisInterviewPlanDetailService.updateById(HHisInterviewPlanDetailModel.builder()
                    .id(execForm.getId())
                    .finishCount(planDetailModel.getFinishCount() + 1)
                    .updateUser(getLoginUser().getRealname())
                    .build());
        }
        Date nextTime=null;
        if (StringUtils.isNotBlank(execForm.getNextTime())) {
             nextTime = DateUtil.parseDateTime(execForm.getNextTime() + " 00:00:00");
        }

        if ("1".equals(planDetailModel.getPlanType())) {
            hHisInterviewPlanLogService.insert(HHisInterviewPlanLogModel.builder()
                    .eyName(getLoginUser().getRealname())
                    .planTitle(planDetailModel.getPlanTitle())
                    .eyId(getLoginUser().getEyId())
                    .patientId(planDetailModel.getPatientId())
                    .patientName(planDetailModel.getPatientName())
                    .patientPhone(planDetailModel.getPatientPhone())
                    .patientSex(planDetailModel.getPatientSex())
                    .patientAge(planDetailModel.getPatientAge())
                    .duration((int) DateUtil.between(execForm.getSignOut(), execForm.getSignIn(), DateUnit.HOUR))
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .createUser(getLoginUser().getRealname())
                    .createTime(new Date())
                    .recordContent(execForm.getRecordContent())
                    .signIn(execForm.getSignIn())
                    .signOut(execForm.getSignOut())
                    .planCode(planDetailModel.getPlanCode())
                    .planType(planDetailModel.getPlanType())
                    .nextTime(nextTime)
                    .mentality(CollectionUtils.isNotEmpty(execForm.getMentality()) ? String.join(",", execForm.getMentality()) : "")
                    .followUpSituation(StringUtils.defaultIfBlank(execForm.getFollowUpSituation(), ""))
                    .complianceBehavior(StringUtils.defaultIfBlank(execForm.getComplianceBehavior(), ""))
                    .doctorGuidance(StringUtils.defaultIfBlank(execForm.getDoctorGuidance(), ""))
                    .medicationCompliance(StringUtils.defaultIfBlank(execForm.getMedicationCompliance(), ""))
                    .medicationSituation(StringUtils.defaultIfBlank(execForm.getMedicationSituation(), ""))
                    .psychologicalAdjust(StringUtils.defaultIfBlank(execForm.getPsychologicalAdjust(), ""))
                    .saltSituation(StringUtils.defaultIfBlank(execForm.getSaltSituation(), ""))
                    .sportsSituation(StringUtils.defaultIfBlank(execForm.getSportsSituation(), ""))
                    .weekSportsSituation(StringUtils.defaultIfBlank(execForm.getWeekSportsSituation(), ""))
                    .positionLat(BigDecimal.ZERO)
                    .positionLng(BigDecimal.ZERO)
                    .build());
        } else {
            hHisInterviewPlanLogService.insert(HHisInterviewPlanLogModel.builder()
                    .eyName(getLoginUser().getRealname())
                    .planTitle(planDetailModel.getPlanTitle())
                    .eyId(getLoginUser().getEyId())
                    .patientId(planDetailModel.getPatientId())
                    .patientName(planDetailModel.getPatientName())
                    .patientPhone(planDetailModel.getPatientPhone())
                    .patientSex(planDetailModel.getPatientSex())
                    .patientAge(planDetailModel.getPatientAge())
                    .duration(0)
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .createUser(getLoginUser().getRealname())
                    .createTime(new Date())
                    .recordContent(execForm.getRecordContent())
                    .signIn(execForm.getSignIn())
                    .planCode(planDetailModel.getPlanCode())
                    .planType(planDetailModel.getPlanType())
                    .positionLat(BigDecimal.ZERO)
                    .positionLng(BigDecimal.ZERO)
                    .nextTime(nextTime)
                    .mentality(CollectionUtils.isNotEmpty(execForm.getMentality()) ? String.join(",", execForm.getMentality()) : "")
                    .followUpSituation(StringUtils.defaultIfBlank(execForm.getFollowUpSituation(), ""))
                    .complianceBehavior(StringUtils.defaultIfBlank(execForm.getComplianceBehavior(), ""))
                    .doctorGuidance(StringUtils.defaultIfBlank(execForm.getDoctorGuidance(), ""))
                    .medicationCompliance(StringUtils.defaultIfBlank(execForm.getMedicationCompliance(), ""))
                    .medicationSituation(StringUtils.defaultIfBlank(execForm.getMedicationSituation(), ""))
                    .psychologicalAdjust(StringUtils.defaultIfBlank(execForm.getPsychologicalAdjust(), ""))
                    .saltSituation(StringUtils.defaultIfBlank(execForm.getSaltSituation(), ""))
                    .sportsSituation(StringUtils.defaultIfBlank(execForm.getSportsSituation(), ""))
                    .weekSportsSituation(StringUtils.defaultIfBlank(execForm.getWeekSportsSituation(), ""))
                    .build());
        }

        if (ObjectUtil.isNotNull(nextTime)) {
            //创建任务提醒
            hisTaskPlanService.insert(HHisTaskPlanModel.builder()
                    .taskTime(0)
                    .endTime(DateUtil.offsetDay(nextTime, 1))
                    .startTime(nextTime)
                    .taskKey(TaskPlanRemindEnums.FOLLOWUP.getCode())
                    .busCode(planDetailModel.getPlanCode())
                    .taskDate(execForm.getNextTime())
                    .orgCode(planDetailModel.getOrgCode())
                    .status(CommonConstant.STATUS_0)//未确认
                    .patientId(planDetailModel.getPatientId())
                    .eyId(planDetailModel.getEyId())
                    .tenantId(planDetailModel.getTenantId())
                    .createTime(new Date())
                    .content(planDetailModel.getPatientName() + "[" + planDetailModel.getPlanTitle() + "]" + "第" + (planDetailModel.getExecCount() + 1) + "次")
                    .build());
        }


        return RespResult.OK("处理成功");
    }


    /**
     * 走访完成
     */
    @Transactional
    @PostMapping(value = "/finish")
    public RespResult finish(@RequestBody HHisInterviewPlanExecFormDTO execForm) {
        HHisInterviewPlanDetailModel planDetailModel = hHisInterviewPlanDetailService.getById(execForm.getId());
        if (ObjectUtil.isNull(planDetailModel)) {
            return RespResult.error("记录不存在");
        }
        Date now = new Date();
        //0待执行 1开始执行 2执行中 3执行完成 4终止执行
        hHisInterviewPlanDetailService.updateById(HHisInterviewPlanDetailModel.builder()
                .id(execForm.getId())
                .status("3")
                .endTime(now)
                .duration((int) DateUtil.between(now, planDetailModel.getStartTime(), DateUnit.HOUR))
                .finishCount(planDetailModel.getFinishCount() + 1)
                .updateUser(getLoginUser().getRealname())
                .build());
        List<HHisTaskPlanModel> taskPlanList = hisTaskPlanService.selectList(HHisTaskPlanQueryModel.builder()
                .patientId(planDetailModel.getPatientId())
                .taskKey(TaskPlanRemindEnums.FOLLOWUP.getCode())
                .busCode(planDetailModel.getPlanCode())
                .status(CommonConstant.STATUS_0)
                .build());
        if (CollectionUtils.isNotEmpty(taskPlanList)) {
            for (HHisTaskPlanModel taskPlan : taskPlanList) {
                hisTaskPlanService.updateById(HHisTaskPlanModel.builder()
                        .id(taskPlan.getId())
                        .status(CommonConstant.STATUS_1)
                        .updateUser("系统更新完成")
                        .build());
            }
        }
        return RespResult.OK("处理成功");
    }

    /**
     * 修改走访明细
     */
    @PutMapping
    public RespResult edit(@RequestBody HHisInterviewPlanDetailModel hHisInterviewPlanDetailModel) {
        hHisInterviewPlanDetailService.updateById(hHisInterviewPlanDetailModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除走访明细
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Integer> ids) {
        hHisInterviewPlanDetailService.deleteByIds(ids);
        return RespResult.OK();
    }


    @PostMapping("/delete")
    public RespResult delete(@RequestBody HHisInterviewPlanDetailQueryModel queryModel) {
        HHisInterviewPlanDetailModel detailModel = hHisInterviewPlanDetailService.getById(queryModel.getId());
        if (ObjectUtil.isNull(detailModel)) {
            return RespResult.error("此会员不存在随访计划中，请刷新查看!");
        }
        String planCode = detailModel.getPlanCode();
        int count = hHisInterviewPlanDetailService.count(HHisInterviewPlanDetailQueryModel.builder()
                .planCode(planCode)
                .build());
        if (count <= 1) {
            return RespResult.error("随访计划中的此会员是唯一随访者，不能删除，如需要删除，请移步删除随访计划!");
        }
        hHisInterviewPlanDetailService.deleteById(queryModel.getId());
        return RespResult.OK("删除成功");
    }
}
