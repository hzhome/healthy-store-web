package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.healthy.domain.ch.model.items.HomeCaregiverModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/4/13 18:55
 * @ClassName: HChCustomOrderDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HChCustomOrderDTO  implements Serializable {

    /** $comment */
    private Integer id;

    /** 租户id */
    private Long tenantId;

    /** 添加时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 下单会员id */
    private Long memberId;

    /** 下单会员名称 */
    private String memberName;

    /** 商户订单号 */
    private String orderSn;

    /** 状态，0等接单，1己接单，2客户己取消 3商家取消 */
    private Integer orderStatus;

    /** 1 居家护工  2住院护工 3陪诊师 4康复师 5挂号服务 6其他服务 */
    private String customType;

    /** 备注 */
    private String remark;

    /** 订单服务状态，1 待接订单  2 待服务 3 服务中  4 待评价 */
    private String confirmStatus;

    /** 确认人员 */
    private String confirmUser;

    private BigDecimal amount;



    private HomeCaregiverModel beanInfoDto;

    //----------- 来自 transient 修饰列
    //----------- 来自 transient 修饰列 End
}
