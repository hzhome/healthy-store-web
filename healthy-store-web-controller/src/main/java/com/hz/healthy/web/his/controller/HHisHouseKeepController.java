package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.doctor.model.HEmployeeModel;
import com.hz.healthy.domain.his.IHHisHouseKeepService;
import com.hz.healthy.domain.his.model.HHisHouseKeepModel;
import com.hz.healthy.domain.his.model.HHisHouseKeepQueryModel;
import com.hz.healthy.domain.patient.IHHisPatientHouseKeepService;
import com.hz.healthy.domain.patient.model.HHisPatientHouseKeepQueryModel;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author hyhuang
 * @Date 2024/6/18 23:04
 * @ClassName: HHisHouseKeepController
 * @Description: TODO
 * @Version 1.0
 */
@RestController
@RequestMapping("/his/house/keep")
public class HHisHouseKeepController extends WebBaseController {

    @Autowired
    private IHHisHouseKeepService hHisHouseKeepService;
    @Autowired
    private IHEmployeeService iHEmployeeService;

    @Autowired
    private IHHisPatientHouseKeepService hHisPatientHouseKeepService;


    /**
     * 管家列表
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<ListResponse<HHisHouseKeepModel>> pageQuery(HHisHouseKeepQueryModel queryModel) {
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisHouseKeepModel> pageList = hHisHouseKeepService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<Long> eyIds = pageList.getRecords().stream().map(HHisHouseKeepModel::getEyId).collect(Collectors.toList());
            Map<Long, HEmployeeModel> modelMap = iHEmployeeService.mapByIds(eyIds);
            pageList.getRecords().forEach(p -> {
                p.setSystemFlag(modelMap.get(p.getEyId()).getMobilePhone());
            });
        }
        return RespResult.OK(pageList);
    }

    /**
     * 删除活动类型
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        int count =  hHisPatientHouseKeepService.count(HHisPatientHouseKeepQueryModel.builder()
                .eyId(id)
                .build());
        if(count>0){
            return RespResult.error("已有客户绑定管家，不允许删除！");
        }
        hHisHouseKeepService.deleteById(id);
        return RespResult.OK("删除成功");
    }

}
