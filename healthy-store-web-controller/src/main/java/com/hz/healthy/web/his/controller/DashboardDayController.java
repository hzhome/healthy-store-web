package com.hz.healthy.web.his.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.ch.IHChCustomOrderService;
import com.hz.healthy.domain.ch.model.HChCustomOrderQueryModel;
import com.hz.healthy.domain.doctor.IHHisRegistrationService;
import com.hz.healthy.domain.doctor.model.HHisRegistrationQueryModel;
import com.hz.healthy.domain.finance.IHHisFinanceIncomeDayDetailService;
import com.hz.healthy.domain.finance.IHHisFinanceIncomeDayService;
import com.hz.healthy.domain.finance.IHHisFinanceIncomeMonthService;
import com.hz.healthy.domain.finance.model.*;
import com.hz.healthy.domain.his.IHHisInterviewPlanLogService;
import com.hz.healthy.domain.his.IHHisMarketingConsultService;
import com.hz.healthy.domain.his.model.HHisInterviewPlanLogQueryModel;
import com.hz.healthy.domain.his.model.HHisMarketingConsultQueryModel;
import com.hz.healthy.domain.member.IHMemberService;
import com.hz.healthy.domain.member.model.HMemberQueryModel;
import com.hz.healthy.domain.patient.IHHisPatientHealthyDataService;
import com.hz.healthy.domain.patient.IHHisPatientQuestionAnswerService;
import com.hz.healthy.domain.patient.model.HHisPatientHealthyDataQueryModel;
import com.hz.healthy.domain.patient.model.HHisPatientQuestionAnswerQueryModel;
import com.hz.healthy.domain.report.IHMonitorEventService;
import com.hz.healthy.domain.report.model.HMonitorEventQueryModel;
import com.hz.healthy.domain.store.IHStoreOrderService;
import com.hz.healthy.domain.store.model.HStoreOrderQueryModel;
import com.hz.healthy.enums.FinanceTypeEnums;
import com.hz.healthy.enums.OrderTypeEnums;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.model.KeyValueModel;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;

/**
 * 首页 Controller
 * 统计
 *
 * @author hyhuang
 * @date 2023-10-06
 */
@RestController
@RequestMapping("/ch/dashboard/day")
public class DashboardDayController extends WebBaseController {


    @Autowired
    private IHHisPatientQuestionAnswerService hisPatientQuestionAnswerService;

    @Autowired
    private IHStoreOrderService hzStoreOrderService;

    @Autowired
    private IHHisRegistrationService hzRegistrationService;


    @Autowired
    private IHMonitorEventService hMonitorEventService;

    @Autowired
    private IHHisPatientHealthyDataService hisPatientHealthyDataService;

    @Autowired
    private IHHisInterviewPlanLogService hisInterviewPlanLogService;

    @Autowired
    private IHHisMarketingConsultService hHisMarketingConsultService;


    @Autowired
    private IHHisFinanceIncomeDayDetailService hisFinanceIncomeDayDetailService;

    /**
     * 预警量
     */
    @GetMapping("/monitor/warn/count")
    public RespResult<Integer> monitorWarnCount() {
        int count = hMonitorEventService.count(HMonitorEventQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .izException(CommonConstant.STATUS_1)
                .monitorDay(DateUtil.formatDate(new Date()))
                .build());
        return RespResult.OK(count);
    }


    /**
     * 设备使用量
     */
    @GetMapping("/device/use/count")
    public RespResult<Integer> deviceUseCount() {
        int count = hisPatientHealthyDataService.count(HHisPatientHealthyDataQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .today(DateUtil.formatDate(new Date()))
                .build());
        return RespResult.OK(count);
    }

    /**
     * 测评量
     */
    @GetMapping("/evaluation/count")
    public RespResult<Integer> evaluationCount() {
        String date = DateUtil.formatDateTime(new Date());
        int count = hisPatientQuestionAnswerService.count(HHisPatientQuestionAnswerQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .startTime(DateUtil.parseDate(date + " 00:00:00"))
                .endTime(DateUtil.parseDate(date + " 23:59:59"))
                .build());
        return RespResult.OK(count);
    }


    /**
     * 随访量
     */
    @GetMapping("/followUp/count")
    public RespResult<Integer> followUpCount() {
        String date = DateUtil.formatDateTime(new Date());
        int count = hisInterviewPlanLogService.count(HHisInterviewPlanLogQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .startTime(DateUtil.parseDate(date + " 00:00:00"))
                .endTime(DateUtil.parseDate(date + " 23:59:59"))
                .build());
        return RespResult.OK(count);
    }

    /**
     * 咨询量
     */
    @GetMapping("/consultation/count")
    public RespResult<Integer> consultationCount() {
        String date = DateUtil.formatDateTime(new Date());
        int count = hHisMarketingConsultService.count(HHisMarketingConsultQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .startTime(DateUtil.parseDate(date + " 00:00:00"))
                .endTime(DateUtil.parseDate(date + " 23:59:59"))
                .build());
        return RespResult.OK(count);
    }

    /**
     * 服务订单量
     */
    @GetMapping("/service/order/count")
    public RespResult<Integer> serviceOrderCount() {
        String date = DateUtil.formatDateTime(new Date());
        int count = hzStoreOrderService.count(HStoreOrderQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .orderTypeList(Lists.newArrayList(OrderTypeEnums.D3.getCode(), OrderTypeEnums.D4.getCode(), OrderTypeEnums.D5.getCode(),
                        OrderTypeEnums.D6.getCode(), OrderTypeEnums.D7.getCode()))
                .startTime(DateUtil.parseDate(date + " 00:00:00"))
                .endTime(DateUtil.parseDate(date + " 23:59:59"))
                .build());
        return RespResult.OK(count);
    }


    /**
     * 商城订单量
     */
    @GetMapping("/shop/order/count")
    public RespResult<Integer> shopOrderCount() {
        String date = DateUtil.formatDateTime(new Date());
        int count = hzStoreOrderService.count(HStoreOrderQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .orderTypeList(Lists.newArrayList(OrderTypeEnums.D1.getCode(), OrderTypeEnums.D2.getCode()))
                .startTime(DateUtil.parseDate(date + " 00:00:00"))
                .endTime(DateUtil.parseDate(date + " 23:59:59"))
                .build());
        return RespResult.OK(count);
    }

    /**
     * 挂号量（门诊）
     */
    @GetMapping("/register/count1")
    public RespResult<Integer> registerCount1() {
        String date = DateUtil.formatDateTime(new Date());
        int count = hzRegistrationService.count(HHisRegistrationQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .schedulingDate(date)
                        .appointmentType("1")
                .build());
        return RespResult.OK(count);
    }

    /**
     * 挂号量（外出）
     */
    @GetMapping("/register/count2")
    public RespResult<Integer> registerCount2() {
        String date = DateUtil.formatDateTime(new Date());
        int count = hzRegistrationService.count(HHisRegistrationQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .schedulingDate(date)
                .appointmentType("2")
                .build());
        return RespResult.OK(count);
    }



    /**
     * 门诊收费
     */
    @GetMapping("/register1/amount")
    public RespResult<BigDecimal> registerAmount1() {
        String date = DateUtil.formatDateTime(new Date());
        HHisFinanceIncomeDayDetailModel incomeDayModel = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                        .financeType(FinanceTypeEnums.F0.getCode())
                .build());
        return RespResult.OK(ObjectUtil.isNull(incomeDayModel)?BigDecimal.ZERO:incomeDayModel.getTotalAmount());
    }

    /**
     * 出诊收费
     */
    @GetMapping("/register2/amount")
    public RespResult<BigDecimal> registerAmount2() {
        String date = DateUtil.formatDateTime(new Date());
        HHisFinanceIncomeDayDetailModel incomeDayModel = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                .financeType(FinanceTypeEnums.F1.getCode())
                .build());
        return RespResult.OK(ObjectUtil.isNull(incomeDayModel)?BigDecimal.ZERO:incomeDayModel.getTotalAmount());
    }

    /**
     * 挂号收费
     */
    @GetMapping("/register/amount")
    public RespResult<BigDecimal> registerAmount() {
        String date = DateUtil.formatDateTime(new Date());
        HHisFinanceIncomeDayDetailModel incomeDayModel0 = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                .financeType(FinanceTypeEnums.F0.getCode())
                .build());

        HHisFinanceIncomeDayDetailModel incomeDayModel1 = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                .financeType(FinanceTypeEnums.F1.getCode())
                .build());
        BigDecimal i0=ObjectUtil.isNull(incomeDayModel0)?BigDecimal.ZERO:incomeDayModel0.getTotalAmount();
        BigDecimal i1=ObjectUtil.isNull(incomeDayModel1)?BigDecimal.ZERO:incomeDayModel1.getTotalAmount();
        return RespResult.OK(NumberUtil.add(i0,i1));
    }


    /**
     * 心血管收费
     */
    @GetMapping("/evaluation/amount")
    public RespResult<BigDecimal> evaluationAmount() {
        String date = DateUtil.formatDateTime(new Date());
        HHisFinanceIncomeDayDetailModel incomeDayModel = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                .financeType(FinanceTypeEnums.F4.getCode())
                .build());
        return RespResult.OK(ObjectUtil.isNull(incomeDayModel)?BigDecimal.ZERO:incomeDayModel.getTotalAmount());
    }

    /**
     * 商城收费
     */
    @GetMapping("/shop/amount")
    public RespResult<BigDecimal> shopAmount() {
        String date = DateUtil.formatDateTime(new Date());
        HHisFinanceIncomeDayDetailModel incomeDayModel = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                .financeType(FinanceTypeEnums.F5.getCode())
                .build());
        return RespResult.OK(ObjectUtil.isNull(incomeDayModel)?BigDecimal.ZERO:incomeDayModel.getTotalAmount());
    }


    /**
     * 服务收费
     */
    @GetMapping("/service/amount")
    public RespResult<BigDecimal> serviceAmount() {
        String date = DateUtil.formatDateTime(new Date());
        HHisFinanceIncomeDayDetailModel incomeDayModel = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                .financeType(FinanceTypeEnums.F6.getCode())
                .build());
        return RespResult.OK(ObjectUtil.isNull(incomeDayModel)?BigDecimal.ZERO:incomeDayModel.getTotalAmount());
    }

    /**
     * 设备收费
     */
    @GetMapping("/device/amount")
    public RespResult<BigDecimal> deviceAmount() {
        String date = DateUtil.formatDateTime(new Date());
        HHisFinanceIncomeDayDetailModel incomeDayModel = hisFinanceIncomeDayDetailService.getOne(HHisFinanceIncomeDayDetailQueryModel.builder()
                .tenantId(getTenantId())
                .orgCode(getOrgCode())
                .incomeDay(date)
                .financeType(FinanceTypeEnums.F7.getCode())
                .build());
        return RespResult.OK(ObjectUtil.isNull(incomeDayModel)?BigDecimal.ZERO:incomeDayModel.getTotalAmount());
    }

}
