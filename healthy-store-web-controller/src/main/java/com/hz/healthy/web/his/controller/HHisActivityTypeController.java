package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisActivityTypeService;
import com.hz.healthy.domain.his.model.HHisActivityTypeModel;
import com.hz.healthy.domain.his.model.HHisActivityTypeQueryModel;
import com.hz.healthy.web.system.dto.KeyValueDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 活动管理Controller
 *
 * @author hyhuang
 * @date 2023-07-10
 */
@RestController
@RequestMapping("/his/activity/type")
public class HHisActivityTypeController extends WebBaseController {
    @Autowired
    private IHHisActivityTypeService hisActivityTypeService;


    /**
     * 查询活动类型管理列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisActivityTypeModel>> pageQuery(HHisActivityTypeQueryModel query) {
        query.setTenantId(getTenantId());
        query.setOrgCode(getOrgCode());
        ListResponse<HHisActivityTypeModel> pageList =hisActivityTypeService.pageQuery(query);
        return RespResult.OK(pageList);
    }




    /**
     * 新增活动类型管理
     */
    @Transactional
    @PostMapping
    public RespResult add(@Validated @RequestBody HHisActivityTypeModel formReq) {
        HHisActivityTypeModel hHisActivityModel = BeanUtil.toBean(formReq, HHisActivityTypeModel.class);
        hHisActivityModel.setTenantId(getTenantId());
        hHisActivityModel.setCreateUser(getLoginUser().getRealname());
        hHisActivityModel.setCreateTime(new Date());
        hHisActivityModel.setOrgCode(getOrgCode());
        hisActivityTypeService.insert(hHisActivityModel);
        return RespResult.OK("添加成功");
    }


    /**
     * 新增活动类型管理
     */
    @Transactional
    @PutMapping
    public RespResult edit(@Validated @RequestBody HHisActivityTypeModel formReq) {
        HHisActivityTypeModel hHisActivityModel = BeanUtil.toBean(formReq, HHisActivityTypeModel.class);
        hHisActivityModel.setUpdateUser(getLoginUser().getRealname());
        hHisActivityModel.setUpdateTime(new Date());
        hisActivityTypeService.updateById(hHisActivityModel);
        return RespResult.OK("变更成功");
    }

    /**
     * 删除活动类型
     */
    @DeleteMapping("/delete/{id}")
    public RespResult remove(@PathVariable Long id) {
        hisActivityTypeService.deleteById(id);
        return RespResult.OK("删除成功");
    }


    /**
     * 查询活动类型
     */
    @Deprecated
    @GetMapping("/select/list")
    public RespResult<List<KeyValueDTO>> selectList() {
        List<KeyValueDTO> dataList = new ArrayList<>();
        List<HHisActivityTypeModel> list = hisActivityTypeService.selectList(HHisActivityTypeQueryModel.builder()
                        .orgCode(getOrgCode())
                        .tenantId(getTenantId())
                .build());
        if (CollectionUtils.isNotEmpty(list)) {
            list.forEach(l -> {
                dataList.add(new KeyValueDTO(String.valueOf(l.getId()), String.valueOf(l.getName())));
            });
        }
        return RespResult.OK(dataList);
    }
}
