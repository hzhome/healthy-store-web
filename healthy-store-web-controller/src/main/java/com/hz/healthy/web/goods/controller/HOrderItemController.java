package com.hz.healthy.web.goods.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.goods.IHOrderItemService;
import com.hz.healthy.domain.goods.model.HOrderItemModel;
import com.hz.healthy.domain.goods.model.HOrderItemQueryModel;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单项Controller
 * 
 * @author ruoyi
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/shop/order/item")
public class HOrderItemController extends WebBaseController
{
    @Autowired
    private IHOrderItemService hOrderItemService;

    /**
     * 查询订单项列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HOrderItemModel>> pageQuery(HOrderItemQueryModel queryModel)  {
        RespResult<ListResponse<HOrderItemModel>> result = new RespResult<ListResponse<HOrderItemModel>>();
        ListResponse<HOrderItemModel> pageList = hOrderItemService.pageQuery(queryModel);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }



    /**
     * 获取订单项详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id)
    {
        return RespResult.OK(hOrderItemService.getById(id));
    }

    /**
     * 新增订单项
     */
    //@PreAuthorize("@ss.hasPermi('system:item:add')")
    //@Log(title = "订单项", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HOrderItemModel hOrderItemModel)
    {
        hOrderItemService.insert(hOrderItemModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改订单项
     */
    @PutMapping
    public RespResult edit(@RequestBody  HOrderItemModel hOrderItemModel)
    {
        hOrderItemService.updateById(hOrderItemModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除订单项
     */
	@DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids)
    {
        hOrderItemService.deleteByIds(ids);
        return RespResult.OK();
    }
}
