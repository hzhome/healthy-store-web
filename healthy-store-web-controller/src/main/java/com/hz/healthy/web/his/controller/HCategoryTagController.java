package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHCategoryTagService;
import com.hz.healthy.domain.his.model.HCategoryTagModel;
import com.hz.healthy.domain.his.model.HCategoryTagQueryModel;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 通用分类下的标记Controller
 *
 * @author ruoyi
 * @date 2023-10-27
 */
@RestController
@RequestMapping("/his/common/category/tag")
public class HCategoryTagController extends WebBaseController {
    @Autowired
    private IHCategoryTagService hCategoryTagService;



    /**
     * 查询通用分类列表
     */
    @GetMapping("/selectList")
    public RespResult<List<KeyLongModel>> selectList(HCategoryTagQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        List<HCategoryTagModel> pageList = hCategoryTagService.selectList(queryModel);
        List<KeyLongModel> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList)) {
            for (HCategoryTagModel d : pageList) {
                dataList.add(KeyLongModel.builder()
                        .label(d.getName())
                        .key(d.getId())
                        .build());
            }
        }
        return RespResult.OK(dataList);
    }
}
