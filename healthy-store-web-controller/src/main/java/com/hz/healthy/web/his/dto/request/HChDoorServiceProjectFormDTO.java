package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/4/9 12:24
 * @ClassName: HChDoorServiceProjectFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HChDoorServiceProjectFormDTO implements Serializable {

    private Long id;

    /** 项目分类 取h_category */
    @NotNull(message = "请选择项目分类")
    private Long categoryId;

    /** 项目分类 取h_category_tag */
    @NotNull(message = "请选择项目分类下的标签")
    private Long categoryTagId;

    /** 项目类型，1.护士上门 2居家保洁 3陪诊服务 4照护服务 */
    private String projectType;

    /** 项目名称 */
    @NotBlank(message = "请输入项目名称")
    private String project;

    /** 项目主图片 */
    @NotBlank(message = "请选择项目封面图")
    private String img;

    /** 项目费用 */
    private BigDecimal amount;

    /** 销售数量 */
    private Integer time;

    /** 服务评分 */
    private Long score;

    /** 项目服务时间(次/分) */
    private Integer saleQty;

    /** 服务描述大文本 */
    @NotBlank(message = "请填写项目详情")
    private String description;


    /** 描述 */
    private String remark;
}
