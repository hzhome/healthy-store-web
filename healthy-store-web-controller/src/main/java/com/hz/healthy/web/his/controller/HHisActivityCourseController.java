package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisActivityCourseService;
import com.hz.healthy.domain.his.IHHisActivityDetailService;
import com.hz.healthy.domain.his.IHHisCommunityButlerService;
import com.hz.healthy.domain.his.model.HHisActivityCourseModel;
import com.hz.healthy.domain.his.model.HHisActivityCourseQueryModel;
import com.hz.healthy.domain.his.model.HHisActivityDetailModel;
import com.hz.healthy.domain.his.model.HHisCommunityButlerModel;
import com.hz.healthy.domain.patient.IHHisPatientService;
import com.hz.healthy.domain.patient.model.HHisPatientBedModel;
import com.hz.healthy.web.his.dto.request.HHisActivityCourseFormDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.ObjectUtils;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 活动课程Controller
 *
 * @author hyhuang
 * @date 2023-11-07
 */
@RestController
@RequestMapping("/his/activity/course")
public class HHisActivityCourseController extends WebBaseController {
    @Autowired
    private IHHisActivityCourseService hHisActivityCourseService;

    @Autowired
    private IHHisCommunityButlerService hisCommunityButlerService;
    @Autowired
    private IHHisActivityDetailService hisActivityDetailService;

    @Autowired
    private IHHisPatientService hisPatientService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    /**
     * 查询活动课程列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisActivityCourseModel>> pageQuery(HHisActivityCourseQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        ListResponse<HHisActivityCourseModel> pageList = hHisActivityCourseService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }


    /**
     * 获取活动课程详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hHisActivityCourseService.getById(id));
    }

    /**
     * 新增活动课程
     */
    //@PreAuthorize("@ss.hasPermi('system:course:add')")
    //@Log(title = "活动课程", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@Validated @RequestBody HHisActivityCourseFormDTO form) {
        if ("1".equals(form.getIzEnroll())) {
            if (ObjectUtil.isNull(form.getEnrollStart()) || ObjectUtil.isNull(form.getEnrollEnd())) {
                return RespResult.error("请输入报名时间范围");
            }
            if (ObjectUtils.isIntNull(form.getEnrollCount())) {
                return RespResult.error("请输入报名人数");
            }
        }
        HHisCommunityButlerModel butlerModel = hisCommunityButlerService.getById(form.getOrganizerId());
        if (ObjectUtil.isNull(butlerModel)) {
            return RespResult.error("社工不存在");
        }


        Date time = new Date();
        String activityCode = seqNoAutoService.buildBatchNo();
        HHisActivityCourseModel hHisActivityCourseModel = BeanUtil.toBean(form, HHisActivityCourseModel.class);
        if (ObjectUtil.isNull(form.getPatientId())) {
            HHisPatientBedModel patientBedModel = hisPatientService.getBedInfo(form.getPatientId());
            if (ObjectUtil.isNull(patientBedModel)) {
                return RespResult.error("长者信息不存在");
            }
            hHisActivityCourseModel.setPatientName(patientBedModel.getPatientName());
        }
        hHisActivityCourseModel.setOrganizer(butlerModel.getRealName());
        hHisActivityCourseModel.setTenantId(getTenantId());
        hHisActivityCourseModel.setCreateUser(getLoginUser().getRealname());
        hHisActivityCourseModel.setCreateTime(time);
        hHisActivityCourseModel.setOrgCode(getOrgCode());
        hHisActivityCourseModel.setActivityCode(activityCode);
        hHisActivityCourseModel.setActivitySource("0");
        hHisActivityCourseService.insert(hHisActivityCourseModel);
        return RespResult.OK("添加成功");
    }


    @PostMapping(value = "/entry")
    public RespResult entry(@RequestBody HHisActivityCourseFormDTO form) {
        HHisActivityCourseModel courseModel = hHisActivityCourseService.getById(form.getId());
        if (ObjectUtil.isNull(courseModel)) {
            return RespResult.error("课程不存在");
        }
        Date time = new Date();
        List<HHisActivityDetailModel> dateilList = new ArrayList<>();
        Map<Long, HHisPatientBedModel> mapName = hisPatientService.mapBedByIds(form.getPatientIds());
        for (Long patientId : form.getPatientIds()) {
            HHisPatientBedModel bedModel = mapName.get(patientId);
            dateilList.add(HHisActivityDetailModel.builder()
                    .activityCode(courseModel.getActivityCode())
                    .personId(patientId)
                    .personName(bedModel.getPatientName())
                    .patientBed(bedModel.getBedInfo())
                    .sex(bedModel.getSexText())
                    .age(bedModel.getAge())
                    .personType("5")
                    .tenantId(getTenantId())
                    .createTime(time)
                    .createUser(getLoginUser().getRealname())
                    .build());
        }
        hisActivityDetailService.insertBatch(dateilList);
        return RespResult.OK("报名成功");
    }

    /**
     * 修改活动课程
     */
    @PutMapping
    public RespResult edit(@RequestBody HHisActivityCourseModel hHisActivityCourseModel) {
        hHisActivityCourseService.updateById(hHisActivityCourseModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除活动课程
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        hHisActivityCourseService.updateById(HHisActivityCourseModel.builder()
                .id(id)
                .isDeleted(CommonConstant.DEL_FLAG_1)
                .updateTime(new Date())
                .updateUser(getLoginUser().getRealname())
                .build());
        return RespResult.OK("删除成功");
    }
}
