package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.hearthealthy.IHHealthStandardIndicatorsService;
import com.hz.healthy.domain.hearthealthy.model.HHealthStandardIndicatorSimpleModel;
import com.hz.healthy.domain.patient.*;
import com.hz.healthy.domain.patient.model.*;
import com.hz.healthy.domain.question.model.item.HQuestionOptionRecordModel;
import com.hz.healthy.domain.question.model.item.HQuestionRecordModel;
import com.hz.healthy.enums.HeartVariableEnums;
import com.hz.healthy.enums.QuestionTypeEnums;
import com.hz.healthy.enums.RiskLevelEnums;
import com.hz.healthy.web.his.dto.response.EstimateAnswerRecordDTO;
import com.hz.healthy.web.his.dto.response.EstimateReportItemDTO;
import com.hz.healthy.web.his.dto.response.HHisPatientQuestionAnswerRecordDTO;
import com.hz.healthy.web.his.dto.response.item.EstimateOptionItem;
import com.hz.healthy.web.his.dto.response.item.EstimateQuestionItem;
import com.hz.ocean.common.aspect.CtrlReq;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.dto.OrderItem;
import com.hz.ocean.common.model.KeyStrModel;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 患者心血管测评Controller
 *
 * @author hyhuang
 * @date 2023-10-06
 */
@RestController
@RequestMapping("/ch/evaluation/result")
public class HHisPatientQuestionAnswerRecordController extends WebBaseController {
    @Autowired
    private IHHisPatientQuestionAnswerService hisPatientQuestionAnswerService;

    @Autowired
    private IHHisPatientQuestionAnswerRecordService hisPatientQuestionAnswerRecordService;

    @Autowired
    private IHHisPatientService hisPatientService;

    @Autowired
    private IHHisPatientHealthReportService hisPatientHealthReportService;

    @Autowired
    private IHHisPatientHealthReportAnalysisService hisPatientHealthReportAnalysisService;

    @Autowired
    private IHHealthStandardIndicatorsService hHealthStandardIndicatorsService;

    @Autowired
    private IHEmployeeService hisEmployeeService;

    /**
     * 查询患者心血管测评列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisPatientQuestionAnswerRecordDTO>> pageQuery(HHisPatientQuestionAnswerQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        //读取本门店的结果
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisPatientQuestionAnswerModel> pageList = hisPatientQuestionAnswerService.pageQuery(queryModel);
        List<HHisPatientQuestionAnswerRecordDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> reportCodeLis=new ArrayList<>();
            List<Long> eIds=new ArrayList<>();
            pageList.getRecords().forEach(p -> {
                reportCodeLis.add(p.getReportCode());
                if(p.getSureOpId()>0L){
                    eIds.add(p.getSureOpId());
                }
            });
            Map<Long, String> empMap=new HashMap<>();
            if(CollectionUtils.isNotEmpty(eIds)){
                empMap.putAll(hisEmployeeService.mapNameByIds(eIds));
            }
            Map<String, Map<String, HPatientHealthReportSimpleItemDTO>> reportMap = hisPatientHealthReportService.mapList(reportCodeLis, true);
            pageList.getRecords().forEach(p -> {
                HHisPatientQuestionAnswerRecordDTO data = BeanUtil.toBean(p, HHisPatientQuestionAnswerRecordDTO.class);
                data.setDoctorName(empMap.getOrDefault(p.getSureOpId(),""));
                data.setRiskLevel(RiskLevelEnums.getEnumToDesc(data.getRiskLevel()));
                if (reportMap.containsKey(p.getReportCode())) {
                    Map<String, HPatientHealthReportSimpleItemDTO> map = reportMap.get(p.getReportCode());
                    data.setW(map.get("W"));
                    data.setC(map.get("C"));
                    data.setD(map.get("D"));
                    data.setH(map.get("H"));
                    data.setDL(map.get("DL"));
                    data.setM(map.get("M"));
                    data.setL(map.get("L"));
                    data.setK(map.get("K"));
                }
                dataList.add(data);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 查询患者个人全部心血管测评列表 历史记录 最近一年的
     *
     */
    @GetMapping("/history/list")
    public RespResult<List<HHisPatientQuestionAnswerRecordDTO>> pageQueryHistory(HHisPatientQuestionAnswerQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        if(ObjectUtil.isNull(queryModel.getPatientId())||queryModel.getPatientId()==0L){
            return RespResult.OK(new ArrayList<>());
        }
        //读取本门店的结果
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisPatientQuestionAnswerModel> pageList = hisPatientQuestionAnswerService.pageQuery(queryModel);
        List<HHisPatientQuestionAnswerRecordDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
             pageList.getRecords().forEach(p -> {
                HHisPatientQuestionAnswerRecordDTO data = BeanUtil.toBean(p, HHisPatientQuestionAnswerRecordDTO.class);
                data.setRiskLevel(RiskLevelEnums.getEnumToDesc(data.getRiskLevel()));
                data.setCreateDate(DateUtil.formatDate(p.getCreateTime()));
                dataList.add(data);
            });
        }
        return RespResult.OK(dataList);
    }
    /**
     * 查询血管测评答题
     */
    @GetMapping("/list/answer")
    public RespResult<List<EstimateAnswerRecordDTO>> answer(HHisPatientQuestionAnswerRecordQueryModel queryModel) {
        List<EstimateAnswerRecordDTO> dataList = new ArrayList<>();
        HHisPatientQuestionAnswerRecordModel answerRecordModel = hisPatientQuestionAnswerRecordService.getOne(HHisPatientQuestionAnswerRecordQueryModel.builder()
                .reportCode(queryModel.getReportCode())
                .build());
        if (ObjectUtil.isNull(answerRecordModel)) {
            return RespResult.OK(dataList);
        }

        List<HQuestionRecordModel> recordList = JSON.parseArray(answerRecordModel.getQuestionContent(), HQuestionRecordModel.class);
        Map<Integer, List<HQuestionRecordModel>> recordMap = recordList.stream().collect(Collectors.groupingBy(HQuestionRecordModel::getQuestionType));
        //1.单选题 2.多选题 3.判断题 4.填空题 5.简答题
        for (Map.Entry<Integer, List<HQuestionRecordModel>> entry : recordMap.entrySet()) {
            EstimateAnswerRecordDTO answerRecord = new EstimateAnswerRecordDTO();
            answerRecord.setQuestionType(QuestionTypeEnums.getEnumToDesc(entry.getKey().toString()));

            List<EstimateQuestionItem> questionList = new ArrayList<>();
            entry.getValue().forEach(question -> {

                List<EstimateOptionItem> optionsList = new ArrayList<>();
                for (HQuestionOptionRecordModel optionItem : question.getOptionsList()) {
                    optionsList.add(EstimateOptionItem.builder()
                            .id(optionItem.getId())
                            .textContent(optionItem.getTextContent())
                            .izCheck(optionItem.getIzCheck())
                            .questionId(question.getId())
                            .build());
                }
                questionList.add(EstimateQuestionItem.builder()
                        .questionType(question.getQuestionType())
                        .id(question.getId())
                        .title(question.getTitle())
                        .optionsList(optionsList)
                        .build());
            });
            answerRecord.setQuestionList(questionList);
            dataList.add(answerRecord);
        }
        return RespResult.OK(dataList);
    }


    /**
     * 查询血管测评 指标项
     */
    @CtrlReq
    @GetMapping(value = "/list/report/target/index")
    public RespResult<List<EstimateReportItemDTO>> reportTargetIndex(HHisPatientHealthReportQueryModel queryModel) {
        List<EstimateReportItemDTO> items = new ArrayList<>();
        queryModel.setOrderBy(Lists.newArrayList(new OrderItem().withField("sort").withOrder(OrderItem.ASC)));
        List<HHisPatientHealthReportModel> pageList = hisPatientHealthReportService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(pageList)) {
            Map<String, HHealthStandardIndicatorSimpleModel> map = hHealthStandardIndicatorsService.listMapDesc();
            for (HHisPatientHealthReportModel qrModel : pageList) {
                HHealthStandardIndicatorSimpleModel indicatorsModel = map.get(qrModel.getIndicatorsCode());
                EstimateReportItemDTO item = BeanUtil.copyProperties(qrModel, EstimateReportItemDTO.class);
                item.setDesc(indicatorsModel.getDescription());
                //如果是心脏年龄需要进行转换
                if (HeartVariableEnums.H.getCode().equals(qrModel.getIndicatorsCode()) && StringUtils.isNotBlank(qrModel.getRemark())) {
                    item.setNormalValue(qrModel.getRemark());
                } else {
                    item.setNormalValue(indicatorsModel.getStandardValue());
                }
                items.add(item);
            }
        }
        return RespResult.OK(items);
    }


    /**
     * 查询血管测评 指标项
     */
    @CtrlReq
    @GetMapping(value = "/list/report/target/suggest")
    public RespResult<List<KeyStrModel>> reportTargetSuggest(HHisPatientHealthReportAnalysisQueryModel queryModel) {
        List<KeyStrModel> items = new ArrayList<>();
        List<HHisPatientHealthReportAnalysisModel> pageList = hisPatientHealthReportAnalysisService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(pageList)) {
            for (HHisPatientHealthReportAnalysisModel qrModel : pageList) {
                items.add(KeyStrModel.builder()
                        .key(qrModel.getContent())
                        .label(qrModel.getTitle())
                        .build());
            }
        }
        return RespResult.OK(items);
    }



}
