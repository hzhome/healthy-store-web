package com.hz.healthy.web.system.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2023/10/31 23:18
 * @ClassName: HWxModuleFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HWxModuleFormDTO implements Serializable {

    private Long id;

    /** app模块名称 */
    @NotBlank(message = "请输入app模块名称")
    private String appName;

    /** app模块编码 */
    @NotBlank(message = "请输入app模块编码")
    private String appCode;

    private String selectedroles;

}
