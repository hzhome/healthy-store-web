package com.hz.healthy.web.his.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/5/15 20:59
 * @ClassName: HStoreMachineGoodsModel
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HStoreMachineGoodsModel implements Serializable {

    private String goodsName;
    private BigDecimal price;
    private String brief;//简介
    private String imageUrl;
    private String description;//详细描述
}
