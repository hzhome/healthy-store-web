package com.hz.healthy.web.his.controller;

import com.alibaba.fastjson.JSON;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisActivityPlanService;
import com.hz.healthy.domain.his.model.HHisActivityPlanModel;
import com.hz.healthy.domain.his.model.HHisActivityPlanQueryModel;
import com.hz.healthy.web.his.dto.request.HHisActivityPlanFormDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


/**
 * 活动计划Controller
 *
 * @author hyhuang
 * @date 2023-11-17
 */
@RestController
@RequestMapping("/his/activity/plan")
public class HHisActivityPlanController extends WebBaseController {
    @Autowired
    private IHHisActivityPlanService hHisActivityPlanService;

    /**
     * 查询活动计划列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisActivityPlanModel>> pageQuery(HHisActivityPlanQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        RespResult<ListResponse<HHisActivityPlanModel>> result = new RespResult<ListResponse<HHisActivityPlanModel>>();
        ListResponse<HHisActivityPlanModel> pageList = hHisActivityPlanService.pageQuery(queryModel);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }


    /**
     * 获取活动计划详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hHisActivityPlanService.getById(id));
    }

    /**
     * 新增活动计划
     */
    //@PreAuthorize("@ss.hasPermi('system:plan:add')")
    //@Log(title = "活动计划", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HHisActivityPlanFormDTO planForm) {
        Date date = new Date();
        String[] ym= StringUtils.split(planForm.getSummarizeTime(),"-");
        List<HHisActivityPlanModel> dataList = JSON.parseArray(planForm.getRows(), HHisActivityPlanModel.class);
        for (HHisActivityPlanModel planModel : dataList) {
            planModel.setOrganizer(getLoginUser().getRealname());
            planModel.setOrgCode(getOrgCode());
            planModel.setTenantId(getTenantId());
            planModel.setCreateTime(date);
            planModel.setCreateUser(getLoginUser().getRealname());
            planModel.setYear(Integer.parseInt(ym[0]));
            planModel.setMonth(Integer.parseInt(ym[1]));
        }
        hHisActivityPlanService.insertBatch(dataList);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改活动计划
     */
    @PutMapping
    public RespResult edit(@RequestBody HHisActivityPlanModel hHisActivityPlanModel) {
        hHisActivityPlanService.updateById(hHisActivityPlanModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除活动计划
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hHisActivityPlanService.deleteByIds(ids);
        return RespResult.OK();
    }
}
