package com.hz.healthy.web.his.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHHisSchedulingPlanService;
import com.hz.healthy.domain.doctor.model.HHisSchedulingPlanModel;
import com.hz.healthy.domain.doctor.model.HHisSchedulingPlanQueryModel;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingPlanFormReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.item.HHisSchedulingPlanFormItem;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingPlanDTO;
import com.hz.healthy.web.his.dto.response.HHisSchedulingPlanFDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 排班方案Controller
 *
 * @author hyhuang
 * @date 2023-06-24
 */
@RestController
@RequestMapping("/his/scheduling/plan")
public class HHisSchedulingPlanController extends WebBaseController {
    @Autowired
    private IHHisSchedulingPlanService hHisSchedulingPlanService;

    /**
     * 查询排班方案列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisSchedulingPlanFDTO>> pageQuery(HHisSchedulingPlanQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisSchedulingPlanModel> pageList = hHisSchedulingPlanService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<HHisSchedulingPlanFDTO> dataList = new ArrayList<>();
            pageList.getRecords().forEach(p -> {
                StringBuffer sb = new StringBuffer();
                List<HHisSchedulingPlanFormItem> items = JSON.parseArray(p.getTime(), HHisSchedulingPlanFormItem.class);
                items.forEach(item -> {
                    sb.append(item.getPlanName() + ":" + item.getStartTime() + "~" + item.getEndTime())
                            .append("            ");
                });

                dataList.add(HHisSchedulingPlanFDTO.builder()
                        .id(p.getId())
                        .name(p.getName())
                        .type(p.getType())
                        .createTime(p.getCreateTime())
                        .status(p.getStatus())
                        .time(sb.toString())
                        .build());
            });
            return RespResult.OK(ListResponse.build(dataList,pageList));
        }
        return RespResult.OK(ListResponse.empty());
    }


    /**
     * 获取排班方案详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult<HHisSchedulingPlanDTO> getInfo(@PathVariable("id") Long id) {
        HHisSchedulingPlanDTO planRep = new HHisSchedulingPlanDTO();
        HHisSchedulingPlanModel planModel = hHisSchedulingPlanService.getById(id);
        if (planModel != null) {
            List<HHisSchedulingPlanFormItem> items = JSON.parseArray(planModel.getTime(), HHisSchedulingPlanFormItem.class);
            planRep.setId(planModel.getId());
            planRep.setName(planModel.getName());
            planRep.setType(planModel.getType());
            planRep.setRows(items);
        }
        return RespResult.OK(planRep);
    }

    /**
     * 新增排班方案
     */
    @PostMapping
    public RespResult add(@Validated @RequestBody HHisSchedulingPlanFormReqDTO formReq) {
        HHisSchedulingPlanModel schedulingPlan = new HHisSchedulingPlanModel();
        schedulingPlan.setName(formReq.getName());
        schedulingPlan.setType(formReq.getType());
        schedulingPlan.setCreateTime(new Date());
        schedulingPlan.setCreateUser(getLoginUser().getRealname());
        schedulingPlan.setOrgCode(getLoginUser().getOrgCode());
        schedulingPlan.setTenantId(getTenantId());
        if (CollectionUtils.isEmpty(formReq.getRows())) {
            return RespResult.error("请输入班次信息");
        }
        List<HHisSchedulingPlanFormItem> items = formReq.getRows();
        for (int i = 0, len = items.size(); i < len; i++) {
            if (StringUtils.isBlank(items.get(i).getPlanName())) {
                return RespResult.error("第" + (i + 1) + "行 请输入班次名称");
            }
            if (StringUtils.isBlank(items.get(i).getStartTime())) {
                return RespResult.error("第" + (i + 1) + "行 请输入起始时间");
            }
            if (StringUtils.isBlank(items.get(i).getEndTime())) {
                return RespResult.error("第" + (i + 1) + "行 请输入结束时间");
            }
            items.get(i).setPlanId(i + 1);
        }
        schedulingPlan.setTime(JSON.toJSONString(items));
        hHisSchedulingPlanService.insert(schedulingPlan);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改排班方案
     */
    @PutMapping
    public RespResult edit(@Validated @RequestBody HHisSchedulingPlanFormReqDTO formReq) {
        HHisSchedulingPlanModel planModel = hHisSchedulingPlanService.getById(formReq.getId());
        if (ObjectUtil.isNull(planModel)) {
            return RespResult.error("记录不存在");
        }
        if (CommonConstant.STATUS_1.equals(planModel.getStatus())) {
            return RespResult.error("排班方案己使用,不能删除");
        }
        if (CollectionUtils.isEmpty(formReq.getRows())) {
            return RespResult.error("请输入班次信息");
        }

        List<HHisSchedulingPlanFormItem> items = formReq.getRows();
        if (CollectionUtils.isNotEmpty(items)) {
            for (int i = 0, len = items.size(); i < len; i++) {
                if (StringUtils.isBlank(items.get(i).getPlanName())) {
                    return RespResult.error("第" + (i + 1) + "行 请输入班次名称");
                }
                if (StringUtils.isBlank(items.get(i).getStartTime())) {
                    return RespResult.error("第" + (i + 1) + "行 请输入起始时间");
                }
                if (StringUtils.isBlank(items.get(i).getEndTime())) {
                    return RespResult.error("第" + (i + 1) + "行 请输入结束时间");
                }
                items.get(i).setPlanId(i + 1);
            }
        }
        HHisSchedulingPlanModel schedulingPlan = new HHisSchedulingPlanModel();
        schedulingPlan.setId(formReq.getId());
        schedulingPlan.setName(formReq.getName());
        schedulingPlan.setType(formReq.getType());
        schedulingPlan.setUpdateTime(new Date());
        schedulingPlan.setUpdateUser(getLoginUser().getRealname());
        schedulingPlan.setTime(JSON.toJSONString(items));
        hHisSchedulingPlanService.updateById(schedulingPlan);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除排班方案
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        HHisSchedulingPlanModel planModel = hHisSchedulingPlanService.getById(id);
        if (ObjectUtil.isNull(planModel)) {
            return RespResult.error("记录不存在");
        }
        if (CommonConstant.STATUS_1.equals(planModel.getStatus())) {
            return RespResult.error("排班方案己使用,不能删除");
        }
        hHisSchedulingPlanService.deleteById(id);
        return RespResult.OK("删除成功");
    }

    @GetMapping(value = "/time/{id}")
    public RespResult planQuery(@PathVariable(value = "id") Long id) {
        HHisSchedulingPlanModel planModel = hHisSchedulingPlanService.getById(id);
        if (ObjectUtil.isNotNull(planModel)) {
            String jsonTime = planModel.getTime();
            List<HHisSchedulingPlanFormItem> items = JSON.parseArray(jsonTime, HHisSchedulingPlanFormItem.class);
            return RespResult.OK(items);
        }
        return RespResult.error("没有相应的排班方案");
    }

    @GetMapping(value = "/selectList")
    public RespResult<List<KeyLongModel>> selectList() {
        List<KeyLongModel> dataList = new ArrayList<>();
        List<HHisSchedulingPlanModel> planList = hHisSchedulingPlanService.selectList(HHisSchedulingPlanQueryModel.builder()
                .orgCode(getLoginUser().getOrgCode())
                .tenantId(getTenantId())
                .build());
        if (CollectionUtils.isNotEmpty(planList)) {
            planList.forEach(p -> {
                Map<String, Object> map = new HashMap<>();
                map.put("type", p.getType());
                KeyLongModel kl = new KeyLongModel();
                kl.setKey(p.getId());
                kl.setLabel(p.getName());
                kl.setMap(map);
                dataList.add(kl);
            });
        }
        return RespResult.OK(dataList);
    }
}
