package com.hz.healthy.web.his.dto.request;

import com.hz.ocean.common.dto.PageRequest;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/3/6 19:09
 * @ClassName: HStoreOrderDeliverFormRepDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HStoreOrderDeliverFormRepDTO extends PageRequest {

    private Long id;


    /** 配送公司(10商家配送20达达30配送员) */
    private String deliverSource;

    /** 第三方配送单号 */
    private String deliverNo;



    /** 骑手姓名 */
    private String linkman;

    /** 骑手电话 */
    private String phone;

    /** 配送状态(待接单＝1,待取货＝2,配送中＝3,已完成＝4,已取消＝5) */
    private Integer deliverStatus;



    /** 订单号 */
    private String orderNo;


    private String orderStatus;

    private String keyword;
}
