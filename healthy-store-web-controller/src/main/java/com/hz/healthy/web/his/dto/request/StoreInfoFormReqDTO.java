package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/2/2 14:17
 * @ClassName: StoreInfoFormReqDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class StoreInfoFormReqDTO implements Serializable {

    private Long id;

    /**
     * 状态
     */
    private String status;

    /**
     * 店铺名称
     */
    @NotBlank(message = "请输入店铺名称")
    private String storeName;

    @NotBlank(message = "请输入联系人")
    private String adminName;
    @NotBlank(message = "请输入联系手机")
    private String phone;

    @NotBlank(message = "请输入登录帐号")
    private String loginName;

    private String pwd;

    /** 门脸图 */
    private String avatar;


    private Long storeUserId;


    private Integer province;
    /** 城市ID */
    private Integer city;
    /** 区域ID */
    private Integer district;
    /** 店铺地址 */
    private String address;
    /** 门牌号 */
    private String houseNo;
    /** 营业执照 图片 */
    private String businessLicense;

    /** 食品经营许可 图片 */
    private String foodBusinessLicense;
    /** 法人身份证 正面 */
    private String cardFront;
    /** 法人身份证 反面 */
    private String cardOpposite;


    /** 客服电话 */
    @NotBlank(message = "请输入客服电话")
    private String customerTel;

}
