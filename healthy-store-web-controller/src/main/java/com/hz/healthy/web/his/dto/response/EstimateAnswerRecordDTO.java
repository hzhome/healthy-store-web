package com.hz.healthy.web.his.dto.response;

import com.hz.healthy.web.his.dto.response.item.EstimateQuestionItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class EstimateAnswerRecordDTO implements Serializable {

    private String questionType;

    private List<EstimateQuestionItem> questionList;
}
