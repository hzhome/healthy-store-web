package com.hz.healthy.web.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.model.SysTreeModel;
import com.hz.ocean.system.service.auth.ISysDepartService;
import com.hz.ocean.system.service.auth.ISysUserDepartService;
import com.hz.ocean.system.service.auth.ISysUserService;
import com.hz.ocean.system.service.auth.model.DepartIdModel;
import com.hz.ocean.system.service.auth.model.SysDepartModel;
import com.hz.ocean.system.service.auth.model.SysDepartTreeModel;
import com.hz.ocean.system.service.auth.model.SysUserModel;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import com.hz.ocean.web.core.utils.jwt.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 部门表 前端控制器
 * <p>
 * @author hyhuang
 * @date 2022-03-27 8:03 PM
 */
@RestController
@RequestMapping("/sys/sysDepart")
@Slf4j
public class SysDepartController {

	@Autowired
	private ISysDepartService sysDepartService;
//	@Autowired
//	public RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	private ISysUserDepartService sysUserDepartService;

	@Autowired
	private OceanBaseConfig oceanBaseConfig;

	/**
	 * 查询数据 查出我的部门,并以树结构数据格式响应给前端
	 *
	 * @return
	 */
	@RequestMapping(value = "/queryMyDeptTreeList", method = RequestMethod.GET)
	public RespResult<List<SysTreeModel>> queryMyDeptTreeList() {
		RespResult<List<SysTreeModel>> result = new RespResult<>();
		LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		try {
			if(oConvertUtils.isNotEmpty(user.getUserIdentity()) && user.getUserIdentity().equals( CommonConstant.USER_IDENTITY_2 )){
				//update-begin--Author:liusq  Date:20210624  for:部门查询ids为空后的前端显示问题 issues/I3UD06
				String departIds = user.getDepartIds();
				if(StringUtils.isNotBlank(departIds)){
					List<SysTreeModel> list = sysDepartService.queryMyDeptTreeList(departIds);
					result.setResult(list);
				}
				//update-end--Author:liusq  Date:20210624  for:部门查询ids为空后的前端显示问题 issues/I3UD06
				result.setMessage(CommonConstant.USER_IDENTITY_2.toString());
				result.setSuccess(true);
			}else{
				result.setMessage(CommonConstant.USER_IDENTITY_1.toString());
				result.setSuccess(true);
			}
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return result;
	}

	/**
	 * 查询数据 查出所有部门,并以树结构数据格式响应给前端
	 * hyhuang 改造为科室
	 * 
	 * @return
	 */
	@RequestMapping(value = "/queryTreeList", method = RequestMethod.GET)
	public RespResult<List<SysDepartTreeModel>> queryTreeList(@RequestParam(name = "ids", required = false) String ids) {
		RespResult<List<SysDepartTreeModel>> result = new RespResult<>();
		try {
			if(oConvertUtils.isNotEmpty(ids)){
				List<SysDepartTreeModel> departList = sysDepartService.queryTreeList(ids);
				result.setResult(departList);
			}else{
//				List<SysDepartTreeModel> list = sysDepartService.queryTreeList();
//				result.setResult(list);
			}
			result.setSuccess(true);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return result;
	}

	/**
	 * 异步查询部门list
	 * @param parentId 父节点 异步加载时传递
	 * @param ids 前端回显是传递
	 * @return
	 */
	@RequestMapping(value = "/queryDepartTreeSync", method = RequestMethod.GET)
	public RespResult<List<SysDepartTreeModel>> queryDepartTreeSync(@RequestParam(name = "pid", required = false) String parentId,@RequestParam(name = "ids", required = false) String ids) {
		RespResult<List<SysDepartTreeModel>> result = new RespResult<>();
		try {
			List<SysDepartTreeModel> list = sysDepartService.queryTreeListByPid(parentId,ids);
			result.setResult(list);
			result.setSuccess(true);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return result;
	}

	/**
	 * 获取某个部门的所有父级部门的ID
	 *
	 * @param departId 根据departId查
	 * @param orgCode  根据orgCode查，departId和orgCode必须有一个不为空
	 */
	@GetMapping("/queryAllParentId")
	public RespResult queryParentIds(
			@RequestParam(name = "departId", required = false) String departId,
			@RequestParam(name = "orgCode", required = false) String orgCode
	) {
		try {
			JSONObject data;
			if (oConvertUtils.isNotEmpty(departId)) {
				data = sysDepartService.queryAllParentIdByDepartId(departId);
			} else if (oConvertUtils.isNotEmpty(orgCode)) {
				data = sysDepartService.queryAllParentIdByOrgCode(orgCode,oceanBaseConfig.getSystemId());
			} else {
				return RespResult.error("departId 和 orgCode 不能都为空！");
			}
			return RespResult.OK(data);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return RespResult.error(e.getMessage());
		}
	}

	/**
	 * 添加新数据 添加用户新建的部门对象数据,并保存到数据库
	 * 
	 * @param sysDepart
	 * @return
	 */
	//@RequiresRoles({"admin"})
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public RespResult<SysDepartModel> add(@RequestBody SysDepartModel sysDepart, HttpServletRequest request) {
		RespResult<SysDepartModel> result = new RespResult<SysDepartModel>();
		String username = JwtUtil.getUserNameByToken(request);
		try {
			sysDepart.setCreateBy(username);
			sysDepartService.saveDepartData(sysDepart, username,"","");
			//清除部门树内存
			// FindsDepartsChildrenUtil.clearSysDepartTreeList();
			// FindsDepartsChildrenUtil.clearDepartIdModel();
			result.success("添加成功！");
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			return RespResult.error("操作失败");
		}
		return result;
	}

	/**
	 * 编辑数据 编辑部门的部分数据,并保存到数据库
	 * 
	 * @param sysDepart
	 * @return
	 */
	//@RequiresRoles({"admin"})
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public RespResult<SysDepartModel> edit(@RequestBody SysDepartModel sysDepart, HttpServletRequest request) {
		String username = JwtUtil.getUserNameByToken(request);
		sysDepart.setUpdateBy(username);
		RespResult<SysDepartModel> result = new RespResult<SysDepartModel>();
		SysDepartModel sysDepartEntity = sysDepartService.getById(sysDepart.getId());
		if (sysDepartEntity == null) {
			return RespResult.error("未找到对应实体");
		} else {
			boolean ok = sysDepartService.updateDepartDataById(sysDepart, username);
			// TODO 返回false说明什么？
			if (ok) {
				//清除部门树内存
				//FindsDepartsChildrenUtil.clearSysDepartTreeList();
				//FindsDepartsChildrenUtil.clearDepartIdModel();
				result.success("修改成功!");
			}
		}
		return result;
	}
	
	 /**
     *   通过id删除
    * @param id
    * @return
    */
	//@RequiresRoles({"admin"})
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
   public RespResult<SysDepartModel> delete(@RequestParam(name="id",required=true) String id) {

		RespResult<SysDepartModel> result = new RespResult<SysDepartModel>();
		SysDepartModel sysDepart = sysDepartService.getById(id);
       if(sysDepart==null) {
           return RespResult.error("未找到对应实体");
       }else {
           boolean ok = sysDepartService.delete(id);
           if(ok) {
	            //清除部门树内存
	   		   //FindsDepartsChildrenUtil.clearSysDepartTreeList();
	   		   // FindsDepartsChildrenUtil.clearDepartIdModel();
               result.success("删除成功!");
           }
       }
       return result;
   }


	/**
	 * 批量删除 根据前端请求的多个ID,对数据库执行删除相关部门数据的操作
	 * 
	 * @param ids
	 * @return
	 */
	//@RequiresRoles({"admin"})
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	public RespResult<SysDepartModel> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {

		RespResult<SysDepartModel> result = new RespResult<SysDepartModel>();
		if (ids == null || "".equals(ids.trim())) {
			return RespResult.error("参数不识别！");
		} else {
			this.sysDepartService.deleteBatchWithChildren(Arrays.asList(ids.split(",")));
			result.success("删除成功!");
		}
		return result;
	}

	/**
	 * 查询数据 添加或编辑页面对该方法发起请求,以树结构形式加载所有部门的名称,方便用户的操作
	 * 
	 * @return
	 */
	@RequestMapping(value = "/queryIdTree", method = RequestMethod.GET)
	public RespResult<List<DepartIdModel>> queryIdTree() {
		RespResult<List<DepartIdModel>> result = new RespResult<>();
		try {
			List<DepartIdModel> list = sysDepartService.queryDepartIdTreeList(oceanBaseConfig.getSystemId());
			result.setResult(list);
			result.setSuccess(true);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return result;
	}
	 
	/**
	 * <p>
	 * 部门搜索功能方法,根据关键字模糊搜索相关部门
	 * </p>
	 * 
	 * @param keyWord
	 * @return
	 */
	@RequestMapping(value = "/searchBy", method = RequestMethod.GET)
	public RespResult<List<SysDepartTreeModel>> searchBy(@RequestParam(name = "keyWord", required = true) String keyWord,@RequestParam(name = "myDeptSearch", required = false) String myDeptSearch) {
		RespResult<List<SysDepartTreeModel>> result = new RespResult<List<SysDepartTreeModel>>();
		//部门查询，myDeptSearch为1时为我的部门查询，登录用户为上级时查只查负责部门下数据
		LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		String departIds = null;
		if(oConvertUtils.isNotEmpty(user.getUserIdentity()) && user.getUserIdentity().equals( CommonConstant.USER_IDENTITY_2 )){
			departIds = user.getDepartIds();
		}
		List<SysDepartTreeModel> treeList = this.sysDepartService.searchByKeyWord(keyWord,myDeptSearch,departIds, oceanBaseConfig.getSystemId());
		if (treeList == null || treeList.size() == 0) {
			result.setSuccess(false);
			result.setMessage("未查询匹配数据！");
			return result;
		}
		result.setResult(treeList);
		return result;
	}





	/**
	 * 查询所有部门信息
	 * @return
	 */
	@GetMapping("listAll")
	public RespResult<List<SysDepartModel>> listAll(@RequestParam(name = "id", required = false) String id) {
		RespResult<List<SysDepartModel>> result = new RespResult<>();
		SysDepartModel queryModel=new SysDepartModel();
		//query.orderByAsc(SysDepart::getOrgCode);
		if(oConvertUtils.isNotEmpty(id)){
			String arr[] = id.split(",");
			queryModel.setIdList(Arrays.asList(arr));
		}
		List<SysDepartModel> ls = this.sysDepartService.list(queryModel);
		result.setSuccess(true);
		result.setResult(ls);
		return result;
	}
	/**
	 * 查询数据 查出所有部门,并以树结构数据格式响应给前端
	 *
	 * @return
	 */
	@RequestMapping(value = "/queryTreeByKeyWord", method = RequestMethod.GET)
	public RespResult<Map<String,Object>> queryTreeByKeyWord(@RequestParam(name = "keyWord", required = false) String keyWord) {
		RespResult<Map<String,Object>> result = new RespResult<>();
		try {
			Map<String,Object> map=new HashMap<String,Object>();
			List<SysTreeModel> list = sysDepartService.queryTreeByKeyWord(keyWord,oceanBaseConfig.getSystemId());
			//根据keyWord获取用户信息
//			LambdaQueryWrapper<SysUser> queryUser = new LambdaQueryWrapper<SysUser>();
//			queryUser.eq(SysUser::getDelFlag,CommonConstant.DEL_FLAG_0);
//			queryUser.and(i -> i.like(SysUser::getUsername, keyWord).or().like(SysUser::getRealname, keyWord));
			SysUserModel queryModel=new SysUserModel();
			queryModel.setDelFlag(CommonConstant.DEL_FLAG_0);
			List<SysUserModel> sysUsers = this.sysUserService.list(queryModel);
			map.put("userList",sysUsers);
			map.put("departList",list);
			result.setResult(map);
			result.setSuccess(true);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return result;
	}

	/**
	 * 根据部门编码获取部门信息
	 *
	 * @param orgCode
	 * @return
	 */
	@GetMapping("/getDepartName")
	public RespResult<SysDepartModel> getDepartName(@RequestParam(name = "orgCode") String orgCode) {
		RespResult<SysDepartModel> result = new RespResult<>();
		SysDepartModel queryModel=new SysDepartModel();
		queryModel.setOrgCode(orgCode);
		SysDepartModel sysDepart = sysDepartService.getOne(queryModel);
		result.setSuccess(true);
		result.setResult(sysDepart);
		return result;
	}

	/**
	 * 根据部门id获取用户信息
	 *
	 * @param id
	 * @return
	 */
	@GetMapping("/getUsersByDepartId")
	public RespResult<List<SysUserModel>> getUsersByDepartId(@RequestParam(name = "id") String id) {
		RespResult<List<SysUserModel>> result = new RespResult<>();
		List<SysUserModel> sysUsers = sysUserDepartService.queryUserByDepId(id,oceanBaseConfig.getSystemId());
		result.setSuccess(true);
		result.setResult(sysUsers);
		return result;
	}
}
