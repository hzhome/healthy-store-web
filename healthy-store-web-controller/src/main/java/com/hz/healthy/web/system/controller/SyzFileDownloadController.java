package com.hz.healthy.web.system.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.service.file.ISyzFileDownloadService;
import com.hz.ocean.system.service.file.model.SyzFileDownloadModel;
import com.hz.ocean.system.service.file.model.SyzFileDownloadQueryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author hyhuang
 * @Date 2023/11/21 15:54
 * @ClassName: SyzFileDownloadController
 * @Description: TODO
 * @Version 1.0
 */
@RestController
@RequestMapping("/syz/download")
public class SyzFileDownloadController extends WebBaseController {

    @Autowired
    private ISyzFileDownloadService syzFileDownloadService;

    /**
     *
     */
    @GetMapping("/list")
    public RespResult<ListResponse<SyzFileDownloadModel>> pageQuery(SyzFileDownloadQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        ListResponse<SyzFileDownloadModel> pageList = syzFileDownloadService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }
}
