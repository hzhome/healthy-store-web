package com.hz.healthy.web.system.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class SysDictTenantFormRepDTO implements Serializable {

    private String id;


    /** 字典编码 */
    @NotBlank(message = "字典编码参数错误")
    private String dictCode;

    /** 字典项文本 */
    @NotBlank(message = "名称不能为空")
    private String itemText;

    /** 字典项值 */
    @NotBlank(message = "编号不能为空")
    private String itemValue;

    /** 描述 */
    private String description;

    private int isChild;

    private int sortOrder;

    private String itemValue1;

    private String itemValue2;

}
