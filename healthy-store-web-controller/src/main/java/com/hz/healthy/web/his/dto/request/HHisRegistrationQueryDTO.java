package com.hz.healthy.web.his.dto.request;

import com.hz.ocean.common.dto.PageRequest;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/12/19 16:36
 * @ClassName: HHisRegistrationQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisRegistrationQueryDTO  extends PageRequest {

    private Long id;

    /** 1挂号 2出诊 */
    private String appointmentType;

    /** 患者ID(成员) */
    private Long patientId;
    /** 患者名称(成员) */
    private String patientName;
    /** 医生名称 */
    private String doctorName;

    /** 医生主键 */
    private Long doctorId;



    /** 科室ID */
    private Long departmentId;


    /** 挂号状态,1为待就诊，3为已退号，2为已就诊,4为作废，5,为未付款,6，为部分支付 */
    private String registrationStatus;

    /**
     * 预约日期
     */
    private String schedulingDate;



    private String sureStatus;



    private String beginToday;

    private String endToday;
}
