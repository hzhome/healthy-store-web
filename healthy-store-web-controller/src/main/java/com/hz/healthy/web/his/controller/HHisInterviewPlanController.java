package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisInterviewPlanDetailService;
import com.hz.healthy.domain.his.IHHisInterviewPlanService;
import com.hz.healthy.domain.his.IHHisTaskPlanService;
import com.hz.healthy.domain.his.model.*;
import com.hz.healthy.domain.patient.IHHospitalArchivesService;
import com.hz.healthy.domain.patient.model.HHospitalArchivesModel;
import com.hz.healthy.enums.TaskPlanRemindEnums;
import com.hz.healthy.web.his.dto.request.HHisInterviewPlanFormDTO;
import com.hz.healthy.web.his.dto.response.HHisInterviewPlanDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.enums.SexEnum;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.DateUtils;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 走访计划Controller
 *
 * @author ruoyi
 * @date 2023-11-05
 */
@RestController
@RequestMapping("/ch/interview/plan")
public class HHisInterviewPlanController extends WebBaseController {
    @Autowired
    private IHHisInterviewPlanService hHisInterviewPlanService;

    @Autowired
    private IHHospitalArchivesService hospitalArchivesService;
    @Autowired
    private IHHisInterviewPlanDetailService hisInterviewPlanDetailService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    @Autowired
    private IHHisTaskPlanService hisTaskPlanService;

    /**
     * 查询走访计划列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisInterviewPlanDTO>> pageQuery(HHisInterviewPlanQueryModel queryModel) {
        List<HHisInterviewPlanDTO> dataList = new ArrayList<>();
        queryModel.setTenantId(getTenantId());
        queryModel.setOrgCode(getOrgCode());
        ListResponse<HHisInterviewPlanModel> pageList = hHisInterviewPlanService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            pageList.getRecords().forEach(p -> {
                HHisInterviewPlanDTO plan = BeanUtil.toBean(p, HHisInterviewPlanDTO.class);
                /** 1走访 2去电 3事件提示 */
                if ("1".equals(p.getPlanType())) {
                    plan.setStartTime(DateUtil.format(p.getStartTime(), "yyyy-MM-dd"));
                    plan.setEndTime(DateUtil.format(p.getEndTime(), "yyyy-MM-dd"));
                } else if ("2".equals(p.getPlanType())) {
                    plan.setStartTime(DateUtil.formatDate(p.getStartTime()));
                    plan.setEndTime(DateUtil.formatDate(p.getEndTime()));
                }

                dataList.add(plan);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 获取走访计划详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Integer id) {
        return RespResult.OK(hHisInterviewPlanService.getById(id));
    }


    private HHisInterviewPlanModel addInterview(HHisInterviewPlanFormDTO planForm, boolean isAdd) {
        String[] startTimeAy = StringUtils.split(planForm.getStartTime(), "-");
        int monthQty = DateUtils.getDaysOfMonth2(Integer.parseInt(startTimeAy[0]), Integer.parseInt(startTimeAy[1]));

        Date startDay = DateUtil.parseDateTime(planForm.getStartTime() + " 00:00:01");
        if (isAdd) {
            String planCode = seqNoAutoService.buildBatchNo();
            HHisInterviewPlanModel planModel = HHisInterviewPlanModel.builder()
                    .planCode(planCode)
                    .planTitle(planForm.getPlanTitle())
                    .year(DateUtil.year(startDay) + "")
                    .startTime(startDay)
                    .endTime(DateUtil.parseDateTime(planForm.getEndTime() + " 00:00:01"))
                    .execCount(planForm.getExecCount())
                    .followUpContent(planForm.getFollowUpContent())
                    .execType(planForm.getExecType())
                    .taskStatus("1")
                    .eyId(planForm.getEyId())
                    .eyName(planForm.getEyName())
                    .finishCount(0)
                    .planType("1")
                    .createTime(new Date())
                    .createUser(getLoginUser().getRealname())
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .build();
            return planModel;
        } else {
            HHisInterviewPlanModel planModel = HHisInterviewPlanModel.builder()
                    .id(planForm.getId())
                    .year(planForm.getYear())
                    .planTitle(planForm.getPlanTitle())
                    .startTime(DateUtil.parseDateTime(planForm.getStartTime() + "-01 00:00:01"))
                    .endTime(DateUtil.parseDateTime(planForm.getEndTime() + "-" + monthQty + " 00:00:01"))
                    .execCount(planForm.getExecCount())
                    .followUpContent(planForm.getFollowUpContent())
                    .execType(planForm.getExecType())
                    .taskStatus("1")
                    .eyId(planForm.getEyId())
                    .eyName(planForm.getEyName())
                    .finishCount(0)
                    .planType("1")
                    .updateTime(new Date())
                    .updateUser(getLoginUser().getRealname())
                    .build();
            return planModel;
        }


    }

    private HHisInterviewPlanModel addPhone(HHisInterviewPlanFormDTO planForm, boolean isAdd) {

        if (isAdd) {
            String planCode = seqNoAutoService.buildBatchNo();
            HHisInterviewPlanModel planModel = HHisInterviewPlanModel.builder()
                    .planCode(planCode)
                    .planTitle(planForm.getPlanTitle())
                    .year(String.valueOf(DateUtil.year(DateUtil.parseDate(planForm.getStartTime()))))
                    .startTime(DateUtil.parseDate(planForm.getStartTime()))
                    .endTime(DateUtil.parseDate(planForm.getEndTime()))
                    .execCount(planForm.getExecCount())
                    .followUpContent(planForm.getFollowUpContent())
                    .planType(planForm.getPlanType())
                    .execType(planForm.getExecType())
                    .taskStatus("1")
                    .eyId(planForm.getEyId())
                    .planType("2")
                    .eyName(planForm.getEyName())
                    .finishCount(0)
                    .createTime(new Date())
                    .createUser(getLoginUser().getRealname())
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .build();
            return planModel;
        } else {
            HHisInterviewPlanModel planModel = HHisInterviewPlanModel.builder()
                    .id(planForm.getId())
                    .year(String.valueOf(DateUtil.year(DateUtil.parseDate(planForm.getStartTime()))))
                    .startTime(DateUtil.parseDate(planForm.getStartTime()))
                    .endTime(DateUtil.parseDate(planForm.getEndTime()))
                    .execCount(planForm.getExecCount())
                    .followUpContent(planForm.getFollowUpContent())
                    .execType(planForm.getExecType())
                    .taskStatus("1")
                    .eyId(planForm.getEyId())
                    .planType("2")
                    .eyName(planForm.getEyName())
                    .finishCount(0)
                    .updateTime(new Date())
                    .updateUser(getLoginUser().getRealname())
                    .build();
            return planModel;
        }

    }

    /**
     * 新增走访计划
     */
    @Transactional
    @PostMapping
    public RespResult add(@Validated @RequestBody HHisInterviewPlanFormDTO planForm) {
        if (CollectionUtils.isEmpty(planForm.getPatientIds())) {
            return RespResult.error("请选择客户信息");
        }
        HHisInterviewPlanModel planModel = null;
        //1走访 2去电 3事件提示
        if ("1".equals(planForm.getPlanType())) {
            planModel = addInterview(planForm, true);
        } else if ("2".equals(planForm.getPlanType())) {
            planModel = addPhone(planForm, true);
        }
        Map<Long, HHospitalArchivesModel> patientMap = hospitalArchivesService.mapByIds(planForm.getPatientIds(),getOrgCode());

        List<HHisInterviewPlanDetailModel> insertList = new ArrayList<>();
        for (Long patientId : planForm.getPatientIds()) {
            HHospitalArchivesModel patient = patientMap.get(patientId);
            insertList.add(HHisInterviewPlanDetailModel.builder()
                    .eyId(planModel.getEyId())
                    .eyName(planModel.getEyName())
                    .execType(planModel.getExecType())
                    .startTime(planModel.getStartTime())
                    .endTime(planModel.getEndTime())
                    .execCount(planModel.getExecCount())
                    .finishCount(planModel.getFinishCount())
                    .planCode(planModel.getPlanCode())
                    .patientId(patientId)
                    .patientName(patient.getRealName())
                    .patientAge(patient.getAge())
                    .patientSex(SexEnum.getEnumToDesc(patient.getSex()))
                    .createTime(planModel.getCreateTime())
                    .createUser(getLoginUser().getRealname())
                    .orgCode(getOrgCode())
                    .tenantId(getTenantId())
                    .planType(planForm.getPlanType())
                    .status("0")
                    .build());
        }
        planModel.setServiceQty(CollectionUtils.size(insertList));
        hisInterviewPlanDetailService.insertBatch(insertList);
        hHisInterviewPlanService.insert(planModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改走访计划
     */
    @PutMapping
    public RespResult edit(@Validated @RequestBody HHisInterviewPlanFormDTO planForm) {
        HHisInterviewPlanModel planModel = null;
        //1走访 2去电 3事件提示
        if ("1".equals(planForm.getPlanType())) {
            planModel = addInterview(planForm, false);
        } else if ("2".equals(planForm.getPlanType())) {
            planModel = addPhone(planForm, false);
        }
        hHisInterviewPlanService.updateById(planModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 开始执行走访计划
     */
    @Transactional
    @PostMapping(value = "/sure")
    public RespResult sure(@RequestBody HHisInterviewPlanFormDTO planForm) {
        HHisInterviewPlanModel planModel = hHisInterviewPlanService.getById(planForm.getId());
        if (ObjectUtil.isNull(planModel)) {
            return RespResult.error("计划不存在");
        }
        Date now = new Date();
        hHisInterviewPlanService.updateById(HHisInterviewPlanModel.builder()
                .id(planModel.getId())
                .taskStatus("1")
                .updateTime(now)
                .updateUser(getLoginUser().getRealname())
                .build());
        //更新记录
        List<HHisInterviewPlanDetailModel> dataList = hisInterviewPlanDetailService.selectList(HHisInterviewPlanDetailQueryModel.builder().planCode(planModel.getPlanCode()).build());
        if (CollectionUtils.isNotEmpty(dataList)) {
            //更新明细，等执行
            List<Long> patientIds = dataList.stream().map(HHisInterviewPlanDetailModel::getPatientId).collect(Collectors.toList());
            Map<Long, HHospitalArchivesModel> patientModelMap = hospitalArchivesService.mapByIds(patientIds,getOrgCode());
            for (HHisInterviewPlanDetailModel p : dataList) {
                HHospitalArchivesModel patientModel = patientModelMap.get(p.getPatientId());
                if (StringUtils.isBlank(patientModel.getAddress())) {
                    throw new BizException("请维护客户详细地址");
                }
                hisInterviewPlanDetailService.updateById(HHisInterviewPlanDetailModel.builder()
                        .id(p.getId())
                        .status("1")
                        .updateTime(now)
                        .planTitle(planModel.getPlanTitle())
                        .patientPhone(patientModel.getMobilePhone())
                        .address(patientModel.getAddress())
                        .updateUser(getLoginUser().getRealname())
                        .build());
                //创建任务提醒
                hisTaskPlanService.insert(HHisTaskPlanModel.builder()
                        .taskTime(0)
                        .endTime(DateUtil.offsetDay(p.getStartTime(), 1))
                        .startTime(p.getStartTime())
                        .taskKey(TaskPlanRemindEnums.FOLLOWUP.getCode())
                        .busCode(p.getPlanCode())
                        .taskDate(DateUtil.formatDate(p.getStartTime()))
                        .orgCode(p.getOrgCode())
                        .status(CommonConstant.STATUS_0)//未确认
                        .patientId(p.getPatientId())
                        .eyId(p.getEyId())
                        .tenantId(p.getTenantId())
                        .createTime(now)
                        .content(p.getPatientName()+"["+planModel.getPlanTitle()+"]")
                        .build());
            }
        }

        return RespResult.OK("确认完成");
    }


    /**
     * 删除走访计划
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Integer id) {
        HHisInterviewPlanModel planModel = hHisInterviewPlanService.getById(id);
        if (ObjectUtil.isNull(planModel)) {
            return RespResult.error("计划不存在");
        }
        if (!"1".equals(planModel.getTaskStatus())) {
            return RespResult.error("计划已经执行,不能删除");
        }
        hHisInterviewPlanService.deleteById(id);
        return RespResult.OK("删除成功");
    }
}
