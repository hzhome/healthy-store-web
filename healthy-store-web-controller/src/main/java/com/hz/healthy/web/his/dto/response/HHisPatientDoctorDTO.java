package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/10/10 11:46
 * @ClassName: HHisPatientDoctorDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientDoctorDTO implements Serializable {

    /** 主键 */
    private Long id;

    /** 医生ID */
    private Long doctorId;
    private String doctorName;

    /**
     * 是否有效
     * 0无效 1有效
     */
    private String status;

    /**
     * 分组
     */
    private String groupId;

    /** 更新人员 */
    private String updateUser;

    /** 最近一次更新时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 创建人员 */
    private String createUser;

    /** 商品的添加时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;






}
