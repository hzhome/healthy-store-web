package com.hz.healthy.web.his.controller;

import cn.hutool.core.date.DateUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHHisSchedulingDescService;
import com.hz.healthy.domain.doctor.IHHisSchedulingPlanService;
import com.hz.healthy.domain.doctor.IHHisSchedulingService;
import com.hz.healthy.domain.doctor.model.HHisSchedulingDescModel;
import com.hz.healthy.domain.doctor.model.HHisSchedulingDescQueryModel;
import com.hz.healthy.domain.doctor.model.HHisSchedulingPlanModel;
import com.hz.healthy.domain.doctor.model.HHisSchedulingQueryModel;
import com.hz.healthy.his.store.service.IHSchedulingAggregationService;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingDoorServiceFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingEditFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingColumnDTO;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingDTO;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingDescDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 我的排班Controller
 *
 * @author hyhuang
 * @date 2023-01-17
 */
@RestController
@RequestMapping("/his/scheduling")
public class HHisSchedulingController extends WebBaseController {
    @Autowired
    private IHHisSchedulingService hHisSchedulingService;

    @Autowired
    private IHHisSchedulingDescService hisSchedulingDescService;
    @Autowired
    private IHSchedulingAggregationService ihSchedulingAggregationService;
    @Autowired
    private IHHisSchedulingPlanService hisSchedulingPlanService;

    /**
     * 查询我的排班列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisSchedulingDescDTO>> pageQuerySchedulingDesc(HHisSchedulingDescQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        return ihSchedulingAggregationService.pageQuerySchedulingDesc(queryModel);
    }

    @GetMapping("/get")
    public RespResult<HHisSchedulingDescDTO> getDesc(HHisSchedulingDescQueryModel queryModel) {
        return RespResult.OK(ihSchedulingAggregationService.getSchedulingDesc(queryModel));
    }

    /**
     * 删除我的排班
     */
    @DeleteMapping("/{id}")
    public RespResult delSchedulingDesc(@PathVariable Long id) {
        ihSchedulingAggregationService.delSchedulingDesc(id);
        return RespResult.OK("删除成功");
    }


    @GetMapping("/week/list")
    public RespResult<ListResponse<HHisSchedulingDTO>> pageQuerySub(HHisSchedulingQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        return ihSchedulingAggregationService.pageQuerySub(queryModel);
    }

    /**
     * 获取我的排班详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hHisSchedulingService.getById(id));
    }


    /**
     * 查询医生信息列表
     */
    @PostMapping("/scheduling/list")
    public RespResult<List<HHisSchedulingDTO>> pageQueryScheduling(@RequestBody HHisSchedulingQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        return ihSchedulingAggregationService.listScheduling(queryModel);
    }


    /**
     * 新增我的排班
     */
    @PostMapping(value = "/allowAdd")
    public RespResult<String> allowAdd(@RequestBody HHisSchedulingFromReqDTO req) {
        String schedulingCode = ihSchedulingAggregationService.saveSchedulingPlan(req, getLoginUser());
        return RespResult.OK(schedulingCode);
    }

    /**
     * 新增我的排班
     */
    @PostMapping
    public RespResult add(@RequestBody HHisSchedulingFromReqDTO req) {
        ihSchedulingAggregationService.saveScheduling(req, getLoginUser());
        return RespResult.OK("添加成功");
    }

    /**
     * 修改我的排班
     */
    @PutMapping
    public RespResult<String> edit(@RequestBody HHisSchedulingEditFromReqDTO fromReq) {
        String result = ihSchedulingAggregationService.editScheduling(fromReq, getLoginUser());
        return RespResult.OK(result);
    }




    /**
     * 读取班次时间
     *
     * @param fromReqDTO
     * @return
     */
    @Deprecated
    @PostMapping(value = "/column")
    public RespResult column(@RequestBody HHisSchedulingFromReqDTO fromReqDTO) {
        HHisSchedulingPlanModel planModel = hisSchedulingPlanService.getById(fromReqDTO.getPlanId());
        List<HHisSchedulingColumnDTO> dataList = new ArrayList<>();
        Date startDate = DateUtil.parseDate(fromReqDTO.getStartDate());
        try {
            Map<String, Integer> dateRange = DateUtils.getTwoDaysDayDes(startDate, DateUtil.offsetDay(startDate, planModel.getType().equals("0") ? 6: 30));
            for (Map.Entry<String, Integer> entry : dateRange.entrySet()) {
                String d = entry.getKey();
                Integer week = entry.getValue();
                dataList.add(HHisSchedulingColumnDTO.builder()
                        .title(CommonConstant.WEEK[week] + "(" + d + ")")
                        .week(week)
                        .date(d)
                        .build());
            }
        } catch (Exception e) {

        }
        return RespResult.OK(dataList);
    }


    /**
     * 读取班次时间
     *
     * @return
     */
    @GetMapping(value = "/column/{schedulingCode}/{pageNo}")
    public RespResult<ListResponse<HHisSchedulingColumnDTO>> column(@PathVariable(value = "schedulingCode") String schedulingCode, @PathVariable(value = "pageNo") Integer pageNo) {
        List<HHisSchedulingColumnDTO> dataList = new ArrayList<>();
        HHisSchedulingDescModel hHisSchedulingDescModel = hisSchedulingDescService.getOne(HHisSchedulingDescQueryModel.builder()
                .schedulingCode(schedulingCode)
                .orgCode(getOrgCode())
                .build());
        try {
            Date end=DateUtil.offsetDay(hHisSchedulingDescModel.getEndTimeThisWeek(),-1);
            Map<String, Integer> dateRange = DateUtils.getTwoDaysDayDes(hHisSchedulingDescModel.getStartTimeThisWeek(), end);
            for (Map.Entry<String, Integer> entry : dateRange.entrySet()) {
                String d = entry.getKey();
                Integer week = entry.getValue();
                dataList.add(HHisSchedulingColumnDTO.builder()
                        .title(CommonConstant.WEEK[week] + "(" + d + ")")
                        .week(week)
                        .date(d)
                        .build());
            }
        } catch (Exception e) {

        }
        return RespResult.OK(partition(dataList, 8, pageNo));
    }


    private ListResponse<HHisSchedulingColumnDTO> partition(List<HHisSchedulingColumnDTO> list, int pageSize, int pageNum) {
        // 将 List 按照 PageSzie 拆分成多个List
        List<? extends List<?>> partition = Lists.partition(list, pageSize);
        // 总页数
        int pages = partition.size();
        pageNum = pageNum <= 0 ? 0 : (pageNum <= (pages - 1) ? pageNum : (pages - 1));
        List<HHisSchedulingColumnDTO> dataList = (List<HHisSchedulingColumnDTO>) partition.get(pageNum);
        return ListResponse.build(dataList).withTotalCount(list.size()).withPageSize(5).withPageNo(pageNum);
    }


    /**
     * 新增上门预约时间
     */
    @PostMapping(value = "/addDoorServiceTime")
    public RespResult addDoorServiceTime(@RequestBody HHisSchedulingDoorServiceFromReqDTO req) {
        ihSchedulingAggregationService.saveDoorServiceScheduling(req, getLoginUser());
        return RespResult.OK("添加成功");
    }
}
