package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/11/20 23:40
 * @ClassName: HHisPrescriptionTemplateDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPrescriptionTemplateDTO implements Serializable {

    private Long id;



    /** 创建人员 */
    private String createUser;

    /** 添加时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;



    /** 处方模板编码 */
    private String prescriptionCode;

    /** 模板分类code */
    private String categoryCode;

    private String categoryName;

    /** 处方类型 1康复处方 2常规项目处方 3西药处方 4中药处方 */
    private String templateType;

    /** 医生ID */
    private Long eyId;

    /** 科室ID */
    private Long departId;

    /** 处方名称 */
    private String prescriptionName;

    /** 状态 0有效 1无效 */
    private String status;

    /** 功能或作用 */
    private String functions;

    /** 主治 */
    private String indications;

    /** 来源 出自于谁，个人，教材等 */
    private String source;

    /** 备注，用于处方加内容或减内容，根据体质等特点 */
    private String remark;
}
