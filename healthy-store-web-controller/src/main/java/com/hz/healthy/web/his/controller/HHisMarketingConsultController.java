package com.hz.healthy.web.his.controller;

import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisMarketingConsultService;
import com.hz.healthy.domain.his.model.HHisMarketingConsultModel;
import com.hz.healthy.domain.his.model.HHisMarketingConsultQueryModel;
import com.hz.healthy.web.his.dto.request.HHisMarketingConsultReplyDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 咨询登记Controller
 *
 * @author hyhuang
 * @date 2023-06-03
 */
@RestController
@RequestMapping("/his/marketing/consult")
public class HHisMarketingConsultController extends WebBaseController {
    @Autowired
    private IHHisMarketingConsultService hHisMarketingConsultService;

    /**
     * 查询咨询登记列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisMarketingConsultModel>> pageQuery(HHisMarketingConsultQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisMarketingConsultModel> pageList = hHisMarketingConsultService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }


    /**
     * 设置失效咨询登记
     */
    @DeleteMapping("/{id}")
    public RespResult remove(@PathVariable Long id) {
        HHisMarketingConsultModel consultModel = new HHisMarketingConsultModel();
        consultModel.setId(id);
        consultModel.setIzInvalid(CommonConstant.DEL_FLAG_1);
        hHisMarketingConsultService.updateById(consultModel);
        return RespResult.OK("设置失效成功");
    }


    /**
     * 回复
     *
     * @param reply
     * @return
     */
    @PostMapping("/reply")
    public RespResult reply(@RequestBody HHisMarketingConsultReplyDTO reply) {
        if (StringUtils.isBlank(reply.getReply())) {
            return RespResult.error("请填写回复内容");
        }
        int count = hHisMarketingConsultService.count(HHisMarketingConsultQueryModel.builder()
                .id(reply.getId()).build());
        if (count == 0) {
            return RespResult.error("咨询内容不存在");
        }
        hHisMarketingConsultService.updateById(HHisMarketingConsultModel.builder()
                .id(reply.getId())
                .followResult(reply.getReply())
                .followStatus(CommonConstant.yes_1)
                .followDate(new Date())
                .receiverId(getLoginUser().getEyId())
                .receiver(getLoginUser().getRealname())
                .build());
        return RespResult.OK("回复保存成功");
    }


}
