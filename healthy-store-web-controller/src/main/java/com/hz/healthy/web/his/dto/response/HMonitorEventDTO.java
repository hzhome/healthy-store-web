package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.healthy.web.his.dto.response.item.WarnItem;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/3/7 11:03
 * @ClassName: HMonitorEventDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HMonitorEventDTO implements Serializable {


    private Long id;

    /** 创建人员 */
    private String createUser;

    /** 创建时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;



    /** 监控编码唯一 */
    private String monitorCode;

    /**
     * 监控KEY 1 心血管测评结果 2 血压  3血糖 4尿酸
     */
    private String monitorType;

    /** 监控设备发出 */
    private String monitorDevice;

    /** 监控数值 */
    private String monitorValue;

    /** 患者主键 */
    private Long patientId;

    /** 患者名称 */
    private String patientName;

    /** 患者性别 */
    private String patientSex;

    /** 患者年龄 */
    private Integer patientAge;

    /** 经度 */
    private BigDecimal positionLng;

    /** 纬度 */
    private BigDecimal positionLat;

    /** 上报数据的省 */
    private String province;

    /** 上报数据的城市 */
    private String city;

    /** 上报数据的区域 */
    private String district;

    /** 上报数据的住址 */
    private String address;

    /** 年 格式YYYY */
    private Integer year;

    /** 月 格式YYYY */
    private Integer month;

    /** 月中的第几周 */
    private Integer week;

    /** 监控状态 0待处理 1处理中  2处理完成  3不需处理 */
    private String status;


    /**
     * 标记当前的数值是否存在异常 0否 1是
     */
    private String izException;

    /**
     * 监控来源 设备上报。人工录入
     */
    private String monitorSource;


    /**
     * 监控内容
     */
    private List<WarnItem> itemList;
}
