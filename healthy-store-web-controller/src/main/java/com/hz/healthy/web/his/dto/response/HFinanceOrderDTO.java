package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/12/1 22:55
 * @ClassName: HChCareDoorOrderDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HFinanceOrderDTO implements Serializable {

    private Long id;




    /**
     * 创建人员
     */
    private String createUser;

    /**
     * 添加时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /**
     * 商户订单号
     */
    private String orderNo;

    /**
     *  状态，取于PayStatusEnum
     */
    @Dict(dicCode = "pay_order_status")
    private String payStatus;



    /** 订单总金额 */
    private BigDecimal originalPrice;

    /** 实际付款金额(包含运费)  */
    private BigDecimal consumeAmount;


    private BigDecimal storeAmount;

    /**
     * 订单类型
     */
    private String orderType;

    /** 退款金额 */
    private BigDecimal refundMoney;


}

