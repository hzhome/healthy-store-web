package com.hz.healthy.web.system.controller;


import com.hz.healthy.base.WebBaseController;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.enums.BaseEnum;
import com.hz.ocean.common.enums.FactoryEnum;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.system.service.auth.ISysDictItemService;
import com.hz.ocean.system.service.auth.ISysDictService;
import com.hz.ocean.system.service.auth.ISysDictTenantService;
import com.hz.ocean.system.service.auth.model.DictCBQueryModel;
import com.hz.ocean.system.service.auth.model.DictQueryModel;
import com.hz.ocean.system.service.auth.model.SysDictTenantModel;
import com.hz.ocean.system.service.auth.model.SysDictTenantQueryModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author hyhuang
 * @date 2022-03-27 8:03 PM
 */
@RestController
@RequestMapping("/sys/dict")
@Slf4j
public class SysDictController extends WebBaseController {

    @Autowired
    private ISysDictService sysDictService;
    @Autowired
    private ISysDictItemService sysDictItemService;
    @Autowired
    public RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private ISysDictTenantService sysDictTenantService;


    /**
     * 获取全部字典数据
     *
     * @return
     */
    @RequestMapping(value = "/queryAllDictItems", method = RequestMethod.GET)
    public RespResult<?> queryAllDictItems(HttpServletRequest request) {
        Map<String, List<DictQueryModel>> res = new HashMap<String, List<DictQueryModel>>();
        res = sysDictService.queryAllDictItems();
        return RespResult.OK(res);
    }

    /**
     * 获取字典数据
     *
     * @param dictCode
     * @return
     */
    @RequestMapping(value = "/getDictText/{dictCode}/{key}", method = RequestMethod.GET)
    public RespResult<String> getDictText(@PathVariable("dictCode") String dictCode, @PathVariable("key") String key) {
        log.info(" dictCode : " + dictCode);
        RespResult<String> result = new RespResult<String>();
        String text = null;
        try {
            text = sysDictService.queryDictTextByKey(dictCode, key);
            result.setSuccess(true);
            result.setResult(text);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }


    /**
     * 获取字典数据 【接口签名验证】
     * 先读系统再读机构下的
     *
     * @param dictCode 字典code
     * @param dictCode 表名,文本字段,code字段  | 举例：sys_user,realname,id
     * @return
     */
    @RequestMapping(value = "/getDictItems/{dictCode}", method = RequestMethod.GET)
    public RespResult<List<DictQueryModel>> getDictItems(@PathVariable String dictCode) {
        RespResult<List<DictQueryModel>> result = new RespResult<List<DictQueryModel>>();

        List<DictQueryModel> dictList = sysDictService.getDictItems(dictCode);
        if (CollectionUtils.isNotEmpty(dictList)) {
            return RespResult.OK(dictList);
        }
        List<DictQueryModel> ls = new ArrayList<>();
        List<BaseEnum> enumList = FactoryEnum.getInstance().get(dictCode);
        if (CollectionUtils.isNotEmpty(enumList)) {
            enumList.forEach(e -> {
                DictQueryModel dq = new DictQueryModel();
                dq.setValue(e.getValue());
                dq.setText(e.getLabel());
                ls.add(dq);
            });
            return RespResult.OK(ls);
        }

        List<SysDictTenantModel> dataList = sysDictTenantService.selectList(SysDictTenantQueryModel.builder()
                .dictCode(dictCode).delFlag(CommonConstant.DEL_FLAG_0)
                .tenantId(getTenantId())
                .isAllowChild(0)
                .build());
        if (CollectionUtils.isNotEmpty(dataList)) {
            dataList.forEach(e -> {
                DictQueryModel dq = new DictQueryModel();
                dq.setValue(e.getItemText());
                dq.setText(e.getItemText());
                ls.add(dq);
            });
            return RespResult.OK(ls);
        }

        List<BaseEnum> baseEnumList = FactoryEnum.getInstance().get(dictCode);
        if(CollectionUtils.isNotEmpty(baseEnumList)){
            baseEnumList.forEach(e -> {
                DictQueryModel dq = new DictQueryModel();
                dq.setValue(e.getValue());
                dq.setText(e.getLabel());
                ls.add(dq);
            });
            return RespResult.OK(ls);
        }

        return result;
    }


    @RequestMapping(value = "/checkbox/{dictCode}", method = RequestMethod.GET)
    public RespResult<List<DictCBQueryModel>> checkbox(@PathVariable String dictCode, HttpServletRequest request) {
        log.info(" dictCode : " + dictCode);
        RespResult<List<DictCBQueryModel>> result = new RespResult<List<DictCBQueryModel>>();
        List<BaseEnum> enumList = FactoryEnum.getInstance().get(dictCode);
        List<DictCBQueryModel> ls = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(enumList)) {
            enumList.forEach(e -> {
                DictCBQueryModel dq = new DictCBQueryModel();
                dq.setValue(e.getValue());
                dq.setLabel(e.getLabel());
                ls.add(dq);
            });
        } else {
            List<DictQueryModel> dataList = sysDictService.getDictItems(dictCode);
            if (CollectionUtils.isNotEmpty(dataList)) {
                dataList.forEach(l -> {
                    DictCBQueryModel dq = new DictCBQueryModel();
                    dq.setValue(l.getValue());
                    dq.setLabel(l.getText());
                    ls.add(dq);
                });
            }
        }
        return RespResult.OK(ls);
    }
}
