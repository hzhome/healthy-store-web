package com.hz.healthy.web.his.dto.response;

import cn.hutool.core.util.DesensitizedUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import com.hz.ocean.common.aspect.Sensitive;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/3/7 18:29
 * @ClassName: HHisPatientDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientSimpleDTO implements Serializable {

    /** 主键 */
    private Long id;
    private Long patientId;
    /** 姓名 */
    private String realName;
    /** 手机 */
    @Sensitive(type = DesensitizedUtil.DesensitizedType.MOBILE_PHONE)
    private String mobilePhone;
    /** 身份证 */
    @Sensitive(type = DesensitizedUtil.DesensitizedType.ID_CARD)
    private String cardNo;
    /** 患者年龄 */
    private Integer age;
    /** 性别，2，男；1，女 */
    @Dict(dicCode = "sex")
    private String sex;

    private String desc;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
     * 床位
     */
    private String bedInfo;

    /** 体重 例如62kg */
    private Integer weight;
    /** 身高 例如178CM */
    private Integer height;



    private String sexText;


    private String address;

    private String archivesCode;

}
