package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/12/1 22:55
 * @ClassName: HChCareDoorOrderDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HChCareDoorOrderDTO implements Serializable {

    private Long id;


    /**
     * 下单会员id
     */
    private Long memberId;
    /**
     * 会员名称
     */
    private String memberName;


    /**
     * 创建人员
     */
    private String createUser;

    /**
     * 添加时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /**
     * 商户订单号
     */
    private String orderSn;

    /**
     *  状态，取于PayStatusEnum
     */
    @Dict(dicCode = "pay_order_status")
    private String payStatus;


    /**
     * 项目分类 取h_category
     */
    private Long categoryId;

    private String categoryName;

    /**
     * 项目ID.  取door_service_project
     */
    private Long projectId;

    /**
     * 项目名称
     */
    private String project;

    /**
     * 总费用
     */
    private BigDecimal amount;

    /**
     * 收费方式 0天 1包月 2包年
     */
    private Integer amountType;

    /**
     * 服务所需时间分数
     */
    private Integer serviceTime;

    /**
     * 项目图片
     */
    private String projectImg;

    /**
     * 上门日期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date appointmentDate;



    /**
     * 收货人的国家,用户页面填写,默认取值于表user_address,其id对应的值在region
     */
    private String country;

    /**
     * 收货人的省份,用户页面填写,默认取值于表user_address, 其id对应的值在region
     */
    private String province;

    /**
     * 收货人的城市,用户页面填写,默认取值于表user_address,其id对应的值在region
     */
    private String city;

    /**
     * 收货人的地区,用户页面填写,默认取值于表user_address,其id对应的值在region
     */
    private String district;

    /**
     * 收货人的详细地址,用户页面填写,默认取值于表user_address
     */
    private String address;

    /**
     * 收货人的详细地址ID
     */
    private Long addressId;

    /**
     * 患者id
     */
    private Long patientId;

    /**
     * 患者名称
     */
    private String patientName;

    private Integer patientAge;

    /**
     * 收货人的手机,用户页面填写,默认取值于表user_address
     */
    private String mobile;

    /**
     * 订单类型
     */
    private String orderType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 业务方的编码
     */
    private String busCode;


    /**
     * 确认接受状态，0未确认，1已确认
     */
    @Dict(dicCode = "order_service_status")
    private String confirmStatus;

    private String confirmUser;

    /**
     * 租户id
     */
    private Long tenantId;

    private String tenantName;
}

