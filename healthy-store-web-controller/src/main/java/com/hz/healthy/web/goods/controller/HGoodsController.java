package com.hz.healthy.web.goods.controller;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.goods.IHGoodsCategoryService;
import com.hz.healthy.domain.goods.IHGoodsService;
import com.hz.healthy.domain.goods.model.HGoodsModel;
import com.hz.healthy.domain.goods.model.HGoodsQueryModel;
import com.hz.healthy.domain.ord.model.OrdCpInfoModel;
import com.hz.healthy.web.goods.dto.request.HGoodsFormDTO;
import com.hz.healthy.web.goods.dto.response.HGoodsDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.constant.ToolConfig;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.model.KeyStrModel;
import com.hz.ocean.common.model.KeyValueModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 商品Controller
 *
 * @author ruoyi
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/shop/goods")
public class HGoodsController extends WebBaseController {
    @Autowired
    private IHGoodsService hGoodsService;

    @Autowired
    private IHGoodsCategoryService hGoodsCategoryService;


    public static void main(String[] args) {
        HGoodsFormDTO f=new HGoodsFormDTO();
        f.setGoodsImg("");
        f.setGoodsSn("");
        f.setBrandCode("");
        f.setGoodsBrief("");
        f.setCategoryCode("");
        f.setClickCount(1);
        f.setEffect("");
        f.setShopPrice(BigDecimal.ZERO);
        f.setPromotePrice(BigDecimal.ZERO);
        f.setProviderName("");
        f.setExtensionCode("");
        f.setWarnNumber(1);
        f.setGiveIntegral(1);
        f.setSortOrder(1);
        f.setSellerNote("");
        f.setPromoteStartDate(new Date());
        f.setPromoteEndDate(new Date());
        f.setGoodsThumb("");
        f.setGoodsDesc("");
        f.setIsOnSale(1);
        f.setKeywords("");
        f.setGoodsName("");
        f.setGoodsWeight(BigDecimal.ZERO);
        System.out.println(JSON.toJSONString(f));


        KeyValueModel s=new KeyValueModel();
        s.setId(1L);
        s.setKey("");
        s.setValue("");

        System.out.println(JSON.toJSONString(s));
    }

    /**
     * 查询商品列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HGoodsDTO>> pageQuery(HGoodsQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantIdList(Lists.newArrayList(getTenantId(),0L));
        ListResponse<HGoodsDTO> pageList = ListResponse.build(hGoodsService.pageQuery(queryModel), HGoodsDTO.class);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> categoryCodeList = pageList.getRecords().stream().map(HGoodsDTO::getCategoryCode).collect(Collectors.toList());
            Map<String, String> mapCategory = hGoodsCategoryService.mapNameByIds(categoryCodeList);
            pageList.getRecords().forEach(p -> {
                p.setCategoryName(mapCategory.getOrDefault(p.getCategoryCode(), ""));
                p.setIzEdit(p.getTenantId().equals(getTenantId()));
                p.setGoodsImg(getImageUrl(p.getGoodsImg()));
            });

        }
        return RespResult.OK(pageList);
    }


    /**
     * 获取商品详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hGoodsService.getById(id));
    }

    /**
     * 新增商品
     */
    //@PreAuthorize("@ss.hasPermi('system:goods:add')")
    //@Log(title = "商品", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@Validated @RequestBody HGoodsFormDTO form) {
        HGoodsModel hGoodsModel = BeanUtil.toBean(form, HGoodsModel.class);
        hGoodsModel.setGoodsSn("");
        hGoodsModel.setOrgCode(getOrgCode());
        hGoodsModel.setTenantId(getTenantId());
        hGoodsModel.setCreateUser(getLoginUser().getRealname());
        hGoodsModel.setCreateTime(new Date());
        hGoodsService.insert(hGoodsModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改商品
     */
    @PutMapping
    public RespResult edit(@RequestBody HGoodsModel hGoodsModel) {
        hGoodsService.updateById(hGoodsModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除商品
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hGoodsService.deleteByIds(ids);
        return RespResult.OK();
    }


    /**
     * 批量上架
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/upBatch")
    public RespResult<?> upBatch(@RequestParam(name = "ids", required = true) String ids) {
        List<String> idStrList = StringUtils.convertList(ids, ",");
        if (idStrList == null || idStrList.isEmpty()) {
            return RespResult.error(ToolConfig.VALID_REQ_PARAM_FAIL);
        }

        List<Long> idList = new ArrayList<>();
        for (String val : idStrList) {
            long id = StringUtils.longValue(val, 0L);
            if (id <= 0L) {
                continue;
            }

            idList.add(id);
        }
        if (idList == null || idList.isEmpty()) {
            return RespResult.error(ToolConfig.VALID_REQ_PARAM_FAIL);
        }

        try {
            List<HGoodsModel> dbs = hGoodsService.listById(idList);
            if (dbs == null || dbs.isEmpty()) {
                return RespResult.error(ToolConfig.RECORD_NOT_FOUNT);
            }

            for (HGoodsModel db : dbs) {
                if (CommonConstant.DEL_FLAG_1.equals(db.getIsDeleted())) {
                    continue;
                }
                if (CommonConstant.STATUS_1.equals(String.valueOf(db.getIsOnSale()))) {
                    continue;
                }

                if (!db.getTenantId().equals(getTenantId())) {
                    continue;
                }

                HGoodsModel po = new HGoodsModel();
                po.setId(db.getId());
                po.setUpdateUser(super.getLoginUser().getRealname());
                po.setUpdateTime(new Date());
                po.setIsOnSale(Integer.parseInt(CommonConstant.STATUS_1));

                hGoodsService.updateById(po);
            }

        } catch (BizException e) {
            return RespResult.error(e.getMessage());
        } catch (Exception e) {
            return RespResult.error("操作失败");
        }
        return RespResult.OK("批量上架成功!");
    }



    /**
     * 批量下架
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/downBatch")
    public RespResult<?> downBatch(@RequestParam(name = "ids", required = true) String ids) {
        List<String> idStrList = StringUtils.convertList(ids, ",");
        if (idStrList == null || idStrList.isEmpty()) {
            return RespResult.error(ToolConfig.VALID_REQ_PARAM_FAIL);
        }

        List<Long> idList = new ArrayList<>();
        for (String val : idStrList) {
            long id = StringUtils.longValue(val, 0L);
            if (id <= 0L) {
                continue;
            }

            idList.add(id);
        }
        if (idList == null || idList.isEmpty()) {
            return RespResult.error(ToolConfig.VALID_REQ_PARAM_FAIL);
        }

        try {
            List<HGoodsModel> dbs = hGoodsService.listById(idList);
            if (dbs == null || dbs.isEmpty()) {
                return RespResult.error(ToolConfig.RECORD_NOT_FOUNT);
            }

            for (HGoodsModel db : dbs) {
                if (CommonConstant.DEL_FLAG_1.equals(db.getIsDeleted())) {
                    continue;
                }
                if (!db.getTenantId().equals(getTenantId())) {
                    continue;
                }

                HGoodsModel po = new HGoodsModel();
                po.setId(db.getId());
                po.setUpdateUser(super.getLoginUser().getRealname());
                po.setUpdateTime(new Date());
                po.setIsOnSale(Integer.parseInt(CommonConstant.no_0));

                hGoodsService.updateById(po);

            }

        } catch (BizException e) {
            return RespResult.error(e.getMessage());
        } catch (Exception e) {
            return RespResult.error("操作失败");
        }
        return RespResult.OK("批量下架成功!");
    }
}
