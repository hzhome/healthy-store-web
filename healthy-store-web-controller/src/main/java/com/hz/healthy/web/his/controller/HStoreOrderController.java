package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.ch.IHChCareDoorOrderTrackService;
import com.hz.healthy.domain.ch.IHChCustomOrderService;
import com.hz.healthy.domain.ch.model.HChCareDoorOrderTrackModel;
import com.hz.healthy.domain.ch.model.HChCustomOrderModel;
import com.hz.healthy.domain.ch.model.HChCustomOrderQueryModel;
import com.hz.healthy.domain.ch.model.items.HomeCaregiverModel;
import com.hz.healthy.domain.member.IHMemberService;
import com.hz.healthy.domain.patient.IHHisPatientQuestionAnswerService;
import com.hz.healthy.domain.store.*;
import com.hz.healthy.domain.store.model.*;
import com.hz.healthy.enums.OrderServiceStatusEnums;
import com.hz.healthy.enums.OrderTypeEnums;
import com.hz.healthy.web.his.dto.request.HStoreOrderDeliverFormRepDTO;
import com.hz.healthy.web.his.dto.request.StoreOrderRefundReqFormDTO;
import com.hz.healthy.web.his.dto.response.*;
import com.hz.healthy.web.his.dto.response.item.HStoreOrderItemDTO;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.pay.aggregation.IPayCentreService;
import com.hz.ocean.pay.aggregation.model.request.RefundReq;
import com.hz.ocean.pay.aggregation.model.response.RefundResult;
import com.hz.ocean.pay.domain.service.IHPayOrderService;
import com.hz.ocean.pay.domain.service.IHPaySharingOrderCollectService;
import com.hz.ocean.pay.domain.service.model.HPayOrderModel;
import com.hz.ocean.pay.domain.service.model.HPaySharingOrderCollectModel;
import com.hz.ocean.pay.enums.PayStatusEnum;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 门店订单记录Controller
 *
 * @author hyhuang
 * @date 2024-02-03
 */
@Slf4j
@RestController
@RequestMapping("/store/order")
public class HStoreOrderController extends WebBaseController {
    @Autowired
    private IHStoreOrderService hStoreOrderService;

    @Autowired
    private IHStoreOrderAddrService hStoreOrderAddrService;
    @Autowired
    private IHPayOrderService hPayOrderService;
    @Autowired
    private IHPaySharingOrderCollectService hSharingOrderCollectService;
    @Autowired
    private IHStoreOrderItemService hStoreOrderItemService;

    @Autowired
    private IHStoreOrderDeliverService hStoreOrderDeliverService;

    @Autowired
    private IHMemberService hMemberService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    @Autowired
    private IHStoreOrderExtService hStoreOrderExtService;
    @Autowired
    private IHChCareDoorOrderTrackService hChCareDoorOrderTrackService;
    @Autowired
    private IHHisPatientQuestionAnswerService hisPatientQuestionAnswerService;

    @Autowired
    private IHStoreInfoService ihStoreInfoService;

    @Autowired
    private IHChCustomOrderService hCustomOrderService;

    @Autowired
    private IPayCentreService payCentreService;

    /**
     * 提供给资产管理的
     * 订单列表
     * <pre>
     *     包括所有订单
     * </pre>
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<ListResponse<HFinanceOrderDTO>> pageQuery(HStoreOrderQueryModel queryModel) {
        List<HFinanceOrderDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HStoreOrderModel> pageList = hStoreOrderService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> orderNoList = pageList.getRecords().stream().map(HStoreOrderModel::getOrderNo).collect(Collectors.toList());
            Map<String, HPaySharingOrderCollectModel> map = hSharingOrderCollectService.mapByOrderNo(orderNoList, getTenantId());
            pageList.getRecords().forEach(p -> {
                HFinanceOrderDTO financeOrderDto = BeanUtil.toBean(p, HFinanceOrderDTO.class);
                if (map.containsKey(p.getOrderNo())) {
                    HPaySharingOrderCollectModel collectModel = map.get(p.getOrderNo());
                    financeOrderDto.setStoreAmount(collectModel.getInAmount());
                } else {
                    financeOrderDto.setStoreAmount(BigDecimal.ZERO);
                }
                dataList.add(financeOrderDto);
            });

        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 查询服务项目订单列表
     * <pre>
     *     包括护理 照护 陪诊 康复等
     * </pre>
     */
    @AutoDict
    @GetMapping("/service/list")
    public RespResult<ListResponse<HChCareDoorOrderDTO>> pageQueryService(HStoreOrderQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        List<HChCareDoorOrderDTO> dataList = new ArrayList<>();
        ListResponse<HStoreOrderModel> pageList = hStoreOrderService.pageQuery(queryModel);
        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> orderList = pageList.getRecords().stream().map(HStoreOrderModel::getOrderNo).collect(Collectors.toList());
            Map<String, List<HStoreOrderItemModel>> itemMap = hStoreOrderItemService.mapListByOrder(orderList);
            Map<String, HStoreOrderExtModel> extMap = hStoreOrderExtService.mapBySn(orderList);
            pageList.getRecords().forEach(p -> {
                HStoreOrderExtModel extModel = extMap.get(p.getOrderNo());
                List<HStoreOrderItemModel> itemList = itemMap.get(p.getOrderNo());
                HStoreOrderItemModel itemModel = itemList.get(0);
                HChCareDoorOrderDTO orderDto = new HChCareDoorOrderDTO();
                orderDto.setId(p.getId());
                orderDto.setOrderSn(p.getOrderNo());
                orderDto.setAmount(p.getConsumeAmount());
                orderDto.setMemberName(p.getMemberName());
                orderDto.setMemberId(p.getMemberId());
                orderDto.setPayStatus(p.getPayStatus());
                orderDto.setCreateTime(p.getCreateTime());
                orderDto.setConfirmStatus(p.getConfirmStatus());
                orderDto.setConfirmUser(p.getConfirmUser());
                orderDto.setRemark(p.getBuyerRemark());
                orderDto.setCategoryName(com.hz.ocean.common.utils.StringUtils.defaultString(itemModel.getCategoryName(), "---"));
                orderDto.setProject(itemModel.getGoodsName());
                orderDto.setProjectImg(itemModel.getGoodsImg());
                if (ObjectUtil.isNotNull(extModel)) {
                    orderDto.setAppointmentDate(extModel.getAppointmentDate());
                    orderDto.setPatientName(extModel.getPatientName());
                    orderDto.setPatientAge(extModel.getPatientAge());
                }

                dataList.add(orderDto);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 确认服务订单
     */
    @Transactional
    @PostMapping(value = "/service/confirm")
    public RespResult confirm(@RequestBody HStoreOrderQueryModel hChCareDoorOrderModel) {
        HStoreOrderModel doorOrderModel = hStoreOrderService.getOne(HStoreOrderQueryModel.builder()
                .id(hChCareDoorOrderModel.getId())
                .orgCode(getOrgCode())
                .build());
        if (ObjectUtil.isNull(doorOrderModel)) {
            return RespResult.error("订单信息不存在");
        }
        hStoreOrderService.updateById(HStoreOrderModel.builder()
                .id(hChCareDoorOrderModel.getId())
                .confirmStatus(OrderServiceStatusEnums.WAIT_SERVICE.getCode())
                .confirmUser(getLoginUser().getRealname())
                .build());

        hChCareDoorOrderTrackService.insert(HChCareDoorOrderTrackModel.builder()
                .orderSn(doorOrderModel.getOrderNo())
                .isAuto(CommonConstant.yes_1)
                .memberId(doorOrderModel.getMemberId())
                .taskDate(new Date())
                .taskName("确认接受")
                .taskOpId(getLoginUser().getEyId())
                .taskOpName(getLoginUser().getRealname())
                .taskDesc(DateUtil.formatDateTime(new Date()) + "  " + getLoginUser().getRealname() + " 确认接受")
                .orgCode(doorOrderModel.getOrgCode())
                .tenantId(doorOrderModel.getTenantId())
                .createTime(new Date())
                .build());
        return RespResult.OK("确认订单完成");
    }



    /**
     * 个人历史订单列表
     * <pre>
     *     包括所有订单
     * </pre>
     */
    @AutoDict
    @GetMapping("/history/list")
    public RespResult<ListResponse<HStoreHistoryOrderDTO>> pageQueryHistoryList(HStoreOrderQueryModel queryModel) {
        List<HStoreHistoryOrderDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        //queryModel.setOrderStatus(OrderServiceStatusEnums.WAIT_COMMENT.getCode());
        if(ObjectUtil.isNull(queryModel.getMemberId())||queryModel.getMemberId()==0L){
            return RespResult.OK(ListResponse.empty());
        }
        ListResponse<HStoreOrderModel> pageList = hStoreOrderService.pageQuery(queryModel);
        // 订单类型 1一体机订单 2.商城订单 3上门护理 4上门照护 5陪诊服务 6慢性康复 7营养指导  */
        if(CollectionUtils.isNotEmpty(pageList.getRecords())){
            pageList.getRecords().forEach(p->{
                HStoreHistoryOrderDTO historyOrderDto=BeanUtil.toBean(p,HStoreHistoryOrderDTO.class);
                historyOrderDto.setOrderTypeText(OrderTypeEnums.getEnumToDesc(historyOrderDto.getOrderType().toString()));
                historyOrderDto.setOrderDay(DateUtil.formatDate(p.getOrderTime()));
                dataList.add(historyOrderDto);
            });
        }
        return RespResult.OK(ListResponse.build(dataList,pageList));
    }


    /**
     * 获取订单记录详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        HStoreOrderModel ordStoreOrderModel = hStoreOrderService.getById(id);
        HStoreOrderDTO orderDto = BeanUtil.toBean(ordStoreOrderModel, HStoreOrderDTO.class);
        if (ObjectUtil.isNotNull(orderDto)) {
            Map<Long, String> memberMap = hMemberService.mapNameByIds(Lists.newArrayList(orderDto.getMemberId()));
            orderDto.setMemberName(memberMap.getOrDefault(orderDto.getMemberId(), ""));
        }
        return RespResult.OK(orderDto);
    }

    @GetMapping(value = "/goods/{orderNo}")
    public RespResult<List<HStoreOrderItemDTO>> listGoods(@PathVariable("orderNo") String orderNo) {
        List<HStoreOrderItemModel> itemList = hStoreOrderItemService.selectList(HStoreOrderItemQueryModel.builder()
                .orderNo(orderNo)
                .build());
        if(CollectionUtils.isNotEmpty(itemList)){
            itemList.forEach(p->{
                p.setGoodsImg(getImageUrl(p.getGoodsImg()));
            });
        }
        return RespResult.OK(BeanUtil.copyToList(itemList, HStoreOrderItemDTO.class));
    }

    @GetMapping(value = "/address/{orderNo}")
    public RespResult<HStoreOrderAddrModel> getAddress(@PathVariable("orderNo") String orderNo) {
        HStoreOrderAddrModel itemList = hStoreOrderAddrService.getOne(HStoreOrderAddrQueryModel.builder()
                .orderNo(orderNo)
                .build());
        return RespResult.OK(itemList);
    }

    /**
     * 订单发货
     *
     * @param deliverFormRepDto
     * @return
     */
    @PostMapping(value = "/sure/delivery")
    public RespResult<String> orderDeliverySend(@RequestBody HStoreOrderDeliverFormRepDTO deliverFormRepDto) {
         if ("20".equals(deliverFormRepDto.getDeliverSource())&& (StringUtils.isAnyBlank(deliverFormRepDto.getLinkman(), deliverFormRepDto.getPhone()))) {
            return RespResult.error("请填写正确的送货人与电话信息");
        } else if ("30" .equals(deliverFormRepDto.getDeliverSource()) && (StringUtils.isAnyBlank(deliverFormRepDto.getLinkman(), deliverFormRepDto.getPhone()))) {
            return RespResult.error("请填写正确的配送公司与配送单号");
        }
        HStoreOrderModel storeOrderModel = hStoreOrderService.getOne(HStoreOrderQueryModel.builder().orderNo(deliverFormRepDto.getOrderNo()).build());
        if (ObjectUtil.isNull(storeOrderModel)) {
            return RespResult.error("订单信息不存在");
        }
        Date time = new Date();
        //配送状态，待接单＝1,待取货＝2,配送中＝3,已完成＝4
        Integer deliverStatus = 4;
        //订单状态 0待支付 1等商家接单 2商家已接单 3进行中 4已完成 5已退单
        String orderStatus = "4";
        if (!"0".equals(deliverFormRepDto.getDeliverSource())) {
            deliverStatus = 3;
            orderStatus = OrderServiceStatusEnums.SERVICE_ING.getCode();
            HStoreOrderDeliverModel hStoreOrderDeliverModel = BeanUtil.toBean(deliverFormRepDto, HStoreOrderDeliverModel.class);
            hStoreOrderDeliverModel.setTenantId(getTenantId());
            hStoreOrderDeliverModel.setOrgCode(getOrgCode());
            hStoreOrderDeliverModel.setCreateTime(new Date());
            hStoreOrderDeliverModel.setCreateUser(getLoginUser().getRealname());
            hStoreOrderDeliverModel.setUpdateTime(hStoreOrderDeliverModel.getCreateTime());
            hStoreOrderDeliverModel.setUpdateUser(getLoginUser().getRealname());
            if ("20".equals(deliverFormRepDto.getDeliverSource())) {
                hStoreOrderDeliverModel.setDeliverNo(seqNoAutoService.buildBatchNo());
            }
            hStoreOrderDeliverService.insert(hStoreOrderDeliverModel);
        }
        //配送状态(1未发货 2已发货)
        hStoreOrderService.updateById(HStoreOrderModel.builder()
                .id(storeOrderModel.getId())
                .deliveryStatus(2)
                .deliveryType(Integer.parseInt(deliverFormRepDto.getDeliverSource()))
                .deliverStatus(deliverStatus)
                .deliveryTime(time)
                .orderStatus(orderStatus)
                .build());
        return RespResult.OK("发货成功");
    }

    /**
     * 商家确认接单/审核
     *
     * @param queryModel
     * @return
     */
    @PostMapping(value = "/sure")
    public RespResult<String> sureOrder(@RequestBody HStoreOrderQueryModel queryModel) {

        HStoreOrderModel storeOrderModel = hStoreOrderService.getById(queryModel.getId());
        if (ObjectUtil.isNull(storeOrderModel)) {
            return RespResult.error("订单信息不存在");
        }
        Date time = new Date();
        //订单状态 0待支付 1等商家接单 2商家已接单 3进行中 4已完成 5已退单
        hStoreOrderService.updateById(HStoreOrderModel.builder()
                .id(storeOrderModel.getId())
                .orderStatus(OrderServiceStatusEnums.WAIT_SERVICE.getCode())
                .updateTime(time)
                .updateUser(getLoginUser().getRealname())
                .build());
        return RespResult.OK("订单已确认");
    }


    /**
     * 商家确认接单
     *
     * @param queryModel
     * @return
     */
    @PostMapping(value = "/finish")
    public RespResult<String> finishOrder(@RequestBody HStoreOrderQueryModel queryModel) {

        HStoreOrderModel storeOrderModel = hStoreOrderService.getById(queryModel.getId());
        if (ObjectUtil.isNull(storeOrderModel)) {
            return RespResult.error("订单信息不存在");
        }
        Date time = new Date();
        HStoreOrderDeliverModel deliverModel = hStoreOrderDeliverService.getOne(HStoreOrderDeliverQueryModel.builder()
                .orderNo(storeOrderModel.getOrderNo())
                .build());
        if (ObjectUtil.isNotNull(deliverModel)) {
            hStoreOrderDeliverService.updateById(HStoreOrderDeliverModel.builder()
                    .id(deliverModel.getId())
                    .deliverStatus(4)
                    .status(30)
                    .updateTime(time)
                    .updateUser(getLoginUser().getRealname())
                    .build());
        }

        //订单状态 0待支付 1等商家接单 2商家已接单 3进行中 4已完成 5已退单
        hStoreOrderService.updateById(HStoreOrderModel.builder()
                .id(storeOrderModel.getId())
                .orderStatus("4")
                .updateTime(time)
                .updateUser(getLoginUser().getRealname())
                .build());
        return RespResult.OK("订单已确认完成");
    }


    /**
     * 查询定制订单
     */
    @GetMapping("/list/customized")
    public RespResult<ListResponse<HChCustomOrderDTO>> pageQueryCustomized(HChCustomOrderQueryModel queryModel) {
        ListResponse<HChCustomOrderModel> pageList = hCustomOrderService.pageQuery(queryModel);
        List<HChCustomOrderDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            pageList.getRecords().forEach(p -> {
                HChCustomOrderDTO customOrderDto = BeanUtil.toBean(p, HChCustomOrderDTO.class);
                HomeCaregiverModel baseInfoDto = JSON.parseObject(p.getContent(), HomeCaregiverModel.class);
                customOrderDto.setBeanInfoDto(baseInfoDto);
                dataList.add(customOrderDto);

            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }


    /**
     * 查询定制订单
     */
    @GetMapping("/list/customized/get/{id}")
    public RespResult<HChCustomOrderDTO> getCustomized(@PathVariable(value = "id") Integer id) {
        HChCustomOrderModel customOrderModel = hCustomOrderService.getById(id);
        HChCustomOrderDTO customOrderDto = BeanUtil.toBean(customOrderModel, HChCustomOrderDTO.class);
        HomeCaregiverModel baseInfoDto = JSON.parseObject(customOrderModel.getContent(), HomeCaregiverModel.class);
        customOrderDto.setBeanInfoDto(baseInfoDto);
        return RespResult.OK(customOrderDto);
    }

    /**
     * 定制订单 确认
     */
    @PostMapping("/customized/confirm")
    public RespResult<HChCustomOrderDTO> confirmCustomized(@RequestBody HChCustomOrderDTO formReq) {
        HChCustomOrderModel customOrderModel = hCustomOrderService.getById(formReq.getId());
        if (ObjectUtil.isNull(customOrderModel)) {
            return RespResult.error("定制订单不存在");
        }
        //单服务状态，1 待接订单 2 待服务 3 服务中 4 待评价
        hCustomOrderService.updateById(HChCustomOrderModel.builder()
                .id(formReq.getId())
                .confirmStatus("2")
                .confirmUser(getLoginUser().getRealname())
                .updateTime(new Date())
                .updateUser(getLoginUser().getRealname())
                .amount(formReq.getAmount())
                .build());
        return RespResult.OK("确认成功");
    }

    /**
     * 查询商城订单记录列表
     */
    @AutoDict
    @GetMapping("/shop/list")
    public RespResult<ListResponse<HStoreOrderDTO>> pageQueryShop(HStoreOrderDeliverFormRepDTO query) {
        HStoreOrderQueryModel queryModel=BeanUtil.toBean(query,HStoreOrderQueryModel.class);
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        queryModel.setOrderTypeList(Lists.newArrayList(OrderTypeEnums.D1.getCode(),OrderTypeEnums.D2.getCode()));
        if(StringUtils.isNotBlank(query.getKeyword())){
            queryModel.setMemberName(query.getKeyword());
        }
        return commonOrder(queryModel);
    }

    public RespResult<ListResponse<HStoreOrderDTO>> commonOrder(HStoreOrderQueryModel queryModel) {
        queryModel.setIsDeleted(CommonConstant.DEL_FLAG_0);
        if(StringUtils.isNotBlank(queryModel.getOrderStatus())){
            List<String> orderStatusList=StrUtil.split(queryModel.getOrderStatus(),",");
            queryModel.setOrderStatusList(orderStatusList);
            queryModel.setOrderStatus(StringUtils.EMPTY);
        }

        ListResponse<HStoreOrderModel> pageList = hStoreOrderService.pageQuery(queryModel);
        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> orderNoList = new ArrayList<>();
            List<Long> memberIdList = new ArrayList<>();
            List<Long> tenantIdList = new ArrayList<>();
            pageList.getRecords().forEach(p -> {
                orderNoList.add(p.getOrderNo());
                tenantIdList.add(p.getTenantId());
                if (p.getMemberId() > 0L) {
                    memberIdList.add(p.getMemberId());
                }
            });
            List<HStoreOrderDTO> dataList = new ArrayList<>();
            pageList.getRecords().forEach(p -> {
                HStoreOrderDTO orderDto = BeanUtil.toBean(p, HStoreOrderDTO.class);
                orderDto.setOrderTypeText(OrderTypeEnums.getEnumToDesc(p.getOrderType().toString()));
                dataList.add(orderDto);
            });
            return RespResult.OK(ListResponse.build(dataList, pageList));
        }
        return RespResult.OK(ListResponse.empty());
    }


    /**
     * 订单退款
     */
    @Transactional
    @AutoDict
    @PostMapping("/order/refund")
    public RespResult orderRefund(@RequestBody StoreOrderRefundReqFormDTO refundReqForm) {
        HStoreOrderModel storeOrderModel = hStoreOrderService.getById(refundReqForm.getId());
        if (ObjectUtil.isNull(storeOrderModel)) {
            return RespResult.error("订单不存在");
        }
        HPayOrderModel hPayOrderModel = hPayOrderService.getByOrderNo(storeOrderModel.getOrderNo());
        if (ObjectUtil.isNull(hPayOrderModel)) {
            return RespResult.error("订单交易记录不存在");
        }
        String refundNo = seqNoAutoService.buildBatchNo();
        Date refundTime = new Date();
        RefundResult refundResult = payCentreService.refund(RefundReq.builder()
                .orderNo(storeOrderModel.getOrderNo())
                .refundAmount(storeOrderModel.getConsumeAmount())
                .refundOrderNo(refundNo)
                .payOrderNo(storeOrderModel.getPayOrderNo())
                .tradeNo(storeOrderModel.getPayTrxNo())//自己系统订单号
                .refundOrderTime(DateUtil.format(refundTime, "yyyyMMddHHmmss"))
                .build());
        hStoreOrderService.updateById(HStoreOrderModel.builder()
                .id(storeOrderModel.getId())
                .payStatus(refundResult.getStatus())
                .refundOrderNo(refundNo)
                .refundMoney(storeOrderModel.getConsumeAmount())
                .refundTime(refundTime)
                .build());
        if (!PayStatusEnum.STATUS_REFUND_FAIL.getCode().equals(refundResult.getStatus())) {
            return RespResult.OK("退款申请中");
        } else {
            return RespResult.error(refundResult.getMsg());
        }
    }


    /**
     * 获得测评购买的订单
     *
     * @param queryModel
     * @return
     */
    @GetMapping("/order/evaluation")
    public RespResult<List<HStoreOrderItemModel>> evaluationOrder(HStoreOrderQueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getOrderNo())) {
            return RespResult.OK(new ArrayList<>());
        }
        queryModel.setIsDeleted(CommonConstant.DEL_FLAG_0);
        List<HStoreOrderItemModel> dataList = hStoreOrderItemService.selectList(HStoreOrderItemQueryModel.builder()
                .orderNo(queryModel.getOrderNo())
                .build());
        return RespResult.OK(dataList);
    }


    /**
     * 服务订单 执行完成
     */
    @Transactional
    @PostMapping(value = "/service/finish")
    public RespResult finish(@RequestBody HStoreOrderQueryModel hChCareDoorOrderModel) {
        HStoreOrderModel doorOrderModel = hStoreOrderService.getOne(HStoreOrderQueryModel.builder()
                .id(hChCareDoorOrderModel.getId())
                .orgCode(getOrgCode())
                .build());
        if (ObjectUtil.isNull(doorOrderModel)) {
            return RespResult.error("订单信息不存在");
        }
        hStoreOrderService.updateById(HStoreOrderModel.builder()
                .id(hChCareDoorOrderModel.getId())
                .confirmStatus(OrderServiceStatusEnums.WAIT_COMMENT.getCode())
                .confirmUser(getLoginUser().getRealname())
                .build());

        hChCareDoorOrderTrackService.insert(HChCareDoorOrderTrackModel.builder()
                .orderSn(doorOrderModel.getOrderNo())
                .isAuto(CommonConstant.yes_1)
                .memberId(doorOrderModel.getMemberId())
                .taskDate(new Date())
                .taskName("订单执行完成")
                .taskOpId(getLoginUser().getEyId())
                .taskOpName(getLoginUser().getRealname())
                .taskDesc(DateUtil.formatDateTime(new Date()) + "  " + getLoginUser().getRealname() + " 订单服务己完成")
                .orgCode(doorOrderModel.getOrgCode())
                .tenantId(doorOrderModel.getTenantId())
                .createTime(new Date())
                .build());
        return RespResult.OK("确认订单完成");
    }


}
