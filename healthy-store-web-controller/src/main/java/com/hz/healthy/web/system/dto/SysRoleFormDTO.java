package com.hz.healthy.web.system.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysRoleFormDTO implements Serializable {

    private String id;
    private String roleName;
    private String roleCode;
    private String systemId;
    private String systemIdName;
    /** 菜单组 */
    private String[] menuIds;

}
