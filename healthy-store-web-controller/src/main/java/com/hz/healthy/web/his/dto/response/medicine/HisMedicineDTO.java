package com.hz.healthy.web.his.dto.response.medicine;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hyhuang
 * @desc
 * @date 2023/3/28 22:16
 */
@Data
public class HisMedicineDTO implements Serializable {

    /** 主键 */
    private Long id;

    /** 药品名称 */
    private String medicineName;

    /** 药品编码 */
    private String medicineNo;

    /** 生产厂家 */
    private Long factoryId;

    private String factoryName;
    /** 药品类型 */
    @Dict(dicCode = "his_medicine_type")
    private String medicineType;

    /** 处方类型 */
    @Dict(dicCode = "his_medicine_prescribe")
    private String prescribe;

    /** 单位 */
    private String unit;

    /** 处方价格 */
    private BigDecimal price;

    private BigDecimal salePrice;

    /** 库存量 */
    private Integer stock;

    /** 预警值 */
    private Integer warn;

    /** 换算量 */
    private Integer conversion;

    private String conversionUnit;

    /** 状态 正常 停用 */
    @Dict(dicCode = "valid_status")
    private String status;




    private String standard;

    /** $comment */
    private String keyword;


    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    private Date createTime;


    private String saleStandard;
}
