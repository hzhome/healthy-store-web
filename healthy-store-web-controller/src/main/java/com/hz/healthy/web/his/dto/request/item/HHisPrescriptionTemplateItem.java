package com.hz.healthy.web.his.dto.request.item;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/11/20 22:13
 * @ClassName: HHisPrescriptionTemplateItem
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPrescriptionTemplateItem implements Serializable {

    private Long medicineId;
    private String medicineName;
    private String unit;
    private BigDecimal day;
    private BigDecimal dose;
    private String usages;
    private String frequency;
    private String remark;

    private String standard;
    private BigDecimal totalDose;

}
