package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/9/26 20:49
 * @ClassName: HDeviceInfoDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HDeviceInfoDTO implements Serializable {


    private Long id;


    /** 添加时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /** 设备号 */
    private String deviceId;

    /** 设备名称 */
    private String deviceName;

    @Dict(dicCode = "network_status")
    private String deviceStatus;

    /** 所属服务点ID */
    private String storeCode;
    private String storeName;

    /** 设备类型 */
    @Dict(dicCode = "device_type")
    private String deviceType;

    /** 备注 */
    private String remark;

    /** 设备状态 */
    @Dict(dicCode = "start_stop_status")
    private String auditStatus;

    /** 品牌名称 */
    private String brandName;

    /**
     * 允许绑定数量
     */
    private Integer allowBindQty;

    /**
     * 已绑定数量
     */
    private Integer bindQty;



    private String patientName;

    private Boolean isBind;

    private String netType;


}
