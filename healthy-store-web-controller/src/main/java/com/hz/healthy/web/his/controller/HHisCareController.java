package com.hz.healthy.web.his.controller;

import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.doctor.IHEmployeeExtService;
import com.hz.healthy.domain.doctor.model.HHisCareQueryModel;
import com.hz.healthy.domain.his.model.HHisAdviceLogQueryModel;
import com.hz.healthy.domain.his.model.HHisCareFormatModel;
import com.hz.healthy.enums.DoctorTypeEnums;
import com.hz.healthy.his.store.service.IHHisCareAggregationService;
import com.hz.healthy.his.store.service.dto.care.request.HHisCareFormReqDTO;
import com.hz.healthy.his.store.service.dto.care.request.HHisCareProjectActionDTO;
import com.hz.healthy.his.store.service.dto.care.request.items.MedicineDataSource;
import com.hz.healthy.his.store.service.dto.care.response.*;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.aspect.CtrlReq;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.dto.OrderItem;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 历史病历Controller
 *
 * @author hyhuang
 * @date 2023-02-14
 */
@RestController
@RequestMapping("/his/care")
public class HHisCareController extends WebBaseController {

    @Autowired
    private IHHisCareAggregationService hisCareAggregationService;

    @Autowired
    private IHEmployeeExtService hEmployeeExtService;

    /**
     * 查询执行项目病历列表
     */
    @AutoDict
    @GetMapping("/exec/list")
    public RespResult<ListResponse<HHisCareDTO>> pageQueryExecList(HHisCareQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        queryModel.setOrderBy(Lists.newArrayList(new OrderItem().withField("id").withOrder(OrderItem.ASC)));
        queryModel.setIzNeedExec(CommonConstant.yes_1);
        ListResponse<HHisCareDTO> pageList = hisCareAggregationService.pageQueryList(queryModel);
        return RespResult.OK(pageList);
    }


    /**
     * 查询发药病历列表
     */
    @AutoDict
    @GetMapping("/send/list")
    public RespResult<ListResponse<HHisCareDTO>> pageQuerySendList(HHisCareQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        queryModel.setOrderBy(Lists.newArrayList(new OrderItem().withField("id").withOrder(OrderItem.ASC)));
        ListResponse<HHisCareDTO> pageList = hisCareAggregationService.pageQueryList(queryModel);
        return RespResult.OK(pageList);
    }


    /**
     * 查询病历中的诊疗项目
     */
    @AutoDict
    @GetMapping("/exec/detail/list")
    public RespResult<List<HHisCareProjectDTO>> pageQueryExecProjectList(HHisCareQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        List<HHisCareProjectDTO> pageList = hisCareAggregationService.pageQueryExecProjectList(queryModel);
        return RespResult.OK(pageList);
    }

    /**
     * 划扣诊疗项目
     */
    @AutoDict
    @PostMapping("/exec/detail/action")
    public RespResult<String> actionExecProject(@RequestBody HHisCareProjectActionDTO action) {
        try {
            hisCareAggregationService.actionExecProject(action, getLoginUser());
            return RespResult.OK("执行完成");
        } catch (BizException e) {
            return RespResult.error(e.getMessage());
        }
    }


    /**
     * 查询划扣诊疗项目操作记录
     */
    @AutoDict
    @GetMapping("/exec/action/log")
    public RespResult<List<HHisAdviceLogDTO>> listActionExecLog(HHisAdviceLogQueryModel queryModel) {
        List<HHisAdviceLogDTO> dataList = hisCareAggregationService.listActionExecLog(queryModel, getLoginUser());
        return RespResult.OK(dataList);
    }

    /**
     * 查询病历中的西药项目
     */
    @AutoDict
    @GetMapping("/western/medicine/list")
    public RespResult<HisCareWesternMedicineDataDTO> pageQueryWesternMedicineList(HHisCareQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        HisCareWesternMedicineDataDTO pageList = hisCareAggregationService.pageQueryWesternMedicineList(queryModel);
        return RespResult.OK(pageList);
    }

    /**
     * 查询病历中的中药项目
     */
    @AutoDict
    @GetMapping("/china/medicine/list")
    public RespResult<List<MedicineDataSource>> pageQueryChinaMedicineList(HHisCareQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        return RespResult.OK(null);
    }

    /**
     * 发药
     */
    @AutoDict
    @PostMapping("/exec/send/action")
    public RespResult<String> execSendAction(@RequestBody HHisCareQueryModel action) {
        try {
            hisCareAggregationService.execSendAction(action, getLoginUser());
            return RespResult.OK("发药完成");
        } catch (BizException e) {
            return RespResult.error(e.getMessage());
        }
    }

//    /**
//     * 获取历史病历详细信息
//     */
//    @GetMapping(value = "/{id}")
//    public RespResult getInfo(@PathVariable("id") Long id) {
//        HHisCareHistoryFormReqDTO formReq = ihHisCareHistorAggregationService.getById(id, getLoginUser().getOrgCode());
//        return formReq == null ? RespResult.error("") : RespResult.OK(formReq);
//    }

    /**
     * 新增门诊病历
     */
    @PostMapping
    public RespResult<Long> add(@RequestBody HHisCareFormReqDTO formReq) {
        Long id = hisCareAggregationService.saveVisit(formReq, DoctorTypeEnums.C0.getCode(), getLoginUser());
        return RespResult.OK(CommonConstant.STATUS_0.equals(formReq) ? "已暂存当前就诊记录" : "当前就诊已完结", id);
    }


    /**
     * 门诊病历列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisCareDTO>> list(@ModelAttribute HHisCareQueryModel queryModel) {
        if (ObjectUtil.isNull(queryModel.getPatientId()) || queryModel.getPatientId() == 0L) {
            return RespResult.OK(ListResponse.empty());
        }
        return RespResult.OK(hisCareAggregationService.list(queryModel));
    }
    /**
     * 门诊病历列表
     */
    @GetMapping("/getCareItem")
    public RespResult<HHisCareDetailDTO> getCareItem(@ModelAttribute HHisCareQueryModel queryModel) {
        return RespResult.OK(hisCareAggregationService.getCareItem(queryModel));
    }




    /**
     * 门诊病历格式 设置
     */
    @CtrlReq(isPrintResponse = true)
    @PostMapping("/setup/care/format")
    public RespResult<String> setUpCareFormat(@RequestBody HHisCareFormatModel formatModel) {
        hEmployeeExtService.saveCareFormat(getDoctorId(), formatModel);
        return RespResult.OK("设置成功");
    }

    /**
     * 门诊病历格式 读取
     */
    @CtrlReq
    @GetMapping("/setup/care/format")
    public RespResult<HHisCareFormatModel> getSetUpCareFormat() {
        HHisCareFormatModel formatModel = hEmployeeExtService.getCareFormat(getDoctorId());
        return RespResult.OK(formatModel);
    }

}
