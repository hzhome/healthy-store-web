package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisInterviewPlanLogService;
import com.hz.healthy.domain.his.model.HHisInterviewPlanLogModel;
import com.hz.healthy.domain.his.model.HHisInterviewPlanLogQueryModel;
import com.hz.healthy.web.his.dto.response.HHisInterviewPlanLogDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 走访明细Controller
 *
 * @author ruoyi
 * @date 2023-11-05
 */
@RestController
@RequestMapping("/ch/interview/log")
public class HHisInterviewPlanLogController extends WebBaseController {
    @Autowired
    private IHHisInterviewPlanLogService hHisInterviewPlanLogService;


    /**
     * 查询走访明细列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisInterviewPlanLogDTO>> pageQuery(HHisInterviewPlanLogQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisInterviewPlanLogModel> pageList = hHisInterviewPlanLogService.pageQuery(queryModel);
        return RespResult.OK(ListResponse.build(pageList,HHisInterviewPlanLogDTO.class));
    }

}
