package com.hz.healthy.web.his.controller;

import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisDiagnosisRecordService;
import com.hz.healthy.domain.his.model.HHisDiagnosisRecordModel;
import com.hz.healthy.domain.his.model.HHisDiagnosisRecordQueryModel;
import com.hz.ocean.common.aspect.AutoDict;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通用分类下的标记Controller
 *
 * @author ruoyi
 * @date 2023-10-27
 */
@RestController
@RequestMapping("/his/online/diagnosis")
public class HHisDiagnosisRecordController extends WebBaseController {

    @Autowired
    private IHHisDiagnosisRecordService hHisDiagnosisRecordService;

    /**
     * 查询门店测评订单记录列表
     */
    @AutoDict
    @GetMapping("/list")
    public RespResult<ListResponse<HHisDiagnosisRecordModel>> pageQuery(HHisDiagnosisRecordQueryModel queryModel) {
        queryModel.setTenantId(getTenantId());
        ListResponse<HHisDiagnosisRecordModel> pageList = hHisDiagnosisRecordService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }



}
