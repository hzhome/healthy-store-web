package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2023/11/5 15:15
 * @ClassName: HHisInterviewPlanFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisInterviewPlanFormDTO implements Serializable {
    /**
     * ID
     */
    private Integer id;

    /**
     * 1走访 2去电 3事件提示
     */
    @NotBlank(message = "类型错误")
    private String planType;

    @NotBlank(message = "计划标题")
    private String planTitle;
    /**
     * 执行年
     */
    private String year;

    /**
     * 开始月/执行日期
     */
    private String startTime;

    /**
     * 结束月/执行日期
     */
    private String endTime;


    /**
     * 执行次数(次/月)
     */
    private Integer execCount;

    /**
     * 执行人类型，0社区  1护理员 3其他
     */
    @NotBlank(message = "请选择执行人类型")
    private String execType;

    /**
     * 执行人id
     */
    private Long eyId;

    /**
     * 执行人
     */
    @NotBlank(message = "请选择执行人")
    private String eyName;

    /**
     * 计划走访事务
     */
    private String followUpContent;

    private List<Long> patientIds;

}
