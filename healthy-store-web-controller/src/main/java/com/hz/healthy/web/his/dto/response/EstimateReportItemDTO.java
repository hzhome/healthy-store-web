package com.hz.healthy.web.his.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/4/13 13:54
 * @ClassName: EstimateReportItemDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EstimateReportItemDTO implements Serializable {



    /** 指标项 */
    private String indicatorsName;

    /** 指标值 */
    private String resultValue;

    /**
     * 区间方向
     * 下降 2/上升 1 /正常 0
     * 当值不为0是，都显示黄色底警告
     */
    private Integer intervalDevelop;
//
//    /**
//     *区间描述
//     */
//    private String intervalRemark;
//
//    /**
//     *区间简短描述
//     */
//    private String intervalBriefRemark;


    /**
     * 指标意义
     */
    private String desc;


    /** 正常值 */
    private String normalValue;
}
