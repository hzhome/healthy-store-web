package com.hz.healthy.web.system.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.web.system.dto.SysRoleDTO;
import com.hz.healthy.web.system.dto.SysRoleFormDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.TreeModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.service.auth.*;
import com.hz.ocean.system.service.auth.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @Author scott
 * @since 2018-12-19
 */
@RestController
@RequestMapping("/sys/role")
@Slf4j
public class SysRoleController extends WebBaseController {

    @Autowired
    private ISysRoleService sysRoleService;

    @Autowired
    private ISysPermissionDataRuleService sysPermissionDataRuleService;

    @Autowired
    private ISysRolePermissionService sysRolePermissionService;

    @Autowired
    private ISysPermissionService sysPermissionService;
    @Autowired
    private ISyzTldChildPermissionService syzTldChildPermissionService;

    /**
     * 分页列表查询
     *
     * @param role
     * @param pageNo
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RespResult<ListResponse<SysRoleDTO>> queryPageList(SysRoleModel role,
                                                              @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                              @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        role.setPageNo(pageNo);
        role.setPageSize(pageSize);
        role.setSystemId(getSystemId());
        role.setTenantId(getTenantId());
        ListResponse<SysRoleModel> pageList = sysRoleService.pageQuery(role);
        return RespResult.OK(ListResponse.build(pageList, SysRoleDTO.class));
    }

    /**
     * 添加
     *
     * @param role
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    //@RequiresRoles({"admin"})
    public RespResult<SysRoleModel> add(@RequestBody SysRoleFormDTO role) {
        RespResult<SysRoleModel> result = new RespResult<SysRoleModel>();
        Integer count = sysRoleService.count(SysRoleModel.builder().roleCode(role.getRoleCode())
                .systemId(getSystemId()).tenantId(getTenantId())
                .build());
        if (count > 0) {
            return RespResult.error("角色名称已存在");
        }
        try {
            SysRoleModel roleModel = BeanUtil.copyProperties(role, SysRoleModel.class);
            roleModel.setSystemId(getSystemId());
            roleModel.setTenantId(getTenantId());
            roleModel.setCreateTime(new Date());
            List<String> menuId = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(role.getMenuIds())) {
                menuId = Arrays.asList(role.getMenuIds());
            }
            sysRoleService.insertRole(roleModel, menuId);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    /**
     * 编辑
     *
     * @param role
     * @return
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public RespResult<SysRoleModel> edit(@RequestBody SysRoleFormDTO role) {
        RespResult<SysRoleModel> result = new RespResult<SysRoleModel>();
        SysRoleModel sysrole = sysRoleService.getById(role.getId());
        if (sysrole == null) {
            return RespResult.error("未找到对应实体");
        } else {
            List<String> menuId = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(role.getMenuIds())) {
                menuId = Arrays.asList(role.getMenuIds());
            }

            SysRoleModel upRoleModel = BeanUtil.copyProperties(role, SysRoleModel.class);
            sysRoleService.updateRole(upRoleModel, menuId);
            //TODO 返回false说明什么？
            result.success("修改成功!");
        }

        return result;
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public RespResult<?> delete(@RequestParam(name = "id", required = true) String id) {
        sysRoleService.deleteRole(id);
        return RespResult.OK("删除角色成功");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public RespResult<SysRoleModel> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        RespResult<SysRoleModel> result = new RespResult<SysRoleModel>();
        if (StringUtils.isEmpty(ids)) {
            return RespResult.error("未选中角色！");
        } else {
            sysRoleService.deleteBatchRole(ids.split(","));
            result.success("删除角色成功!");
        }
        return result;
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public RespResult<SysRoleModel> queryById(@RequestParam(name = "id", required = true) String id) {
        RespResult<SysRoleModel> result = new RespResult<SysRoleModel>();
        SysRoleModel sysrole = sysRoleService.getById(id);
        if (sysrole == null) {
            return RespResult.error("未找到对应实体");
        } else {
            result.setResult(sysrole);
            result.setSuccess(true);
        }
        return result;
    }

    @RequestMapping(value = "/queryall", method = RequestMethod.GET)
    public RespResult<List<SysRoleModel>> queryall() {
        RespResult<List<SysRoleModel>> result = new RespResult<>();
        SysRoleModel queryModel = new SysRoleModel();
        queryModel.setSystemId(getSystemId());
        queryModel.setTenantId(getTenantId());
        List<SysRoleModel> list = sysRoleService.list(queryModel);
        if (list == null || list.size() <= 0) {
            return RespResult.error("未找到角色信息");
        } else {
            result.setResult(list);
            result.setSuccess(true);
        }
        return result;
    }

    /**
     * 校验角色编码唯一
     */
    @RequestMapping(value = "/checkRoleCode", method = RequestMethod.GET)
    public RespResult<Boolean> checkUsername(String id, String roleCode) {
        RespResult<Boolean> result = new RespResult<>();
        result.setResult(true);//如果此参数为false则程序发生异常
        log.info("--验证角色编码是否唯一---id:" + id + "--roleCode:" + roleCode);
        try {
            SysRoleModel role = null;
            if (StringUtils.isNotEmpty(id)) {
                role = sysRoleService.getById(id);
            }
            SysRoleModel queryModel = new SysRoleModel();
            queryModel.setRoleCode(roleCode);
            queryModel.setSystemId(getSystemId());
            SysRoleModel newRole = sysRoleService.getOne(queryModel);
            if (newRole != null) {
                //如果根据传入的roleCode查询到信息了，那么就需要做校验了。
                if (role == null) {
                    //role为空=>新增模式=>只要roleCode存在则返回false
                    result.setSuccess(false);
                    result.setMessage("角色编码已存在");
                    return result;
                } else if (!id.equals(newRole.getId())) {
                    //否则=>编辑模式=>判断两者ID是否一致-
                    result.setSuccess(false);
                    result.setMessage("角色编码已存在");
                    return result;
                }
            }
        } catch (Exception e) {
            result.setSuccess(false);
            result.setResult(false);
            result.setMessage(e.getMessage());
            return result;
        }
        result.setSuccess(true);
        return result;
    }


    /**
     * 查询数据规则数据
     */
    @GetMapping(value = "/datarule/{permissionId}/{roleId}")
    public RespResult<?> loadDatarule(@PathVariable("permissionId") String permissionId, @PathVariable("roleId") String roleId) {
        List<SysPermissionDataRuleModel> list = sysPermissionDataRuleService.getPermRuleListByPermId(permissionId);
        if (list == null || list.size() == 0) {
            return RespResult.error("未找到权限配置信息");
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("datarule", list);
            SysRolePermissionModel queryModel = new SysRolePermissionModel();
            queryModel.setPermissionId(permissionId);
            queryModel.setRoleId(roleId);
            SysRolePermissionModel sysRolePermission = sysRolePermissionService.getOne(queryModel);
            if (sysRolePermission == null) {
                //return Result.error("未找到角色菜单配置信息");
            } else {
//				String drChecked = sysRolePermission.getDataRuleIds();
//				if(StringUtils.isNotEmpty(drChecked)) {
//					map.put("drChecked", drChecked.endsWith(",")?drChecked.substring(0, drChecked.length()-1):drChecked);
//				}
            }
            return RespResult.OK(map);
            //TODO 以后按钮权限的查询也走这个请求 无非在map中多加两个key
        }
    }


    /**
     * 门店菜单授权，查询菜单权限树
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/queryTenantTreeList", method = RequestMethod.GET)
    public RespResult<Map<String, Object>> queryTenantTreeList(HttpServletRequest request) {
        RespResult<Map<String, Object>> result = new RespResult<>();
        //全部权限ids
        List<String> ids = new ArrayList<>();
        try {
            List<SysPermissionModel> list = new ArrayList<>();
            List<SyzTldChildPermissionModel> syzTldChildList = syzTldChildPermissionService.selectList(SyzTldChildPermissionQueryModel.builder()
                    .orgCode(getOrgCode())
                    .userId(getTenantCode())
                    .build());
            if (CollectionUtil.isNotEmpty(syzTldChildList)) {
                List<String> pid = syzTldChildList.stream().map(SyzTldChildPermissionModel::getPermissionId).collect(Collectors.toList());
                list.addAll(sysPermissionService.list(pid));
                ids.addAll(pid);
            }
            List<TreeModel> treeList = new ArrayList<>();
            getTreeModelList(treeList, list, null);
            Map<String, Object> resMap = new HashMap<String, Object>();
            resMap.put("treeList", treeList); //全部树节点数据
            resMap.put("ids", ids);//全部树ids
            result.setResult(resMap);
            result.setSuccess(true);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * 保存数据规则至角色菜单关联表
     */
    @PostMapping(value = "/datarule")
    public RespResult<?> saveDatarule(@RequestBody JSONObject jsonObject) {
        try {
            String permissionId = jsonObject.getString("permissionId");
            String roleId = jsonObject.getString("roleId");
            String dataRuleIds = jsonObject.getString("dataRuleIds");
            log.info("保存数据规则>>" + "菜单ID:" + permissionId + "角色ID:" + roleId + "数据权限ID:" + dataRuleIds);
            SysRolePermissionModel queryModel = new SysRolePermissionModel();
            queryModel.setPermissionId(permissionId);
            queryModel.setRoleId(roleId);
            SysRolePermissionModel sysRolePermission = sysRolePermissionService.getOne(queryModel);
            if (sysRolePermission == null) {
                return RespResult.error("请先保存角色菜单权限!");
            } else {
                //sysRolePermission.setDataRuleIds(dataRuleIds);
                this.sysRolePermissionService.updateById(sysRolePermission);
            }
        } catch (Exception e) {
            log.error("SysRoleController.saveDatarule()发生异常：" + e.getMessage(), e);
            return RespResult.error("保存失败");
        }
        return RespResult.OK("保存成功!");
    }



    private void getTreeModelList(List<TreeModel> treeList, List<SysPermissionModel> metaList, TreeModel temp) {
        for (SysPermissionModel permission : metaList) {
            String tempPid = permission.getParentId();
            TreeModel tree = new TreeModel(permission.getId(), tempPid, permission.getName(), permission.getRuleFlag(), permission.getLeaf());
            if (temp == null && oConvertUtils.isEmpty(tempPid)) {
                treeList.add(tree);
                if (!tree.getIsLeaf()) {
                    getTreeModelList(treeList, metaList, tree);
                }
            } else if (temp != null && tempPid != null && tempPid.equals(temp.getKey())) {
                temp.getChildren().add(tree);
                if (!tree.getIsLeaf()) {
                    getTreeModelList(treeList, metaList, tree);
                }
            }

        }
    }

}
