package com.hz.healthy.web.system.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.enums.EmployeeFlagEnums;
import com.hz.healthy.web.system.dto.SysPositionDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.model.KeyStrModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.auth.ISysPositionService;
import com.hz.ocean.system.service.auth.model.PositionSimpleModel;
import com.hz.ocean.system.service.auth.model.SysPositionModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author hyhuang
 * @Description: 职务表
 * @date 2022-03-27 8:03 PM
 */
@Slf4j
@RestController
@RequestMapping("/sys/position")
public class SysPositionController extends WebBaseController {

    @Autowired
    private ISysPositionService sysPositionService;




    /**
     * 分页列表查询
     * 获得组织的部门岗位，门店不允许独立建立岗位信息
     * @param sysPosition
     * @return
     */
    @GetMapping(value = "/list")
    public RespResult<ListResponse<PositionSimpleModel>> queryPageList(SysPositionModel sysPosition) {
        sysPosition.setOrgCode(getOrgCode());
        List<PositionSimpleModel> pageList = sysPositionService.selectListByHealthy(sysPosition);
        return RespResult.OK(ListResponse.build(pageList).withPage(1,100).withTotalCount(pageList.size()));
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */

    @GetMapping(value = "/queryById")
    public RespResult<SysPositionModel> queryById(@RequestParam(name = "id", required = true) String id) {
        RespResult<SysPositionModel> result = new RespResult<SysPositionModel>();
        SysPositionModel sysPosition = sysPositionService.getById(id);
        if (sysPosition == null) {
            return RespResult.error("未找到对应实体");
        } else {
            result.setResult(sysPosition);
            result.setSuccess(true);
        }
        return result;
    }


    /**
     * 通过code查询
     *
     * @param code
     * @return
     */
    @GetMapping(value = "/queryByCode")
    public RespResult<PositionSimpleModel> queryByCode(@RequestParam(name = "code", required = true) String code) {
        SysPositionModel query = new SysPositionModel();
        query.setCode(code);
        PositionSimpleModel sysPosition = sysPositionService.getOne(query);
        if (sysPosition == null) {
            return RespResult.error("未找到对应实体");
        }
        return RespResult.OK(sysPosition);
    }


    @GetMapping("/selectList")
    public RespResult<List<KeyStrModel>> selectList(SysPositionModel queryModel) {
        List<KeyStrModel> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        List<PositionSimpleModel> pageList = sysPositionService.selectListByHealthy(queryModel);
        if (CollectionUtils.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                dataList.add(KeyStrModel.builder()
                        .key(p.getCode())
                        .label(p.getName())
                        .build());
            });
        }
        return RespResult.OK(dataList);
    }


}
