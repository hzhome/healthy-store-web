package com.hz.healthy.web.his.dto.request.item;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/20 22:13
 * @ClassName: HHisPrescriptionTemplateItem
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPrescriptionTemplateGroupItem implements Serializable {


    private String type;
    private String frequency;

    private List<HHisPrescriptionTemplateItem> items;

}
