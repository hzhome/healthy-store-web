package com.hz.healthy.web.his.dto.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/7/20 17:15
 * @ClassName: HHisPatientDoctorBindFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientDoctorBindFormDTO implements Serializable {

    /** 医生ID */
    @NotNull(message = "请选择签约医生")
    private Long doctorId;

    @NotNull(message = "客户信息错误")
    private Long memberId;
}
