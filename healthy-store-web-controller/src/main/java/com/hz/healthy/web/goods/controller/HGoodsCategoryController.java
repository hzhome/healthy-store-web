package com.hz.healthy.web.goods.controller;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.goods.IHGoodsCategoryService;
import com.hz.healthy.domain.goods.IHGoodsService;
import com.hz.healthy.domain.goods.model.HGoodsCategoryModel;
import com.hz.healthy.domain.goods.model.HGoodsCategoryQueryModel;
import com.hz.healthy.domain.goods.model.HGoodsQueryModel;
import com.hz.healthy.domain.his.model.HCategoryModel;
import com.hz.healthy.domain.his.model.HCategoryQueryModel;
import com.hz.healthy.web.goods.dto.request.HGoodsCategoryFormDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.constant.ToolConfig;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.model.KeyStrModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.model.SysTreeModel;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 商品分类Controller
 *
 * @author ruoyi
 * @date 2024-01-21
 */
@RestController
@RequestMapping("/shop/category")
public class HGoodsCategoryController extends WebBaseController {
    @Autowired
    private IHGoodsCategoryService hGoodsCategoryService;

    @Autowired
    private IHGoodsService hGoodsService;
    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    /**
     * 查询商品分类列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HGoodsCategoryModel>> pageQuery(HGoodsCategoryQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        ListResponse<HGoodsCategoryModel> pageList = hGoodsCategoryService.pageQuery(queryModel);
        return RespResult.OK(pageList);
    }


    /**
     * 获取商品分类详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hGoodsCategoryService.getById(id));
    }

    /**
     * 新增商品分类
     */
    //@PreAuthorize("@ss.hasPermi('system:category:add')")
    //@Log(title = "商品分类", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@Validated @RequestBody HGoodsCategoryFormDTO form) {
        if (StringUtils.isNoneBlank(form.getDescription())) {
            if (form.getDescription().length() > 50) {
                return RespResult.error("分类描述50字符以内");
            }
        }
        HGoodsCategoryModel hGoodsCategoryModel = BeanUtil.toBean(form, HGoodsCategoryModel.class);
        hGoodsCategoryModel.setStoreId(getTenantId());
        hGoodsCategoryModel.setOrgCode(super.getOrgCode());
        hGoodsCategoryModel.setCreateUser(super.getLoginUser().getUsername());
        hGoodsCategoryModel.setCreateTime(new Date());
        hGoodsCategoryModel.setCategoryCode(seqNoAutoService.buildOrdCategoryId());
        hGoodsCategoryService.insert(hGoodsCategoryModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改商品分类
     */
    @PutMapping
    public RespResult edit(@Validated @RequestBody HGoodsCategoryFormDTO form) {
        if (form.getId() == null) {
            return RespResult.error(ToolConfig.VALID_REQ_PARAM_FAIL);
        }
        if (StringUtils.isNoneBlank(form.getDescription())) {
            if (form.getDescription().length() > 50) {
                return RespResult.error("分类描述50字符以内");
            }
        }

        int count = hGoodsCategoryService.count(HGoodsCategoryQueryModel.builder()
                .id(form.getId())
                .build());
        if (count == 0) {
            return RespResult.error(ToolConfig.RECORD_NOT_FOUNT);
        }

        HGoodsCategoryModel upPo = new HGoodsCategoryModel();
        upPo.setSort(form.getSort());
        upPo.setCategoryName(form.getCategoryName());
        upPo.setDescription(form.getDescription());
        upPo.setId(form.getId());
        upPo.setUpdateUser(super.getLoginUser().getUsername());
        upPo.setUpdateTime(new Date());
        upPo.setStatus(form.getStatus());
        hGoodsCategoryService.updateById(upPo);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除商品分类
     */
    @DeleteMapping
    public RespResult remove(@RequestParam(value = "id") Long id) {
        HGoodsCategoryModel dbModel = hGoodsCategoryService.getById(id);
        if (dbModel == null) {
            return RespResult.error(ToolConfig.RECORD_NOT_FOUNT);
        }
        // 分类有没有绑定菜品 如果有，禁止删除
        int count = hGoodsService.count(HGoodsQueryModel.builder()
                .categoryCode(dbModel.getCategoryCode())
                .isDeleted(CommonConstant.DEL_FLAG_0)
                .build());
        if (count > 0 ){
            return RespResult.error("当前分类已绑定菜品,请先解绑！");
        }
        hGoodsCategoryService.deleteById(id);
        return RespResult.OK("操作成功");
    }


    @GetMapping("/selectList")
    public RespResult<List<KeyStrModel>> selectList(HGoodsCategoryQueryModel queryModel) {
        List<KeyStrModel> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        List<HGoodsCategoryModel> pageList = hGoodsCategoryService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(pageList)) {
            pageList.forEach(p -> {
                dataList.add(KeyStrModel.builder()
                        .key(String.valueOf(p.getCategoryCode()))
                        .label(p.getCategoryName())
                        .build());
            });
        }
        return RespResult.OK(dataList);
    }

    /**
     * 查询通用分类列表
     */
    @GetMapping("/tree/selectList")
    public RespResult<List<SysTreeModel>> selectTreeList(HGoodsCategoryQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        List<HGoodsCategoryModel> pageList = hGoodsCategoryService.selectList(queryModel);
        List<SysTreeModel> dataList = new ArrayList<>();
        SysTreeModel t1 = new SysTreeModel();
        t1.setKey("");
        t1.setValue("");
        t1.setTitle("全部");
        t1.setId("");
        dataList.add(t1);
        if (org.apache.commons.collections.CollectionUtils.isNotEmpty(pageList)) {
            for (HGoodsCategoryModel d : pageList) {
                SysTreeModel t = new SysTreeModel();
                t.setKey(d.getCategoryCode());
                t.setValue(d.getCategoryCode());
                t.setTitle(d.getCategoryName());
                t.setId(d.getCategoryCode());
                dataList.add(t);
            }
        }
        return RespResult.OK(dataList);
    }
}
