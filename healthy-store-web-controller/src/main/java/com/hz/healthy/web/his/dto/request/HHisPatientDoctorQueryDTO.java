package com.hz.healthy.web.his.dto.request;

import com.hz.ocean.common.dto.PageRequest;
import lombok.Data;

import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/5/15 22:12
 * @ClassName: HHisPatientDoctorQueryDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientDoctorQueryDTO  extends PageRequest {

    /** 主键 */
    private Long id;

    /** 医生ID */
    private Long doctorId;

    /** 会员ID */
    private Long memberId;




    private Date startTime;
    private Date endTime;
}
