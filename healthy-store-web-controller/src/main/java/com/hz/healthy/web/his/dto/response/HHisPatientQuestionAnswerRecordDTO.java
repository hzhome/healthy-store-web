package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.healthy.domain.patient.model.HPatientHealthReportSimpleItemDTO;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/10/28 23:01
 * @ClassName: HHisPatientQuestionAnswerDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientQuestionAnswerRecordDTO implements Serializable {

    private Long id;

    /** 测评唯一编码 */
    private String answerCode;

    /** 用户主键 */
    private Long patientId;
    private Integer patientAge;
    private String patientSex;
    private String patientName;
    private String orderNo;

    /** 问卷Id->t_exam_paper */
    private Integer examPaperId;

    /** 测评版本*/
    private String version;

    /** 医生主键 被动发起填写发起人主键 */
    private Long doctorId;

    /** 是否主动发起，0，否；1，是 */
    private Integer isAuto;


    /** 添加时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String createDate;


    /** 观察期结束时间 大于是创建日期+30天 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date effectiveTime;

    /**
     * 报告编码
     */
    private String reportCode;


    private String riskLevel;

    /**
     * 是否有回访 0未回访，1已回访
     */
    private String izFollow;

    private HPatientHealthReportSimpleItemDTO W;
    private HPatientHealthReportSimpleItemDTO K;
    private HPatientHealthReportSimpleItemDTO M;
    private HPatientHealthReportSimpleItemDTO H;
    private HPatientHealthReportSimpleItemDTO L;
    private HPatientHealthReportSimpleItemDTO D;
    private HPatientHealthReportSimpleItemDTO C;
    private HPatientHealthReportSimpleItemDTO DL;


    /**
     * 是否确认 0未确认，1已确认
     */
    private String izSure;

    /**
     * 确认人员id，来自于employee表的主键
     */
    private Long sureOpId;

    private String doctorName;



}
