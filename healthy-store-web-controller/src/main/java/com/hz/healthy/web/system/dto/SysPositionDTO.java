package com.hz.healthy.web.system.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2023/9/1 22:36
 * @ClassName: SysPositionDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class SysPositionDTO implements Serializable {

    private String id;
    /**
     * 职务编码
     */
    private String code;
    /**
     * 职务名称
     */
    private String name;
    /**
     * 职级
     */
    private String postRank;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private java.util.Date createTime;
    /**
     * 修改人
     */
    private String updateBy;
    /**
     * 修改时间
     */
    private java.util.Date updateTime;
    /**
     * 租户id配置，编辑用户的时候设置
     */
    private Long tenantId;

    /**
     * 系统身份标识ID
     **/
    private String systemId;


    private String isSystem;

    private String isSystemText;

    private String remark;
}
