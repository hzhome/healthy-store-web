package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.base.table.Columns;
import com.hz.healthy.dao.patient.entity.HealthyUnit;
import com.hz.healthy.domain.patient.IHHisPatientHealthyDataService;
import com.hz.healthy.domain.patient.IHHisPatientService;
import com.hz.healthy.domain.patient.model.HHisPatientHealthyDataModel;
import com.hz.healthy.domain.patient.model.HHisPatientHealthyDataQueryModel;
import com.hz.healthy.domain.patient.model.HealthyDataModel;
import com.hz.healthy.domain.patient.model.HealthyDataUnit;
import com.hz.healthy.enums.HealthyDataEnums;
import com.hz.healthy.enums.HealthyDataSubEnums;
import com.hz.healthy.utils.HealthyDataUtil;
import com.hz.healthy.web.his.dto.request.HHisMemberQuestionAnswerQueryDTO;
import com.hz.healthy.web.his.dto.response.HHisPatientHealthyDataDTO;
import com.hz.healthy.web.his.dto.response.HealthyDataExceptionDTO;
import com.hz.healthy.web.his.dto.response.LineMultid;
import com.hz.healthy.web.his.dto.response.item.WorkHealthyDataToDayItem;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.dto.MapResponse;
import com.hz.ocean.common.model.KeyVModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.model.SysTreeModel;
import com.hz.ocean.system.service.auth.ISysDictItemService;
import com.hz.ocean.system.service.auth.ISysDictService;
import com.hz.ocean.system.service.auth.model.DictQueryModel;
import com.hz.ocean.system.service.auth.model.SysDictItemModel;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 自备药登记Controller
 *
 * @author hyhuang
 * @date 2023-08-08
 */
@RestController
@RequestMapping("/his/patient/healthy/data")
public class HHisPatientHealthyDataController extends WebBaseController {
    @Autowired
    private IHHisPatientHealthyDataService hisPatientHealthyDataService;
    @Autowired
    private ISysDictService sysDictService;


    private Map<String, String> takeMap(String keyCode) {
        Map<String, String> objectMap = new HashMap<>();
        if (StringUtils.isBlank(keyCode)) {
            return objectMap;
        }
        Map<String, HealthyUnit> columnMap=HealthyDataSubEnums.getSub(keyCode);
        for(Map.Entry<String,HealthyUnit> column:columnMap.entrySet()){
            objectMap.put(HealthyDataEnums.getEnumToDesc(column.getKey()),column.getKey());
        }
        return objectMap;
    }

    /**
     * 动态列表
     *
     * @return
     */
    @GetMapping("/column")
    public RespResult<List<Columns>> columns(@RequestParam(value = "keyCode", required = false) String keyCode) {
        Map<String, String> columnMap = takeMap(keyCode);

        List<Columns> columnsList = new ArrayList<>();
        columnsList.add(Columns.builder()
                .title("录入时间")
                .dataIndex("createTime")
                .align("center")
                .width(100)
                .build());
        columnsList.add(Columns.builder()
                .title("长者姓名")
                .dataIndex("patientName")
                .align("center")
                .width(110)
                .build());
        columnsList.add(Columns.builder()
                .title("床位")
                .dataIndex("patientBed")
                .width(130)
                .align("center")
                .build());

        for (Map.Entry<String, String> entry : columnMap.entrySet()) {
            columnsList.add(Columns.builder()
                    .title(entry.getKey())
                    .dataIndex(entry.getValue())
                    .width(130)
                    .align("center")
                    .build());
        }

        columnsList.add(Columns.builder()
                .title("数据来源")
                .dataIndex("inputSource")
                .align("center")
                .width(100)
                .build());
        columnsList.add(Columns.builder()
                .title("状态")
                .dataIndex("resultStatus")
                .align("center")
                .width(100)
                .build());
        columnsList.add(Columns.builder()
                .title("设备型号")
                .dataIndex("deveiceId")
                .align("center")
                .width(100)
                .build());
        return RespResult.OK(columnsList);
    }


    /**
     * 查询列表
     */
    @GetMapping("/queryTreeList")
    public RespResult<List<SysTreeModel>> treeList() {
        List<DictQueryModel> baseIndex = sysDictService.getDictItems("healthy_base_index");
        List<DictQueryModel> baseData = sysDictService.getDictItems("healthy_base_data");
        List<SysTreeModel> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(baseIndex)) {
            SysTreeModel indexTree = new SysTreeModel("01", "基础指标", "基础指标");
            indexTree.setChildren(new ArrayList<>());
            for (DictQueryModel d : baseIndex) {
                SysTreeModel t = new SysTreeModel();
                t.setKey(d.getText());
                t.setValue(d.getValue());
                t.setTitle(d.getText());
                t.setId(d.getValue());
                indexTree.getChildren().add(t);
            }
            dataList.add(indexTree);
        }
        if (CollectionUtils.isNotEmpty(baseData)) {
            SysTreeModel indexTree = new SysTreeModel("02", "基础数据", "基础数据");
            indexTree.setChildren(new ArrayList<>());
            for (DictQueryModel d : baseData) {
                SysTreeModel t = new SysTreeModel();
                t.setKey(d.getText());
                t.setValue(d.getValue());
                t.setTitle(d.getText());
                t.setId(d.getValue());
                indexTree.getChildren().add(t);
            }
            dataList.add(indexTree);
        }
        return RespResult.OK(dataList);
    }


    /**
     *
     */
    @GetMapping("/list")
    public RespResult<MapResponse> pageQuery(HHisPatientHealthyDataQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        String keyCode = queryModel.getKeyCode();
        if (StringUtils.isBlank(keyCode)) {
            return RespResult.OK(MapResponse.empty());
        }
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ListResponse<HHisPatientHealthyDataModel> pageList = hisPatientHealthyDataService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            Map<Long, HealthyDataModel>  healthyDataMap = hisPatientHealthyDataService.convertMap(pageList.getRecords());
            pageList.getRecords().forEach(p -> {
                HealthyDataModel dataModel=healthyDataMap.get(p.getId());
                Map<String, Object> map =new HashMap<>();
                for(Map.Entry<String,HealthyDataUnit> entry:dataModel.getDataValue().entrySet()){
                    map.put(entry.getKey(),entry.getValue().getValue());
                }
                map.put("createTime", DateUtil.formatDateTime(p.getCreateTime()));
                map.put("patientName", p.getPatientName());
                map.put("patientBed", StringUtils.defaultIfBlank(p.getPatientBed(),"~"));
                map.put("inputSource", HealthyDataUtil.convertSource(p.getInputSource()));
                map.put("resultStatus", "1".equals(p.getResultStatus()) ? "达标" : "不达标");
                map.put("deviceId", p.getDeviceId());
                map.put("id", p.getId());
                list.add(map);
            });
        }
        return RespResult.OK(MapResponse.build(list, pageList.getPageNo(), pageList.getTotal()));
    }

    /**
     * 这个接口不会处理不同的类型动态组合
     * 统一按格式返回
     */
    @GetMapping("/dy/list")
    public RespResult<ListResponse<HHisPatientHealthyDataDTO>> pageQueryList(HHisPatientHealthyDataQueryModel queryModel) {
        List<HHisPatientHealthyDataDTO> dataList = new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        ListResponse<HHisPatientHealthyDataModel> pageList = hisPatientHealthyDataService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<HHisPatientHealthyDataModel> dbList = pageList.getRecords();
            Map<Long, HealthyDataModel>  healthyDataMap = hisPatientHealthyDataService.convertMap(dbList);
            for (HHisPatientHealthyDataModel p : pageList.getRecords()) {
                HHisPatientHealthyDataDTO d = BeanUtil.toBean(p, HHisPatientHealthyDataDTO.class);
                d.setItemList(new ArrayList<>());
                HealthyDataModel healthyData=healthyDataMap.get(p.getId());
                for (Map.Entry<String, HealthyDataUnit> entry : healthyData.getDataValue().entrySet()) {
                    HealthyDataUnit hdu=entry.getValue();
                    d.getItemList().add(WorkHealthyDataToDayItem.builder()
                            .dataValue(hdu.getValueUnit())
                            .indexName(HealthyDataEnums.getEnumToDesc(entry.getKey()))
                            .indexCode(entry.getKey())
                            .beforeDataValue(StringUtils.isBlank(hdu.getHistoryValue())?"": hdu.getHistoryValue()+hdu.getUnit())
                            .compareResult("")
                            .build());
                }
                dataList.add(d);
            }
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }

    /**
     *
     */
    @GetMapping("/list/member")
    public RespResult<MapResponse> pageQuery(HHisMemberQuestionAnswerQueryDTO queryModel) {
        Long patientId =queryModel.getPatientId();
        HHisPatientHealthyDataQueryModel dataQueryModel = HHisPatientHealthyDataQueryModel.builder()
                .patientId(patientId).orgCode(getOrgCode())
                .tenantId(getTenantId())
                .keyCode(queryModel.getKeyCode())
                .build();
        return pageQuery(dataQueryModel);
    }

    @GetMapping("/list/line")
    public RespResult<LineMultid> lineMultiQuery(HHisPatientHealthyDataQueryModel queryModel) {
        queryModel.setOrgCode(getOrgCode());
        String keyCode = queryModel.getKeyCode();
        if (StringUtils.isBlank(keyCode)) {
            return RespResult.OK(LineMultid.builder().fields(new ArrayList<>())
                    .dataSource(new ArrayList<>())
                    .build());
        }
        List<HHisPatientHealthyDataModel> pageList = hisPatientHealthyDataService.selectList(queryModel);
        if (CollectionUtils.isEmpty(pageList)) {
            return RespResult.OK(LineMultid.builder().fields(new ArrayList<>())
                    .dataSource(new ArrayList<>())
                    .build());
        }
        List<String> fields = new ArrayList<>();
        Map<String, String> columnMap = takeMap(keyCode);
        for (Map.Entry<String, String> entry : columnMap.entrySet()) {
            fields.add(entry.getKey());
        }

        columnMap.putAll(reverse(columnMap));
        List<LinkedHashMap<String, Object>> dataSource = new ArrayList<>();
        for (int i = 0, len = pageList.size(); i < len; i++) {
            HHisPatientHealthyDataModel p = pageList.get(i);
            LinkedHashMap<String, Object> data = new LinkedHashMap<>();
            data.put("type", DateUtil.formatDate(p.getCreateTime()));
            String content = p.getDyData();
            Map<String, Object> map = JSON.parseObject(content, Map.class);
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                data.put(columnMap.get(entry.getKey()), entry.getValue());
            }
            dataSource.add(data);
        }
        return RespResult.OK(LineMultid.builder().fields(fields).dataSource(dataSource).build());
    }

    @GetMapping("/list/line/member")
    public RespResult<LineMultid> lineMultiQuery(HHisMemberQuestionAnswerQueryDTO queryModel) {
        Long patientId = 0L;//hisPatientService.getIdByMemberId(queryModel.getMemberId());
        HHisPatientHealthyDataQueryModel dataQueryModel = HHisPatientHealthyDataQueryModel.builder().patientId(patientId).orgCode(getOrgCode())
                .tenantId(getTenantId())
                .keyCode(queryModel.getKeyCode())
                .build();
        return lineMultiQuery(dataQueryModel);
    }

    public static <K, V> Map<V, K> reverse(Map<K, V> map) {
        return map.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

    /**
     * 异常数据
     * @param queryModel
     * @return
     */
    @GetMapping("/exception/list")
    public RespResult<ListResponse<HealthyDataExceptionDTO>> pageQueryException(HHisPatientHealthyDataQueryModel queryModel) {
        List<HealthyDataExceptionDTO> dataList=new ArrayList<>();
        queryModel.setOrgCode(getOrgCode());
        queryModel.setTenantId(getTenantId());
        queryModel.setResultStatus(CommonConstant.STATUS_1);
        ListResponse<HHisPatientHealthyDataModel> pageList=hisPatientHealthyDataService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<HHisPatientHealthyDataModel> dbList = pageList.getRecords();
            Map<Long, HealthyDataModel>  healthyDataMap = hisPatientHealthyDataService.convertMap(dbList);
            for (HHisPatientHealthyDataModel p : pageList.getRecords()) {
                HealthyDataExceptionDTO d = BeanUtil.toBean(p, HealthyDataExceptionDTO.class);
                d.setItemList(new ArrayList<>());
                HealthyDataModel healthyData=healthyDataMap.get(p.getId());
                for (Map.Entry<String, HealthyDataUnit> entry : healthyData.getDataValue().entrySet()) {
                    HealthyDataUnit hdu=entry.getValue();
                    d.getItemList().add(WorkHealthyDataToDayItem.builder()
                            .dataValue(hdu.getValueUnit())
                            .indexName(HealthyDataEnums.getEnumToDesc(entry.getKey()))
                            .indexCode(entry.getKey())
                            .beforeDataValue(StringUtils.isBlank(hdu.getHistoryValue())?"": hdu.getHistoryValue()+hdu.getUnit())
                            .compareResult("")
                            .build());
                }
                dataList.add(d);
            }
        }
        return RespResult.OK(ListResponse.build(dataList,pageList));
    }
}
