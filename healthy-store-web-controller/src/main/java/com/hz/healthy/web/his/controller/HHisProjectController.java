package com.hz.healthy.web.his.controller;

import cn.hutool.core.bean.BeanUtil;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.domain.his.IHHisProjectService;
import com.hz.healthy.domain.his.model.HHisProjectModel;
import com.hz.healthy.domain.his.model.HHisProjectQueryModel;
import com.hz.healthy.web.his.dto.request.HHisProjectFormDTO;
import com.hz.healthy.web.his.dto.response.HHisProjectDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 医疗项目Controller
 * 家
 * @author ruoyi
 * @date 2024-11-26
 */
@RestController
@RequestMapping("/his/project")
public class HHisProjectController extends WebBaseController {
    @Autowired
    private IHHisProjectService hHisProjectService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;
    /**
     * 查询医疗项目列表
     */
    @GetMapping("/list")
    public RespResult<ListResponse<HHisProjectDTO>> pageQuery(HHisProjectQueryModel queryModel) {
        List<HHisProjectDTO> dataList=new ArrayList<>();
        queryModel.setTenantId(getTenantId());
        queryModel.setOrgCode(getOrgCode());
        queryModel.setCategoryCode("0");
        ListResponse<HHisProjectModel> pageList = hHisProjectService.pageQuery(queryModel);
        if(CollectionUtils.isNotEmpty(pageList.getRecords())){
            pageList.getRecords().forEach(p->{
                HHisProjectDTO project=BeanUtil.toBean(p,HHisProjectDTO.class);
                if("1".equals(project.getCategoryCode())){
                    project.setCategoryName("治疗理疗");
                }else  if("3".equals(project.getCategoryCode())){
                    project.setCategoryName("检查子项");
                }
                dataList.add(project);
            });
        }
        return RespResult.OK(ListResponse.build(dataList,pageList));
    }


    /**
     * 获取医疗项目详细信息
     */
    @GetMapping(value = "/{id}")
    public RespResult getInfo(@PathVariable("id") Long id) {
        return RespResult.OK(hHisProjectService.getById(id));
    }

    /**
     * 新增医疗项目
     */
    //@PreAuthorize("@ss.hasPermi('system:project:add')")
    //@Log(title = "医疗项目", businessType = BusinessType.INSERT)
    @PostMapping
    public RespResult add(@RequestBody HHisProjectFormDTO form) {
        HHisProjectModel hHisProjectModel= BeanUtil.toBean(form,HHisProjectModel.class);
        if(StringUtils.isBlank(hHisProjectModel.getProjectCode())){
            hHisProjectModel.setProjectCode(seqNoAutoService.buildBatchNo());
        }
        hHisProjectModel.setTenantId(getTenantId());
        hHisProjectModel.setOrgCode(getOrgCode());
        hHisProjectService.insert(hHisProjectModel);
        return RespResult.OK("添加成功");
    }

    /**
     * 修改医疗项目
     */
    @PutMapping
    public RespResult edit(@RequestBody HHisProjectFormDTO form) {
        HHisProjectModel hHisProjectModel= BeanUtil.toBean(form,HHisProjectModel.class);
        hHisProjectService.updateById(hHisProjectModel);
        return RespResult.OK("变更完成");
    }

    /**
     * 删除医疗项目
     */
    @DeleteMapping("/{ids}")
    public RespResult remove(@PathVariable List<Long> ids) {
        hHisProjectService.deleteByIds(ids);
        return RespResult.OK();
    }
}
