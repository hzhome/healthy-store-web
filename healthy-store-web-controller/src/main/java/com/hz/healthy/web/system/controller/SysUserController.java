package com.hz.healthy.web.system.controller;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.hz.healthy.base.WebBaseController;
import com.hz.healthy.gateway.system.SysUserGatewayImpl;
import com.hz.healthy.web.system.dto.SysDepartUsersDTO;
import com.hz.healthy.web.system.dto.SysUserDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.service.auth.ISysUserService;
import com.hz.ocean.system.service.auth.model.*;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import com.hz.ocean.web.core.utils.jwt.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @Author scott
 * @since 2018-12-20
 */
@Slf4j
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends WebBaseController {

    @Autowired
    private SysUserGatewayImpl sysUserGateway;

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private OceanBaseConfig oceanBaseConfig;



    private SysUserModel buildQuery(HttpServletRequest req) {
        SysUserModel queryModel = new SysUserModel();
        String username = req.getParameter("username");
        if (StringUtils.isNotBlank(username)) {
            queryModel.setUsername(username);
        }
        String realname = req.getParameter("realname");
        if (StringUtils.isNotBlank(realname)) {
            queryModel.setRealname(realname);
        }
        String phone = req.getParameter("phone");
        if (StringUtils.isNotBlank(realname)) {
            queryModel.setPhone(phone);
        }
        String sex = req.getParameter("sex");
        if (StringUtils.isNotBlank(sex)) {
            queryModel.setSex(Integer.parseInt(sex));
        }
        String status = req.getParameter("status");
        if (oConvertUtils.isNotEmpty(status)) {
            queryModel.setStatus(Integer.parseInt(status));
        }
        String systemId = req.getParameter("systemId");
        if (oConvertUtils.isNotEmpty(systemId)) {
            queryModel.setSystemId(systemId);
        } else {
            queryModel.setSystemId(getSystemId());
        }
        return queryModel;
    }

    /**
     * 获取用户列表数据
     *
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RespResult<ListResponse<SysUserDTO>> queryPageList(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                              @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                              HttpServletRequest req) {

        String code = req.getParameter("code");
        SysUserModel queryModel = buildQuery(req);
        queryModel.setTenantId(getTenantId());
        ListResponse<SysUserModel> pageList = sysUserGateway.queryPageList(queryModel, pageNo, pageSize, code);
        RespResult<ListResponse<SysUserDTO>> result = new RespResult<ListResponse<SysUserDTO>>();
        ListResponse<SysUserDTO> listResponse = ListResponse.build(new ArrayList<SysUserDTO>());
        if (pageList != null && CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<SysUserDTO> voList = new ArrayList<>();
            pageList.getRecords().forEach(p -> {
                SysUserDTO userVO = BeanUtil.copyProperties(p, SysUserDTO.class);
                userVO.setSystemIdName(oceanBaseConfig.getSystemIdMaps().getOrDefault(p.getSystemId(), "未知系统"));
                voList.add(userVO);
            });
            listResponse = ListResponse.build(voList)
                    .withTotalCount(pageList.getTotal())
                    .withPage(pageList.getPageSize(), pageList.getPageNo());
        }
        result.setResult(listResponse);
        result.setSuccess(true);
        return result;
    }

    //@RequiresRoles({"admin"})
    //@RequiresPermissions("user:add")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public RespResult<SysUserModel> add(@RequestBody JSONObject jsonObject) {
        RespResult<SysUserModel> result = new RespResult<SysUserModel>();
        try {
            sysUserGateway.addEl(jsonObject,getTenantId());
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    //@RequiresRoles({"admin"})
    //@RequiresPermissions("user:edit")
    @RequestMapping(value = "/edit", method = {RequestMethod.PUT, RequestMethod.POST})
    public RespResult<SysUserModel> edit(@RequestBody JSONObject jsonObject) {
        RespResult<SysUserModel> result = new RespResult<SysUserModel>();
        try {
            sysUserGateway.edit(jsonObject);
            result.success("修改成功!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败");
        }
        return result;
    }

    /**
     * 删除用户
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public RespResult<?> delete(@RequestParam(name = "id", required = true) String id) {
        sysUserGateway.delete(id);
        return RespResult.OK("删除用户成功");
    }

    /**
     * 批量删除用户
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public RespResult<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        sysUserGateway.deleteBatch(ids);
        return RespResult.OK("批量删除用户成功");
    }

    /**
     * 冻结&解冻用户
     *
     * @param jsonObject
     * @return
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/frozenBatch", method = RequestMethod.PUT)
    public RespResult<SysUserModel> frozenBatch(@RequestBody JSONObject jsonObject) {
        RespResult<SysUserModel> result = new RespResult<SysUserModel>();
        try {
            sysUserGateway.frozenBatch(jsonObject);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("操作失败" + e.getMessage());
        }
        result.success("操作成功!");
        return result;

    }

    @RequestMapping(value = "/queryById", method = RequestMethod.GET)
    public RespResult<SysUserModel> queryById(@RequestParam(name = "id", required = true) String id) {
        RespResult<SysUserModel> result = new RespResult<SysUserModel>();
        SysUserModel sysUser = sysUserGateway.queryById(id);
        if (sysUser == null) {
            return RespResult.error("未找到对应实体");
        } else {
            result.setResult(sysUser);
            result.setSuccess(true);
        }
        return result;
    }

    @RequestMapping(value = "/queryUserRole", method = RequestMethod.GET)
    public RespResult<List<String>> queryUserRole(@RequestParam(name = "userid", required = true) String userid) {
        RespResult<List<String>> result = new RespResult<>();
        List<String> list = sysUserGateway.queryUserRole(userid);
        if (CollectionUtils.isEmpty(list)) {
            return RespResult.error("未找到用户相关角色信息");
        }
        result.setSuccess(true);
        result.setResult(list);
        return result;
    }


    /**
     * 校验用户账号是否唯一<br>
     * 可以校验其他 需要检验什么就传什么。。。
     *
     * @param sysUserModel
     * @return
     */
    @RequestMapping(value = "/checkOnlyUser", method = RequestMethod.GET)
    public RespResult<Boolean> checkOnlyUser(SysUserModel sysUserModel) {
        RespResult<Boolean> result = new RespResult<>();
        //如果此参数为false则程序发生异常
        Boolean checkOnlyUser = sysUserGateway.checkOnlyUser(sysUserModel);
        if (!checkOnlyUser) {
            result.setSuccess(false);
            result.setMessage("用户账号已存在");
        }
        result.setResult(true);
        return result;
    }

    /**
     * 修改密码
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/changePassword", method = RequestMethod.PUT)
    public RespResult<?> changePassword(@RequestBody SysUserModel sysUser) {
        try {
            sysUserGateway.changePassword(sysUser);
        } catch (BizException e) {
            return RespResult.error("用户不存在！");
        }
        return RespResult.OK();
    }

    /**
     * 查询指定用户和部门关联的数据
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/userDepartList", method = RequestMethod.GET)
    public RespResult<List<DepartIdModel>> getUserDepartsList(@RequestParam(name = "userId", required = true) String userId) {
        RespResult<List<DepartIdModel>> result = new RespResult<>();
        try {
            List<DepartIdModel> depIdModelList = sysUserGateway.getUserDepartsList(userId);
            if (CollectionUtils.isNotEmpty(depIdModelList)) {
                result.setSuccess(true);
                result.setMessage("查找成功");
                result.setResult(depIdModelList);
            } else {
                result.setSuccess(false);
                result.setMessage("查找失败");
            }
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("查找过程中出现了异常: " + e.getMessage());
            return result;
        }

    }

    /**
     * 生成在添加用户情况下没有主键的问题,返回给前端,根据该id绑定部门数据
     *
     * @return
     */
    @RequestMapping(value = "/generateUserId", method = RequestMethod.GET)
    public RespResult<String> generateUserId() {
        RespResult<String> result = new RespResult<>();
        System.out.println("我执行了,生成用户ID==============================");
        String userId = UUID.randomUUID().toString().replace("-", "");
        result.setSuccess(true);
        result.setResult(userId);
        return result;
    }

    /**
     * 根据部门id查询用户信息
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/queryUserByDepId", method = RequestMethod.GET)
    public RespResult<List<SysUserModel>> queryUserByDepId(@RequestParam(name = "id", required = true) String id, @RequestParam(name = "realname", required = false) String realname) {
        RespResult<List<SysUserModel>> result = new RespResult<>();
        try {
            List<SysUserModel> userList = sysUserGateway.queryUserByDepId(id, realname);
            result.setSuccess(true);
            result.setResult(userList);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            return result;
        }
    }

    /**
     * 用户选择组件 专用  根据用户账号或部门分页查询
     *
     * @param departId
     * @param username
     * @return
     */
    @RequestMapping(value = "/queryUserComponentData", method = RequestMethod.GET)
    public RespResult<ListResponse<SysUserModel>> queryUserComponentData(
            @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(name = "departId", required = false) String departId,
            @RequestParam(name = "realname", required = false) String realname,
            @RequestParam(name = "username", required = false) String username) {
        ListResponse<SysUserModel> pageList = sysUserGateway.queryUserComponentData(pageSize, pageNo, departId, username, realname);
        return RespResult.OK(pageList);
    }


    /**
     * @param userIds
     * @return
     * @功能：根据id 批量查询
     */
    @RequestMapping(value = "/queryByIds", method = RequestMethod.GET)
    public RespResult<List<SysUserModel>> queryByIds(@RequestParam String userIds) {
        RespResult<List<SysUserModel>> result = new RespResult<>();
        String[] userId = userIds.split(",");
        List<String> idList = Arrays.asList(userId);
        List<SysUserModel> userRole = sysUserGateway.queryByIds(userIds);
        result.setSuccess(true);
        result.setResult(userRole);
        return result;
    }

    /**
     * 首页用户重置密码
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/updatePassword", method = RequestMethod.PUT)
    public RespResult<?> updatePassword(@RequestBody JSONObject json) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        try {
            sysUserGateway.updatePassword(json, sysUser.getUsername());
        } catch (BizException e) {
            return RespResult.error(e.getMessage());
        }
        return RespResult.OK("重置密码成功");
    }

    @RequestMapping(value = "/userRoleList", method = RequestMethod.GET)
    public RespResult<ListResponse<SysUserModel>> userRoleList(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                               @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
        RespResult<ListResponse<SysUserModel>> result = new RespResult<ListResponse<SysUserModel>>();
        String roleId = req.getParameter("roleId");
        String username = req.getParameter("username");
        String systemId = req.getParameter("systemId");
        ListResponse<SysUserModel> pageList = sysUserGateway.userRoleList(pageNo, pageSize, roleId, username, systemId);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }

    /**
     * 给指定角色添加用户
     *
     * @param
     * @return
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/addSysUserRole", method = RequestMethod.POST)
    public RespResult<String> addSysUserRole(@RequestBody SysUserRoleInfoModel sysUserRoleVO) {
        RespResult<String> result = new RespResult<String>();
        try {
            sysUserGateway.addSysUserRole(sysUserRoleVO);
            result.setMessage("添加成功!");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("出错了: " + e.getMessage());
            return result;
        }
    }

    /**
     * 删除指定角色的用户关系
     *
     * @param
     * @return
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/deleteUserRole", method = RequestMethod.DELETE)
    public RespResult<SysUserRoleModel> deleteUserRole(@RequestParam(name = "roleId") String roleId,
                                                       @RequestParam(name = "userId", required = true) String userId
    ) {
        RespResult<SysUserRoleModel> result = new RespResult<SysUserRoleModel>();
        try {
            sysUserGateway.deleteUserRole(roleId, userId);
            result.success("删除成功!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("删除失败！");
        }
        return result;
    }

    /**
     * 批量删除指定角色的用户关系
     *
     * @param
     * @return
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/deleteUserRoleBatch", method = RequestMethod.DELETE)
    public RespResult<SysUserRoleModel> deleteUserRoleBatch(
            @RequestParam(name = "roleId") String roleId,
            @RequestParam(name = "userIds", required = true) String userIds) {
        RespResult<SysUserRoleModel> result = new RespResult<SysUserRoleModel>();
        try {
            sysUserGateway.deleteUserRoleBatch(roleId, Arrays.asList(userIds.split(",")));
            result.success("删除成功!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("删除失败！");
        }
        return result;
    }

    /**
     * 部门用户列表
     */
    @RequestMapping(value = "/departUserList", method = RequestMethod.GET)
    public RespResult<ListResponse<SysUserModel>> departUserList(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                                 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
        RespResult<ListResponse<SysUserModel>> result = new RespResult<ListResponse<SysUserModel>>();

        String depId = req.getParameter("depId");
        String username = req.getParameter("username");
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        ListResponse<SysUserModel> pageList = sysUserGateway.departUserList(pageNo, pageSize, depId, username, user);
        result.setSuccess(true);
        result.setResult(pageList);
        return result;
    }


    /**
     * 根据 orgCode 查询用户，包括子部门下的用户
     * 若某个用户包含多个部门，则会显示多条记录，可自行处理成单条记录
     */
    @GetMapping("/queryByOrgCode")
    public RespResult<?> queryByDepartId(
            @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(name = "orgCode") String orgCode,
            SysUserModel userParams
    ) {
        userParams.setPageSize(pageSize);
        userParams.setPageNo(pageNo);
        ListResponse<SysUserSysDepartModel> pageList = sysUserService.queryUserByOrgCode(orgCode, getSystemId(), userParams);
        return RespResult.OK(pageList);
    }

    /**
     * 根据 orgCode 查询用户，包括子部门下的用户
     * 针对通讯录模块做的接口，将多个部门的用户合并成一条记录，并转成对前端友好的格式
     */
    @GetMapping("/queryByOrgCodeForAddressList")
    public RespResult<?> queryByOrgCodeForAddressList(
            @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
            @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
            @RequestParam(name = "orgCode", required = false) String orgCode,
            SysUserModel userParams
    ) {
        List<JSONObject> result = sysUserGateway.queryByOrgCodeForAddressList(pageNo, pageSize, orgCode, userParams);
        return RespResult.OK(result);
    }

    /**
     * 给指定部门添加对应的用户
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/editSysDepartWithUser", method = RequestMethod.POST)
    public RespResult<String> editSysDepartWithUser(@RequestBody SysDepartUsersDTO sysDepartUsersVO) {
        RespResult<String> result = new RespResult<String>();
        try {
            String sysDepId = sysDepartUsersVO.getDepId();
            for (String sysUserId : sysDepartUsersVO.getUserIdList()) {
                sysUserGateway.editSysDepartWithUser(sysUserId, sysDepId);
            }
            result.setMessage("添加成功!");
            result.setSuccess(true);
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.setSuccess(false);
            result.setMessage("出错了: " + e.getMessage());
            return result;
        }
    }

    /**
     * 删除指定机构的用户关系
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/deleteUserInDepart", method = RequestMethod.DELETE)
    public RespResult<SysUserDepartModel> deleteUserInDepart(@RequestParam(name = "depId") String depId,
                                                             @RequestParam(name = "userId", required = true) String userId
    ) {
        RespResult<SysUserDepartModel> result = new RespResult<SysUserDepartModel>();
        try {
            sysUserGateway.deleteUserInDepart(depId, userId);
            result.success("删除成功!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("删除失败！");
        }
        return result;
    }

    /**
     * 批量删除指定机构的用户关系
     */
    //@RequiresRoles({"admin"})
    @RequestMapping(value = "/deleteUserInDepartBatch", method = RequestMethod.DELETE)
    public RespResult<SysUserDepartModel> deleteUserInDepartBatch(
            @RequestParam(name = "depId") String depId,
            @RequestParam(name = "userIds", required = true) String userIds) {
        RespResult<SysUserDepartModel> result = new RespResult<SysUserDepartModel>();
        try {
            sysUserGateway.deleteUserInDepartBatch(depId, userIds);
            result.success("删除成功!");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("删除失败！");
        }
        return result;
    }

    /**
     * 查询当前用户的所有部门/当前部门编码
     *
     * @return
     */
    @RequestMapping(value = "/getCurrentUserDeparts", method = RequestMethod.GET)
    public RespResult<Map<String, Object>> getCurrentUserDeparts() {
        RespResult<Map<String, Object>> result = new RespResult<Map<String, Object>>();
        try {
            LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            Map<String, Object> map = sysUserGateway.getCurrentUserDeparts(sysUser);
            result.setSuccess(true);
            result.setResult(map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error("查询失败！");
        }
        return result;
    }


    /**
     * 用户注册接口
     *
     * @param jsonObject
     * @param user
     * @return
     */
    @PostMapping("/register")
    public RespResult<JSONObject> userRegister(@RequestBody JSONObject jsonObject, SysUserModel user) {
        RespResult<JSONObject> result = new RespResult<JSONObject>();
        try {
            sysUserGateway.userRegister(jsonObject, user);
            result.success("注册成功");
        } catch (Exception e) {
            return RespResult.error("注册失败");
        }
        return result;
    }


    /**
     * 用户手机号验证
     */
    @PostMapping("/phoneVerification")
    public RespResult<Map<String, String>> phoneVerification(@RequestBody JSONObject jsonObject) {
        RespResult<Map<String, String>> result = new RespResult<Map<String, String>>();
        try {
            Map<String, String> map = sysUserGateway.phoneVerification(jsonObject);
            result.setResult(map);
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        }

        return result;
    }

    /**
     * 用户更改密码
     */
    @GetMapping("/passwordChange")
    public RespResult<SysUserModel> passwordChange(@RequestParam(name = "username") String username,
                                                   @RequestParam(name = "password") String password,
                                                   @RequestParam(name = "smscode") String smscode,
                                                   @RequestParam(name = "phone") String phone) {
        RespResult<SysUserModel> result = new RespResult<SysUserModel>();
        try {
            sysUserGateway.passwordChange(username, password, smscode, phone);
        } catch (Exception e) {
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            return result;
        }
        result.setSuccess(true);
        result.setMessage("密码重置完成！");
        return result;
    }


    /**
     * 根据TOKEN获取用户的部分信息（返回的数据是可供表单设计器使用的数据）
     *
     * @return
     */
    @GetMapping("/getUserSectionInfoByToken")
    public RespResult<?> getUserSectionInfoByToken(HttpServletRequest request, @RequestParam(name = "token", required = false) String token) {
        try {
            String username = null;
            // 如果没有传递token，就从header中获取token并获取用户信息
            if (StringUtils.isEmpty(token)) {
                username = JwtUtil.getUserNameByToken(request);
            } else {
                username = JwtUtil.getUsername(token);
            }

            log.debug(" ------ 通过令牌获取部分用户信息，当前用户： " + username);

            // 根据用户名查询用户信息
            SysUserModel sysUser = sysUserService.getUserByName(username, getSystemId());
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("sysUserId", sysUser.getId());
            map.put("sysUserCode", sysUser.getUsername()); // 当前登录用户登录账号
            map.put("sysUserName", sysUser.getRealname()); // 当前登录用户真实名称
            map.put("sysOrgCode", sysUser.getOrgCode()); // 当前登录用户部门编号

            log.debug(" ------ 通过令牌获取部分用户信息，已获取的用户信息： " + map);

            return RespResult.OK(map);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return RespResult.error(500, "查询失败:" + e.getMessage());
        }
    }


    /**
     * 根据userid获取用户信息和部门员工信息
     *
     * @return Result
     */
    @GetMapping("/queryChildrenByUsername")
    public RespResult queryChildrenByUsername(@RequestParam("userId") String userId) {
        //获取用户信息
        Map<String, Object> map = new HashMap<String, Object>();
        SysUserModel sysUser = sysUserService.getById(userId);
        String username = sysUser.getUsername();
        Integer identity = sysUser.getUserIdentity();
        map.put("sysUser", sysUser);
        if (identity != null && identity == 2) {
            //获取部门用户信息
            String departIds = sysUser.getDepartIds();
            if (StringUtils.isNotBlank(departIds)) {
                List<String> departIdList = Arrays.asList(departIds.split(","));
                List<SysUserModel> childrenUser = sysUserService.queryByDepIds(departIdList, username, getSystemId());
                map.put("children", childrenUser);
            }
        }
        return RespResult.OK(map);
    }

    /**
     * 移动端查询部门用户信息
     *
     * @param departId
     * @return
     */
    @GetMapping("/appQueryByDepartId")
    public RespResult<List<SysUserModel>> appQueryByDepartId(@RequestParam(name = "departId", required = false) String departId) {
        RespResult<List<SysUserModel>> result = new RespResult<List<SysUserModel>>();
        List<String> list = new ArrayList<String>();
        list.add(departId);
        List<SysUserModel> childrenUser = sysUserService.queryByDepIds(list, null, getSystemId());
        result.setResult(childrenUser);
        return result;
    }


    /**
     * 根据用户名修改手机号
     *
     * @param json
     * @return
     */
    @RequestMapping(value = "/updateMobile", method = RequestMethod.PUT)
    public RespResult<?> changMobile(@RequestBody JSONObject json, HttpServletRequest request) {
        RespResult<SysUserModel> result = new RespResult<SysUserModel>();
        try {
            //获取登录用户名
            String username = JwtUtil.getUserNameByToken(request);
            sysUserGateway.changMobile(json, username);
        } catch (BizException e) {
            result.setMessage(e.getMessage());
            result.setSuccess(false);
            return result;
        }
        return RespResult.OK("手机号设置成功!");
    }


    /**
     * 根据对象里面的属性值作in查询 属性可能会变 用户组件用到
     *
     * @param sysUser
     * @return
     */
    @GetMapping("/getMultiUser")
    public List<SysUserModel> getMultiUser(SysUserModel sysUser) {
        SysUserModel sysUserModel = new SysUserModel();
        //update-begin---author:wangshuai ---date:20220104  for：[JTC-297]已冻结用户仍可设置为代理人------------
        sysUserModel.setStatus(Integer.parseInt(CommonConstant.STATUS_1));
        //update-end---author:wangshuai ---date:20220104  for：[JTC-297]已冻结用户仍可设置为代理人------------
        List<SysUserModel> ls = this.sysUserService.list(sysUserModel);
        for (SysUserModel user : ls) {
            user.setPassword(null);
            user.setSalt(null);
        }
        return ls;
    }


    /*****************************************以下功能是支持ElemenuUI***************************************************************/

    /**
     * 重置密码
     */
    @PutMapping("/resetPwd")
    public RespResult<?> resetPwd(@RequestBody SysUserDTO user) {
        SysUserModel sysUser=new SysUserModel();
        sysUser.setId(user.getId());
        sysUser.setPassword(user.getPassword());
        sysUserService.changePassword(sysUser);
        return RespResult.OK("重置成功");
    }


}
