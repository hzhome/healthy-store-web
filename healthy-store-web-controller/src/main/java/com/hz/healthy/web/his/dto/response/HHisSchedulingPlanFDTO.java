package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/11/6 18:56
 * @ClassName: HHisSchedulingPlanDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
public class HHisSchedulingPlanFDTO implements Serializable {

    /** 主键 */
    private Long id;


    /** 方案名称 */
    private String name;

    private String type;

    /** 方案说明 */
    private String remark;

    /** 添加时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

private String time;
    private String status;
}
