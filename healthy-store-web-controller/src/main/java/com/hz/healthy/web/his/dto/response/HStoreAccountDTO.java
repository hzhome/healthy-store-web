package com.hz.healthy.web.his.dto.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/3/12 12:56
 * @ClassName: HStoreAccountDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
public class HStoreAccountDTO implements Serializable {


    /**
     * 销售额（元）
     */
    private BigDecimal totalAmount;

    /**
     * 待结算金额（元）
     */
    private BigDecimal waitPayAmount;

    /**
     * 已结算金额（元）
     */
    private BigDecimal finishPayAmount;

    /**
     * 退款金额(元)
     */
    private BigDecimal refundAmount;

    /**
     * 预计收入（元）
     */
    private BigDecimal realityAmount;
}
