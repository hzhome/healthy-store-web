package com.hz.healthy.web.his.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/3/7 18:34
 * @ClassName: HHisPatientCommunityArchivesDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisPatientCommunityArchivesDTO implements Serializable {

    /** ID */
    private Integer id;

    /** 患者id */
    private Long patientId;

    /** 患者姓名 */
    private String patientName;

    /** 疾病等级 */
    private String diseaseLevel;

    /** 档案状态，0无效  1有效 */
    private String status;

    /** 档案编码 */
    private String archivesCode;

    /**  档案激活时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date activateBeginTime;

    /** 档案结束时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date activateEndTime;

    /** 随访次数 总次数 */
    private Integer followUpQty;


    /** 最近一次更新时间  */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 创建人员  */
    private String createUser;

    /**  建档时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;



    /**
     * 民族
     */
    private String nation;
    /**
     * 政治面貌 1群众 2党员
     */
    private String politicsType;
}
