package com.hz.healthy.web.his.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2023/11/9 15:00
 * @ClassName: HHisActivityCourseFormDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisActivityCourseFormDTO implements Serializable {

    /** 主键ID */
    private Long id;


    /** 课程名称 */
    @NotBlank(message = "请输入课程名称")
    private String courseName;

    /** 课程地点 */
    @NotBlank(message = "请输入课程地点")
    private String place;

    /** 课程老师 */
    @NotBlank(message = "请输入课程老师")
    private String teacher;

    /** 课程背景 */
    @NotBlank(message = "请输入课程背景")
    private String courseBackground;

    /** 目标 */
    @NotBlank(message = "请输入课程目标")
    private String target;

    /** 负责社工 */
    private String organizer;

    /** 负责社工ID */
    @NotNull(message = "请选择负责社工")
    private Long organizerId;

    /** 老人义工ID */
    private Long patientId;

    /** 老人义工 */
    private String patientName;

    /** 课程日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date courseDate;

    /** 活动类型id */
    private Long activityType;

    /** 活动 0养老院 1社区 */
    private String activitySource;

    /** 参与对像 */
    private String enrollObj;

    /** 是否需要报名 0不需要 1需要 */
    private String izEnroll;

    /** 允许报名人数 当iz_enroll为1就填写 */
    private Integer enrollCount;

    /** 报名开始时间 当iz_enroll为1就填写 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date enrollStart;

    /** 报名结束时间 当iz_enroll为1就填写 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date enrollEnd;

    /** 是否可以获得积分 0不可以 1可以 */
    private String izIntegral;

    /** 课程封面 */
    private String img;

    /** 课程安排内容 */
    private String content;

    /** 积分数 */
    private Integer integral;


    private List<Long> patientIds;


}
