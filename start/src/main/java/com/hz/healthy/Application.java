package com.hz.healthy;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Spring Boot Starter
 *
 * @author hyhuang
 */
@Slf4j
@SpringBootApplication(scanBasePackages = {"com.hz.healthy","com.hz.ocean"})
@ServletComponentScan
@MapperScan(value={"com.hz.**.dao","com.hz.ocean.system.**.dao","com.hz.ocean.pay.**.dao", "com.hz.**.model","com.hz.ocean.web.core.base.dao"})
public class Application {

    public static void main(String[] args) {
        try {
            SpringApplication app =new SpringApplication(Application.class);
            app.setWebApplicationType(WebApplicationType.SERVLET);
            Environment environment = app.run(args).getEnvironment();
            logApplicationStartup(environment);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * 日志打印启动信息
     *
     * @param env 当前环境
     */
    private static void logApplicationStartup(Environment env) {
        String protocol = "http";
        String serverPort = env.getProperty("server.port");
        String contextPath = env.getProperty("server.servlet.context-path");
        if (StringUtils.isBlank(contextPath)) {
            contextPath = "/";
        }
        String hostAddress = "localhost";
        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }
        log.info(
                "\n----------------------------------------------------------\n\t"
                        + "Application '{}' is running! Access URLs:\n\t" + "Local: \t\t{}://localhost:{}{}\n\t"
                        + "External: \t{}://{}:{}{}\n\t"
                        + "Profile(s): \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"), protocol, serverPort, contextPath, protocol, hostAddress,
                serverPort, contextPath, env.getActiveProfiles());
    }
}
