package com.hz.healthy;

import com.hz.healthy.enums.*;
import com.hz.ocean.common.enums.BaseEnum;
import com.hz.ocean.common.enums.FactoryEnum;
import com.hz.ocean.pay.enums.PayStatusEnum;
import com.hz.ocean.system.enums.ArticleSourceEnum;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * load mt to cache
 * @author hyhuang
 * @desc
 * @date 2022/11/9 15:26
 */
@Slf4j
@Component
@Order(1)
public class SpringAppRunner implements CommandLineRunner {


    @Autowired
    private OceanBaseConfig oceanBaseConfig;

    @Override
    public void run(String... args) throws Exception {
        load();
    }

    private List<BaseEnum>  allSystemIds() {
        List<BaseEnum> list = new ArrayList<>();
        Map<String, String> systemIdMaps = oceanBaseConfig.getSystemIdMaps();
        if (MapUtils.isNotEmpty(systemIdMaps)) {
            for (Map.Entry<String, String> entry : systemIdMaps.entrySet()) {
                BaseEnum kv = new BaseEnum(entry.getKey(), entry.getValue());
                list.add(kv);
            }
        }
        return list;
    }

    private void load(){
        FactoryEnum.getInstance().register("heart_code", HeartVariableEnums.toUIList());
        FactoryEnum.getInstance().register("test_type", TestTypeEnums.toList());
        FactoryEnum.getInstance().register("question_type", QuestionTypeEnums.toList());
        FactoryEnum.getInstance().register("question_attr", QuestionAttrEnums.toList());
        FactoryEnum.getInstance().register("test_version", TestVersionEnums.toList());
        FactoryEnum.getInstance().register("merchant_type", MerchantTypeEnums.toList());
        FactoryEnum.getInstance().register("system_ids", allSystemIds());
        FactoryEnum.getInstance().register("article_source", ArticleSourceEnum.toList());
        FactoryEnum.getInstance().register("registration_status", RegistrationStatusEnums.toList());
        FactoryEnum.getInstance().register("audit_status", HisAuditStatusEnums.toList());
        FactoryEnum.getInstance().register("advice_order_status", AdviceStatusEnums.toList());
        FactoryEnum.getInstance().register("pay_order_status", PayStatusEnum.toList());
        FactoryEnum.getInstance().register("pay_status", PayStatusEnum.toList());
        FactoryEnum.getInstance().register("employee_flag", EmployeeFlagEnums.toList());
        FactoryEnum.getInstance().register("order_service_status", OrderServiceStatusEnums.toList());
        FactoryEnum.getInstance().register("healthy_data", HealthyDataEnums.toList());
        FactoryEnum.getInstance().register("risk_level", RiskLevelEnums.toList());
        FactoryEnum.getInstance().register("device_type", DeviceTypeEnums.toList());
        FactoryEnum.getInstance().register("nationality_type", NationalityEnum.toList());
        log.info(">>>已加载完成>>>");
    }
}
