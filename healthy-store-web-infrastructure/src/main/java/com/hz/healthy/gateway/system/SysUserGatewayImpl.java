package com.hz.healthy.gateway.system;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.PasswordUtil;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.service.auth.*;
import com.hz.ocean.system.service.auth.model.*;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import com.hz.ocean.web.core.utils.redis.RedisStringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class SysUserGatewayImpl {
    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysDepartService sysDepartService;

    @Autowired
    private ISysUserRoleService sysUserRoleService;

    @Autowired
    private ISysUserDepartService sysUserDepartService;

    @Autowired
    private ISysUserRoleService userRoleService;

    @Autowired
    private ISysDepartRoleUserService departRoleUserService;

    @Autowired
    private ISysDepartRoleService departRoleService;

    @Autowired
    private RedisStringUtil redisStringUtil;

    @Autowired
    private OceanBaseConfig oceanBaseConfig;
    private String getSystemId(){
        return oceanBaseConfig.getSystemId();
    }

    /**
     * 聚合依赖的包
     * 获取用户列表数据
     *
     * @return
     */
    public ListResponse<SysUserModel> queryPageList(SysUserModel queryModel, int pageNo, int pageSize, String code) {
        if (queryModel == null) {
            queryModel = new SysUserModel();
        }
        //部门ID
        String departId = queryModel.getDepartIds();
        if (oConvertUtils.isNotEmpty(departId)) {
            SysUserDepartModel queryUserDepartModel = new SysUserDepartModel(null, departId);
            List<SysUserDepartModel> list = sysUserDepartService.list(queryUserDepartModel);
            List<String> userIds = list.stream().map(SysUserDepartModel::getUserId).collect(Collectors.toList());
            queryModel.setIds(userIds);
        }
        //用户ID
        if (oConvertUtils.isNotEmpty(code)) {
            queryModel.setIds(Arrays.asList(code.split(",")));
            pageSize = code.split(",").length;
        }
        //TODO 外部模拟登陆临时账号，列表不显示
        //queryModel.setUsername("_reserve_user_external");
        queryModel.setPageNo(pageNo);
        queryModel.setPageSize(pageSize);
        ListResponse<SysUserModel> pageList = sysUserService.pageQuery(queryModel);

        //批量查询用户的所属部门
        //step.1 先拿到全部的 useids
        //step.2 通过 useids，一次性查询用户的所属部门名字
        List<String> userIds = pageList.getRecords().stream().map(SysUserModel::getId).collect(Collectors.toList());
        if (userIds != null && userIds.size() > 0) {
            Map<String, String> useDepNames = sysUserService.getDepNamesByUserIds(userIds);
            pageList.getRecords().forEach(item -> {
                //item.setOrgCodeTxt(useDepNames.get(item.getId()));
            });
        }
        return pageList;
    }

    public void add(JSONObject jsonObject) {
        String selectedRoles = jsonObject.getString("selectedroles");
        String selectedDeparts = jsonObject.getString("selecteddeparts");
        SysUserModel user = JSON.parseObject(jsonObject.toJSONString(), SysUserModel.class);
        user.setCreateTime(new Date());//设置创建时间
        String salt = PasswordUtil.randomGen(8);
        user.setSalt(salt);
        String passwordEncode = PasswordUtil.encrypt(user.getUsername(), user.getPassword(), salt);
        user.setPassword(passwordEncode);
        user.setStatus(1);
        user.setDelFlag(CommonConstant.DEL_FLAG_0);
        user.setSystemId(getSystemId());
        // 保存用户走一个service 保证事务
        sysUserService.saveUser(user, selectedRoles, selectedDeparts);
    }

    public void addEl(JSONObject jsonObject,Long tenantId) {
        JSONArray rolesJsonArray = jsonObject.getJSONArray("roleIds");
        StringBuffer roles=new StringBuffer();
        if(rolesJsonArray!=null){
            for(int i=0,len=rolesJsonArray.size();i<len;i++){
                if(i>0){ roles.append(",");}
                roles.append(rolesJsonArray.get(i).toString());
            }
        }
        SysUserModel user = JSON.parseObject(jsonObject.toJSONString(), SysUserModel.class);
        String selectedDeparts =user.getDepartIds();
        user.setCreateTime(new Date());//设置创建时间
        String salt = PasswordUtil.randomGen(8);
        user.setSalt(salt);
        String passwordEncode = PasswordUtil.encrypt(user.getUsername(), user.getPassword(), salt);
        user.setPassword(passwordEncode);
        user.setStatus(1);
        user.setDelFlag(CommonConstant.DEL_FLAG_0);
        user.setSystemId(getSystemId());
        user.setTenantId(tenantId);
        // 保存用户走一个service 保证事务
        sysUserService.saveUser(user, roles.toString(), selectedDeparts);
    }

    public void edit(JSONObject jsonObject) throws BizException {
        SysUserModel sysUser = sysUserService.getById(jsonObject.getString("id"));
        if (sysUser == null) {
            throw new BizException("未找到对应实体");
        } else {
            SysUserModel user = JSON.parseObject(jsonObject.toJSONString(), SysUserModel.class);
            user.setUpdateTime(new Date());
            //String passwordEncode = PasswordUtil.encrypt(user.getUsername(), user.getPassword(), sysUser.getSalt());
            user.setPassword(sysUser.getPassword());
            String roles = jsonObject.getString("selectedroles");
            String departs = jsonObject.getString("selecteddeparts");
            if (StringUtils.isEmpty(departs)) {
                //vue3.0前端只传递了departIds
                departs = user.getDepartIds();
            }
            // 修改用户走一个service 保证事务
            sysUserService.editUser(user, roles, departs);
        }
    }

    /**
     * 删除用户
     */
    public void delete(String id) {
        this.sysUserService.deleteUser(id);
    }

    /**
     * 批量删除用户
     */
    public void deleteBatch(String ids) {
        this.sysUserService.deleteBatchUsers(ids);
    }

    /**
     * 冻结&解冻用户
     *
     * @param jsonObject
     * @return
     */
    public void frozenBatch(JSONObject jsonObject) {
        String ids = jsonObject.getString("ids");
        String status = jsonObject.getString("status");
        String[] arr = ids.split(",");
        for (String id : arr) {
            if (StringUtils.isNotEmpty(id)) {
                SysUserModel sysUserModel = new SysUserModel();
                sysUserModel.setId(id);
                sysUserModel.setStatus(Integer.parseInt(status));
                this.sysUserService.updateById(sysUserModel);
            }
        }
    }

    public SysUserModel queryById(String id) {
        SysUserModel sysUser = sysUserService.getById(id);
        return sysUser;
    }

    public List<String> queryUserRole(String userid) {
        List<String> list = new ArrayList<String>();
        SysUserRoleModel queryModel = new SysUserRoleModel();
        queryModel.setUserId(userid);
        List<SysUserRoleModel> userRole = sysUserRoleService.list(queryModel);
        if (CollectionUtils.isNotEmpty(userRole)) {
            for (SysUserRoleModel sysUserRole : userRole) {
                list.add(sysUserRole.getRoleId());
            }
        }
        return list;
    }


    /**
     * 校验用户账号是否唯一<br>
     * 可以校验其他 需要检验什么就传什么。。。
     *
     * @param sysUserModel
     * @return
     */
    public Boolean checkOnlyUser(SysUserModel sysUserModel) {
        try {
            //通过传入信息查询新的用户信息
            sysUserModel.setPassword(null);
            SysUserModel user = sysUserService.getUserByName(sysUserModel.getUsername(),getSystemId());
            if (user != null) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 修改密码
     */
    public void changePassword(SysUserModel sysUser) {
        SysUserModel u = sysUserService.getUserByName(sysUser.getUsername(),getSystemId());
        if (u == null) {
            throw new BizException("用户不存在！");
        }
    }

    /**
     * 查询指定用户和部门关联的数据
     *
     * @param userId
     * @return
     */
    public List<DepartIdModel> getUserDepartsList(String userId) {
        List<DepartIdModel> depIdModelList = this.sysUserDepartService.queryDepartIdsOfUser(userId);
        return depIdModelList;
    }


    /**
     * 根据部门id查询用户信息
     *
     * @param id
     * @return
     */
    public List<SysUserModel> queryUserByDepId(String id, String realname) {
        RespResult<List<SysUserModel>> result = new RespResult<>();
        SysDepartModel sysDepart = sysDepartService.getById(id);
        List<SysUserModel> userList = sysUserDepartService.queryUserByDepCode(sysDepart.getOrgCode(), realname,getSystemId());

        //批量查询用户的所属部门
        //step.1 先拿到全部的 useids
        //step.2 通过 useids，一次性查询用户的所属部门名字
        List<String> userIds = userList.stream().map(SysUserModel::getId).collect(Collectors.toList());
        if (userIds != null && userIds.size() > 0) {
            Map<String, String> useDepNames = sysUserService.getDepNamesByUserIds(userIds);
            userList.forEach(item -> {
                //TODO 临时借用这个字段用于页面展示
                //item.setOrgCodeTxt(useDepNames.get(item.getId()));
            });
        }
        return userList;
    }

    /**
     * 用户选择组件 专用  根据用户账号或部门分页查询
     *
     * @param departId
     * @param username
     * @return
     */
    public ListResponse<SysUserModel> queryUserComponentData(Integer pageNo, Integer pageSize,
                                                             String departId, String realname, String username) {
        ListResponse<SysUserModel> pageList = sysUserDepartService.queryDepartUserPageList(departId, username, realname,getSystemId(), pageSize, pageNo);
        return pageList;
    }


    /**
     * @param userIds
     * @return
     * @功能：根据id 批量查询
     */
    public List<SysUserModel> queryByIds(String userIds) {
        List<SysUserModel> userRole = new ArrayList<>();
        if (StringUtils.isBlank(userIds)) {
            return userRole;
        }
        String[] userId = userIds.split(",");
        List<String> idList = Arrays.asList(userId);
        userRole = sysUserService.listByIds(idList);
        return userRole;
    }

    /**
     * 首页用户重置密码
     */
    public void updatePassword(JSONObject json, String loginUserName) {
        String username = json.getString("username");
        String oldpassword = json.getString("oldpassword");
        String password = json.getString("password");
        String confirmpassword = json.getString("confirmpassword");
        if (!loginUserName.equals(username)) {
            throw new BizException("只允许修改自己的密码！");
        }
        SysUserModel query = new SysUserModel();
        query.setUsername(username);
        SysUserModel user = this.sysUserService.getOne(query);
        if (user == null) {
            throw new BizException("用户不存在！");
        }
        sysUserService.resetPassword(username, oldpassword, password, confirmpassword,getSystemId());
    }

    public ListResponse<SysUserModel> userRoleList(Integer pageNo,
                                                   Integer pageSize,
                                                   String roleId,
                                                   String username,
                                                   String systemId) {
        ListResponse<SysUserModel> pageList = sysUserService.getUserByRoleId(pageNo,pageSize, roleId, username,systemId);
        return pageList;
    }

    /**
     * 给指定角色添加用户
     *
     * @param
     * @return
     */
    //@RequiresRoles({"admin"})
    public void addSysUserRole(SysUserRoleInfoModel sysUserRoleVO) {
        RespResult<String> result = new RespResult<String>();
        try {
            String sysRoleId = sysUserRoleVO.getRoleId();
            for (String sysUserId : sysUserRoleVO.getUserIdList()) {
                SysUserRoleModel query = new SysUserRoleModel();
                query.setRoleId(sysRoleId);
                query.setUserId(sysUserId);
                SysUserRoleModel one = sysUserRoleService.getOne(query);
                if (one == null) {
                    SysUserRoleModel sysUserRole = new SysUserRoleModel(sysUserId, sysRoleId);
                    sysUserRoleService.saveOrUpdate(sysUserRole);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BizException("出错");
        }
    }

    /**
     * 删除指定角色的用户关系
     *
     * @param
     * @return
     */
    public void deleteUserRole(String roleId, String userId) {
        try {
            sysUserRoleService.remove(roleId, userId);
        } catch (Exception e) {
            throw new BizException("删除失败");
        }
    }

    /**
     * 批量删除指定角色的用户关系
     *
     * @param
     * @return
     */
    public void deleteUserRoleBatch(String roleId, List<String> userIds) {
        try {
            sysUserRoleService.remove(roleId, userIds);
        } catch (Exception e) {
            throw new BizException("删除失败");
        }
    }

    /**
     * 部门用户列表
     */
    public ListResponse<SysUserModel> departUserList(Integer pageNo,
                                                     Integer pageSize,
                                                     String depId,
                                                     String username,
                                                     LoginUser user) {

        //根据部门ID查询,当前和下级所有的部门IDS
        List<String> subDepids = new ArrayList<>();
        //部门id为空时，查询我的部门下所有用户
        if (StringUtils.isEmpty(depId)) {
            int userIdentity = user.getUserIdentity() != null?user.getUserIdentity():CommonConstant.USER_IDENTITY_1;
            if (oConvertUtils.isNotEmpty(userIdentity) && userIdentity == CommonConstant.USER_IDENTITY_2) {
                subDepids = sysDepartService.getMySubDepIdsByDepId(user.getDepartIds());
            }
        } else {
            subDepids = sysDepartService.getSubDepIdsByDepId(depId);
        }
        if (subDepids != null && subDepids.size() > 0) {
            ListResponse<SysUserModel> pageList = sysUserService.getUserByDepIds(pageNo,pageSize, subDepids, username,getSystemId());
            //批量查询用户的所属部门
            //step.1 先拿到全部的 useids
            //step.2 通过 useids，一次性查询用户的所属部门名字
            List<String> userIds = pageList.getRecords().stream().map(SysUserModel::getId).collect(Collectors.toList());
            if (userIds != null && userIds.size() > 0) {
                Map<String, String> useDepNames = sysUserService.getDepNamesByUserIds(userIds);
                pageList.getRecords().forEach(item -> {
                    //批量查询用户的所属部门
                    item.setOrgCode(useDepNames.get(item.getId()));
                });
            }
            return pageList;
        }
        return null;
    }


    /**
     * 根据 orgCode 查询用户，包括子部门下的用户
     * 若某个用户包含多个部门，则会显示多条记录，可自行处理成单条记录
     */
    public  ListResponse<SysUserSysDepartModel> queryByDepartId(Integer pageNo,Integer pageSize,String orgCode, SysUserModel userParams
    ) {
        userParams.setPageSize(pageSize);
        userParams.setPageNo(pageNo);
        ListResponse<SysUserSysDepartModel> pageList = sysUserService.queryUserByOrgCode(orgCode,getSystemId(), userParams);
        return pageList;
    }

    /**
     * 根据 orgCode 查询用户，包括子部门下的用户
     * 针对通讯录模块做的接口，将多个部门的用户合并成一条记录，并转成对前端友好的格式
     */
    public List<JSONObject> queryByOrgCodeForAddressList( Integer pageNo,Integer pageSize, String orgCode,
                                                          SysUserModel userParams
    ) {
        userParams.setPageSize(pageSize);
        userParams.setPageNo(pageNo);
        ListResponse<SysUserSysDepartModel> pageList = sysUserService.queryUserByOrgCode(orgCode,getSystemId(), userParams);
        List<SysUserSysDepartModel> list = pageList.getRecords();

        // 记录所有出现过的 user, key = userId
        Map<String, JSONObject> hasUser = new HashMap<>(list.size());

        JSONArray resultJson = new JSONArray(list.size());

        for (SysUserSysDepartModel item : list) {
            String userId = item.getId();
            // userId
            JSONObject getModel = hasUser.get(userId);
            // 之前已存在过该用户，直接合并数据
            if (getModel != null) {
                String departName = getModel.get("departName").toString();
                getModel.put("departName", (departName + " | " + item.getDepartName()));
            } else {
                // 将用户对象转换为json格式，并将部门信息合并到 json 中
                JSONObject json = JSON.parseObject(JSON.toJSONString(item));
                json.remove("id");
                json.put("userId", userId);
                json.put("departId", item.getDepartId());
                json.put("departName", item.getDepartName());
                resultJson.add(json);
                hasUser.put(userId, json);
            }
        }
        return resultJson.toJavaList(JSONObject.class);
    }

    /**
     * 给指定部门添加对应的用户
     */
    public void editSysDepartWithUser(String sysUserId,String sysDepId) {
        SysUserDepartModel sysUserDepart = new SysUserDepartModel(null, sysUserId, sysDepId);
        SysUserDepartModel queryModel = new SysUserDepartModel(null, sysUserId, sysDepId);
        queryModel.setSystemId(getSystemId());
        SysUserDepartModel one = sysUserDepartService.getOne(queryModel);
        if (one == null) {
            sysUserDepartService.saveOrUpdate(sysUserDepart);
        }
    }

    /**
     * 删除指定机构的用户关系
     */
    public void deleteUserInDepart(String depId, String userId ) {
        sysUserDepartService.remove(depId, userId);
        SysDepartRoleModel queryModel = new SysDepartRoleModel();
        queryModel.setDepartId(depId);
        List<SysDepartRoleModel> sysDepartRoleList = departRoleService.list(queryModel);
        List<String> roleIds = sysDepartRoleList.stream().map(SysDepartRoleModel::getId).collect(Collectors.toList());
        if (roleIds != null && roleIds.size() > 0) {
            departRoleUserService.remove(userId, roleIds);
        }
    }

    /**
     * 批量删除指定机构的用户关系
     */
    public void deleteUserInDepartBatch( String depId,String userIds) {
        try {
            sysUserDepartService.remove(depId, Arrays.asList(userIds.split(",")));
            departRoleUserService.removeDeptRoleUser(Arrays.asList(userIds.split(",")), depId);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BizException("删除失败！");
        }
    }

    /**
     * 查询当前用户的所有部门/当前部门编码
     *
     * @return
     */
    public Map<String, Object> getCurrentUserDeparts( LoginUser sysUser) {
        List<SysDepartModel> list = this.sysDepartService.queryUserDeparts(sysUser.getId());
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("list", list);
        map.put("orgCode", sysUser.getOrgCode());
        return map;
    }


    /**
     * 用户注册接口
     *
     * @param jsonObject
     * @param user
     * @return
     */
    public void userRegister(JSONObject jsonObject, SysUserModel user) {
        String phone = jsonObject.getString("phone");
        String smscode = jsonObject.getString("smscode");
        Object code = redisStringUtil.get(phone);
        String username = jsonObject.getString("username");
        //未设置用户名，则用手机号作为用户名
        if (StringUtils.isEmpty(username)) {
            username = phone;
        }
        //未设置密码，则随机生成一个密码
        String password = jsonObject.getString("password");
        if (StringUtils.isEmpty(password)) {
            password = RandomUtil.randomString(8);
        }
        String email = jsonObject.getString("email");
        SysUserModel sysUser1 = sysUserService.getUserByName(username,getSystemId());
        if (sysUser1 != null) {
            throw new BizException("用户名已注册");
        }
        SysUserModel sysUser2 = sysUserService.getUserByPhone(phone,getSystemId());
        if (sysUser2 != null) {
            throw new BizException("该手机号已注册");
        }

        if (StringUtils.isNotEmpty(email)) {
            SysUserModel sysUser3 = sysUserService.getUserByEmail(email,getSystemId());
            if (sysUser3 != null) {
                throw new BizException("邮箱已被注册");
            }
        }
        if (null == code) {
            throw new BizException("手机验证码失效，请重新获取");
        }
        if (!smscode.equals(code.toString())) {
            throw new BizException("手机验证码错误");
        }

        try {
            user.setCreateTime(new Date());// 设置创建时间
            String salt = PasswordUtil.randomGen(8);
            String passwordEncode = PasswordUtil.encrypt(username, password, salt);
            user.setSalt(salt);
            user.setUsername(username);
            user.setRealname(username);
            user.setPassword(passwordEncode);
            user.setEmail(email);
            user.setPhone(phone);
            user.setStatus(CommonConstant.USER_UNFREEZE);
            user.setDelFlag(CommonConstant.DEL_FLAG_0);
            sysUserService.addUserWithRole(user, "ee8626f80f7c2619917b6236f3a7f02b");//默认临时角色 test
        } catch (Exception e) {
            throw new BizException("注册失败");
        }
    }




    public SysUserModel getUserInfoByPhone(String username){
        SysUserModel sysUser = sysUserService.getUserByPhone(username, getSystemId());
        return sysUser;
    }


    /**
     * 用户手机号验证
     */
    public Map<String, String> phoneVerification(JSONObject jsonObject) {
        String phone = jsonObject.getString("phone");
        String smscode = jsonObject.getString("smscode");
        Object code = redisStringUtil.get(phone);
        if (!smscode.equals(code)) {
            throw new BizException("手机验证码错误");
        }
        //设置有效时间
        redisStringUtil.set(phone, smscode, 600);
        //新增查询用户名
        SysUserModel queryModel = new SysUserModel();
        queryModel.setPhone(phone);
        SysUserModel user = sysUserService.getOne(queryModel);
        Map<String, String> map = new HashMap<>();
        map.put("smscode", smscode);
        map.put("username", user.getUsername());
        return map;
    }

    /**
     * 用户更改密码
     */
    public void passwordChange(String username,String password,String smscode, String phone) {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password) || StringUtils.isEmpty(smscode) || StringUtils.isEmpty(phone)) {
            throw new BizException("重置密码失败");
        }

        SysUserModel sysUser = new SysUserModel();
        Object object = redisStringUtil.get(phone);
        if (null == object) {
            throw new BizException("短信验证码失效");
        }
        if (!smscode.equals(object.toString())) {
            throw new BizException("短信验证码不匹配");
        }
        SysUserModel queryModel = new SysUserModel();
        queryModel.setUsername(username);
        queryModel.setPhone(phone);
        sysUser = this.sysUserService.getOne(queryModel);
        if (sysUser == null) {
            throw new BizException("未找到用户");
        } else {
            String salt = PasswordUtil.randomGen(8);
            sysUser.setSalt(salt);
            String passwordEncode = PasswordUtil.encrypt(sysUser.getUsername(), password, salt);
            sysUser.setPassword(passwordEncode);
            this.sysUserService.updateById(sysUser);
        }
    }
    /**
     * 根据用户名修改手机号
     * @param json
     * @return
     */
    public void changMobile(JSONObject json,String username) {
        String smscode = json.getString("smscode");
        String phone = json.getString("phone");
        RespResult<SysUserModel> result = new RespResult<SysUserModel>();
        //获取登录用户名
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(smscode) || StringUtils.isEmpty(phone)) {
            throw new BizException("修改手机号失败！");
        }
        Object object= redisStringUtil.get(phone);
        if(null==object) {
            throw new BizException("短信验证码失效！");
        }
        if(!smscode.equals(object.toString())) {
            throw new BizException("短信验证码不匹配！");
        }
        SysUserModel user = sysUserService.getUserByName(username,getSystemId());
        if(user==null) {
            throw new BizException("用户不存在！");
        }
        user.setPhone(phone);
        sysUserService.updateById(user);
    }


    public static void main(String[] args) {
        String username="jsyxin";
        String password="123456";
        String salt="RCGTeGiH";
        String passwordEncode = PasswordUtil.encrypt(username, password, salt);
        System.out.println(passwordEncode);
    }


}
