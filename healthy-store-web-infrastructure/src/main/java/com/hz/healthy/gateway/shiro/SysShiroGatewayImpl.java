package com.hz.healthy.gateway.shiro;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.healthy.domain.doctor.model.HEmployeeModel;
import com.hz.healthy.domain.doctor.model.HEmployeeQueryModel;
import com.hz.healthy.domain.his.IHHospitalService;
import com.hz.healthy.domain.his.model.HHospitalModel;
import com.hz.healthy.domain.his.model.HHospitalQueryModel;
import com.hz.healthy.domain.store.IHStoreUserService;
import com.hz.healthy.domain.store.model.HStoreUserModel;
import com.hz.healthy.domain.store.model.HStoreUserQueryModel;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.common.utils.oConvertUtils;
import com.hz.ocean.system.service.auth.ISysPermissionService;
import com.hz.ocean.system.service.auth.ISysUserRoleService;
import com.hz.ocean.system.service.auth.ISysUserService;
import com.hz.ocean.system.service.auth.model.SysPermissionModel;
import com.hz.ocean.system.service.auth.model.SysUserModel;
import com.hz.ocean.uac.api.ShiroAPI;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.uac.model.SysPermissionDataRuleModel;
import com.hz.ocean.uac.model.SysUserCacheInfo;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用于登录验证使用
 */
@Service
public class SysShiroGatewayImpl implements ShiroAPI {

    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysUserRoleService sysUserRoleService;
    @Autowired
    private ISysPermissionService sysPermissionService;

    @Autowired
    private OceanBaseConfig oceanBaseConfig;
    @Autowired
    private IHStoreUserService ihStoreUserService;

    @Autowired
    private IHHospitalService hhospitalService;


    @Autowired
    private IHEmployeeService hEmployeeService;

    private String getSystemId() {
        return oceanBaseConfig.getSystemId();
    }

    /**
     * 1查询用户角色信息
     *
     * @param username
     * @return
     */
    @Override
    public Set<String> queryUserRoles(String username) {
        //TODO 修改角色来源
        List<String> roles = sysUserRoleService.getRoleByUserName(username, getSystemId());
        return new HashSet<>(roles);
    }

    /**
     * 2查询用户权限信息
     *
     * @param username
     * @return
     */
    @Override
    public Set<String> queryUserAuths(String username) {
        //TODO 修改菜单
        return getUserPermissionSet(username);
    }

    @Override
    public LoginUser getUserByName(String username, String token) {
        return getUserByName(username);
    }

    @Override
    public Set<String> getUserPermissionSet(String username) {
        Set<String> permissionSet = new HashSet<>();
        //List<SysPermissionModel> permissionList = sysPermissionService.queryByUser(username,getSystemId());
        SysPermissionModel query = new SysPermissionModel();
        query.setSystemId(getSystemId());
        List<SysPermissionModel> permissionList = sysPermissionService.list(query);
        if (CollectionUtils.isNotEmpty(permissionList)) {
            for (SysPermissionModel po : permissionList) {
                if (oConvertUtils.isNotEmpty(po.getPerms())) {
                    permissionSet.add(po.getPerms());
                }
            }
        }
        return permissionSet;
    }

    /**
     * 5根据用户账号查询用户信息
     *
     * @param username
     * @return
     */
    @Override
    public LoginUser getUserByName(String username) {
        LoginUser loginUser = new LoginUser();
        HStoreUserModel storeUserModel = ihStoreUserService.getOne(HStoreUserQueryModel.builder()
                .loginName(username)
                .isDeleted(CommonConstant.DEL_FLAG_0)
                .build());
        if (ObjectUtil.isNotNull(storeUserModel)) {

            loginUser.setId(storeUserModel.getId().toString());
            loginUser.setDepartIds(storeUserModel.getStoreCode());
            loginUser.setRealname(storeUserModel.getRealName());
            loginUser.setUsername(storeUserModel.getLoginName());
            loginUser.setStatus(Integer.parseInt(CommonConstant.STATUS_1));
            loginUser.setPassword(storeUserModel.getPassword());
            loginUser.setSex(storeUserModel.getSex());
            loginUser.setOrgCode(storeUserModel.getOrgCode());
            loginUser.setTenantId(storeUserModel.getTenantId());
            loginUser.setStoreCode(storeUserModel.getStoreCode());
            loginUser.setUserId(storeUserModel.getUserId());

            HHospitalModel hospitalModel = hhospitalService.getOne(HHospitalQueryModel.builder()
                    .orgCode(storeUserModel.getOrgCode())
                    .build());
            if(ObjectUtil.isNotNull(hospitalModel)){
                loginUser.setCompany(hospitalModel.getHospitalName());
            }

            if (StringUtils.isNotBlank(storeUserModel.getUserId())) {
                HEmployeeModel employeeModel = hEmployeeService.getOne(HEmployeeQueryModel.builder()
                        .userId(storeUserModel.getUserId())
                        .orgCode(storeUserModel.getOrgCode())
                        .build());
                if(ObjectUtil.isNotNull(employeeModel)){
                    loginUser.setEyId(employeeModel.getId());
                    loginUser.setRealname(employeeModel.getName());
                }
            }
        }
        return loginUser;
    }

    @Override
    public LoginUser getUserById(String id) {
        SysUserModel sysUserModel = sysUserService.getById(id);
        return BeanUtil.copyProperties(sysUserModel, LoginUser.class);
    }

    @Override
    public List<SysPermissionDataRuleModel> queryPermissionDataRule(String component, String requestPath, String username) {
        return null;
    }

    @Override
    public SysUserCacheInfo getCacheUser(String username) {
        return null;
    }

}
