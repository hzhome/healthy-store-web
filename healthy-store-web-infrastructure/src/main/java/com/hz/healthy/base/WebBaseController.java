package com.hz.healthy.base;

import com.hz.healthy.domain.doctor.IHEmployeeService;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.web.core.base.BaseController;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author hyhuang
 * @desc
 * @date 2022/11/25 14:34
 */
public class WebBaseController extends BaseController {

    @Autowired
    private OceanBaseConfig oceanBaseConfig;



    protected OceanBaseConfig getBaseConfig(){
        return oceanBaseConfig;
    }

    protected LoginUser getLoginUser(){
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return loginUser;
    }

    protected Long getDoctorId(){
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return loginUser.getEyId();
    }

    protected String getStoreCode(){
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return loginUser.getDepartIds();
    }

    protected String getSystemId() {
        return oceanBaseConfig.getSystemId();
    }

    protected Long getTenantId() {
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return loginUser.getTenantId();
    }

    protected String getTenantCode() {
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return loginUser.getStoreCode();
    }

    protected String getUserId(){
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return loginUser.getUserId();
    }

    protected String getOrgCode() {
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        return loginUser.getOrgCode();
    }

    protected String getImageUrl(String img){
        return  oceanBaseConfig.getResource().getUpfsUrl()+img;
    }
}
