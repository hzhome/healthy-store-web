package com.hz.healthy.base.table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/8/7 13:27
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Columns implements Serializable {

    private String title;
    private String dataIndex;
    private String align;
    private int width;
    private List<Columns> children;
    private String action;
}
