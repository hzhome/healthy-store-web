package com.hz.healthy.config;


public class SanheEduConfig {

    public final static String MANAGE_ORG_CODE = "EDU_%s";

    /**
     * 学生总人数
     */
    public final static String STU_COUNT = "EDU_STU_COUNT_%s";

    /**
     * 缴费总人数
     */
    public final static String PAY_COUNT="EDU_PAY_COUNT_%";
}