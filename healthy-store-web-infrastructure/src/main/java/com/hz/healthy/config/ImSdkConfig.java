package com.hz.healthy.config;

import com.easemob.im.ApiClient;
import com.easemob.im.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 * @Author hyhuang
 * @Date 2025/2/19 20:43
 * @ClassName: ImSdkConfig
 * @Description: 环信 im 通讯
 * @Version 1.0
 */
@Slf4j
@Configuration
public class ImSdkConfig {

    static {
        try {
            com.easemob.im.Configuration.setDefaultApiClient(ApiClient.builder()
                    .setBasePath("https://a1.easemob.com")
                    .setAppKey("1126250215193807#demo")
                    .setClientId("YXA6EBKEF-8xRgSh8In8eh2VBg")
                    .setClientSecret("YXA6KvXNCqw_EuQOezTsI2QydydJ-fA")
                    .build());
            log.info("环信im通讯配置初始化完成");
        } catch (ApiException e) {
            e.printStackTrace();
            log.error("环信im通讯配置初始化失败");
        }
    }

}
