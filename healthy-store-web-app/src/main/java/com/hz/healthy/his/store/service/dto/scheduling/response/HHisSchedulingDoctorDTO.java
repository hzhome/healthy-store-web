package com.hz.healthy.his.store.service.dto.scheduling.response;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/12/19 13:39
 * @ClassName: HHisSchedulingDoctorDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisSchedulingDoctorDTO implements Serializable {


    private Long id;



    /** 真实姓名 */
    @NotBlank(message = "姓名不能为空")
    private String name;


    /**
     * 总约人数
     */
    private Integer totalQty;


    /**
     * 等待人数
     */
    private Integer waitQty;
}
