package com.hz.healthy.his.store.service.dto.care.request.items;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/26 15:13
 * @ClassName: ChineMedicineData
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InfusionDataSource implements Serializable {


    /** 用法 */
    private String usages;

    /** 频次 */
    private String frequency;

    /**
     * 天数
     */
    private String day;

    /** 每次用量 */
    private String dose;

    List<MedicineDataSource> itemList;
}
