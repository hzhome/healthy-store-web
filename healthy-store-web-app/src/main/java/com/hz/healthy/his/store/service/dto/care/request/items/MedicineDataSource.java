package com.hz.healthy.his.store.service.dto.care.request.items;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author hyhuang
 * @Date 2024/11/26 15:13
 * @ClassName: ChineMedicineData
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class MedicineDataSource implements Serializable {

    private Long medicineId;
    private String medicineName;
    private String unit;
    private String conversionUnit;
    private BigDecimal day;
    private BigDecimal dose;
    private String usages;
    private String frequency;
    private String remark;

    private String standard;
    private BigDecimal totalDose;
}
