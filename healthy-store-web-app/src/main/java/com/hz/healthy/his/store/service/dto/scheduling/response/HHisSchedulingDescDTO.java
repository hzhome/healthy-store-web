package com.hz.healthy.his.store.service.dto.scheduling.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/19 10:28
 */
@Data
public class HHisSchedulingDescDTO implements Serializable {

    /** 主键 */
    private Long id;

    private String schedulingCode;
    /** 科室ID */
    private Long departmentId;

    private String departmentName;
    private Long planId;
    private String planName;


    /** 本周开始时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date startTimeThisWeek;

    /** 本周结束时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date endTimeThisWeek;


}
