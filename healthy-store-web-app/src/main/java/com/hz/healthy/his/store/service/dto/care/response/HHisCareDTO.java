package com.hz.healthy.his.store.service.dto.care.response;

import com.hz.healthy.his.store.service.dto.care.request.items.ChineMedicineData;
import com.hz.healthy.his.store.service.dto.care.request.items.MedicineDataSource;
import com.hz.healthy.his.store.service.dto.care.request.items.ProjectDataSource;
import com.hz.healthy.his.store.service.dto.care.response.item.HHisCareItemDTO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/28 15:19
 * @ClassName: HHisCareDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisCareDTO implements Serializable {

    private Long id;
    private String careCode;


    private String title;
    /** 0：暂存  1：已结 */
    private String status;

    /** 医生姓名 */
    private String doctorName;

    /** 医生id */
    private Long doctorId;

    /** 挂号id */
    private Long registrationId;

    /** 患者姓名 */
    private String patientName;
    /** 患者id */
    private Long patientId;
    /** 患者性别 */
    private String sex;
    /** 患者年龄 */
    private Integer age;

    /** 科室名称 */
    private String departmentName;

    /** 科室id */
    private Integer departmentId;

    /** 接诊类型：0初诊，1复诊，2急诊 */
    private Integer inquiriesType;

    /** 是否传染，0否，1是 */
    private Integer isContagious;

    /** 发病日期 */
    private String caseDate;

    /** 诊断编号 */
    private String caseCode;

    /** 主诉 */
    private String caseTitle;

    /** 诊断信息 */
    private String caseResult;

    /** 往史信息 */
    private String historyResult;

    /** 医生建议 */
    private String doctorTips;

    /** 备注 */
    private String remark;


    /** 添加时间 */
    private String createTime;

    /** 婚姻 0:未婚 1:已婚 */
    private String izMarriage;

    /** 舒张压 */
    private BigDecimal diastolicPressure;

    /** 收缩压 */
    private BigDecimal systolicPressure;

    /** 血糖 */
    private BigDecimal bloodSugarValue;

    /** 呼吸 */
    private BigDecimal breathing;

    /** 脉博 */
    private BigDecimal pulse;

    /** 体温 */
    private BigDecimal temperature;

    /** 体重 */
    private BigDecimal weight;

    /** 是否已经删除，0，否；1，已删除 */
    private Integer isDeleted;

    /** 身高 */
    private Integer height;
    /**
     * 是否住院病历
     * 0否，1是
     */
    private String izInHospital;


    /**
     * 西医 中医
     */
    private String careType;


    /**
     * 望闻切诊
     */
    private String inspection;

    /**
     * 辨证
     */
    private String dialectic;
    /**
     * 体格检查
     */
    private String checkUp;


    private String allergyResult;

    /**
     * 执行的内容
     */
    private String  execDesc;
//    /**
//     * 中药处方
//     */
//    private List<ChineMedicineData> chineMedicineDataSource;
//
//
//    /**
//     * 西药处方
//     */
//    private List<MedicineDataSource> medicineDataSource;


    /**
     * 姓名 性别  年纪  手机号
     */
    private String patientDesc;


    private String patientPhone;


    /**
     * 0待发药 1已发药 2已退药 3已关闭
     */
    private String sendStatus;

    /**
     * 0待发药 1已发药 2已退药 3已关闭
     */
    private String sendStatusText;


    /**
     * 中药处方
     */
    private List<HHisCareItemDTO> chineMedicineDataSource;


    /**
     * 西药处方
     */
    private List<HHisCareItemDTO> medicineDataSource;


    /**
     * 输注处方
     */
    private List<HHisCareItemDTO> infusionDataSource;


    /**
     * 诊疗项目
     */
    private List<ProjectDataSource> projectDataSource;
}
