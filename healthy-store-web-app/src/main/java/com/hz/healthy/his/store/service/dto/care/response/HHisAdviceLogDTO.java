package com.hz.healthy.his.store.service.dto.care.response;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/12/13 12:08
 * @ClassName: HHisAdviceLogDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisAdviceLogDTO implements Serializable {

    private Long id;


    /** 患者id */
    private String patientName;

    /** 长期标志 0临时,1长期 */
    private String longOnceFlag;

    /** 医嘱名称 */
    private String displayName;

    /** 医嘱开始时间 */
    private Date startTime;

    /** 医嘱结束时间 */
    private Date stopTime;

    /** 最后执行时间 */
    private String execTime;

    /** 执行日期yyyy-mm-dd */
    private String execDay;

    /** 医嘱订单号 父医嘱号 */
    private String orderSn;

    /** 医嘱分类 1 西药 2 中药 3 常规项目 4 康复 5 治疗 */
    private String orderType;


    /** 执行护工id */
    private Long execId;

    /** 执行护工名称 */
    private String execName;

    /**  执行者身份 0是医生 1护士 2是社工 3护工  3康复师 4 其他 */
    private String execRole;

    /** 关联护士工作单 */
    private String relationNurseOrder;

    /**
     * 用法
     */
    private String usages;

}
