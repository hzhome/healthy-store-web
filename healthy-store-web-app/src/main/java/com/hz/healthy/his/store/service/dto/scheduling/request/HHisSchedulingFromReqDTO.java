package com.hz.healthy.his.store.service.dto.scheduling.request;

import com.hz.healthy.his.store.service.dto.scheduling.request.item.HHisSchedulingFromItem;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/19 17:35
 */
@Data
public class HHisSchedulingFromReqDTO implements Serializable {

    /**
     * 修改时候使用
     */
    private Long id;

    private Long timeId;
    private Long planId;
    @NotNull(message = "请选择相应的科室")
    private Long departId;
    private String name;
    private String num;

    private String startDate;
    private String endDate;

    private String date;
    //多个值班人员信息
    private List<HHisSchedulingFromItem> dataSource;


    private List<Long> eyIds;

    private String psType;







}
