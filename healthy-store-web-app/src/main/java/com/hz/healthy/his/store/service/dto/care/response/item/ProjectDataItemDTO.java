package com.hz.healthy.his.store.service.dto.care.response.item;

import com.hz.healthy.his.store.service.dto.care.request.items.ProjectDataSource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/12/15 20:40
 * @ClassName: ProjectDataItem
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectDataItemDTO implements Serializable {


    /** 处方金额 */
    private BigDecimal amount;
    /** 处方原金额 */
    private BigDecimal originalAmount;


    private String desc;

    /**
     * 诊疗项目
     */
    private List<ProjectDataSource> itemList;
}
