package com.hz.healthy.his.store.service;

import com.hz.healthy.domain.doctor.model.HHisCareQueryModel;
import com.hz.healthy.domain.his.model.HHisAdviceLogQueryModel;
import com.hz.healthy.his.store.service.dto.care.request.HHisCareFormReqDTO;
import com.hz.healthy.his.store.service.dto.care.request.HHisCareProjectActionDTO;
import com.hz.healthy.his.store.service.dto.care.request.items.ChineMedicineData;
import com.hz.healthy.his.store.service.dto.care.response.*;
import com.hz.healthy.his.store.service.dto.care.response.item.HisCareWesternMedicineDataItemDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.uac.model.LoginUser;

import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/2/17 14:19
 */
public interface IHHisCareAggregationService {

    /**
     * 暂存
     *
     * @param formReq
     * @param doctorType 中医还是西医
     * @param loginUser
     * @return 主键
     */
    public Long saveVisit(HHisCareFormReqDTO formReq, String doctorType, LoginUser loginUser);

    public ListResponse<HHisCareDTO> pageQueryList(HHisCareQueryModel queryModel);

    public List<HHisCareProjectDTO> pageQueryExecProjectList(HHisCareQueryModel queryModel);


    public HisCareWesternMedicineDataDTO pageQueryWesternMedicineList(HHisCareQueryModel queryModel);


    public List<ChineMedicineData> pageQueryChineMedicineList(HHisCareQueryModel queryModel);


    public void actionExecProject(HHisCareProjectActionDTO action,LoginUser loginUser);
    public void execSendAction(HHisCareQueryModel action,LoginUser loginUser);



    public List<HHisAdviceLogDTO> listActionExecLog(HHisAdviceLogQueryModel queryModel, LoginUser loginUser);

    /**
     * 结束就诊
     *
     * @param formReq
     * @param loginUser
     */
    public void finishVisit(HHisCareFormReqDTO formReq, LoginUser loginUser);


    public HHisCareFormReqDTO getById(Long id, String orgCode);


    public void deleteById(Long id);


    /**
     * 删除医嘱
     *
     * @param id
     * @param loginUser
     */
    public void removeAdvice(Long id, LoginUser loginUser);



    public HHisCareFormReqDTO getFirstByPatientId(Long patientId, String izInhospital, String orgCode);


    public ListResponse<HHisCareDTO> list(HHisCareQueryModel queryModel);

    public HHisCareDetailDTO getCareItem(HHisCareQueryModel queryModel);
}
