package com.hz.healthy.his.store.service.dto.scheduling.response;

import com.hz.healthy.his.store.service.dto.scheduling.request.item.HHisSchedulingPlanFormItem;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class HHisSchedulingPlanDTO implements Serializable {

    private Long id;
    private String name;
    private String type;

    private List<HHisSchedulingPlanFormItem> rows;

}
