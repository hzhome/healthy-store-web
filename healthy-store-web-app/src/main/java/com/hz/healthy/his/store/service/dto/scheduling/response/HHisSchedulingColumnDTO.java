package com.hz.healthy.his.store.service.dto.scheduling.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author hyhuang
 * @desc
 * @date 2023/2/2 20:43
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class HHisSchedulingColumnDTO implements Serializable {

    private String title;
    private Integer week;
    private String date;
}
