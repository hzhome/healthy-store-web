package com.hz.healthy.his.store.service.dto.care.response.item;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/12/15 19:36
 * @ClassName: HHisCareItemDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HHisCareItemDTO implements Serializable {


    /** 处方金额 */
    private BigDecimal amount;
    /** 处方原金额 */
    private BigDecimal originalAmount;

    /** 分类：0中药，1西药 2输注 3外治 */
    private String prescriptionType;

    private String desc;


    private List<HHisCareItemMedicineDTO> itemList;
}
