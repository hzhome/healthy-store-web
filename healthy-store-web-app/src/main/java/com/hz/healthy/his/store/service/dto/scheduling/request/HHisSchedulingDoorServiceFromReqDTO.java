package com.hz.healthy.his.store.service.dto.scheduling.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/19 17:35
 */
@Data
public class HHisSchedulingDoorServiceFromReqDTO implements Serializable {

    private Long planId;
    private Integer allowQty;
    private String rangeDateTime;
    private List<Long> projectId;

}
