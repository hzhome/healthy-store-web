package com.hz.healthy.his.store.service.dto.care.request;

import com.hz.healthy.his.store.service.dto.care.response.HHisCareProjectItemDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/12/12 19:27
 * @ClassName: HHisCareProjectDto
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisCareProjectActionDTO implements Serializable {

    private Long id;
    private int actionQty;
}
