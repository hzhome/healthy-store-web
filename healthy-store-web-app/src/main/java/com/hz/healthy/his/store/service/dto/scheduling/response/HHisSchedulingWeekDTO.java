package com.hz.healthy.his.store.service.dto.scheduling.response;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author hyhuang
 * @desc
 * @date 2023/2/1 11:12
 */
@Data
public class HHisSchedulingWeekDTO implements Serializable {

    private List<String> columns;
    private List<Map<Integer,String>> values;

}
