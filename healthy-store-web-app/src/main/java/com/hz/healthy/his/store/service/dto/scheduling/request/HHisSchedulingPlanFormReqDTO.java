package com.hz.healthy.his.store.service.dto.scheduling.request;

import com.hz.healthy.his.store.service.dto.scheduling.request.item.HHisSchedulingPlanFormItem;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/6/25 11:57
 */
@Data
public class HHisSchedulingPlanFormReqDTO implements Serializable {

    private Long id;
    @NotBlank(message = "请输入排班方案")
    private String name;
    @NotBlank(message = "请输入排班周期")
    private String type;

    private List<HHisSchedulingPlanFormItem> rows;
}
