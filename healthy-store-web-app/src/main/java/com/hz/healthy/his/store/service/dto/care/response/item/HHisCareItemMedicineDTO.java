package com.hz.healthy.his.store.service.dto.care.response.item;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/12/15 19:36
 * @ClassName: HHisCareItemDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HHisCareItemMedicineDTO implements Serializable {

    private Long id;

    /** 商品id，药品id */
    private Long medicineId;
    /** 药品名 */
    private String medicineName;

    /** 每次服用剂量 */
    private BigDecimal dose;

    /** 服用剂量单位 */
    private String unit;

    /** 销售单价 */
    private BigDecimal price;

    /**
     * 用法
     */
    private String usages;

    /**
     * 频次
     */
    private String frequency;

    /**
     * 总天数
     */
    private BigDecimal totalDay;

    /**
     * 总用量
     */
    private BigDecimal totalDose;

    /** 金额 */
    private BigDecimal amount;

    /** 特殊要求，备注 */
    private String tips;

    /** 排序 */
    private Integer sort;

    private String prescriptionCode;

    /** 换算单位 例如1盒拆分6粒  换算单位粒 最小包装单位 */
    private String conversionUnit;


}
