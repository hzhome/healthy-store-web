package com.hz.healthy.his.store.service.dto.scheduling.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/20 10:54
 */
@Data
public class HHisTimeConfigFromReqDTO implements Serializable {

    /** $comment */
    private Long id;
    /**
     * 号源名称
     */
    private String name;

    /**
     * 就诊分钟数
     */
    private int minute;

    /**
     * 时段(多少分钟为一个时段)
     */
    private int timeSpan;

    /**
     * 可预约人数
     */
    private int allowQty;

}
