package com.hz.healthy.his.store.service.dto.scheduling.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/19 17:35
 */
@Data
public class HHisSchedulingEditFromReqDTO implements Serializable {

    private Long id;
    private List<Integer> timeValueList;

    private String appointmentType;

}
