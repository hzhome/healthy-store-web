package com.hz.healthy.his.store.service.dto.care.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.healthy.his.store.service.dto.care.request.items.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/2/17 14:19
 */
@Data
public class HHisCareFormReqDTO implements Serializable {

    private Long id;
    /** 患者id */
    private Long patientId;

    /** 接诊类型：0初诊，1复诊，2急诊 */
    private String inquiriesType;

    /** 是否传染，0否，1是 */
    private Integer isContagious;

    /** 发病日期 */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date caseDate;

    /** 诊断编号 */
    @NotBlank(message = "诊断编号错误")
    private String caseCode;

    /** 主诉 */
    @NotBlank(message = "请输入主诉内容")
    private String caseTitle;

    /** 诊断信息 */
    @NotBlank(message = "请输入诊断信息")
    private String caseResult;

    /**过敏信息 */
    @NotBlank(message = "请输入过敏")
    private String allergyResult;

    /** 往史信息 */
    private String historyResult;

    /**
     * 望闻切诊(中医)
     */
    private String inspection;

    /**
     * 辨证
     */
    private String dialectic;

    /** 医生建议 */
    @NotBlank(message = "请输入诊断建议")
    private String doctorTips;

    /** 备注 */
    private String remark;


    private Integer height;
    private BigDecimal weight;
    private BigDecimal temperature;
    private BigDecimal pulse;
    private BigDecimal breathing;
    private BigDecimal bloodSugarValue;
    private BigDecimal systolicPressure;
    private BigDecimal diastolicPressure;

    /** 是否婚姻，0否，1是 */
    private String izMarriage;
    private String careCode;



    private boolean izEdit;


    /**
     * 执行状态 0待执行 1执行中 2执行完
     */
    private String  execStatus;
    /**
     * 西医 中医
     */
    private String careType;



    /**
     * 体格检查
     */
    private String checkUp;


    /**
     * 暂存
     * 结束
     */
    private String submitType;


    /**
     * 中药处方
     */
    private List<ChineMedicineDataSource> chineMedicineDataSource;


    /**
     * 西药处方
     */
    private List<MedicineDataSource> medicineDataSource;


    /**
     * 诊疗项目
     */
    private List<ProjectDataSource> projectDataSource;


    /**
     * 输注处方
     */
    private List<InfusionDataSource> infusionDataSource;


    /** 剂数，代表多少剂(中药) */
    private String attrType;
    /**剂数 表示共开出多少剂**/
    private Integer doseQty;

    /** 剂药用法(中药) */
    private String usages;

    /** 剂药服用频次(中药) */
    private String frequency;

    /** 每次用量(中药) */
    private String dose;

    /** 服用剂数 每日剂量  */
    private String dailyDose;



//    /**
//     * 外治处方
//     */
//    private List<MedicineData> historyModel;
}
