package com.hz.healthy.his.store.service.dto.scheduling.request.item;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hyhuang
 * @desc
 * @date 2023/6/25 11:57
 */
@Data
public class HHisSchedulingPlanFormItem implements Serializable {

    private int planId;
    private String planName;
    private String startTime;
    private String endTime;
    private int timeSpan;
    private int allowQty;
}
