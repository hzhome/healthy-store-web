package com.hz.healthy.his.store.service.dto.care.response;

import com.hz.healthy.his.store.service.dto.care.request.items.ProjectDataSource;
import com.hz.healthy.his.store.service.dto.care.response.item.HHisCareItemDTO;
import com.hz.healthy.his.store.service.dto.care.response.item.ProjectDataItemDTO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/28 15:19
 * @ClassName: HHisCareDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisCareDetailDTO implements Serializable {

    private Long id;
    private String careCode;

    /**
     * 中药处方
     */
    private List<HHisCareItemDTO> chineMedicineDataSource;


    /**
     * 西药处方
     */
    private List<HHisCareItemDTO> medicineDataSource;


    /**
     * 输注处方
     */
    private List<HHisCareItemDTO> infusionDataSource;


    /**
     * 诊疗项目
     */
    private List<ProjectDataItemDTO> projectDataSource;



    public HHisCareDetailDTO(){
        this.chineMedicineDataSource=new ArrayList<>();
        this.medicineDataSource=new ArrayList<>();
        this.infusionDataSource=new ArrayList<>();
        this.projectDataSource=new ArrayList<>();
    }

}
