package com.hz.healthy.his.store.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.hz.healthy.domain.doctor.*;
import com.hz.healthy.domain.doctor.model.*;
import com.hz.healthy.domain.his.*;
import com.hz.healthy.domain.his.model.*;
import com.hz.healthy.domain.patient.IHHospitalArchivesService;
import com.hz.healthy.domain.patient.model.HHospitalArchivesModel;
import com.hz.healthy.enums.MedicineSendStatusEnums;
import com.hz.healthy.his.store.service.IHHisCareAggregationService;
import com.hz.healthy.his.store.service.dto.care.request.HHisCareFormReqDTO;
import com.hz.healthy.his.store.service.dto.care.request.HHisCareProjectActionDTO;
import com.hz.healthy.his.store.service.dto.care.request.items.*;
import com.hz.healthy.his.store.service.dto.care.response.*;
import com.hz.healthy.his.store.service.dto.care.response.item.HHisCareItemDTO;
import com.hz.healthy.his.store.service.dto.care.response.item.HHisCareItemMedicineDTO;
import com.hz.healthy.his.store.service.dto.care.response.item.HisCareWesternMedicineDataItemDTO;
import com.hz.healthy.his.store.service.dto.care.response.item.ProjectDataItemDTO;
import com.hz.healthy.utils.BigDecimalUtils;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.dto.OrderItem;
import com.hz.ocean.common.enums.SexEnum;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import com.hz.ocean.uac.model.LoginUser;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author hyhuang
 * @desc
 * @date 2023/2/17 14:19
 */
@Service
public class IHHisCareAggregationServiceImpl implements IHHisCareAggregationService {

    @Autowired
    private IHHisCareService hHisCareService;


    @Autowired
    private IHHisCarePrescriptionService hisCarePrescriptionService;

    @Autowired
    private IHHisCareProjectService hisCareProjectService;

    @Autowired
    private IHHisAdviceLogService hisAdviceLogService;

    @Autowired
    private IHHisCarePrescriptionDetailService hisCarePrescriptionDetailService;


    @Autowired
    private IHHospitalArchivesService hospitalArchivesService;

    @Autowired
    private IHHisAdviceService hisAdviceService;
    @Autowired
    private IHisMedicineService hisMedicineService;

    @Autowired
    private IHHisProjectService ihHisProjectService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;

    @Autowired
    private IHEmployeeExtService hEmployeeExtService;

    @Override
    public ListResponse<HHisCareDTO> pageQueryList(HHisCareQueryModel queryModel) {
        List<HHisCareDTO> dataList = new ArrayList<>();
        queryModel.setOrderBy(Lists.newArrayList(new OrderItem().withField("id").withOrder(OrderItem.ASC)));
        ListResponse<HHisCareModel> pageList = hHisCareService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            pageList.getRecords().forEach(p -> {
                HHisCareDTO dto = BeanUtil.toBean(p, HHisCareDTO.class);
                if (p.getExecDesc().length() > 8) {
                    dto.setExecDesc(p.getExecDesc().substring(0, 7) + "...");
                }
                dto.setSendStatusText(MedicineSendStatusEnums.getEnumToDesc(dto.getSendStatus()));
                dto.setCreateTime(DateUtil.format(p.getCreateTime(), "MM-dd HH:mm"));
                dto.setPatientDesc(p.getPatientName() + "    " + p.getSex() + "    " + p.getAge() + "岁" + "     " + p.getPatientPhone());
                dataList.add(dto);
            });
        }
        return ListResponse.build(dataList, pageList);
    }

    @Override
    public List<HHisCareProjectDTO> pageQueryExecProjectList(HHisCareQueryModel queryModel) {
        List<HHisCareProjectDTO> dataList = new ArrayList<>();
        List<HHisCareProjectModel> dbList = hisCareProjectService.selectList(HHisCareProjectQueryModel.builder()
                .careCode(queryModel.getCareCode())
                .build());
        if (CollectionUtils.isNotEmpty(dbList)) {
            dbList.forEach(d -> {
                HHisCareProjectDTO p = new HHisCareProjectDTO();
                p.setItemList(Lists.newArrayList(BeanUtil.toBean(d, HHisCareProjectItemDTO.class)));
                dataList.add(p);
            });
        }
        return dataList;
    }

    @Override
    public HisCareWesternMedicineDataDTO pageQueryWesternMedicineList(HHisCareQueryModel queryModel) {
        List<HHisCarePrescriptionModel> prescriptList = hisCarePrescriptionService.selectList(HHisCarePrescriptionQueryModel.builder()
                .prescriptionType("1")
                .careCode(queryModel.getCareCode())
                .build());

        if (CollectionUtils.isNotEmpty(prescriptList)) {
            List<HisCareWesternMedicineDataItemDTO> dataList = new ArrayList<>();
            HHisCarePrescriptionModel prescriptionModel = prescriptList.get(0);
            HisCareWesternMedicineDataDTO dto = BeanUtil.toBean(prescriptionModel, HisCareWesternMedicineDataDTO.class);
            for (HHisCarePrescriptionModel prescription : prescriptList) {
                List<HHisCarePrescriptionDetailModel> detailList = hisCarePrescriptionDetailService.selectList(HHisCarePrescriptionDetailQueryModel.builder()
                        .careCode(queryModel.getCareCode())
                        .prescriptionCode(prescription.getPrescriptionCode())
                        .build());
                for (HHisCarePrescriptionDetailModel d : detailList) {
                    HisCareWesternMedicineDataItemDTO item = BeanUtil.toBean(d, HisCareWesternMedicineDataItemDTO.class);
                    item.setWaitSendDose(BigDecimal.ZERO);
                    if (CommonConstant.no_0.equals(prescriptionModel.getSendStatus())) {
                        item.setWaitSendDose(item.getTotalDose());
                        item.setSendDose(BigDecimal.ZERO);
                    }
                    dataList.add(item);
                }
            }
            dto.setItemList(dataList);
            return dto;
        }

        return new HisCareWesternMedicineDataDTO();
    }


    @Override
    public List<ChineMedicineData> pageQueryChineMedicineList(HHisCareQueryModel queryModel) {
        List<MedicineDataSource> dataList = new ArrayList<>();
        List<HHisCarePrescriptionModel> prescriptList = hisCarePrescriptionService.selectList(HHisCarePrescriptionQueryModel.builder()
                .prescriptionType("1")
                .careCode(queryModel.getCareCode())
                .build());

        if (CollectionUtils.isNotEmpty(prescriptList)) {
            for (HHisCarePrescriptionModel prescription : prescriptList) {
                List<HHisCarePrescriptionDetailModel> detailList = hisCarePrescriptionDetailService.selectList(HHisCarePrescriptionDetailQueryModel.builder()
                        .careCode(queryModel.getCareCode())
                        .prescriptionCode(prescription.getPrescriptionCode())
                        .build());
                dataList.addAll(BeanUtil.copyToList(detailList, MedicineDataSource.class));
            }
        }
        return null;
    }

    @Transactional

    @Override
    public void actionExecProject(HHisCareProjectActionDTO action, LoginUser loginUser) {
        HHisCareProjectModel db = hisCareProjectService.getById(action.getId());
        if (ObjectUtil.isNull(db)) {
            throw new BizException("记录不存在");
        }
        int totalQty = db.getFinishQty() + action.getActionQty();
        if (totalQty > db.getTotalQty()) {
            throw new BizException("执行记录已超出总执行记录，请重新核实");
        }
        HHospitalArchivesModel archivesModel = hospitalArchivesService.getByPatientId(db.getPatientId());
        if (ObjectUtil.isNull(archivesModel)) {
            throw new BizException("执行患者信息不存在");
        }
        Date now = new Date();
        hisCareProjectService.updateById(HHisCareProjectModel.builder()
                .id(db.getId())
                .finishQty(db.getFinishQty() + action.getActionQty())
                .execStatus(totalQty == db.getTotalQty() ? CommonConstant.STATUS_1 : CommonConstant.STATUS_0)
                .build());

        hisAdviceLogService.insert(HHisAdviceLogModel.builder()
                .displayName(db.getProjectName())
                .execTime(now)
                .execName(loginUser.getRealname())
                .execId(loginUser.getEyId())
                .createTime(now)
                .relationNurseOrder(db.getCareCode())
                .orderSn(db.getCareCode())
                .orgCode(db.getOrgCode())
                .tenantId(db.getTenantId())
                .longOnceFlag(CommonConstant.STATUS_0)
                .patientId(db.getPatientId())
                .patientName(archivesModel.getRealName())
                .orderType("5")
                .execDay(DateUtil.formatDate(now))
                .startTime(now)
                .build());
    }

    @Override
    public List<HHisAdviceLogDTO> listActionExecLog(HHisAdviceLogQueryModel queryModel, LoginUser loginUser) {
        List<HHisAdviceLogDTO> dataList = new ArrayList<>();
        List<HHisAdviceLogModel> dbList = hisAdviceLogService.selectList(queryModel);
        if (CollectionUtils.isNotEmpty(dbList)) {
            dbList.forEach(p -> {
                HHisAdviceLogDTO dto = BeanUtil.toBean(p, HHisAdviceLogDTO.class);
                dto.setExecTime(DateUtil.formatDateTime(p.getExecTime()));
                dataList.add(dto);
            });
        }
        return dataList;
    }

    @Transactional
    @Override
    public void execSendAction(HHisCareQueryModel action, LoginUser loginUser) {
        HHisCareModel careModel = hHisCareService.getById(action.getId());
        if (ObjectUtil.isNull(careModel)) {
            throw new BizException("就诊记录不存在");
        }
        List<HHisCarePrescriptionModel> prescriptList = hisCarePrescriptionService.selectList(HHisCarePrescriptionQueryModel.builder()
                .prescriptionTypeList(Lists.newArrayList("0", "1"))
                .careCode(careModel.getCareCode())
                .build());
        if (CollectionUtils.isNotEmpty(prescriptList)) {
            Date now = new Date();
            for (HHisCarePrescriptionModel p : prescriptList) {
                List<HHisCarePrescriptionDetailModel> detailList = hisCarePrescriptionDetailService.selectList(HHisCarePrescriptionDetailQueryModel.builder()
                        .careCode(p.getCareCode())
                        .prescriptionCode(p.getPrescriptionCode())
                        .build());
                if (CollectionUtils.isNotEmpty(detailList)) {
                    for (HHisCarePrescriptionDetailModel d : detailList) {
                        hisCarePrescriptionDetailService.updateById(HHisCarePrescriptionDetailModel.builder()
                                .id(d.getId())
                                .sendDose(d.getTotalDose())
                                .build());
                    }
                }
                hisCarePrescriptionService.updateById(HHisCarePrescriptionModel.builder()
                        .id(p.getId())
                        .sendStatus(CommonConstant.yes_1)
                        .updateTime(now)
                        .updateUser(loginUser.getRealname())
                        .build());
            }
            hHisCareService.updateById(HHisCareModel.builder()
                    .id(careModel.getId())
                    .sendStatus(CommonConstant.yes_1)
                    .build());
            hisAdviceLogService.insert(HHisAdviceLogModel.builder()
                    .displayName("发药")
                    .execTime(now)
                    .execName(loginUser.getRealname())
                    .execId(loginUser.getEyId())
                    .createTime(now)
                    .relationNurseOrder(careModel.getCareCode())
                    .orderSn(careModel.getCareCode())
                    .orgCode(careModel.getOrgCode())
                    .tenantId(careModel.getTenantId())
                    .longOnceFlag(CommonConstant.STATUS_0)
                    .patientId(careModel.getPatientId())
                    .patientName(careModel.getPatientName())
                    .orderType("6")
                    .execDay(DateUtil.formatDate(now))
                    .startTime(now)
                    .build());
        }
    }

    private void check(HHisCareFormReqDTO formReq, HHisCareModel historyModel, LoginUser loginUser) {
        HHisCareFormatModel formatModel = hEmployeeExtService.getCareFormat(loginUser.getEyId());
        historyModel.setCareType(formatModel.getFormat());
        if (StringUtils.isAnyBlank(historyModel.getCaseTitle(), historyModel.getCaseResult())) {
            throw new BizException("主诉/诊断信息 不能为空");
        }
        if (formatModel.getCareLook().isChecked() && StringUtils.isBlank(historyModel.getInspection())) {
            throw new BizException("请完善望闻切诊");
        }

        if (formatModel.getCareLook().isChecked() && StringUtils.isBlank(historyModel.getDialectic())) {
            throw new BizException("请完善望辨证");
        }

        if (formatModel.getCareCheckUp().isChecked() && StringUtils.isBlank(historyModel.getCheckUp())) {
            historyModel.setCheckUp("未见明显异常");
        }
        if (formatModel.getCareAllergy().isChecked() && StringUtils.isBlank(historyModel.getAllergyResult())) {
            historyModel.setAllergyResult("否认药物过敏史");
        }
    }

    @Override
    public Long saveVisit(HHisCareFormReqDTO formReq, String doctorType, LoginUser loginUser) {
        if (ObjectUtil.isNull(formReq.getPatientId())) {
            throw new BizException("请选择对应患者信息");
        }
        HHospitalArchivesModel patientModel = hospitalArchivesService.getByPatientId(formReq.getPatientId());
        if (patientModel == null) {
            throw new BizException("患者信息不存在");
        }
        HHisCareModel historyModel = BeanUtil.toBean(formReq, HHisCareModel.class);
        check(formReq, historyModel, loginUser);
        boolean isInsert = true;
        String careCode = seqNoAutoService.buildBatchNo();
        if (ObjectUtil.isNotNull(formReq.getId()) && formReq.getId() > 0) {
            HHisCareModel careModel = hHisCareService.getById(formReq.getId());
            if (ObjectUtil.isNull(careModel)) {
                throw new BizException("患者暂存诊断记录不存在,请确认！");
            }
            careCode = careModel.getCareCode();
            hisCarePrescriptionService.deleteByCaseCode(careCode);
            isInsert = false;
        }
        historyModel.setStatus(CommonConstant.STATUS_0.equals(formReq.getSubmitType()) ? CommonConstant.STATUS_0 : CommonConstant.STATUS_1);
        historyModel.setPatientId(patientModel.getPatientId());
        historyModel.setPatientName(patientModel.getRealName());
        historyModel.setPatientPhone(patientModel.getMobilePhone());
        historyModel.setSex(SexEnum.getEnumToDesc(patientModel.getSex()));
        historyModel.setAge(DateUtil.ageOfNow(patientModel.getBirthday()));

        historyModel.setAppointmentType("2");


        //0中药，1西药 2输注 3外治
        Date now = new Date();
        handleInfusionData(formReq, careCode, loginUser, historyModel);
        handleProjectData(formReq, careCode, loginUser, historyModel);
        handleMedicineData(formReq, careCode, loginUser, historyModel);
        handleChineMedicineData(formReq, careCode, loginUser, historyModel);
        Long id = formReq.getId();
        if (!isInsert) {
            historyModel.setUpdateTime(new Date());
            historyModel.setUpdateUser(loginUser.getUsername());
            hHisCareService.updateById(historyModel);
        } else {
            historyModel.setCareCode(careCode);
            historyModel.setDoctorId(loginUser.getEyId());
            historyModel.setDoctorName(loginUser.getUsername());
            historyModel.setCreateTime(now);
            historyModel.setCreateUser(loginUser.getUsername());
            historyModel.setOrgCode(loginUser.getOrgCode());
            historyModel.setTenantId(loginUser.getTenantId());
            historyModel.setIzInHospital(CommonConstant.no_0);
            historyModel.setCareType(doctorType);
            hHisCareService.insert(historyModel);
            id = historyModel.getId();
        }
        return id;
    }


    /**
     * 处理中药数据
     */
    private void handleChineMedicineData(HHisCareFormReqDTO formReq, String careCode, LoginUser loginUser, HHisCareModel historyModel) {
        if (CollectionUtils.isNotEmpty(formReq.getChineMedicineDataSource())) {
            List<ChineMedicineDataSource> chineMedicineDataSource = formReq.getChineMedicineDataSource();
            for (ChineMedicineDataSource chineMedicineDataForm : chineMedicineDataSource) {
                if (StringUtils.isAnyBlank(chineMedicineDataForm.getAttrType(), chineMedicineDataForm.getUsages(),
                        chineMedicineDataForm.getFrequency())) {
                    throw new BizException("中药类型/剂药用法/服用频次 不能为空");
                }
                if (ObjectUtil.isNull(chineMedicineDataForm.getDoseQty())) {
                    throw new BizException("请确定中药剂数");
                }
                if (ObjectUtil.isNull(chineMedicineDataForm.getDose())) {
                    throw new BizException("请确定中药每次服用量");
                }
                if (ObjectUtil.isNull(chineMedicineDataForm.getDailyDose())) {
                    throw new BizException("请确定中药每日服用剂量");
                }
                Date now = new Date();
                String prescriptionType = "0";
                String prescriptionCode = "CM" + System.currentTimeMillis();
                BigDecimal originalAmount = BigDecimal.ZERO;
                BigDecimal qty = BigDecimal.ZERO;
                ChineseMedicinePrescription chineMedicineData = ChineseMedicinePrescription.builder()
                        .doseQty(chineMedicineDataForm.getDoseQty())
                        .usages(chineMedicineDataForm.getUsages())
                        .dailyDose(chineMedicineDataForm.getDailyDose())
                        .frequency(chineMedicineDataForm.getFrequency())
                        .attrType(chineMedicineDataForm.getAttrType())
                        .dose(chineMedicineDataForm.getDose())
                        .build();

                List<HHisCarePrescriptionDetailModel> chineMedicineDataList = new ArrayList<>();

                List<Long> medicineIds = chineMedicineDataForm.getItemList().stream().map(ChineMedicineData::getMedicineId).collect(Collectors.toList());
                Map<Long, HisMedicineSimpleModel> medicineMap = hisMedicineService.mapByIds(medicineIds);
                for (ChineMedicineData formChineMedicine : chineMedicineDataForm.getItemList()) {
                    HisMedicineSimpleModel medicineSimpleModel = medicineMap.get(formChineMedicine.getMedicineId());
                    BigDecimal amount = NumberUtil.add(originalAmount, NumberUtil.mul(medicineSimpleModel.getPrice(), formChineMedicine.getDose()));
                    originalAmount = BigDecimalUtils.add(amount, originalAmount);
                    chineMedicineDataList.add(HHisCarePrescriptionDetailModel.builder()
                            .amount(amount)
                            .prescriptionType(prescriptionType)
                            .prescriptionCode(prescriptionCode)
                            .tenantId(loginUser.getTenantId())
                            .orgCode(loginUser.getOrgCode())
                            .careCode(careCode)
                            .medicineId(medicineSimpleModel.getId())
                            .medicineName(medicineSimpleModel.getMedicineName())
                            .conversionUnit(medicineSimpleModel.getConversionUnit())
                            .dose(formChineMedicine.getDose())
                            .totalDose(formChineMedicine.getDose())
                            .price(medicineSimpleModel.getSalePrice())
                            .unit(formChineMedicine.getUnit())
                            .build());
                    qty = BigDecimalUtils.add(qty, new BigDecimal("1"));
                }

                hisCarePrescriptionService.insert(HHisCarePrescriptionModel.builder()
                        .careCode(careCode)
                        .createTime(now)
                        .amount(originalAmount)
                        .originalAmount(originalAmount)
                        .qty(qty)
                        .useTips(JSON.toJSONString(chineMedicineData))
                        .prescriptionType(prescriptionType)
                        .orgCode(loginUser.getOrgCode())
                        .prescriptionCode(prescriptionCode)
                        .tenantId(loginUser.getTenantId())
                        .build());

                hisCarePrescriptionDetailService.insertBatch(chineMedicineDataList);
            }

        }
    }


    /**
     * 处理西药数据
     */
    private void handleMedicineData(HHisCareFormReqDTO formReq, String careCode, LoginUser loginUser, HHisCareModel historyModel) {
        Date now = new Date();
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal qty = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(formReq.getMedicineDataSource())) {
            String prescriptionType = "1";
            String prescriptionCode = "M" + System.currentTimeMillis();

            List<Long> medicineIds = formReq.getMedicineDataSource().stream().map(MedicineDataSource::getMedicineId).collect(Collectors.toList());
            Map<Long, HisMedicineSimpleModel> medicineMap = hisMedicineService.mapByIds(medicineIds);
            List<HHisCarePrescriptionDetailModel> medicineDataList = new ArrayList<>();

            for (MedicineDataSource formMedicine : formReq.getMedicineDataSource()) {
                if (StringUtils.isAllBlank(formMedicine.getUnit(), formMedicine.getConversionUnit())) {
                    throw new BizException("请完善【" + formMedicine.getMedicineName() + "】药品信息");
                }
                if (ObjectUtil.isNull(formMedicine.getDose())) {
                    throw new BizException("请完善【" + formMedicine.getMedicineName() + "】药品信息");
                }
                if (ObjectUtil.isNull(formMedicine.getTotalDose())) {
                    throw new BizException("请完善【" + formMedicine.getMedicineName() + "】药品信息");
                }
                HisMedicineSimpleModel medicineSimpleModel = medicineMap.get(formMedicine.getMedicineId());
                BigDecimal price = BigDecimal.ZERO;
                //包装单位一致
                if (formMedicine.getUnit().equals(medicineSimpleModel.getUnit())) {
                    price = medicineSimpleModel.getPrice();
                } else {
                    price = medicineSimpleModel.getSalePrice();
                }
                qty = NumberUtil.add(qty, 1);
                BigDecimal amount = NumberUtil.mul(formMedicine.getTotalDose(), price);
                totalAmount = NumberUtil.add(totalAmount, amount);
                medicineDataList.add(HHisCarePrescriptionDetailModel.builder()
                        .amount(amount)
                        .price(price)
                        .frequency(formMedicine.getFrequency())
                        .usages(formMedicine.getUsages())
                        .dose(formMedicine.getDose())
                        .totalDay(formMedicine.getDay())
                        .totalDose(formMedicine.getTotalDose())
                        .unit(formMedicine.getUnit())
                        .conversionUnit(formMedicine.getConversionUnit())
                        .medicineId(medicineSimpleModel.getId())
                        .medicineName(medicineSimpleModel.getMedicineName())
                        .prescriptionType(prescriptionType)
                        .prescriptionCode(prescriptionCode)
                        .careCode(careCode)
                        .orgCode(loginUser.getOrgCode())
                        .tenantId(loginUser.getTenantId())
                        .build());
            }
            hisCarePrescriptionDetailService.insertBatch(medicineDataList);

            hisCarePrescriptionService.insert(HHisCarePrescriptionModel.builder()
                    .careCode(careCode)
                    .createTime(now)
                    .qty(qty)
                    .amount(totalAmount)
                    .originalAmount(totalAmount)
                    .prescriptionType(prescriptionType)
                    .orgCode(loginUser.getOrgCode())
                    .prescriptionCode(prescriptionCode)
                    .tenantId(loginUser.getTenantId())
                    .build());
        }
    }


    /**
     * 处理输注数据
     */
    private void handleInfusionData(HHisCareFormReqDTO formReq, String careCode, LoginUser loginUser, HHisCareModel historyModel) {
        if (CollectionUtils.isNotEmpty(formReq.getInfusionDataSource())) {
            List<InfusionDataSource> infusionDataSource = formReq.getInfusionDataSource();
            for (InfusionDataSource infusionDataForm : infusionDataSource) {

                if (ObjectUtil.isNull(infusionDataForm.getDay())) {
                    throw new BizException("请完善输注天数");
                }
                if (ObjectUtil.isNull(infusionDataForm.getDose())) {
                    throw new BizException("请完善输注用量");
                }
                Date now = new Date();
                String prescriptionType = "2";
                String prescriptionCode = "I" + System.currentTimeMillis();
                BigDecimal originalAmount = BigDecimal.ZERO;
                BigDecimal qty = BigDecimal.ZERO;
                InfusionMedicinePrescription chineMedicineData = InfusionMedicinePrescription.builder()
                        .usages(infusionDataForm.getUsages())
                        .frequency(infusionDataForm.getFrequency())
                        .day(infusionDataForm.getDay())
                        .dose(infusionDataForm.getDose())
                        .build();

                List<HHisCarePrescriptionDetailModel> chineMedicineDataList = new ArrayList<>();

                List<Long> medicineIds = infusionDataForm.getItemList().stream().map(MedicineDataSource::getMedicineId).collect(Collectors.toList());
                Map<Long, HisMedicineSimpleModel> medicineMap = hisMedicineService.mapByIds(medicineIds);
                for (MedicineDataSource formChineMedicine : infusionDataForm.getItemList()) {
                    HisMedicineSimpleModel medicineSimpleModel = medicineMap.get(formChineMedicine.getMedicineId());
                    BigDecimal amount = NumberUtil.add(originalAmount, NumberUtil.mul(medicineSimpleModel.getPrice(), formChineMedicine.getDose()));
                    originalAmount = BigDecimalUtils.add(amount, originalAmount);
                    chineMedicineDataList.add(HHisCarePrescriptionDetailModel.builder()
                            .amount(amount)
                            .prescriptionType(prescriptionType)
                            .prescriptionCode(prescriptionCode)
                            .tenantId(loginUser.getTenantId())
                            .orgCode(loginUser.getOrgCode())
                            .careCode(careCode)
                            .medicineId(medicineSimpleModel.getId())
                            .medicineName(medicineSimpleModel.getMedicineName())
                            .conversionUnit(medicineSimpleModel.getConversionUnit())
                            .dose(formChineMedicine.getDose())
                            .price(medicineSimpleModel.getSalePrice())
                            .totalDose(formChineMedicine.getTotalDose())
                            .unit(formChineMedicine.getUnit())
                            .build());
                    qty = BigDecimalUtils.add(qty, new BigDecimal("1"));
                }

                hisCarePrescriptionService.insert(HHisCarePrescriptionModel.builder()
                        .careCode(careCode)
                        .createTime(now)
                        .amount(originalAmount)
                        .originalAmount(originalAmount)
                        .qty(qty)
                        .useTips(JSON.toJSONString(chineMedicineData))
                        .prescriptionType(prescriptionType)
                        .orgCode(loginUser.getOrgCode())
                        .prescriptionCode(prescriptionCode)
                        .tenantId(loginUser.getTenantId())
                        .build());

                hisCarePrescriptionDetailService.insertBatch(chineMedicineDataList);
            }

            historyModel.setExecDesc("输注处方");
        }
    }


    /**
     * 处理诊疗项目
     */
    private void handleProjectData(HHisCareFormReqDTO formReq, String careCode, LoginUser loginUser, HHisCareModel historyModel) {
        Date now = new Date();
        String execDesc = StringUtils.defaultIfBlank(historyModel.getExecDesc(), "");
        if (CollectionUtils.isNotEmpty(formReq.getProjectDataSource())) {
            List<String> projectCodes = formReq.getProjectDataSource().stream().map(ProjectDataSource::getProjectCode).collect(Collectors.toList());
            Map<String, HHisProjectModel> medicineMap = ihHisProjectService.map(projectCodes);
            List<HHisCareProjectModel> medicineDataList = new ArrayList<>();
            for (ProjectDataSource medicine : formReq.getProjectDataSource()) {
                HHisProjectModel projectModel = medicineMap.get(medicine.getProjectCode());
                BigDecimal totalAmount = projectModel.getSaleAmount();
                //项目分类 1 治疗理疗 2检验 3检查
                if ("1".equals(medicine.getCategoryCode())) {
                    if (ObjectUtil.isNull(medicine.getQty()) || ObjectUtil.isNull(medicine.getDay())) {
                        throw new BizException("诊疗项目【" + medicine.getProjectName() + "】请完善");
                    }
                }
                int day = StringUtils.intValue(medicine.getDay(), 1);
                int total = medicine.getQty() * day;
                totalAmount = BigDecimalUtils.mul(totalAmount, total);
                medicineDataList.add(HHisCareProjectModel.builder()
                        .amount(totalAmount)
                        .careCode(careCode)
                        .categoryCode(projectModel.getCategoryCode())
                        .projectType(projectModel.getProjectType())
                        .projectCode(projectModel.getProjectCode())
                        .projectName(projectModel.getProjectName())
                        .unit(projectModel.getUnit())
                        .izMinus(projectModel.getIzMinus())
                        .medicalInsuranceCode(projectModel.getMedicalInsuranceCode())
                        .day(day)
                        .qty(medicine.getQty())
                        .remark(medicine.getRemark())
                        .doctorId(loginUser.getEyId())
                        .patientId(formReq.getPatientId())
                        .orgCode(loginUser.getOrgCode())
                        .tenantId(loginUser.getTenantId())
                        .createTime(now)
                        .build());
                execDesc += projectModel.getProjectName() + ",";
            }
            hisCareProjectService.insertBatch(medicineDataList);
            if (StringUtils.isNotBlank(execDesc)) {
                historyModel.setExecDesc(execDesc.substring(0, execDesc.length() - 1));
            }
            historyModel.setIzNeedExec(CommonConstant.STATUS_1);
        }
    }

    @Override
    public void finishVisit(HHisCareFormReqDTO formReq, LoginUser loginUser) {
//        Long id = formReq.getId();
//        if(ObjectUtil.isNull(id)){
//           throw new BizException("请先保存就诊，然后再确认完结");
//        }
//        HHisCareHistoryModel careHistory = hHisCareHistoryService.getOne(HHisCareHistoryQueryModel.builder()
//                .id(id)
//                .orgCode(loginUser.getOrgCode())
//                .build());
//        if (ObjectUtil.isNull(careHistory)) {
//            throw new BizException("就诊记录不存在,请核实!");
//        }
//        //判断超24小时不能编辑
//
//        //处方信息
//        BigDecimal careAmount = finishCare(formReq, loginUser);
//        //缴费信息
//        //finishPkg(formReq, loginUser, careAmount);
//        //病历档案完结
//        HHisCareHistoryModel historyModel = BeanUtil.copyProperties(formReq, HHisCareHistoryModel.class);
//        historyModel.setStatus("1");//完结
//        historyModel.setUpdateTime(new Date());
//        historyModel.setUpdateUser(loginUser.getUsername());
//        hHisCareHistoryService.updateById(historyModel);
    }


    /**
     * 医嘱跟项目走
     *
     * @param formReq
     * @param loginUser
     */
    private void finishAdvice(HHisCareFormReqDTO formReq, LoginUser loginUser) {
        HHisAdviceModel hisAdviceModel = new HHisAdviceModel();
        hisAdviceModel.setPatientId(formReq.getPatientId());
        hisAdviceModel.setDoctorId(loginUser.getEyId());
        hisAdviceModel.setOrgCode(loginUser.getOrgCode());
        hisAdviceModel.setDisplayName("用药");
        hisAdviceModel.setStartTime(new Date());
        hisAdviceModel.setStopTime(new Date());
        hisAdviceModel.setOrderSn("");
        hisAdviceModel.setInputType("d");//医生录入
        hisAdviceModel.setLongOnceFlag(CommonConstant.no_0);//0临时,1长期
        hisAdviceModel.setOrderType("3");//1 检查检验 2 手术治疗 3 药品 4 饮食 5 供应材料 6 护理及管理
        hisAdviceModel.setOrderStatus("0");//0 医生录入 1 医生确认 2 护士接收确认 3 首次执行 4 执行 5 停止 6 撤销
        hisAdviceModel.setCreateTime(new Date());
        hisAdviceModel.setPaySelf("1");//审批标志 0无需审批 1 需审批
        hisAdviceService.insert(hisAdviceModel);
    }


    @Override
    public HHisCareFormReqDTO getById(Long id, String orgCode) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void removeAdvice(Long id, LoginUser loginUser) {

    }


    @Override
    public HHisCareFormReqDTO getFirstByPatientId(Long patientId, String izInhospital, String orgCode) {
        return null;
    }

    @Override
    public ListResponse<HHisCareDTO> list(HHisCareQueryModel queryModel) {
        List<HHisCareDTO> dataList = new ArrayList<>();
        ListResponse<HHisCareModel> pageList = hHisCareService.pageQuery(queryModel);
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            pageList.getRecords().forEach(p -> {

                HHisCareDTO c = BeanUtil.toBean(p, HHisCareDTO.class);
                c.setCreateTime(DateUtil.formatDateTime(p.getCreateTime()));
                c.setCaseDate(DateUtil.formatDate(p.getCreateTime()));

                if (c.getCaseResult().length() > 6) {
                    c.setTitle(c.getCaseResult().substring(0, 6) + "...");
                } else {
                    c.setTitle(c.getCaseResult());
                }

                c.setInfusionDataSource(new ArrayList<>());
                c.setMedicineDataSource(new ArrayList<>());
                c.setChineMedicineDataSource(new ArrayList<>());
                c.setProjectDataSource(new ArrayList<>());
                dataList.add(c);
            });
        }
        return ListResponse.build(dataList, pageList);
    }

    @Override
    public HHisCareDetailDTO getCareItem(HHisCareQueryModel queryModel) {
        HHisCareDetailDTO dto = new HHisCareDetailDTO();
        String careCode = queryModel.getCareCode();
        List<HHisCarePrescriptionModel> prescriptList = hisCarePrescriptionService.selectList(HHisCarePrescriptionQueryModel.builder()
                .careCode(careCode)
                .build());
        if (CollectionUtils.isNotEmpty(prescriptList)) {
            Map<String, List<HHisCarePrescriptionModel>> collect = prescriptList.stream().collect(Collectors.groupingBy(HHisCarePrescriptionModel::getPrescriptionType));

            for (Map.Entry<String, List<HHisCarePrescriptionModel>> entry : collect.entrySet()) {
                String prescriptionType = entry.getKey();
                List<HHisCareItemDTO> list = new ArrayList<>();
                List<HHisCarePrescriptionModel> dbList = entry.getValue();
                for (HHisCarePrescriptionModel prescription : dbList) {
                    HHisCareItemDTO medicineDataSource = HHisCareItemDTO.builder()
                            .desc("")
                            .amount(prescription.getAmount())
                            .originalAmount(prescription.getOriginalAmount())
                            .prescriptionType(prescription.getPrescriptionType())
                            .build();
                    List<HHisCarePrescriptionDetailModel> detailList = hisCarePrescriptionDetailService.selectList(HHisCarePrescriptionDetailQueryModel.builder()
                            .careCode(careCode)
                            .prescriptionCode(prescription.getPrescriptionCode())
                            .build());
                    medicineDataSource.setItemList(BeanUtil.copyToList(detailList, HHisCareItemMedicineDTO.class));
                    list.add(medicineDataSource);
                }

                //：0中药，1西药 2输注 3外治
                if ("0".equals(prescriptionType)) {
                    dto.setChineMedicineDataSource(list);
                } else if ("1".equals(prescriptionType)) {
                    dto.setMedicineDataSource(list);
                } else if ("2".equals(prescriptionType)) {
                    dto.setInfusionDataSource(list);
                }
            }
        }

        List<HHisCareProjectModel> projectList = hisCareProjectService.selectList(HHisCareProjectQueryModel.builder()
                .careCode(careCode)
                .build());
        if (CollectionUtils.isNotEmpty(projectList)) {
            ProjectDataItemDTO project = new ProjectDataItemDTO();
            BigDecimal amount = BigDecimal.ZERO;
            List<ProjectDataSource> itemList = new ArrayList<>();
            for (HHisCareProjectModel careProjectModel : projectList) {
                amount = BigDecimalUtils.add(careProjectModel.getAmount(), amount);
                itemList.add(BeanUtil.toBean(careProjectModel, ProjectDataSource.class));
            }
            project.setAmount(amount);
            project.setOriginalAmount(amount);
            project.setItemList(itemList);
            dto.getProjectDataSource().add(project);
        }
        return dto;
    }
}
