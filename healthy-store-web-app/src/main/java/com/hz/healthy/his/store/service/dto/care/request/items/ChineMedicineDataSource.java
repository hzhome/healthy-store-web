package com.hz.healthy.his.store.service.dto.care.request.items;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/11/26 15:13
 * @ClassName: ChineMedicineData
 * @Description: TODO
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChineMedicineDataSource implements Serializable {

    /** 剂数，代表多少剂(中药) */
    private String attrType;
    /**剂数 表示共开出多少剂**/
    private Integer doseQty;

    /** 剂药用法(中药) */
    private String usages;

    /** 剂药服用频次(中药) */
    private String frequency;

    /** 每次用量(中药) */
    private String dose;

    /** 服用剂数 每日剂量  */
    private String dailyDose;

    List<ChineMedicineData> itemList;
}
