package com.hz.healthy.his.store.service.dto.care.response;

import com.hz.healthy.his.store.service.dto.care.response.item.HisCareWesternMedicineDataItemDTO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/12/12 22:35
 * @ClassName: HisCareMedicineDataDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HisCareWesternMedicineDataDTO implements Serializable {


    /** 处方金额 */
    private BigDecimal amount;
    /** 处方原金额 */
    private BigDecimal originalAmount;
    /** 诊断编号 */
    private String careCode;
    /** 服药要求 */
    private String useTips;
    /** 备注 */
    private String memo;


    private BigDecimal qty;

    private List<HisCareWesternMedicineDataItemDTO> itemList;
}
