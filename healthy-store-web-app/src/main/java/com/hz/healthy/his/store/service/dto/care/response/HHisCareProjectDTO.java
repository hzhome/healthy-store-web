package com.hz.healthy.his.store.service.dto.care.response;

import com.hz.healthy.domain.his.model.HHisCareProjectModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author hyhuang
 * @Date 2024/12/12 19:27
 * @ClassName: HHisCareProjectDto
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisCareProjectDTO implements Serializable {

    List<HHisCareProjectItemDTO> itemList;
}
