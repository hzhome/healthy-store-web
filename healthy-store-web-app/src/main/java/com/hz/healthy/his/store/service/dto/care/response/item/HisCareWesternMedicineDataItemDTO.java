package com.hz.healthy.his.store.service.dto.care.response.item;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/12/12 22:35
 * @ClassName: HisCareMedicineDataDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HisCareWesternMedicineDataItemDTO implements Serializable {

    private Long id;
    /** 关联病历编码 */
    private String careCode;
    /** 医院组织编码,是唯一码 */
    private String orgCode;
    /** 分类：0中药，1西药 2输注 3外治 */
    private String prescriptionType;
    private Long tenantId;
    /** 商品id，药品id */
    private Long medicineId;
    /** 药品名 */
    private String medicineName;
    /**
     * 开始时间
     */
    private Date startTime;

    /** 每次服用剂量 */
    private BigDecimal dose;

    /** 服用剂量单位 */
    private String unit;

    /** 销售单价 */
    private BigDecimal price;

    /**
     * 用法
     */
    private String usages;

    /**
     * 频次
     */
    private String frequency;

    /**
     * 总天数
     */
    private BigDecimal totalDay;

    /**
     * 总用量
     */
    private BigDecimal totalDose;

    /** 金额 */
    private BigDecimal amount;

    /** 特殊要求，备注 */
    private String tips;

    /** 排序 */
    private Integer sort;

    private String prescriptionCode;

    /** 换算单位 例如1盒拆分6粒  换算单位粒 最小包装单位 */
    private String conversionUnit;


    /**
     * 已发数量
     */
    private BigDecimal sendDose;


    /**
     * 待发数量
     */
    private BigDecimal waitSendDose;
}
