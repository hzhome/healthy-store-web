package com.hz.healthy.his.store.service.dto.scheduling.request.item;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/19 17:46
 */
@Data
public class HHisSchedulingFromItem implements Serializable {

    private Long id;

    private String employeeName;
    /**
     * 1早上 2下午 3晚上
     */
    private List<Integer> timeValueIds;

    /**
     * 总预约数
     */
    private Integer allowQty;

    /**
     * 剩余预约数
     */
    private Integer residueQty;

    private String amount;

}
