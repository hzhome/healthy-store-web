package com.hz.healthy.his.store.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import com.hz.healthy.domain.doctor.*;
import com.hz.healthy.domain.doctor.model.*;
import com.hz.healthy.enums.SchedulingTypeEnums;
import com.hz.healthy.his.store.service.IHSchedulingAggregationService;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingDoorServiceFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingEditFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.item.HHisSchedulingFromItem;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingDTO;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingDescDTO;
import com.hz.healthy.store.serve.dto.HEmployeeSchedulingDTO;
import com.hz.ocean.common.constant.CommonConstant;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.model.KeyLongModel;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.DateUtils;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.service.auth.ISysDictService;
import com.hz.ocean.system.service.common.ISeqNoAutoService;
import com.hz.ocean.uac.model.LoginUser;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/18 18:06
 */
@Service
public class IHSchedulingAggregationServiceImpl implements IHSchedulingAggregationService {

    private static final String[] weekStr = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
    @Autowired
    private IHHisSchedulingService hHisSchedulingService;

    @Autowired
    private IHHisSchedulingSubsectionService hisSchedulingSubsectionService;

    @Autowired
    private IHHisSchedulingDescService hisSchedulingDescService;

    @Autowired
    private IHHisSchedulingTimeService hisSchedulingTimeService;

    @Autowired
    private IHHisSchedulingPlanService hisSchedulingPlanService;

    @Autowired
    private IHHisTimeConfigService hHisTimeConfigService;

    @Autowired
    private IHEmployeeService iHEmployeeService;

    @Autowired
    private IHHisDepartmentService ihHisDepartmentService;

    @Autowired
    private ISysDictService sysDictService;

    @Autowired
    private IHHisRegisteredfeeService hHisRegisteredfeeService;

    @Autowired
    private ISeqNoAutoService seqNoAutoService;


    @Override
    public RespResult<ListResponse<HHisSchedulingDescDTO>> pageQuerySchedulingDesc(HHisSchedulingDescQueryModel queryModel) {
        List<HHisSchedulingDescDTO> dataList = new ArrayList<>();
        ListResponse<HHisSchedulingDescModel> pageList = hisSchedulingDescService.pageQuery(queryModel);
        if (pageList != null && CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<Long> departIdList = pageList.getRecords().stream().map(HHisSchedulingDescModel::getDepartmentId).collect(Collectors.toList());
            List<Long> planIdList = pageList.getRecords().stream().map(HHisSchedulingDescModel::getPlanId).collect(Collectors.toList());

            Map<Long, String> departMap = ihHisDepartmentService.mapNameByIds(departIdList);
            Map<Long, String> planMap = hisSchedulingPlanService.mapByIds(planIdList);
            dataList = BeanUtil.copyToList(pageList.getRecords(), HHisSchedulingDescDTO.class);
            dataList.forEach(d -> {
                d.setDepartmentName(departMap.getOrDefault(d.getDepartmentId(), ""));
                d.setPlanName(planMap.getOrDefault(d.getPlanId(), ""));
            });
        }
        ListResponse response = ListResponse.build(dataList).withPage(pageList.getPageNo(), pageList.getPageSize()).withTotalCount(pageList.getTotal());
        return RespResult.OK(response);
    }

    @Override
    public HHisSchedulingDescDTO getSchedulingDesc(HHisSchedulingDescQueryModel queryModel) {
        HHisSchedulingDescModel descModel = hisSchedulingDescService.getById(queryModel.getId());
        HHisSchedulingDescDTO schedulingDesc = BeanUtil.toBean(descModel, HHisSchedulingDescDTO.class);
        List<Long> planIdList = new ArrayList<>();
        List<Long> departIdList = new ArrayList<>();
        planIdList.add(descModel.getPlanId());
        departIdList.add(descModel.getDepartmentId());
        Map<Long, String> planMap = hisSchedulingPlanService.mapByIds(planIdList);
        Map<Long, String> departMap = ihHisDepartmentService.mapNameByIds(departIdList);
        schedulingDesc.setPlanName(planMap.getOrDefault(schedulingDesc.getPlanId(), ""));
        schedulingDesc.setDepartmentName(departMap.getOrDefault(schedulingDesc.getDepartmentId(), ""));
        return schedulingDesc;
    }

    @Override
    public void delSchedulingDesc(Long id) {
        hisSchedulingDescService.deleteById(id);
    }

    @Override
    public RespResult<List<HHisSchedulingDTO>> listScheduling(HHisSchedulingQueryModel queryModel) {
        List<HHisSchedulingModel> dbList = hHisSchedulingService.selectList(queryModel);
        List<HHisSchedulingDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(dbList)) {
            List<KeyLongModel> timeValueList = new ArrayList<>();
            HHisSchedulingDescModel descModel = hisSchedulingDescService.getOne(HHisSchedulingDescQueryModel.builder().schedulingCode(queryModel.getSchedulingCode()).build());
            Map<Integer, HHisSchedulingPlanItemModel> mapPlan = hisSchedulingPlanService.mapItemById(descModel.getPlanId());
            for (Map.Entry<Integer, HHisSchedulingPlanItemModel> entry : mapPlan.entrySet()) {
                HHisSchedulingPlanItemModel e = entry.getValue();
                timeValueList.add(KeyLongModel.builder().key((long) e.getPlanId()).label(e.getPlanName()).build());
            }
            List<Long> schedulingIdLis = dbList.stream().map(HHisSchedulingModel::getId).collect(Collectors.toList());
            Map<Long, List<HHisSchedulingTimeSimpleModel>> listMap = hisSchedulingTimeService.mapSimpleBySchedulingIds(schedulingIdLis);

            dbList.forEach(d -> {
                HHisSchedulingDTO s = BeanUtil.toBean(d, HHisSchedulingDTO.class);
                s.setTimeValueList(timeValueList);
                List<Long> timeValueIds = new ArrayList<>();
                if (listMap.containsKey(d.getId())) {
                    List<HHisSchedulingTimeSimpleModel> timeList = listMap.get(d.getId());
                    StringBuffer sb = new StringBuffer();
                    timeList.forEach(t -> {
                        timeValueIds.add(t.getSubsectionType().longValue());
                    });
                }
                s.setTimeValueIds(timeValueIds);
                dataList.add(s);
            });
        }
        return RespResult.OK(BeanUtil.copyToList(dataList, HHisSchedulingDTO.class));
    }

    @Override
    public RespResult<ListResponse<HHisSchedulingDTO>> pageQuerySub(HHisSchedulingQueryModel queryModel) {
        ListResponse<HHisSchedulingModel> pageList = hHisSchedulingService.pageQuery(queryModel);
        List<HHisSchedulingDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<KeyLongModel> timeValueList = new ArrayList<>();
            HHisSchedulingDescModel descModel = hisSchedulingDescService.getOne(HHisSchedulingDescQueryModel.builder().schedulingCode(queryModel.getSchedulingCode()).build());
            Map<Integer, HHisSchedulingPlanItemModel> mapPlan = hisSchedulingPlanService.mapItemById(descModel.getPlanId());
            for (Map.Entry<Integer, HHisSchedulingPlanItemModel> entry : mapPlan.entrySet()) {
                HHisSchedulingPlanItemModel e = entry.getValue();
                timeValueList.add(KeyLongModel.builder().key((long) e.getPlanId()).label(e.getPlanName()).build());
            }
            List<Long> schedulingIdLis = pageList.getRecords().stream().map(HHisSchedulingModel::getId).collect(Collectors.toList());
            Map<Long, List<HHisSchedulingTimeSimpleModel>> listMap = hisSchedulingTimeService.mapSimpleBySchedulingIds(schedulingIdLis);
            pageList.getRecords().forEach(p -> {
                HHisSchedulingDTO s = BeanUtil.toBean(p, HHisSchedulingDTO.class);
                if (listMap.containsKey(p.getId())) {
                    List<Long> timeValueIds = new ArrayList<>();
                    List<HHisSchedulingTimeSimpleModel> timeList = listMap.get(p.getId());
                    StringBuffer sb = new StringBuffer();
                    timeList.forEach(t -> {
                        sb.append(t.getSubsectionName()).append("     ");
                        timeValueIds.add(t.getSubsectionType().longValue());
                    });
                    s.setTimeValueStr(sb.length() > 0 ? sb.toString() : "");
                    s.setTimeValueIds(timeValueIds);
                } else {
                    s.setTimeValueStr("");
                    s.setTimeValueIds(new ArrayList<>());
                }
                s.setTimeValueList(timeValueList);
                dataList.add(s);
            });
        }


        return RespResult.OK(ListResponse.build(dataList, pageList));

    }

    private Date covertDate(String date) {
        try {
            return DateUtils.parseDate(date, "yyyy-MM-dd");
        } catch (Exception e) {
            return new Date();
        }
    }

    private Date covertTime(String configTime) {
        try {
            return DateUtils.parseDate("2023-06-26 " + configTime + ":00", "yyyy-MM-dd HH:mm:ss");
        } catch (Exception e) {
            return new Date();
        }
    }

    private List<HEmployeeSchedulingDTO> pageQueryScheduling(HEmployeeQueryModel queryModel) {
        List<HEmployeeModel> pageList = iHEmployeeService.selectList(queryModel);
        List<HEmployeeSchedulingDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList)) {
            dataList.addAll(BeanUtil.copyToList(pageList, HEmployeeSchedulingDTO.class));
        }
        return dataList;
    }


    @Transactional
    @Override
    public String saveSchedulingPlan(HHisSchedulingFromReqDTO req, LoginUser loginUser) {
        if (ObjectUtil.isNotNull(req.getId()) && req.getId() > 0L) {
            return updateSchedulingPlan(req, loginUser);
        }
        Date time = new Date();
        HHisSchedulingPlanModel planModel = hisSchedulingPlanService.getById(req.getPlanId());
        Date startDate = DateUtil.parseDate(req.getStartDate() + " 00:00:01");
        Date endDate = DateUtil.offsetDay(startDate, planModel.getType().equals("0") ? 7 : 30);
        List<HHisSchedulingDescModel> dataList = hisSchedulingDescService.selectList(HHisSchedulingDescQueryModel.builder()
                .planId(req.getPlanId())
                .departmentId(req.getDepartId())
                .build());
        if (CollectionUtils.isNotEmpty(dataList)) {
            for (HHisSchedulingDescModel scheduling : dataList) {
                long start = scheduling.getStartTimeThisWeek().getTime();
                long end = scheduling.getEndTimeThisWeek().getTime();
                long now = startDate.getTime();
                if (end > now || (start > now) || (start < now && now < end)) {
                    throw new BizException("该科室己生成该方案的排班表");
                }
            }
        }
        // String descExistCode = hisSchedulingDescService.isExist(req.getDepartId(), loginUser.getOrgCode(), loginUser.getTenantId(), startDate);
        //if (StringUtils.isBlank(descExistCode)) {
        List<HEmployeeSchedulingDTO> employeeList = pageQueryScheduling(HEmployeeQueryModel.builder()
                .orgCode(loginUser.getOrgCode())
                .tenantId(loginUser.getTenantId())
                .departId(req.getDepartId())
                .build());

        if (CollectionUtils.isEmpty(employeeList)) {
            throw new BizException("科室暂没有人员,排班表不能生成");
        }
        String schedulingCode = seqNoAutoService.buildBatchNo();
        hisSchedulingDescService.insert(HHisSchedulingDescModel.builder()
                .createTime(time)
                .createUser(loginUser.getUsername())
                .orgCode(loginUser.getOrgCode())
                .tenantId(loginUser.getTenantId())
                .planId(req.getPlanId())
                .departmentId(req.getDepartId())
                .schedulingCode(schedulingCode)
                .startTimeThisWeek(startDate)
                .endTimeThisWeek(endDate)
                .build());
        //生成个人
        List<HHisSchedulingModel> insertList = new ArrayList<>();
        for (HEmployeeSchedulingDTO employee : employeeList) {
            for (int i = 0; i < 7; i++) {
                Date date = DateUtil.offsetDay(startDate, i);
                String dateStr = DateUtil.formatDate(date);
                //判断日期是否生成 生成就跳过不添加
                int employTimeCount = hHisSchedulingService.count(HHisSchedulingQueryModel.builder()
                        .employeeId(employee.getId())
                        .departmentId(req.getDepartId())
                        .date(dateStr)
                        .orgCode(loginUser.getOrgCode())
                        .tenantId(loginUser.getTenantId())
                        .build());
                if (employTimeCount > 0) {
                    continue;
                }
                insertList.add(HHisSchedulingModel.builder()
                        .planId(planModel.getId())
                        .employeeId(employee.getId())
                        .employeeName(employee.getName())
                        .busType(SchedulingTypeEnums.TYPE1.getCode())
                        .departmentId(req.getDepartId())
                        .date(dateStr)
                        .week(DateUtil.dayOfWeek(date) - 1)
                        .schedulingCode(schedulingCode)
                        .orgCode(loginUser.getOrgCode())
                        .tenantId(loginUser.getTenantId())
                        .createTime(time)
                        .createUser(loginUser.getUsername())
                        .build());
            }
        }
        if (CollectionUtils.isNotEmpty(insertList)) {
            hHisSchedulingService.insertBatch(insertList);
        }
        return schedulingCode;
    }


    public String updateSchedulingPlan(HHisSchedulingFromReqDTO req, LoginUser loginUser) {
        if (CollectionUtils.isEmpty(req.getEyIds())) {
            throw new BizException("请选择人员进行排班");
        }
        Date time = new Date();
        HHisSchedulingDescModel hHisSchedulingDescModel = hisSchedulingDescService.getById(req.getId());
        String schedulingCode = hHisSchedulingDescModel.getSchedulingCode();
        List<HEmployeeModel> employeeList = iHEmployeeService.listById(req.getEyIds());

        List<HHisSchedulingModel> insertList = new ArrayList<>();
        for (HEmployeeModel employee : employeeList) {
            for (int i = 0; i < 7; i++) {
                Date date = DateUtil.offsetDay(hHisSchedulingDescModel.getStartTimeThisWeek(), i);
                HHisSchedulingModel sm = HHisSchedulingModel.builder()
                        .employeeId(employee.getId())
                        .employeeName(employee.getName())
                        .busType(SchedulingTypeEnums.TYPE1.getCode())
                        .departmentId(employee.getDepartId())
                        .date(DateUtil.formatDate(date))
                        .week(DateUtil.dayOfWeek(date) - 1)
                        .schedulingCode(schedulingCode)
                        .orgCode(loginUser.getOrgCode())
                        .tenantId(loginUser.getTenantId())
                        .createTime(time)
                        .createUser(loginUser.getUsername())
                        .build();
                insertList.add(sm);
            }
        }
        hHisSchedulingService.insertBatch(insertList);
        return schedulingCode;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveScheduling(HHisSchedulingFromReqDTO req, LoginUser loginUser) {
        String pType = StringUtils.defaultIfBlank(req.getPsType(), "0");
        List<HHisSchedulingFromItem> employeeFormList = req.getDataSource();
        Date time = new Date();
        Map<Integer, HHisSchedulingPlanItemModel> mapPlan = hisSchedulingPlanService.mapItemById(req.getPlanId());
        Map<Long, HHisSchedulingFromItem> map = employeeFormList.stream().collect(Collectors.toMap(HHisSchedulingFromItem::getId, Function.identity()));
        //拆分排班人员
        Map<Long, HHisSchedulingFromItem> batchData = new HashMap<>();
        Set<Long> schedulingIds = new HashSet<>();
        for (Map.Entry<Long, HHisSchedulingFromItem> entry : map.entrySet()) {
            HHisSchedulingModel schedulingModel = hHisSchedulingService.getById(entry.getKey());
            Integer allowQty = 0, totalQty = 0;
            HHisSchedulingFromItem formItem = entry.getValue();

            batchData.put(schedulingModel.getEmployeeId(), formItem);
            if (CollectionUtils.isNotEmpty(formItem.getTimeValueIds())) {
                List<HHisSchedulingTimeModel> timeModelList = new ArrayList<>();
                for (int timeValue : formItem.getTimeValueIds()) {
                    int count = hisSchedulingTimeService.count(HHisSchedulingTimeQueryModel.builder()
                            .schedulingId(schedulingModel.getId())
                            .employeeId(schedulingModel.getEmployeeId())
                            .subsectionType(timeValue)
                            .build());
                    if (count > 0) {
                        break;
                    }
                    allowQty = allowQty + (formItem.getResidueQty() == null ? 0 : formItem.getResidueQty());
                    totalQty = allowQty + (formItem.getAllowQty() == null ? 0 : formItem.getAllowQty());
                    String amount = StringUtils.defaultIfBlank(formItem.getAmount(), "0");

                    HHisSchedulingPlanItemModel planItemModel = mapPlan.get(timeValue);
                    HHisSchedulingTimeModel hHisSchedulingTimeModel = new HHisSchedulingTimeModel();
                    hHisSchedulingTimeModel.setAllowQty(formItem.getAllowQty());
                    //总共可预约人数
                    hHisSchedulingTimeModel.setTotalQty(formItem.getAllowQty());
                    hHisSchedulingTimeModel.setSubsectionType(timeValue);
                    //已预约人数
                    hHisSchedulingTimeModel.setBookedQty(hHisSchedulingTimeModel.getTotalQty() - StringUtils.intValue(formItem.getResidueQty(), 0));
                    hHisSchedulingTimeModel.setSubsectionName(mapPlan.get(timeValue).getPlanName());
                    hHisSchedulingTimeModel.setStartTime(DateUtil.parseDateTime(schedulingModel.getDate() + " " + planItemModel.getStartTime() + ":00"));
                    hHisSchedulingTimeModel.setEndTime(DateUtil.parseDateTime(schedulingModel.getDate() + " " + planItemModel.getEndTime() + ":00"));
                    hHisSchedulingTimeModel.setCreateTime(time);
                    hHisSchedulingTimeModel.setCreateUser(loginUser.getUsername());
                    hHisSchedulingTimeModel.setSchedulingId(schedulingModel.getId());
                    hHisSchedulingTimeModel.setEmployeeId(schedulingModel.getEmployeeId());
                    hHisSchedulingTimeModel.setAmount(new BigDecimal(amount));
                    timeModelList.add(hHisSchedulingTimeModel);
                }
                hisSchedulingTimeService.insertBatch(timeModelList);
            }
            hisSchedulingPlanService.updateById(HHisSchedulingPlanModel.builder()
                    .id(req.getPlanId())
                    .status(CommonConstant.STATUS_1)
                    .build());
            hHisSchedulingService.updateById(HHisSchedulingModel.builder()
                    .id(schedulingModel.getId())
                    .allowQty(allowQty)
                    .totalQty(totalQty)
                    .build());
            schedulingIds.add(schedulingModel.getId());
        }
        //是否复制
        if (CommonConstant.yes_1.equals(pType)) {
            List<HHisSchedulingModel> allList = hHisSchedulingService.selectList(HHisSchedulingQueryModel.builder()
                    .planId(req.getPlanId())
                    .build());
            if (CollectionUtils.isNotEmpty(allList)) {
                allList.forEach(p -> {
                    if (!schedulingIds.contains(p.getId())) {
                        copyTime(batchData, p, mapPlan, loginUser);
                    }
                });
            }
        }
    }

    private void copyTime(Map<Long, HHisSchedulingFromItem> batchData, HHisSchedulingModel p,
                          Map<Integer, HHisSchedulingPlanItemModel> mapPlan, LoginUser loginUser) {
        HHisSchedulingFromItem formItem = batchData.get(p.getEmployeeId());
        Integer allowQty = 0, totalQty = 0;
        Date now = new Date();
        if (CollectionUtils.isNotEmpty(formItem.getTimeValueIds())) {
            List<HHisSchedulingTimeModel> timeModelList = new ArrayList<>();
            for (int timeValue : formItem.getTimeValueIds()) {
                int count = hisSchedulingTimeService.count(HHisSchedulingTimeQueryModel.builder()
                        .schedulingId(p.getId())
                        .employeeId(p.getEmployeeId())
                        .subsectionType(timeValue)
                        .build());
                if (count > 0) {
                    break;
                }
                allowQty = allowQty + (formItem.getResidueQty() == null ? 0 : formItem.getResidueQty());
                totalQty = allowQty + (formItem.getAllowQty() == null ? 0 : formItem.getAllowQty());
                String amount = StringUtils.defaultIfBlank(formItem.getAmount(), "0");

                HHisSchedulingPlanItemModel planItemModel = mapPlan.get(timeValue);
                HHisSchedulingTimeModel hHisSchedulingTimeModel = new HHisSchedulingTimeModel();
                hHisSchedulingTimeModel.setAllowQty(formItem.getResidueQty());
                hHisSchedulingTimeModel.setTotalQty(formItem.getAllowQty());
                hHisSchedulingTimeModel.setSubsectionType(timeValue);
                hHisSchedulingTimeModel.setSubsectionName(mapPlan.get(timeValue).getPlanName());
                hHisSchedulingTimeModel.setStartTime(DateUtil.parseDateTime(p.getDate() + " " + planItemModel.getStartTime() + ":00"));
                hHisSchedulingTimeModel.setEndTime(DateUtil.parseDateTime(p.getDate() + " " + planItemModel.getEndTime() + ":00"));
                hHisSchedulingTimeModel.setCreateTime(now);
                hHisSchedulingTimeModel.setCreateUser(loginUser.getUsername());
                hHisSchedulingTimeModel.setSchedulingId(p.getId());
                hHisSchedulingTimeModel.setEmployeeId(p.getEmployeeId());
                hHisSchedulingTimeModel.setAmount(new BigDecimal(amount));
                timeModelList.add(hHisSchedulingTimeModel);
            }
            hisSchedulingTimeService.insertBatch(timeModelList);
        }
        hHisSchedulingService.updateById(HHisSchedulingModel.builder()
                .id(p.getId())
                .allowQty(allowQty)
                .totalQty(totalQty)
                .build());
    }


    @Transactional
    @Override
    public String editScheduling(HHisSchedulingEditFromReqDTO req, LoginUser loginUser) {
        StringBuffer sb = new StringBuffer();
        HHisSchedulingModel schedulingModel = hHisSchedulingService.getById(req.getId());
        HHisSchedulingDescModel hHisSchedulingDescModel = hisSchedulingDescService.getOne(HHisSchedulingDescQueryModel.builder()
                .schedulingCode(schedulingModel.getSchedulingCode())
                .build());
        Map<Integer, HHisSchedulingPlanItemModel> mapPlan = hisSchedulingPlanService.mapItemById(hHisSchedulingDescModel.getPlanId());
        List<HHisSchedulingTimeModel> dataList = hisSchedulingTimeService.selectList(HHisSchedulingTimeQueryModel.builder().schedulingId(schedulingModel.getId()).build());
        Map<Integer, HHisSchedulingTimeModel> mapDataList = dataList.stream().collect(Collectors.toMap(HHisSchedulingTimeModel::getSubsectionType, Function.identity()));

        //去掉重复的
        List<Integer> timeValueList = req.getTimeValueList();
        Iterator<Integer> iterator = timeValueList.iterator();
        while (iterator.hasNext()) {
            int k = iterator.next();
            if (mapDataList.containsKey(k)) {
                sb.append(mapDataList.get(k).getSubsectionName()).append("  ");
                iterator.remove();
                mapDataList.remove(k);

            }
        }
        Date now = new Date();
        if (CollectionUtils.isNotEmpty(timeValueList)) {
            List<HHisSchedulingTimeModel> timeModelList = new ArrayList<>();
            for (Integer timeValue : timeValueList) {
                HHisSchedulingPlanItemModel planItem = mapPlan.get(timeValue);
                HHisSchedulingTimeModel hHisSchedulingTimeModel = new HHisSchedulingTimeModel();
                hHisSchedulingTimeModel.setAllowQty(0);
                hHisSchedulingTimeModel.setTotalQty(0);
                hHisSchedulingTimeModel.setSubsectionType(timeValue);
                hHisSchedulingTimeModel.setSubsectionName(planItem.getPlanName());
                hHisSchedulingTimeModel.setStartTime(hHisSchedulingDescModel.getStartTimeThisWeek());
                hHisSchedulingTimeModel.setEndTime(hHisSchedulingDescModel.getEndTimeThisWeek());
                hHisSchedulingTimeModel.setCreateTime(now);
                hHisSchedulingTimeModel.setCreateUser(loginUser.getUsername());
                hHisSchedulingTimeModel.setSchedulingId(schedulingModel.getId());
                hHisSchedulingTimeModel.setAppointmentType(req.getAppointmentType());
                timeModelList.add(hHisSchedulingTimeModel);

                sb.append(planItem.getPlanName()).append("  ");
            }
            hisSchedulingTimeService.insertBatch(timeModelList);
        }
        if (MapUtils.isNotEmpty(mapDataList)) {
            for (Map.Entry<Integer, HHisSchedulingTimeModel> entry : mapDataList.entrySet()) {
                hisSchedulingTimeService.deleteById(entry.getValue().getId());
            }
        }
        hHisSchedulingService.updateById(HHisSchedulingModel.builder()
                .id(schedulingModel.getId())
                .appointmentType(req.getAppointmentType())
                .updateTime(now)
                .updateUser(loginUser.getRealname())
                .build());

        return sb.toString();
    }

    private void e() {

    }


    public static void main(String[] args) {


    }

    public Date convertDate(String time) {
        try {
            Date start = DateUtils.parseDate("2023-06-26 " + time, "yyyy-MM-dd HH:mm:ss");
            return start;
        } catch (Exception e) {

        }
        return null;
    }

    public List<Pair<Date, Date>> handleTime(String startTime, String endTime, int timeSpan) {
        List<Pair<Date, Date>> list = new ArrayList<>();
        try {
            Date start = convertDate(startTime);
            Date end = convertDate(endTime);
            long m = DateUtil.between(start, end, DateUnit.MINUTE);
            int t = (int) NumberUtil.div(m, timeSpan, 0);

            for (int i = 1; i <= t; i++) {
                Date tempStart = DateUtil.offsetMinute(start, timeSpan * (i - 1));
                Date tempEnd = DateUtil.offsetMinute(start, timeSpan * i);
                list.add(Pair.of(tempStart, tempEnd));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Transactional
    @Override
    public void saveDoorServiceScheduling(HHisSchedulingDoorServiceFromReqDTO req, LoginUser loginUser) {
        List<Long> projectList = req.getProjectId();
        String startDateStr = req.getRangeDateTime();
        HHisSchedulingPlanSimpleModel planModel = hisSchedulingPlanService.getSimpleById(req.getPlanId());
        if (ObjectUtil.isNull(planModel)) {
            throw new BizException("方案不存在");
        }
        int day = "0".equals(planModel.getType()) ? 7 : 31;

        StringBuffer sb = new StringBuffer();
        Date startTime = DateUtil.parseDateTime(startDateStr + " 00:00:01");
        Date time = new Date();
        for (Long projectId : projectList) {
            boolean isExist = hHisSchedulingService.isExist(projectId, SchedulingTypeEnums.TYPE2.getCode(), loginUser.getOrgCode(), startTime);
            if (isExist) {
                sb.append(projectId);
                throw new BizException("项目服务预约时间已经存在相同的时间，请确认!");
            }
//            HHisSchedulingModel sm = HHisSchedulingModel.builder()
//                    .busId(projectId)
//                    .busType(SchedulingTypeEnums.TYPE2.getCode())
//                    .departmentId(0L)
//                    .startTimeThisWeek(startTime)
//                    .endTimeThisWeek(DateUtil.offsetDay(startTime,day))
//                    .orgCode(loginUser.getOrgCode())
//                    .createTime(time)
//                    .createUser(loginUser.getUsername())
//                    .build();
//            //添加排班时间段
//            hHisSchedulingService.insert(sm);
//
//            for(int i=0;i<day;i++){
//                Date today=DateUtil.offsetDay(startTime,i);
//                int index = DateUtil.dayOfWeek(today);
//                HHisSchedulingWeekModel weekModel = HHisSchedulingWeekModel.builder()
//                        .week(index)
//                        .schedulingId(sm.getId())
//                        .date(today)
//                        .registeredfeeId(0L)
//                        .build();
//                //添加排班时间段天数
//                hisSchedulingWeekService.insert(weekModel);
//                List<HHisSchedulingTimeModel> timeModelList=new ArrayList<>();
//                List<HHisSchedulingPlanItemModel> items=planModel.getItems();
//
//                for(HHisSchedulingPlanItemModel item:items){
//                    HHisSchedulingTimeModel hHisSchedulingTimeModel=new HHisSchedulingTimeModel();
//                    hHisSchedulingTimeModel.setAllowQty(req.getAllowQty());
//                    hHisSchedulingTimeModel.setTotalQty(req.getAllowQty());
//                    hHisSchedulingTimeModel.setStartTime(covertTime(item.getStartTime()));
//                    hHisSchedulingTimeModel.setEndTime(covertTime(item.getEndTime()));
//                    hHisSchedulingTimeModel.setSchedulingWeekId(weekModel.getId());
//                    hHisSchedulingTimeModel.setCreateTime(time);
//                    hHisSchedulingTimeModel.setCreateUser(loginUser.getUsername());
//                    hHisSchedulingTimeModel.setSubsectionType(item.getPlanId());
//                    timeModelList.add(hHisSchedulingTimeModel);
//                }
//                hisSchedulingTimeService.insertBatch(timeModelList);
//            }
        }
    }
}
