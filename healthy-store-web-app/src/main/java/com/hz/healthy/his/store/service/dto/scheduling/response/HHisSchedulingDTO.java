package com.hz.healthy.his.store.service.dto.scheduling.response;

import com.hz.ocean.common.model.KeyLongModel;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/19 10:28
 */
@Data
public class HHisSchedulingDTO implements Serializable {

    /** 主键 */
    private Long id;

    private Long employeeId;

    private String employeeName;


    private String timeValueStr;

    /** 1挂号 2出诊 */
    private String appointmentType;
    private boolean editable;

    private List<Long> timeValueIds;
    private List<KeyLongModel> timeValueList;




}
