package com.hz.healthy.his.store.service.dto.care.request.items;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author hyhuang
 * @Date 2024/11/26 15:13
 * @ClassName: ChineMedicineData
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class ProjectDataSource implements Serializable {


    private Long id;

    /**
     * 项目分类 1 治疗理疗 2检验 3检查
     */
    private String categoryCode;

    /**
     * 项目类型， 治疗理疗(1治疗 2理疗)
     */
    private String projectType;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目编码
     */
    private String projectCode;

    /**
     * 每天次数
     */
    private Integer qty;
    /**
     * 共多少天
     */
    private Integer day;
    private String remark;

    /** 总次数 */
    private Integer totalQty;
}
