package com.hz.healthy.his.store.service.dto.care.response;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2024/12/12 20:51
 * @ClassName: HHisCareProjectItemDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HHisCareProjectItemDTO implements Serializable {

    private Long id;


    /**
     * 单处方金额
     */
    private BigDecimal amount;

    /**
     * 处方原金额
     */
    private BigDecimal originalAmount;

    /**
     * 诊断编号
     */
    private String careCode;

    /**
     * 项目分类 1 治疗理疗 2检验 3检查
     */
    private String categoryCode;

    /**
     * 项目类型， 治疗理疗(1治疗 2理疗)
     */
    private String projectType;

    /**
     * 项目名称
     */
    private String projectName;


    /**
     * 执行划扣(消费一次划扣一次，适用于多次为一个疗程) 0不需要/1需要
     */
    private String izMinus;

    /**
     * 单位 次 时等
     */
    private String unit;

    /**
     * 对应的医保的项目
     */
    private String medicalInsuranceCode;

    /**
     * 每天次数
     */
    private Integer qty;

    /**
     * 共多少天
     */
    private Integer day;


    /**
     * 总次数
     */
    private Integer totalQty;
    /**
     * 备注
     */
    private String remark;

    /**
     * 完成次数
     */
    private Integer finishQty;

    /**
     * 支付状态
     * 0待支付 1已支付  2已关闭
     */
    private String payStatus;
}
