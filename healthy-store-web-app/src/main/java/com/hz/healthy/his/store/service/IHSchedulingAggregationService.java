package com.hz.healthy.his.store.service;

import com.hz.healthy.domain.doctor.model.HHisSchedulingDescQueryModel;
import com.hz.healthy.domain.doctor.model.HHisSchedulingQueryModel;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingDoorServiceFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingEditFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.request.HHisSchedulingFromReqDTO;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingDTO;
import com.hz.healthy.his.store.service.dto.scheduling.response.HHisSchedulingDescDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.uac.model.LoginUser;

import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2022/11/21 14:27
 */
public interface IHSchedulingAggregationService {


    public RespResult<ListResponse<HHisSchedulingDescDTO>> pageQuerySchedulingDesc(HHisSchedulingDescQueryModel queryModel);

    public HHisSchedulingDescDTO getSchedulingDesc(HHisSchedulingDescQueryModel queryModel);

    public void  delSchedulingDesc(Long id);
    public RespResult<ListResponse<HHisSchedulingDTO>> pageQuerySub(HHisSchedulingQueryModel queryModel);


    public RespResult<List<HHisSchedulingDTO>> listScheduling(HHisSchedulingQueryModel queryModel);


    public String saveSchedulingPlan(HHisSchedulingFromReqDTO req,LoginUser loginUser);

    /**
     * 由医生端发起添架
     * @param req
     * @param loginUser
     */
    public void saveScheduling(HHisSchedulingFromReqDTO req,LoginUser loginUser);

    public String editScheduling(HHisSchedulingEditFromReqDTO req, LoginUser loginUser);
    public void saveDoorServiceScheduling(HHisSchedulingDoorServiceFromReqDTO req, LoginUser loginUser);
}
