package com.hz.healthy.store.serve.dto;

import cn.hutool.core.util.DesensitizedUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Sensitive;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/18 18:16
 */
@Data
public class HEmployeeDTO extends HDoctorFromReqDTO {

    /** 主键 */
    private Long id;


    /** 审核状态 0待审核 1审核失败 2审核通过 */
    private Integer status;

    /** 注册时间 */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm")
    private Date createTime;
    /**
     * 是否开通帐号
     */
    private boolean izOpenAccount;

    private String positionAttr;

    private String izOnline;
    /**
     * 民族
     */
    private String nation;

    private String postName;

    private Integer sex;

    /**
     * 入职日期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date workTime;

    private String cardNo;

    @Sensitive(type = DesensitizedUtil.DesensitizedType.MOBILE_PHONE)
    private String cardNo1;

    private String mobilePhone;

    @Sensitive(type = DesensitizedUtil.DesensitizedType.ID_CARD)
    private String mobilePhone1;

    private String doctorType;

    /**
     * 挂号费-成本价
     */
    private BigDecimal registrationFeeCost;


    /**
     * 挂号费-销售价
     */
    private BigDecimal registrationFeeSale;

    /**
     * 是否开通chat
     */
    private boolean izOpenChat;
}
