package com.hz.healthy.store.serve.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author hyhuang
 * @Date 2023/8/23 14:59
 * @ClassName: HEmployeeFromReqDTO
 * @Description: TODO
 * @Version 1.0
 */
@Data
public class HEmployeeFromReqDTO implements Serializable {

    /** 主键 */
    private Long id;

    /** 邮箱 */
    private String email;

    /** 登录帐号 手机 */
    @NotBlank(message = "手机号不能为空")
    private String mobilePhone;


    /** 真实姓名 */
    @NotBlank(message = "姓名不能为空")
    private String name;

    /** 身份证 */
    private String cardNo;

    /** 性别，0，男；1，女 */
    private Integer sex;

    /** 生日日期 */
//    @NotNull(message = "出生日期不能为空")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date birthday;

    /** 头像 */
    private String workImg;

    /** 荣誉 */
    private String honor;

    /** 学历 1：专科  2：本科  3：研究生  4：博士  5：博士后 */
    @NotNull(message = "请选择相应的学历")
    @Dict(dicCode = "background")
    private Integer background;

    private String workNo;


    @NotNull(message = "请选择相应的科室")
    private Integer departId;

    private String description;

    private String userId;

    /**
     * 职务 0医生 1护士 2康复师 3护工  4其他
     */
    private String post;

    /**
     * position_attr
     */
    private Integer positionAttr;
    /** 在岗情况 0否 1是 取自于dict */
    private String izOnline;

    /**
     * 民族
     */
    private String nation;


}
