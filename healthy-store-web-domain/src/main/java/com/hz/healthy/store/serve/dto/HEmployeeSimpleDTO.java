package com.hz.healthy.store.serve.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/31 10:31
 */
@Data
public class HEmployeeSimpleDTO implements Serializable {

    /** 主键 */
    private Long id;


    private String name;

    private Integer departId;
}
