package com.hz.healthy.store.serve.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.ObjectUtil;
import com.google.common.collect.Lists;
import com.hz.healthy.domain.doctor.*;
import com.hz.healthy.domain.doctor.model.*;
import com.hz.healthy.domain.his.IHHisCommunityButlerService;
import com.hz.healthy.domain.member.IHMemberService;
import com.hz.healthy.store.serve.IHEmployeeAggregationService;
import com.hz.healthy.store.serve.dto.HDoctorFromReqDTO;
import com.hz.healthy.store.serve.dto.HEmployeeDTO;
import com.hz.healthy.store.serve.dto.HEmployeeSchedulingDTO;
import com.hz.healthy.store.serve.dto.HEmployeeSimpleDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.exception.BizException;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.common.utils.DateUtils;
import com.hz.ocean.common.utils.PasswordUtil;
import com.hz.ocean.common.utils.StringUtils;
import com.hz.ocean.system.enums.PositionHealthyFlagEnums;
import com.hz.ocean.system.service.auth.ISysPositionService;
import com.hz.ocean.system.service.auth.ISysUserRoleService;
import com.hz.ocean.system.service.auth.ISysUserService;
import com.hz.ocean.system.service.auth.model.PositionSimpleModel;
import com.hz.ocean.system.service.auth.model.SysPositionModel;
import com.hz.ocean.system.service.auth.model.SysUserModel;
import com.hz.ocean.uac.model.LoginUser;
import com.hz.ocean.web.core.config.OceanBaseConfig;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/18 18:06
 */
@Service
public class IHEmployeeAggregationServiceImpl implements IHEmployeeAggregationService {

    @Autowired
    private IHEmployeeService iHEmployeeService;

    @Autowired
    private IHEmployeeWorkService iHEmployeeWorkService;

    @Autowired
    private IHEmployeeGroupService hEmployeeGroupService;
    @Autowired
    private ISysUserRoleService iSysUserRoleService;
    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private IHHisRegistrationService ihHisRegistrationService;

    @Autowired
    private IHMemberService iHzMemberService;

    @Autowired
    private IHHisDepartmentService hHisDepartmentService;

    @Autowired
    private IHHisCommunityButlerService hisCommunityButlerService;

    @Autowired
    private ISysPositionService sysPositionService;

    @Autowired
    private OceanBaseConfig oceanBaseConfig;

    @Override
    public RespResult<ListResponse<HEmployeeDTO>> pageQuery(HEmployeeQueryModel queryModel, LoginUser loginUser) {
        ListResponse<HEmployeeModel> pageList = iHEmployeeService.pageQuery(queryModel);
        List<HEmployeeDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList.getRecords())) {
            List<String> postList = pageList.getRecords().stream().map(HEmployeeModel::getPostCode).collect(Collectors.toList());
            Map<String, String> stringMap = sysPositionService.mapString(postList, loginUser.getTenantId());
            pageList.getRecords().forEach(p->{
                HEmployeeDTO d=BeanUtil.toBean(p,HEmployeeDTO.class);
                d.setIzOpenAccount(StringUtils.isNotEmpty(p.getUserId()) ? true : false);
                d.setMobilePhone1(p.getMobilePhone());
                d.setCardNo1(p.getCardNo());
                d.setPostName(stringMap.getOrDefault(p.getPostCode(), "---"));
                d.setIzOpenChat(StringUtils.isNotEmpty(p.getChatId()) ? true : false);
                if(StringUtils.isNotBlank(d.getWorkImg())){
                    d.setWorkImg(oceanBaseConfig.getResource().getUpfsUrl()+d.getWorkImg());
                }
                dataList.add(d);
            });
        }
        return RespResult.OK(ListResponse.build(dataList, pageList));
    }

    @Override
    public List<HEmployeeSimpleDTO> list(HEmployeeQueryModel queryModel) {
        if (queryModel.getDepartId() == null || queryModel.getDepartId() == 0) {
            return new ArrayList<>();
        }
        List<HEmployeeModel> list = iHEmployeeService.selectList(queryModel);
        return BeanUtil.copyToList(list, HEmployeeSimpleDTO.class);
    }

//    @Override
//    public void createAccount(AccountRoleReqDTO fromReq, LoginUser loginUser) {
//        Long tableId = 0L;
//        /**
//         * 关联人员表，0是医生护士 1是社工护工
//         **/
//        String systemFlag = "0";
//        String phone = StringUtils.EMPTY, name = StringUtils.EMPTY, userId = StringUtils.EMPTY;
//        if ("1".equals(fromReq.getSource())) {
//            HEmployeeModel doctorModel = iHEmployeeService.getById(fromReq.getUserId());
//            if (ObjectUtil.isNull(doctorModel)) {
//                throw new BizException("人员信息不存在");
//            }
//            userId = doctorModel.getUserId();
//            phone = doctorModel.getMobilePhone();
//            name = doctorModel.getName();
//            tableId = doctorModel.getId();
//            systemFlag = doctorModel.getSystemFlag();
//        } else if ("2".equals(fromReq.getSource())) {
//            //外部员工
//            HHisCommunityButlerModel butlerModel = hisCommunityButlerService.getById(fromReq.getUserId());
//            if (ObjectUtil.isNull(butlerModel)) {
//                throw new BizException("人员信息不存在");
//            }
//            userId = butlerModel.getUserId();
//            phone = butlerModel.getMobilePhone();
//            name = butlerModel.getRealName();
//            tableId = butlerModel.getId();
//            systemFlag = "100";
//        }
//
//        String roleId = fromReq.getRoleId();
//        String password = fromReq.getPassword();
//        String salt = PasswordUtil.randomGen(8);
//        String passwordEncode = PasswordUtil.encrypt(phone, password, salt);
//
//        //已创建
//        if (StringUtils.isNoneBlank(userId)) {
//            SysUserModel user = new SysUserModel();
//            user.setId(userId);
//            user.setPassword(passwordEncode);
//            user.setRealname(name);
//            user.setUsername(name);
//            user.setSalt(salt);
//            user.setRelationTable(systemFlag);
//            user.setSystemId(loginUser.getSystemId());
//            user.setOrgCode(loginUser.getOrgCode());
//            user.setTenantId(loginUser.getTenantId());
//            iSysUserService.editUser(user, roleId, "");
//        } else {
//
//            SysUserModel user = new SysUserModel();
//            user.setPhone(phone);
//            user.setSalt(salt);
//            user.setUsername(fromReq.getLoginName());
//            user.setRealname(name);
//            user.setPassword(passwordEncode);
//            user.setStatus(1);
//            user.setOrgCode(loginUser.getOrgCode());
//            user.setTenantId(loginUser.getTenantId());
//            user.setDelFlag(CommonConstant.DEL_FLAG_0);
//            user.setCreateTime(new Date());//设置创建时间
//            user.setSystemId(loginUser.getSystemId());
//            user.setRelationTable(systemFlag);
//            iSysUserService.saveUser(user, roleId, "");
//
//            if ("1".equals(fromReq.getSource())) {
//                HEmployeeModel updateDoctor = new HEmployeeModel();
//                updateDoctor.setId(tableId);
//                updateDoctor.setUserId(user.getId());
//                iHEmployeeService.updateById(updateDoctor);
//            } else if ("2".equals(fromReq.getSource())) {
//                HHisCommunityButlerModel updateButlerModel = new HHisCommunityButlerModel();
//                updateButlerModel.setId(tableId);
//                updateButlerModel.setUserId(user.getId());
//                hisCommunityButlerService.updateById(updateButlerModel);
//            }
//        }
//
//    }
//
//    @Override
//    public AccountRoleReqDTO getAccount(Long id) {
//        AccountRoleReqDTO roleReq = new AccountRoleReqDTO();
//        roleReq.setRoleId("");
//        HEmployeeModel HEmployeeModel = iHEmployeeService.getById(id);
//        String userId = HEmployeeModel.getUserId();
//        if (StringUtils.isNoneBlank(userId)) {
//            SysUserRoleModel userRoleModel = new SysUserRoleModel();
//            userRoleModel.setUserId(userId);
//            SysUserRoleModel roleModel = iSysUserRoleService.getOne(userRoleModel);
//            roleReq.setRoleId(roleModel.getRoleId());
//            roleReq.setSource("1");
//            roleReq.setSurePassword("");
//            roleReq.setPassword("");
//            roleReq.setUserId(id);
//            roleReq.setLoginName(HEmployeeModel.getMobilePhone());
//        }
//        return roleReq;
//    }

    private void checkPhone(HDoctorFromReqDTO fromReq, LoginUser loginUser) {
        //一个门店允许支持
        String phone = fromReq.getMobilePhone();
        SysUserModel userModel = iSysUserService.getOne(SysUserModel.builder()
                .phone(phone)
                .tenantId(loginUser.getTenantId())
                .systemId(loginUser.getSystemId())
                .build());
        if (userModel != null) {
            throw new BizException("此手机号已经存在,不允许重复添架,如有问题请联系管理员");
        }
    }


    private void buildDoctor(HDoctorFromReqDTO fromReq, LoginUser loginUser, String userId) {
        int count = iHEmployeeService.count(HEmployeeQueryModel.builder()
                .mobilePhone(fromReq.getMobilePhone())
                .tenantId(loginUser.getTenantId())
                .build());
        if (count > 0) {
            throw new BizException("此手机号已经存在,不允许重复添架,如有问题请联系管理员");
        }
        Date time = new Date();
        HEmployeeModel doctorModel = BeanUtil.toBean(fromReq, HEmployeeModel.class);
        String cardNo = fromReq.getCardNo().trim();
        try {
            Date birth = DateUtils.parseDate(IdcardUtil.getBirth(cardNo), "yyyyMMdd");
            doctorModel.setBirthday(DateUtil.parse(DateUtils.formatDate(birth), "yyyy-MM-dd"));
        } catch (Exception e) {
            throw new BizException("身份证无效");
        }
        int sex = IdcardUtil.getGenderByIdCard(cardNo);
        int age = IdcardUtil.getAgeByIdCard(cardNo);
        doctorModel.setSex(sex);
        doctorModel.setAge(age);
        doctorModel.setCreateTime(time);
        doctorModel.setCreateUser(loginUser.getUsername());
        doctorModel.setStatus(0);
        doctorModel.setOrgCode(loginUser.getOrgCode());
        doctorModel.setTenantId(loginUser.getTenantId());
        doctorModel.setUserId(userId);
        doctorModel.setDoctorType(StringUtils.defaultIfBlank(fromReq.getDoctorType(), ""));
        //入职日期等于合同起始日期
        doctorModel.setWorkTime(fromReq.getContractStartTime());

        //标记身份
        String postCode = fromReq.getPostCode();
        if (StringUtils.isBlank(postCode)) {
            throw new BizException("请选择岗位信息");
        }

        PositionSimpleModel positionModel = sysPositionService.getOne(SysPositionModel.builder()
                .code(postCode).orgCode(loginUser.getOrgCode())
                .build());
        if (ObjectUtil.isNull(positionModel)) {
            throw new BizException("岗位信息不存在");
        }
        doctorModel.setPostCode(positionModel.getCode());

        HEmployeeWorkModel workModel = new HEmployeeWorkModel();
        workModel.setCreateTime(time);
        workModel.setDepart(String.valueOf(fromReq.getDepartId()));
        workModel.setStartTime(DateUtil.formatTime(time));
        workModel.setTitle(fromReq.getTitle());
        workModel.setDescription(fromReq.getDescription());
        workModel.setDoctorId(0L);
        workModel.setHspitalName("");
        iHEmployeeService.insert(doctorModel);
        iHEmployeeWorkService.insert(workModel);
        //创建默认分组
        if (PositionHealthyFlagEnums.DOCTOR.getCode().equals(postCode)) {
            hEmployeeGroupService.insert(HEmployeeGroupModel.builder()
                    .createTime(time)
                    .createUser(loginUser.getRealname())
                    .eyId(doctorModel.getId())
                    .eyType("1")
                    .groupName("默认分组")
                    .groupId(StringUtils.get16UUID())
                    .groupCount(0)
                    .orgCode(loginUser.getOrgCode())
                    .tenantId(loginUser.getTenantId())
                    .build());
        }


    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(HDoctorFromReqDTO fromReq, LoginUser loginUser) {
        // 不限制手机号判断，因为登录帐号手机号是唯一，但员工可以在不同机构任职，例如退休的，知名的专家
        // checkPhone(fromReq, loginUser);
        buildDoctor(fromReq, loginUser, "");
    }


    @Override
    public void updateById(HDoctorFromReqDTO fromReq, LoginUser loginUser) {
        Date time = new Date();
        HEmployeeModel doctorModel = BeanUtil.toBean(fromReq, HEmployeeModel.class);
        doctorModel.setUpdateTime(time);
        doctorModel.setUpdateUser(loginUser.getUsername());
        //入职日期等于合同起始日期
        doctorModel.setWorkTime(fromReq.getContractStartTime());
        String cardNo = fromReq.getCardNo();
        try {
            Date birth = DateUtils.parseDate(IdcardUtil.getBirth(cardNo), "yyyyMMdd");
            doctorModel.setBirthday(DateUtil.parse(DateUtils.formatDate(birth), "yyyy-MM-dd"));
        } catch (Exception e) {
            throw new BizException("身份证无效");
        }
        int sex = IdcardUtil.getGenderByIdCard(cardNo);
        int age = IdcardUtil.getAgeByIdCard(cardNo);
        doctorModel.setSex(sex);
        //doctorModel.setAge(age);
        //标记身份
        if (StringUtils.isBlank(fromReq.getPostCode())) {
            throw new BizException("请选择岗位信息");
        }

        PositionSimpleModel positionModel = sysPositionService.getOne(SysPositionModel.builder()
                .code(fromReq.getPostCode()).orgCode(loginUser.getOrgCode())
                .build());
        if (ObjectUtil.isNull(positionModel)) {
            throw new BizException("岗位信息不存在");
        }
        doctorModel.setPostCode(positionModel.getCode());
        iHEmployeeService.updateById(doctorModel);
    }

    @Override
    public void setUpRegistrationFee(HDoctorFromReqDTO fromReq, LoginUser loginUser) {
        int count = iHEmployeeService.count(HEmployeeQueryModel.builder()
                .id(fromReq.getId())
                .build());
        if (count == 0) {
            throw new BizException("医生记录不存在");
        }

        if(ObjectUtil.isNull(fromReq.getRegistrationFeeCost())){
            throw new BizException("挂号费-成本价不能为空");
        }
        if(ObjectUtil.isNull(fromReq.getRegistrationFeeSale())){
            throw new BizException("挂号费-销售价不能为空");
        }

        iHEmployeeService.updateById(HEmployeeModel.builder()
                .id(fromReq.getId())
                .registrationFeeCost(fromReq.getRegistrationFeeCost())
                .registrationFeeSale(fromReq.getRegistrationFeeSale())
                .build());
    }

    @Override
    public HEmployeeDTO getById(Long id) {
        HEmployeeModel HEmployeeModel = iHEmployeeService.getById(id);
        HEmployeeDTO doctor = BeanUtil.toBean(HEmployeeModel, HEmployeeDTO.class);
        if (doctor != null) {
            Map<String, List<String>> map = iSysUserRoleService.mapRoles(Lists.newArrayList(doctor.getUserId()));
            List<String> roles = map.getOrDefault(doctor.getUserId(), new ArrayList<>());
            if (CollectionUtils.isNotEmpty(roles)) {
                //doctor.setSelectedroles(roles.get(0));
            }
        }
        return doctor;
    }

    @Override
    public void deleteByIds(List<Long> ids) {
        iHEmployeeService.deleteByIds(ids);
    }

    @Override
    public List<HEmployeeSchedulingDTO> pageQueryScheduling(HEmployeeQueryModel queryModel) {
        List<HEmployeeModel> pageList = iHEmployeeService.selectList(queryModel);
        List<HEmployeeSchedulingDTO> dataList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(pageList)) {
            dataList.addAll(BeanUtil.copyToList(pageList, HEmployeeSchedulingDTO.class));
        }
        return dataList;
    }

    @Override
    public void updatePwd(Long id) {
        HEmployeeModel employeeModel = iHEmployeeService.getById(id);
        if (ObjectUtil.isNull(employeeModel)) {
            throw new BizException("用户记录不存在");
        }
        SysUserModel sysUserModel = iSysUserService.getById(employeeModel.getUserId());
        if (ObjectUtil.isNull(sysUserModel)) {
            throw new BizException("移动端帐号不存在,请创建");
        }

        String loginName = sysUserModel.getPhone() + sysUserModel.getUsername();
        String password = sysUserModel.getPhone();
        String salt = sysUserModel.getSalt();
        String passwordEncode = PasswordUtil.encrypt(loginName, password, salt);

        iSysUserService.updateById(SysUserModel.builder()
                .id(sysUserModel.getId())
                .password(passwordEncode)
                .build());

    }


    //    @Override
//    public RespResult<ListResponse<HHisRegistrationDTO>> pageQueryRegistrationToDay(HHisRegistrationQueryReqDTO req) {
//        List<HHisRegistrationDTO> resultList = new ArrayList<>();
//        HHisRegistrationQueryModel queryModel = BeanUtil.copyProperties(req, HHisRegistrationQueryModel.class);
//        queryModel.setPageNo(req.getPageNo());
//        queryModel.setPageSize(req.getPageSize());
//        ListResponse<HHisRegistrationModel> pageList = ihHisRegistrationService.pageQuery(queryModel);
//        if (pageList != null && CollectionUtils.isNotEmpty(pageList.getRecords())) {
//            List<HHisRegistrationModel> dataList = pageList.getRecords();
//            List<Long> memberIds = dataList.stream().map(HHisRegistrationModel::getMemberId).collect(Collectors.toList());
//            List<Long> departmentIds = dataList.stream().map(HHisRegistrationModel::getDepartmentId).collect(Collectors.toList());
//            Map<Long, HMemberSimpleModel> memberMap = iHzMemberService.mapSimpleByIds(memberIds);
//            Map<Long, String> departmentMap = hHisDepartmentService.mapNameByIds(departmentIds);
//            pageList.getRecords().forEach(d -> {
//                String timeRangeStr = null;//DateUtils.f(d.getSchedulingBeginTime(), "HH:mm") + "-" + DateUtils.formatDate(d.getSchedulingEndTime(), "HH:mm");
//                HHisRegistrationDTO data = BeanUtil.copyProperties(d, HHisRegistrationDTO.class);
//                HMemberSimpleModel ms = memberMap.getOrDefault(d.getMemberId(), new HMemberSimpleModel());
//                data.setMobilePhone(ms.getMobilePhone());
//                data.setMemberSex(ms.getSex());
//                data.setTimeRangeStr(timeRangeStr);
//                data.setBirthday(ms.getBirthday());
//                data.setDepartmentName(departmentMap.getOrDefault(d.getDepartmentId(), "---"));
//                resultList.add(data);
//            });
//        }
//        return RespResult.OK(ListResponse.build(resultList, pageList));
//    }
//
//    @Override
//    public void registrationChangeStatus(HHisRegistrationStatusFormReqDTO fromReq, LoginUser loginUser) {
//        HHisRegistrationModel registrationModel = ihHisRegistrationService.getById(fromReq.getId());
//        if (registrationModel == null) {
//            throw new BizException("不存在预约信息");
//        }
//        HHisRegistrationModel updateModel = new HHisRegistrationModel();
//        updateModel.setId(fromReq.getId());
//        updateModel.setRegistrationStatus(fromReq.getRegistrationStatus());
//        updateModel.setCreateTime(new Date());
//        updateModel.setCreateUser(loginUser.getUsername());
//        ihHisRegistrationService.updateById(updateModel);
//    }
}
