package com.hz.healthy.store.serve;

import com.hz.healthy.domain.doctor.model.HEmployeeQueryModel;
import com.hz.healthy.store.serve.dto.HDoctorFromReqDTO;
import com.hz.healthy.store.serve.dto.HEmployeeDTO;
import com.hz.healthy.store.serve.dto.HEmployeeSchedulingDTO;
import com.hz.healthy.store.serve.dto.HEmployeeSimpleDTO;
import com.hz.ocean.common.dto.ListResponse;
import com.hz.ocean.common.reponse.RespResult;
import com.hz.ocean.uac.model.LoginUser;

import java.util.List;

/**
 * @author hyhuang
 * @desc
 * @date 2022/11/21 14:27
 */
public interface IHEmployeeAggregationService {


    public RespResult<ListResponse<HEmployeeDTO>> pageQuery(HEmployeeQueryModel queryModel, LoginUser loginUser);

    /**
     * 获得今天值班医生
     *
     * @param queryModel
     * @return
     */
    public List<HEmployeeSimpleDTO> list(HEmployeeQueryModel queryModel);


    /**
     * 由医生端发起添架
     *
     * @param fromReq
     * @param loginUser
     */
    public void save(HDoctorFromReqDTO fromReq, LoginUser loginUser);


    /**
     * 由医生端发起添架
     *
     * @param fromReq
     * @param loginUser
     */
    public void updateById(HDoctorFromReqDTO fromReq, LoginUser loginUser);


    public HEmployeeDTO getById(Long id);

    public void deleteByIds(List<Long> ids);


    /**
     * 排班医生列表
     *
     * @param queryModel
     * @return
     */
    public List<HEmployeeSchedulingDTO> pageQueryScheduling(HEmployeeQueryModel queryModel);



    public void updatePwd(Long id);

    public void setUpRegistrationFee(HDoctorFromReqDTO fromReq, LoginUser loginUser);
}
