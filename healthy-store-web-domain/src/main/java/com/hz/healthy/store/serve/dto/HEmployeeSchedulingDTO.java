package com.hz.healthy.store.serve.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/19 15:47
 */
@Data
public class HEmployeeSchedulingDTO implements Serializable {

    private Long id;
    private String name;
}
