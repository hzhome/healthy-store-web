package com.hz.healthy.store.serve.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hz.ocean.common.aspect.Dict;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author hyhuang
 * @desc
 * @date 2023/1/18 18:04
 */
@Data
public class HDoctorFromReqDTO implements Serializable {


    /** 主键 */
    private Long id;

    /** 邮箱 */
    private String email;

    /** 登录帐号 手机 */
    @NotBlank(message = "手机号不能为空")
    private String mobilePhone;


    /** 真实姓名 */
    @NotBlank(message = "姓名不能为空")
    private String name;

    /** 身份证 */
    @NotBlank(message = "身份证不能为空")
    private String cardNo;

//    /** 性别，0，男；1，女 */
//    @NotNull(message = "性别不能为空")
//    @Dict(dicCode = "sex")
//    private Integer sex;
//
//    /** 生日日期 */
//    @NotNull(message = "出生日期不能为空")
//    @DateTimeFormat(pattern="yyyy-MM-dd")
//    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
//    private Date birthday;

    private String doctorType;

    /**
     * 签订合同起始时间
     */
    @NotNull(message = "签订合同起始时间不能为空")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date contractStartTime;

    /**
     * 签订合同结束时间
     */
    @NotNull(message = "签订合同结束时间不能为空")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date contractEndTime;

    /** 头像 */
    private String workImg;

    /** 专业擅长 */
    @NotBlank(message = "专业擅长不能为空")
    private String professional;

    /** 简介 */
    @NotBlank(message = "简介不能为空")
    private String introduction;

    /** 荣誉 */
    private String honor;

    /** 学历 1：专科  2：本科  3：研究生  4：博士  5：博士后 */
    @NotNull(message = "请选择相应的学历")
    @Dict(dicCode = "background")
    private Integer background;

    @NotBlank(message = "工号不能为空")
    private String workNo;

    @NotNull(message = "请选择相应的职称")
    private String title;

    @NotBlank(message = "请选择相应的科室")
    private String departId;

    private String description;

    private String userId;

    private String postCode;

    /**
     * 身份
     * 1:普通用户
     * 2:上级
     */
    private String userIdentity;

    /**
     * 挂号费-成本价
     */
    private BigDecimal registrationFeeCost;


    /**
     * 挂号费-销售价
     */
    private BigDecimal registrationFeeSale;

}
